var express = require('express');
var app = express();
var fs = require("fs");
var json = fs.readFileSync('node_modules/bdpelis.json').toString();
var pelis = JSON.parse(json);
//var codis = [];
/*for (var i = 0; i < pelis.length; i++) {
  codis.push(pelis[i].Codpeli);
}*/





app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/peli/list', function (req, res) {
  var s = "";
  for (var i = 0; i < pelis.length; i++) {
    s += pelis[i].Codpeli + " - " + pelis[i].Titol + "<br>";
  }
  res.send(s);
});

app.get('/peli/list/:id', function (req, res) {

  var id = parseInt(req.params.id);
  if (pelis[id-1] == null) {
     res.status(404).send("page not found");
  }
  res.send(pelis[id-1].Titol + "<br>" + pelis[id-1].Sinopsi)
  });


app.use(function(req, res, next){
  // the status option, or res.statusCode = 404
  // are equivalent, however with the option we
  // get the "status" local available as well

  res.status(404)        // HTTP status 404: NotFound
  .send('Error 404, Page Not found');
  res.render('404', { status: 404, url: req.url });
});







app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})
Farem un script molt complet. Abans però, voldrem fer els següents exercicis:

1)	Com trobo els fitxers més grans d'una certa quantitat?
	I d'una certa antigüitat?

2)	Amb quina ordre de la consola puc enviar un email als usuaris del sistema ?
	Suposem que l'ordre fos mail però no estigués instal·lada, com la instal·lo ?
	A quin fitxer es poden veure entre d'altres variables, el directori on s'emmagatzemarà el correu d'un usuari (MAIL_DIR)?
	Quin és l'altre fitxer, que també conté variables importants a l'hora de crear un usuari, que conté la variable
	CREATE_MAIL_SPOOL (yes/no) que em diu si es crearà la bústia de correu o no ?

	Per cert, quina és la situació que tenim nosaltres ?

	Com puc per exemple enviar amb una sola ordre un email amb un contingut que ja tinc a un fitxer, per exemple?
	(jugueu amb canonades, és a dir "pipes")

3)	Com s'utilitza la comanda at?
	Quina seria l'ordre per executar "comanda1" d'aqui a 48 hores?

	Més concretament: tinc obertes dues terminals xterm a una sessió gràfica.
	Quina ordre he d'escriure per que 1 minut despres de l'hora actual
	saludi a la segona terminal?
	Nota: En una sola ordre, per tant no s'hi val dues ordres amb un sleep i enviant a segon pla.
	Nota2: mirar que ens ofereix l'ordre tty.


SCRIPT:

Fer un shell script que llisti tots els usuaris que tinguin arxius més grans de X Mbytes i més antics de Y dies.
A continuació el script ha de generar un arxiu que contingui el nom de tots els fitxers amb el path complet i enviar un correu a cada usuari que compleixi aquesta condició amb un avís que han de salvar (backup) aquests arxius ja que seran eliminats del sistema.
Aixi cada usuari rebra en el correu mencionat els noms dels arxius en qüestió afectats.
Finalment utilitzar l'ordre at per executar la comanda que esborri tots els arxius del fitxer generat 48 hores després d'haver enviat el correu.
Treballarem en local, i l'script se suposa que l'executa l'administrador del sistema, o sigui root.
És recomanable que creeu 4 o 5 usuaris i jugueu amb diferents mides i antiguitats de fitxers.



RECORDEU FER PRIMER CAPÇALERA, DESCRIPCIÓ i COMENTARIS


#!/bin/bash
# Filename:		exercici35.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici35.sh [arg1...]
# Description:		Dissenyar un script que demani un nom d'usuari i ens digui si aquest existeix i, si existeix, ens digui si està conectat.

#control d'arguments
if [ $# -ne 1 ]
then
	echo "has de posar un usuari com a argument"
	echo "ajuda: ./exercici35.sh [arg1]"
	exit 1
fi
#Evaluar si l'usuari $1 existeix
if id $1 >/dev/null 2>&1
then
	#Si existeix, buscar l'usuari amb who, si el troba vol dir que està conectat.
	who |tr -s ' '| cut -d" " -f1 | grep $1 > /dev/null 2>&1
	if [ $? -ne 0 ]
	then
		echo "l'usuari $1 existeix però no està conectat"
	else
		echo "l'usuari $1 està conectat"
	fi
else
	#si no existeix...
	echo "no existeix l'usuari $1"
	exit 1
fi	

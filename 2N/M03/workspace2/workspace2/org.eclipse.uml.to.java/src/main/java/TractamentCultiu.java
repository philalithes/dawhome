/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of TractamentCultiu.
 * 
 * @author iaw47951368
 */
public class TractamentCultiu {
	/**
	 * Description of the property quantitat.
	 */
	public double quantitat = 0D;
	
	// Start of user code (user defined attributes for TractamentCultiu)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public TractamentCultiu() {
		// Start of user code constructor for TractamentCultiu)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for TractamentCultiu)
	
	// End of user code
	/**
	 * Returns quantitat.
	 * @return quantitat 
	 */
	public double getQuantitat() {
		return this.quantitat;
	}
	
	/**
	 * Sets a value to attribute quantitat. 
	 * @param newQuantitat 
	 */
	public void setQuantitat(double newQuantitat) {
	    this.quantitat = newQuantitat;
	}



}

package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

import java.util.Comparator;

public class CompareByAlta implements Comparator<Dispositiu>{
	public CompareByAlta() {
		
	}
	
	public int compare(Dispositiu d1, Dispositiu d2) {
		return d1.getAlta().compareTo(d2.getAlta());
	}
}
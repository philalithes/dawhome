package m08_uf1_apg;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/*
 * Client
 */
public class Main {

	public static void main(String[] args) {
		//Carreguem el Application Context amb annotacions
		AnnotationConfigApplicationContext ctx = new
				AnnotationConfigApplicationContext();
		ctx.register(SpringConfig.class);
		ctx.refresh();
		
		Mobil m = ctx.getBean("Mobil", Mobil.class);
		
		m.call("1234"); //quedara registre en el log
		m.call(""); //no quedara registre en el log
		
		System.out.println("ok");
		ctx.close();
		
	}
 
}

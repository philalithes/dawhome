package org.escoladeltreball.springdemo1;
import java.io.Serializable;

public class Motor implements Serializable {

	private Integer cavalls;
	public Motor() {
		// TODO Auto-generated constructor stub
	}
	public Motor(Integer cavalls) {
		this.cavalls = cavalls;
	}
	@Override
	public String toString() {
		return "Motor [cavalls=" + cavalls + "]";
	}
}

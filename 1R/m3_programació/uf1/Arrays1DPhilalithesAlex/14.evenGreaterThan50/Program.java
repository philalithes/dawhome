
/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula inicialitzada
     * amb els següents valors,
     * 1 4 10 32 60 91 34 56 73 88 86
     * retorna una altra taula amb tots aquells elements parells majors que 50.
     */
    
    public int[] evenGreaterThan50() {
        //variable declaration
        int[] tabla = {1,4,10,32,60,91,34,56,73,88,86};
        int n = 0;
        int i= 0;
        int j = 0;
        for (i = 0; i < tabla.length; i++) {
            if (tabla[i] % 2 == 0 && tabla[i] > 50) {
                n++;
            }
        }
        int[] novaTabla = new int[n];
        for (i = 0; i < tabla.length; i++) {
            if (tabla[i] % 2 == 0 && tabla[i] > 50) {
                novaTabla[j] = tabla[i];
                j++;
            }
        }
        return novaTabla;
    }
}

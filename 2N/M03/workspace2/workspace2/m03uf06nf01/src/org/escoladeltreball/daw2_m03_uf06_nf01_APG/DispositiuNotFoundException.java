package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

public class DispositiuNotFoundException extends RuntimeException {
	public DispositiuNotFoundException(String message) {
		super(message);
	}
}

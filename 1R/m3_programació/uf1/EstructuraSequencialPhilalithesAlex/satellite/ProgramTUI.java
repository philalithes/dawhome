/*
 * Program.java 2015/21/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * A partir de la distància d'un satèl·lit a la 
  * superfície terrestre en km, calcula el període de rotació del satèl·lit en torn del planeta i 
  * escriu-lo en hores i minuts.
  * 
  * @param h = distància a la superfície de la terra
  * @param r = radi terrestre en metres
  * @param g = acceleració de la gravetat a la superfície del planeta
  * @param hh = hores
  * @param m = minuts
  *
  * 
  * @return string hh:m = període de rotació del satèl.lit
  */


    
    public String rotationPeriod(double h) {
        double r = 6371000;
        double g = 9.81;
        double t = (2 * Math.PI * Math.pow(r + h, 3. / 2)) / r * Math.sqrt(g);
        int hh;
        int m;
        hh = (int)t / 3600;
        m = (int)t % 3600 / 60;
        String hm = (hh + ":" + m);
        return hm;
    }
    public static void main(String args[]){
        //declaracio de variables
        double h;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("escriu la distància a la terra del satél.lit");
        h = sc.nextDouble();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        String hm = p.rotationPeriod(h);
        //retorn
        System.out.printf("temps estimat: %s",hm);
    }
}

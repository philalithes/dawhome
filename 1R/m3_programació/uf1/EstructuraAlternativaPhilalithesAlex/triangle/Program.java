/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determines if 3 segments form a triangle.
     *
     * @param a the length of a segment.
     * @param b the length of a segment.
     * @param c the length of a segment.
     * @return true if -a-, -b- and -c- form a triangle, false otherwise.
     */
    public boolean isTriangle(double a, double b, double c) {
        //variable declaration
        boolean triangle;
        //calculate
        if (a < (b + c)) {
            triangle = false;
        }
        else if (b < (a + c)) {
            triangle = false;
        }
        else if (c < (a + b)) {
            triangle = false;
        }
        else {
            triangle = true;
        }
        return triangle;
                   
    }
    
    /**
     * Finds out the type of a triangle.
     *
     * @param a the length of a segment.
     * @param b the length of a segment.
     * @param c the length of a segment.
     * @return "Equilàter", "Isòsceles" or "Escalè" if the triangle is valid or
     * "ERROR", otherwise.
     */
    public String typeOfTriangle(double a, double b, double c) {
        //variable declaration
        String triangle;
        //calculate
        if (a == b && a == c) {
            triangle = "Equilater";
        }
        else if (a == b && c < (a + b)) {
            triangle = "Isòsceles";
        }
        else if (a != b && a != c && b != c) {
            triangle = "Escalè";
        }
        else {
            triangle = "ERROR";
        }
        return triangle;
    }
}

package m08_uf1_apg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CallService {

	@Autowired
	private CallRepository callRepository;
	
	public CallService() {
		
	}
	
	public void save(Log call) {
		callRepository.saveAndFlush(call);
	}

}

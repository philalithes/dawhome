/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.*;

public class Program {
    /*
     * Donat un nombre enter positiu, construeix
     * una cadena que representi el mateix nombre en el sistema de numeració binari.
     * 
     * 
     */
    public String decimal2binary(int numero) {
        //vars
        String binary = "";
        //calc
        if (numero <= 1) {
            binary = String.valueOf(numero);
        } else {
            while (numero > 0) {
                binary += numero % 2;
                numero = numero / 2;
            }
            
            binary = reverse(binary);
        }
        return binary;
    }
    public String reverse (String cadena) {
        //vars
        int occurrencesOfChar = 0;
        int i = 0;
        String novaCadena = "";
        //calc
        for (i = cadena.length()-1; i >= 0; i--) {
            novaCadena += cadena.charAt(i);
            
        }
        
        
        //ret
        return novaCadena;
        
        
    }
}
#!/bin/bash
# Filename:		quota_comentaris.sh
# Author:		ordinari
# Date:			12/11/2015
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./quota_comentaris.sh
# Description:	Script que controla la quota una única vegada
# 				Si la quota és superior a un cert percentatge s'envia un missatge d'avís
# 				(Es podria fer com a dimoni, de manera que estigués escoltant tota l'estona: de fet cada cert temps)
# 				Estem suposant que la quota està activada, si no fos així aquest script no funcionaria (i no tindria sentit utilitzar-lo ;D)
#errorlevel 1 = masses arguments


# Control d'arguments
# Si no hi ha arguments el pecentatge perillós serà del 70 %
# Si hi ha arguments, s'agafarà el primer com a percentatge
if $# -lt 1
	then
	echo "posa un sol argument com a percentatge (per defecte 70%)"
	exit 1
elif $# -eq 1
	then
	percentatge=$1
else
	percentatge=70
fi



# Emmagatzemem a una variable l'espai utilitzat (estem suposant que l'usuari té la quota activada)


# Emmagatzemem a una variable la quota de l'usuari:



# Calculem el percentatge (% 100) utilitzat

# Mirem si el percentage utilitzat és més gran que el límit d'avís que ens marca l'script
# A la següent comparació relacional obtindrem un 1 si és cert (diferent de 1 en cas contrari)



# Si el resultat és 1 mostrem un missatge d'alerta

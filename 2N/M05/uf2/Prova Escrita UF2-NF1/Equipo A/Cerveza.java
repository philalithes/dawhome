

public class Cerveza {

	public String nombre = "";
	public String marca = "";
	public int likes = 0;
	public int dislikes = 0;

	public Cerveza() {
		super();
	}
	
	public Cerveza(String nombre) {
		super();
	}
	
	public int getLikes() {
		return likes;
	}

	public void setLikes(int likes) {
		this.likes = likes;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public int getDislikes() {
		return dislikes;
	}

	public void setDislikes(int dislikes) {
		this.dislikes = dislikes;
	}
	
	public String toString() {
		return "Cerveza [nombre=" + nombre + ", marca=" + marca + ", likes=" + likes + ", dislikes=" + dislikes + "]";
	}
	
}


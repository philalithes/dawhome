/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of MatriculaPostGrau.
 * 
 * @author iaw47951368
 */
public class MatriculaPostGrau extends Matricula {
	/**
	 * Description of the property nomTesi.
	 */
	public String nomTesi = "";
	
	// Start of user code (user defined attributes for MatriculaPostGrau)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public MatriculaPostGrau() {
		// Start of user code constructor for MatriculaPostGrau)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for MatriculaPostGrau)
	
	// End of user code
	/**
	 * Returns nomTesi.
	 * @return nomTesi 
	 */
	public String getNomTesi() {
		return this.nomTesi;
	}
	
	/**
	 * Sets a value to attribute nomTesi. 
	 * @param newNomTesi 
	 */
	public void setNomTesi(String newNomTesi) {
	    this.nomTesi = newNomTesi;
	}



}

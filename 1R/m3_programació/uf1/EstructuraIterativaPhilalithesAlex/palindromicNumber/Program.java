/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Determina el nombre de xifres d'un nombre
     * 
     * return xifres = nombre de xifres
     */
    
    public static int numberOfDigits(int n) {
        //variable declaration
        int xifres = 1;
        //calcul
        while (n > 9 || n < -9) {
            n = n / 10;
            xifres++;
        }
        return xifres;
    }
    
    /**
     * Determina la xifra de la posició donada
     * 
     * @param n int number
     * @param d digit position
     * 
     * @return digit from digit position
     */
    public int digitPosition(int n, int d){
        //variable declaration    
        int digit;
        //calculate return
        digit = (int)(n / Math.pow(10,d -1) % 10);
        //return
        return digit;
    }
    
    /*
     * Donat un nombre enter, determina si és capicua o no.
     * Considerarem també capicues els nombres enters d'una xifra.
     * 
     * 
     * return capicua = false/true.
     */
    
    public boolean isPalindromic(int n) {
        //variable declaration
        boolean capicua = false;
        int numD = numberOfDigits(n);
        int digitP = 1;
        //calcul
        //si és 1 digit el considerem capicua.
        if (numberOfDigits(n) == 1) {
            capicua = true;
        }
        //si té més d'1 digit...
        else if (numberOfDigits(n) > 1) {
            //mentre el primer coincideixi amb el últim...
            while (digitPosition(n,numD) == digitPosition(n,digitP)) {
                //restem 1 al últim i sumem 1 al primer
                numD--;
                digitP++;
                //si arrivem al mateix digit o ens passem és capicua.
                if (numD <= digitP) {
                    capicua = true;
                }
            }
        }
        return capicua;
    }
}
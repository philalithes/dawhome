/*
 * Exercici3.java        1.0 22/11/2010
 *
 * Fes un programa que simuli una versió del joc de cartes "set i mig" per jugar en solitari.
 * El joc funcionarà de la següent manera:
 * 
 * - El programa demanarà a l'usuari si vol destapar una carta (0-> no vol carta, 1 -> sí que vol carta). 
 * - Les cartes poden ser 1,2,3,4,5,6,7, sota, cavall o rei. Es donaran de manera aleatòria.
 * - Les cartes menors o iguals a 7 tenen el seu valor corresponent (1-7). Sota, cavall i rei tenen per valor 0.5
 * - L'usuari pot anar demanant cartes, però si el valor acumulat de les cartes demanades és superior a 7.5, 
 * 	 ja no en pot demanar més i el programa informa a l'usuari que ha perdut, doncs s'ha passat.
 * - L'usuari pot demanar no demanar més cartes, és a dir, és planta. En aquest cas: 
 * 		- si el valor acumulat és 7.5 el programa informa l'usuari que ha guanyat
 * 		- si el valor acumulat és inferior a 7.5 el programa informa l'usuari 
 * 		  de la diferència entre el valor acumulat i 7.5.
 *
 * Copyright 2010 NOM COGNOM EMAIL
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import library.inout.Terminal;

public class Exercici3 {
	public static void main(String[] args) {
		

	}
}

/*
 * ProgramTUI.java        2015/6/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculate area of a triangle
     * 
     * @param b base
     * @param h height
     *
     * @return area of the triangle
     */
    public double add(double b, double h){
        //variable declaration    
        double retorno;
        //calculate return
        retorno = b * h / 2;
        //retorn
        return retorno;
    }

    /**
     * Modelizes the UI as a TUI.
     * 
     * @param args not used.
     */
    public static void main(String[] args) {
        
    }
}

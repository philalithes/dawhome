/**
 * 
 */
package org.escoladeltreball.m08_uf3_apg_final;

import org.springframework.security.access.annotation.Secured;

/**
 * @author root
 *
 */
public interface UserService {
	@Secured("ROLE_USER")
	void userMethod();
	
	@Secured("ROLE_ADMIN")
	void adminMethod();
}

/*
 * Program.java        2015/2/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombre enters n1 i n2
     * amb n1 < n2 , escriu tots els nombres enters dins l’interval [n1, n2] en ordre decreixent.
     * 
     */

    public void writeDescNumbers(int n1, int n2) {
        
        while (n1 <= n2) {
            
            System.out.println(n2);
            n2--;
               
        }
    }
    public void writeDescNumbersFor(int n1, int n2) {
        
        for ( ; n1 <= n2; n2--) {
            
            System.out.println(n2);
        }
    }
    
    
}

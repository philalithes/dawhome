/*
 * Program.java        2015/27/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Locale;
    
import java.util.Scanner;
public class ProgramTUI {
    
    /**
     *
     * Escriu un menú amb 4 opcions,
     * que ens permeti fer una de les operacions bàsiques amb dos nombres (usar switch).
     * 
     * 
     * 
     * @return 
     */
    public double add(double a, double b) {
        //declaració de variables
        double suma;
        //calcul
        suma = a + b;
        //retorn
        return suma;
    }
    
    public double substract(double a, double b) {
        //declaració de variables
        double resta;
        //calcul
        resta = a - b;
        //retorn
        return resta;
    }
    public double multiply(double a, double b) {
        //declaració de variables
        double producte;
        //calcul
        producte = a * b;
        //retorn
        return producte;
    }
    public double divide(double a, double b) {
        //declaració de variables
        double divisio;
        //calcul
        divisio = a / b;
        //retorn
        return divisio;
    }
    
    public static void main(String[] args) {
        //declaració de variables
        double a;
        double b;
       
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("escull un mètode: suma, resta, multiplicacio o divisio");
        String metode = sc.next();
        System.out.println("escriu el primer nombre");
        a = sc.nextDouble();
        System.out.println("escriu el primer nombre");
        b = sc.nextDouble();
        ProgramTUI p = new ProgramTUI();
        switch (metode){
            case "suma":
                System.out.println(p.add(a,b));
                break;
            case "resta":
                System.out.println(p.substract(a,b));
                break;
            case "multiplicacio":
                System.out.println(p.multiply(a,b));
                break;
            case "divisio":
                System.out.println(p.divide(a,b));
                break;
            default:
                System.out.println("mètode invalid");
                break;
        }
                
        }
            
        
        
}

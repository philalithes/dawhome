/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de nombres enters
     * acabada en zero i escriu la suma dels que són parells.
     */
    
    public static void main(String[] args) {
        //variable declaration
        int n = 0;
        int pars = 0;
        //interacció amb l'usuari
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una sequència de nombres acabada en 0 i sumaré els parells.");
        //càlculs
        do {
            System.out.println("Escriu un nombre: ");
            n = s.nextInt();
            if (n % 2 == 0) {
                pars += n;
            }
        }
        while (n != 0);
        System.out.printf("La suma dels nombres parells és: %d.",pars);
        
       
    }
}


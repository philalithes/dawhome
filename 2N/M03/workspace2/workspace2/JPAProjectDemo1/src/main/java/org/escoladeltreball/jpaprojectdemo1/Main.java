package org.escoladeltreball.jpaprojectdemo1;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAProjectDemo1");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Estudiants estudiant1 = new Estudiants(0L, "alex", 21, "A");
		Estudiants estudiant2 = new Estudiants(0L, "pep", 35, "B");
		Estudiants estudiant3 = new Estudiants(0L, "marta", 20, "A");
		Estudiants estudiant4 = new Estudiants(0L, "maria", 19, "B");
		Estudiants estudiant5 = new Estudiants(0L, "anna", 24, "A");
		Estudiants estudiant6 = new Estudiants(0L, "isaac", 17, "B");
		Estudiants estudiant7 = new Estudiants(0L, "mey", 20, "A");
		Estudiants estudiant8 = new Estudiants(0L, "joan", 60, "B");
		Estudiants estudiant9 = new Estudiants(0L, "isaac", 64, "C");
		Estudiants estudiant10 = new Estudiants(0L, "mey", 23, "C");
		Estudiants estudiant11 = new Estudiants(0L, "joan", 60, "C");
		
		
		em.persist(estudiant1);
		em.persist(estudiant2);
		em.persist(estudiant3);
		em.persist(estudiant4);
		em.persist(estudiant5);
		em.persist(estudiant6);
		em.persist(estudiant7);
		em.persist(estudiant8);
		em.persist(estudiant9);
		em.persist(estudiant10);
		em.persist(estudiant11);
		
		em.getTransaction().commit();
		
		Query query = em.createNamedQuery("select students");
		
		query.getResultList().stream().forEach(System.out::println);
		
		
		query = em.createNamedQuery("select students avgAge aula");
		
		List<Object[]> list = query.getResultList();
		
		for (Object o[] : list) {
			for (Object o2 : o) {
				System.out.print(o2);
				System.out.print(" ");
			}
			System.out.println();
		}
		em.close();
		emf.close();
		System.out.println("ok");
	}

}

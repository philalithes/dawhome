#!/bin/bash
# Filename:		exercici33.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici33.sh [arg1...]
# Description:		Script que suma tots els caràcters dels fitxers del directori actual.

#faig ls i amb una pipe compto els caràcters
ls | wc -m

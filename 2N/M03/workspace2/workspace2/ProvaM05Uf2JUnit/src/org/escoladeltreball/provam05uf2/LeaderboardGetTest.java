package org.escoladeltreball.provam05uf2;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

public class LeaderboardGetTest {
	public static User user1;
	public static User user2;
	public static User user3;
	public static Leaderboard lb;
	public static List<User> users = new ArrayList<>();

	@BeforeClass
	public static void testBefore() {
		user1 = new User(1l, "alex", 1500);
		user2 = new User(2l, "carlos", 1599);
		user3 = new User(3l, "pepe", 1100);
		lb = new Leaderboard();
		lb.addUser(user1);
		lb.addUser(user2);
		lb.addUser(user3);
		users.add(user1);
		users.add(user2);
		users.add(user3);
	}
	/**
	 * Testeja la llista d'usuaris.
	 */
	@Test
	public void test() {
		assertEquals(users, lb.getUsers());
	}

}

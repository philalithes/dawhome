#!/bin/bash
# Fitxer: here_document.sh
# Autor: jamoros
# Data: 17/12/2014
# Versio: 0.8
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information.
# Descripcio:	L'script rep un argument que serà el nom del fitxer script a crear
#		i l'afegeix una capçalera semblant a la d'aquest script.
#		Fem control d'errors: si no hi ha un únic argument sortim de l'script,
#		si el fitxer ja existeix sortim de l'script, 
#		si l'argument que li passem no conté l'extensió '.sh' l'afegim nosaltres

#		Canvis en aquesta versió: treballem amb els here-document en comptes dels echo


# Si el nombre d'arguments no és exactament 1, mostrem missatge d'error, ajuda de sintaxi i sortim amb errorlevel diferent de 1
if [ $# -ne 1 ]
then
        echo "L'ordre necessita un argument"
        echo "ús: ./$0 nom_fitxer"
        echo "on nom_fitxer és el nom de l'script sense extensio"
        exit 1
fi

# Si el fitxer ja exixteix, ho indiquem i sortim de l'script

if [ -f "$1" ] || [ -f "$1.sh"  ]
then
        echo "El fitxer "$1" o "$1.sh" ja exixteix"
        exit 2
fi

# Si l'usuari ja li ha posat l'extensió sh
echo $1 | grep '\.sh$' # Com podem fer-ho perquè no es mostri per pantalla el possible missatge si el troba (s'enten aquest comentari?:D)
if [ $? -ne 0 ]
then
        EXTENSION=".sh"
fi

cat << message_block > $1$EXTENSION
#!/bin/bash 
# Fitxer: $1$EXTENSION
# Autor: $(whoami)
# Data: $(date +%d/%m/%Y)
# Versio: 0.1
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio:  
message_block
chmod u+x "$1$EXTENSION" # establim el permís d'eXecució per l'usuari propietari del fitxer
num_chars_fitxer=$(wc -m $1$EXTENSION| cut -d" " -f 1) # comptem el número de caràcters del fitxer
vim +"${num_chars_fitxer}go" $1$EXTENSION -c start   # ens col·loquem a la posició indicada pel número de caràcters.


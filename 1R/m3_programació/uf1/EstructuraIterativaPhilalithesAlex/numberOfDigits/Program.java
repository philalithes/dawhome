/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donat un nombre enter, calcula el nombre de xifres que té.
     * 
     * 
     * return xifres = nombre de xifres.
     */
    
    public int numberOfDigits(int n) {
        //variable declaration
        int xifres = 1;
        //calcul
        while (n > 9 || n < -9) {
            n = n / 10;
            xifres++;
        }
        return xifres;
    }
}
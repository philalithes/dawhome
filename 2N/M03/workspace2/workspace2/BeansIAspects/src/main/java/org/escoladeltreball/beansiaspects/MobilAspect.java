package org.escoladeltreball.beansiaspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class MobilAspect {

	@Pointcut("execution( * org.escoladeltreball.beansiaspects.Mobil.AA*(..))")
	public void pointcut1() {	}
	
	@Pointcut("execution( * org.escoladeltreball.beansiaspects.Mobil.turnOn())")
	public void pointcut2() {	}
//	@Before("pointcut1()")
//	public void beforePointcut1(JoinPoint joinPoint) {
//		System.out.println("checking oil levels");
//		System.out.println(joinPoint.getTarget());
//	}
	
//	@After
//	public void afterPointcut1() {}
//	
	@Around("pointcut1()")
	public void aroundPointcut1(ProceedingJoinPoint pjp) {
		System.out.println("around1...");
		try {
			Mobil m = (Mobil) pjp.getTarget();
			if (m.isPower() && m.getBattery().getCharge() > 0) {
				m.getBattery().spend();
				pjp.proceed();
				System.out.println(pjp.getTarget());
			} else {
				System.out.println("Battery died... shutting down");
				m.turnOff();
			}
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//Turn on
	@Around("pointcut2()")
	public void aroundPointcut2(ProceedingJoinPoint pjp) {
		System.out.println("around2...");
		try {
			Mobil m = (Mobil) pjp.getTarget();
			if (m.getBattery().getCharge() > 10) {
				m.getBattery().spend();
				pjp.proceed();
				System.out.println(pjp.getTarget());
			} else {
				System.out.println("Low battery...");
				
			}
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public MobilAspect() {
		// TODO Auto-generated constructor stub
	}

}

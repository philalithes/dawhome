package org.escoladeltreball.helloworld2;

public class Alumno {

	private long id;
	private String name;
	
	@Override
	public String toString() {
		return "Alumno [id=" + id + ", name=" + name + "]";
	}

	public Alumno(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Alumno() {
		// TODO Auto-generated constructor stub
	}

}

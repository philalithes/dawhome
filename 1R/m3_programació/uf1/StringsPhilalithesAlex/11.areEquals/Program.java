/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     * Donades dues cadenes, determina si són iguals o no.
     * Fes dues versions del programa: una usant el mètode areEquals(s1,s2) i una altra versió
     * sense usar aquest mètode.
     * 
     * 
     */
    
    public boolean areEquals(String cadena1, String cadena2) {
        //vars
        boolean areEquals = false;
        int i = 0;
        boolean exit = false;
        //calc
        if (cadena1.length() == cadena2.length()) {
            while (!exit && i < cadena1.length()) {
                if (cadena1.charAt(i) != cadena2.charAt(i)) {
                    exit = true;
                } else {
                i++;
                }
            }
            
        }
        if (i == cadena1.length()) {
            areEquals = true;
        }
        return areEquals;
    }
    
}
var mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;


// instància de l'esquema
var sociSchema = new Schema( {
	user: String,
	password: String,
	codsoci: Number,
	nom: String,
	cognoms : String,
	recarrecs : [{
	    codpeli : Number,
	    import: Number,   
	    cobrat: Number,
	}]
});



// registrem la col·lecció Pelicules amb un esquema concret i exportem
module.exports = mongoose.model('Socis', sociSchema);
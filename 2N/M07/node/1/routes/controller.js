var Pelicules = require("../model/peli.js");
var Socis = require("../model/soci.js");





module.exports = function(app, passport, LocalStrategy) {
	principal = function(req, res) {
		res.render("principal");
	}

	formLogin = function(req, res) {
		res.render("login");
	}

	llistarPelis = function(req, res) {

		return Pelicules.find(function(err, pelis) {
			if(!err) {
				//return res.send(pelis);
				res.render("mostrarPelis", {title:"Pelicules", pelis:pelis});
			} else 
				return console.log("error" + err);
		});
	}

	llistarPeli = function(req, res) {
		var id = parseInt(req.params.CodPeli);
		return Pelicules.findOne({"Codpeli" : id}, function(err, pelis) {
			if (!err) {
				res.render("mostrarPelis1", {title:"Resum peli", pelis:pelis});
			} else
				return console.log("err " + err);
		});
	}

	formInsert = function(req, res) {
		res.render("form", {accio:"Insereix", action:"/altaPeli"});
	}

	formUpdate = function(req, res) {
		res.render("form", {accio:"Modifica", action:"/modPeli"});
	}

	formDelete = function(req, res) {
		res.render("form", {accio:"Esborra", action:"/delPeli"});
	}

	formRent = function(req, res) {
		Pelicules.find({}, function(err, data) {
			if (err) throw err;
			res.render("formRent", {accio: "Llogar", action: "/mirarDispo", pelis: data});
		});
	}

	formReturn = function(req, res) {
		Pelicules.find({}, function(err, data) {
			if (err) throw err;
			res.render("formReturn", {accio: "Retornar", action: "/retornar", pelis: data});
		});
	}

	altaPeli = function(req, res) {
		var peliJSON = JSON.stringify(req.body);
		var peli = new Pelicules(JSON.parse(peliJSON));
		//peli nova
		Pelicules.find({"Codpeli" : peli.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				peli.save();
				res.render("mostrarPelis1", {title: "peli insertada", pelis: peli});
			} else {
				res.render("Error", {error: "la peli amb codi " + peli.Codpeli + " ja existeix"});

			}
		});
			
	}

	modPeli = function(req, res) {
		var peliJSON = JSON.stringify(req.body);
		var peli = new Pelicules(JSON.parse(peliJSON));

		Pelicules.find({"Codpeli" : peli.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + peli.Codpeli + " no existeix"})
			} else {
				peli.save();
			}
		});
	}

	delPeli = function(req, res) {
		Pelicules.find({"Codpeli" : req.body.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
			} else {
				Pelicules.remove({"Codpeli" : req.body.Codpeli}, function(err) {
					if (err) throw err;
					res.render("mostrarPelis1", {title: "peli borrada", isDelete: true});
				});
			}
		});
	}

	mirarDispo = function(req, res) {
		Pelicules.findOne({"Codpeli" : req.body.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
			} else {
				res.render("mostrarDispo", {title: "Disponibilitat", pelis: data});
			}
		});
	}

	llogarPeli = function(req, res) {
		Pelicules.find({ "Codpeli": req.body.Codpeli }, function(err, peli) {
			if (err) {
                return console.log("ERROR " + err);
            }

            var arrDvdsPrestats = [];
            var dvdDisponible;

            // mirar quins dvds estan alquilats
            peli[0].prestecs.forEach(function (prestec) {
                if (!prestec.DataDev) { 
                    arrDvdsPrestats.push(prestec.Coddvd);
                }
            });

            // agafar un dvd no alquilat
            peli[0].dvds.forEach(function (dvd) {

                var indexOfDvd = arrDvdsPrestats.indexOf(dvd);

                if (indexOfDvd == -1) {
                    dvdDisponible = dvd;
                    return;
                }
            })

            if (!dvdDisponible) {
                return res.render('error', {error: "No hi ha dvds disponibles"})
            }
                    console.log(dvdDisponible)
                    console.log("asdf")

            // Rent the movie
            Pelicules.update({_id: req.body.id}, {
                    $inc: {DvdsPrestats: 1},
                    $push: {
                        prestecs: {
                            dataPres: new Date(),
                            soci: req.body.soci,
                            coddvd: dvdDisponible
                        }
                    }
                }, {},
                function (err, peli) {
                    if (err) return console.log(err);
                    return res.render('mostrarPelis1', {'title': "S'ha llogat una peli", pelis: peli});
                });
		});
	}

	retornarPeli = function(req, res) {
		Pelicules.findOneAndUpdate(
			{ 
				"Codpeli": req.body.Codpeli,
				"prestecs": {
			  		"$elemMatch" : {
			  			"dataDev": { $exists : false },
			  			"soci": req.body.soci 
			  		} 
		  		} 
		  	},
			{
				"$inc": {"dvdsPrestats": -1},
				"$set": {
					"prestecs.$.dataDev": new Date()
				}
			},
			function(err, data) {
				if (err) throw err;
				if (data.length == 0) {
					res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
				} else {
					res.render("mostrarPelis1", {title: "S'ha retornat una peli", pelis: data});
				}
			}
		);
	}

	// PASSPORT LOGIN
	passport.serializeUser(function(user, done) {
	  done(null, user);
	});

	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});
	
	passport.use(new LocalStrategy(function(username, password, done) {
		console.log(username, password)
	  process.nextTick(function() {
	    Socis.findOne({
	      'user': username, 
	    }, function(err, user) {
	    	console.log("user: "+user);

	      if (err) return done(err);

	      if (!user) {
	        return done(null, false);
	      }

	      if (user.password != password) {
	      	console.log(password);
	        return done(null, false);
	      }
	      userSession = username;
	      return done(null, user);
	    });
	  });
	}));
	/* END LOGIN */
	/* LOGOUT */
	app.get('/logout',function(req,res){
		req.session.destroy(function(err) {
		  if(err) {
		    console.log(err);
		  } else {
		    res.redirect('/');
		  }
		});
	});

	app.get("/", formLogin);
	app.get("/list", llistarPelis);
	app.get("/list/:CodPeli", llistarPeli);
	app.get("/add", formInsert);
	app.get("/update", formUpdate);
	app.get("/delete", formDelete);
	app.get("/rent", formRent);
	app.get("/return", formReturn);
	app.get('/loginFailure', function(req, res, next) {
	  res.send('Failed to authenticate');
	});

	app.get('/loginSuccess', function(req, res, next) {
	  res.send('Successfully authenticated');
	});


	app.post("/loginPost", passport.authenticate('local', {
	    	successRedirect: '/loginSuccess',
	    	failureRedirect: '/loginFailure'
  		})
	);
	app.post("/altaPeli", altaPeli);
	app.post("/modPeli", modPeli);
	app.post("/delPeli", delPeli);
	app.post("/mirarDispo", mirarDispo);
	app.post("/llogarPeli", llogarPeli);
	app.post("/retornar", retornarPeli)
}


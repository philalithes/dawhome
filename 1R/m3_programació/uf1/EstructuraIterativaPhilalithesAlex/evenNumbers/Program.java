/*
 * Program.java        2015/9/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombre enters n1 i n2 amb n1 < n2,
     * escriu els nombres enters parells que hi ha dins l’interval [n1, n2] en ordre creixent.
     * El nombre zero es considera parell.
     * 
     */
    
    public void writeEvenNumbers(int n1, int n2) {
        while (n1 <= n2){
            n1++;
            if (n1 % 2 == 0) {
                System.out.println(n1);
            }
        }
        
    }
}
package org.escoladeltreball.m08_uf3_apg_final;

import java.security.SecureRandom;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author root
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		 * SHA encoder with a randomly generated salt (noise)
		 */
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10, new SecureRandom("topsecret".getBytes()));
		String password = "admin";
		String s = encoder.encode(password);
		System.out.println(s);
		System.out.println(encoder.matches(password, s));
	}

}
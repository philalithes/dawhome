#!/bin/bash
# Fitxer: ex32_kp_V0.3.sh
# Autor: jamoros
# Data: 31/03/2011
# Versio: 0.1
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		Fem control d'errors. Només acceptem enters (positius o negatius)


 

# Sumarem els nombres de la següent manera
# Al principi la nostra suma valdrà zero

suma=0
# Per a cada un dels arguments:
# si no és un número mostraré un missatge d'error i sortiré de l'script
# altrament afegiré, l'argument, a la suma anterior


while [ $# -gt 0  ] 
do
	echo $1 | grep '^[-+]\{0,1\}[0-9]\+$' > /dev/null   # Mireu la nota (*)
	
	if [ $? -ne 0 ]
	then
		echo "error: has introduit un argument que no és un número: $1 "
		exit 1
	fi
	suma=$((suma + $1))
	shift
done

# Mostraré el resultat

echo $suma



<< *
Recordeu que:
	^ indica principi de línia
	$ indica final de línia
	{M,N} indica que el caràcter que el precedeix apareix M vegades com a mínim i N com a MÀXIM
		Les claus {, } s'han d'escapar
	+  indica al menys 1 vegada, seria equivalent a {1, }
*




<< o

Recordeu que amb vim podem canviar el nom de totes les variables de cop amb l'ordre:
:1,$ s/nom_antic/nom_nou/g

on 1,$ vol dir des de la 1a línia fins a l'última, la 's' substuitució,
i la g de 'global' que ho faci tantes vegades per línia com calgui,
si no la escrivim només ho faria una vegada per línia com a màxim

o


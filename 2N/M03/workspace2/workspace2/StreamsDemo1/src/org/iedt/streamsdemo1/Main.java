package org.iedt.streamsdemo1;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalDouble;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {

	// nextInt/Long = numero entre MIN_VALUE i MAX_VALUE
	// nextDouble = numero entre 0 i 1

	private static List<Empleat> newList(int n) {
		List<Empleat> empleats = new ArrayList<>();
		Random rnd = new Random();
		for (int i = 0; i < n; i++) {
			long id = Math.abs(rnd.nextLong()) % 1000;
			String alias = "";
			for (int j = 0; j < 3; j++) {
				int ascii = 65 + Math.abs(rnd.nextInt() % 25);
				alias += (char) ascii;
			}
			int departamentI = rnd.nextInt();
			String departament = "";
			if (departamentI < 0) {
				departament = "Informatica";
			} else {
				departament = "Electronica";
			}
			double salari = rnd.nextDouble() * 2000 + 1000;
			empleats.add(new Empleat(id, alias, departament, salari));
		}
		return empleats;
	}

	public static void main(String[] args) throws Exception {
		
		Path path = Paths.get("file_in");
//		List<String> l = Files.readAllLines(path);
//		
//		l.stream().forEach(System.out::println);
		List<Estudiant> estudiants = Files.lines(path)
				.map(Estudiant::new)
				.collect(Collectors.toList());
		
		estudiants.stream().forEach(System.out::println);
		//quants hi ha?
		System.out.println(estudiants.stream().count());
		//llistat ordenat per id
		estudiants.stream().sorted().forEach(System.out::println);
		System.out.println("*****");
		//ordenats x nota
		Comparator<Estudiant> comparatorNota = (e1, e2) -> new Double(e1.getNota()).compareTo(new Double(e2.getNota()));
		estudiants.stream().sorted((e1, e2) -> new Double(e1.getNota()).compareTo(new Double(e2.getNota()))).forEach(System.out::println);
		System.out.println("*****");
		//ordenar x alias
		estudiants.stream()
			.sorted((e1, e2) -> e1.getAlias().compareTo(e2.getAlias()))
			.forEach(System.out::println);
		//estudiant amb millor nota i pitjor nota
		System.out.println("*****");

		Estudiant pitjor = estudiants.stream()
			.min(comparatorNota)
			.get();
		System.out.println(pitjor);
		Estudiant millor = estudiants.stream()
				.max(comparatorNota)
				.get();
			System.out.println(millor);
		//nota mitjana per sexe
		OptionalDouble mitjaH =  estudiants.stream()
				.filter(e -> e.getSexe().equals(Sexe.DONA))
				.mapToDouble(e -> e.getNota())
				.average();
		System.out.println(mitjaH.getAsDouble());
		//mitjana per grups
				
		//afegir un camp de tipus data iso 8601 (yyyy-mm-dd)
		
				
				
				
				
				
				
				
				
				
				
				
				
				
				
		// // // TODO Auto-generated method stub
		// List<Empleat> empleats = newList(10);
		//
		// // informatics
		// // empleats.stream()
		// // .filter(e -> e.getDepartament().equals("Informatica"))
		// // .forEach(e -> System.out.println(e));
		//
		// // informatics que cobren mes de 2000 euros
		// // empleats.stream()
		// // .filter(e -> e.getDepartament().equals("Electronica"))
		// // .filter(e -> e.getSalari() > 2000)
		// // .forEach(System.out::println);
		//
		// // informatics q guanyen entre 2k i 3k
		// double od = empleats.stream().filter(e ->
//		// e.getDepartament().equals("Informatica"))
//		// .mapToDouble(e -> e.getSalari()).sum();
//		// // if (od.isPresent()) {
//		// System.out.println("Nomina informatica " + od);
//		// // }
//		// od = empleats.stream().filter(e ->
//		// e.getDepartament().equals("Electronica"))
//		// .mapToDouble(e -> e.getSalari()).sum();
//		// // if (od.isPresent()) {
//		 System.out.println("Nomina electronica " + od);
//		 // }
////		  implementa la interface Function<String, Integer>
//		 // Function<String, Integer> f = s -> s.length();
//		 Function<String, String> f2 = s -> s.substring(0, 1);
//		 Predicate<String> p = s -> s.matches(".*");
//		// Consumer<String> c = s -> System.out.println(s);
//		// Supplier s1 = () -> 1.2;
		// System.out.println(s1.get());
		//
		// List<Empleat> empInformatica = new ArrayList<>();
		// for (Empleat e : empleats) {
		// if (e.getDepartament().equals("Informatica")) {
		// empInformatica.add(e);
		// }
		// }
		// for (Empleat e : empInformatica) {
		// System.out.println(e);
		// }
		// Corredor corredor1 = new Corredor() {
		//
		// @Override
		// public void corre() {
		// System.out.println("running1...");
		//
		// }
		//
		// };
		//
		// Corredor corredor2 = () -> {System.out.println("running2...");};
		// Corredor corredor3 = () -> {System.out.println("running3...");};
		// Operador op1 = (n1, n2) -> n1 + n2;
		// Productor p1 = s -> s.length();

	}

}

package org.escoladeltreball.consoledemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	private static Scanner sc = new Scanner(System.in);


	public static void main(String[] args) {

		/*
		 * suma args
//		 */
//		if (args.length == 0) {
//			System.err.println("Yo, I need some args m8.");
//			System.exit(1);
//		}
//		 int result = 0;
//		 for (String arg : args) {
//		 result += Integer.parseInt(arg);
//		 }
//		 System.out.println(result);
		/*
		 * ordenar strings
		 */
//		llista de provincies
//		List<String> provincies = new ArrayList<>();
		//omplir llista
//		while (sc.hasNext()) {
//			provincies.add(sc.next());
//		}
//		//ordena
//		Collections.sort(provincies);
//		//recorre y printa
//		for (String str : provincies) {
//			System.out.println(str);
//		}
//		int numeroDeCasos = sc.nextInt();
//		for (int i = 0; i < numeroDeCasos; i++) {
//			provincies.add(sc.next());
//		}
//		//ordena
//		Collections.sort(provincies);
//		System.out.println(provincies.get(numeroDeCasos - 1));
		/*
		 * DEURES: ordenar file_in per n caracters
		 */
		List<String> provincies = new ArrayList<>();
		
		int size = sc.nextInt();
		for (int i = 0; i < size; i++) {
			provincies.add(sc.next());
		}
		//ordena
		Collections.sort(provincies, new Comparator<String>() {
			public int compare(String s1, String s2) {
				return s1.length() - s2.length();
			}
		});
		
		for (int i = 0; i < size; i++) {
			System.out.println(provincies.get(i));
		}
		
	}

}

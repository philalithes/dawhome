#!/bin/bash
# Fitxer: ex36_kp.sh
# Autor: jamoros
# Data: 04/04/2011
# Versio: 0.1
# Descripcio:	Script que ens mostrarà per pantalla els fitxers i directoris que hi ha al directori actual. 
#			 	Però ho farà amb el següent format:
# 				Si amb la comanda ls obtenim la següent sortida
#				fit1   prog1   prog2   joc
#				Amb el nostre script ens ho mostrarà així:
#			    |_fit1
#			    .|_prog1
#				..|_prog2
#				...|_joc


# Emmagatzemem la sortida de l'ordre ls
files=`ls`

# canviem el separador de camps a només '\n'
OLD_IFS=$IFS
IFS=$'\n'

# Per a cada fitxer creem la cadena de sortida i el mostrem
# Creem una cadena de sortida inicial
cadena='|_'
for fitxer in $files
do
	echo "$cadena$fitxer" # concateno les dues cadenes
	cadena=.${cadena} # Afegeixo un punt a la cadena
done

# Tornem a deixar l'Input Field Separator com estava
IFS=$OLD_IFS



# Nota: adonem-nos que el canvi de l'IFS només és necessari per evitar els possibles espais en els noms dels fitxers.


/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Donats el dividend, un nombre enter positiu i el divisor,
     * un nombre enter positiu, calcula el quocient i el residu de la divisió entera i retorna la cadena
     * “quocient:residu”. No emprar els operadors divisió entera ni mòdul.
     * 
     * return String "quocient:residu"
     * 
     * nota: no dividir entre 0.
     */
    
    public String divide(int dividend, int divisor) {
        //variable declaration
        
        int residu = dividend;
        int quocient = 0;
        
        //calcul
        while (residu >= divisor) {
            residu -= divisor;
            quocient++;
        }
        String resultat = quocient + ":" + residu;
        return resultat;
        
    }
}
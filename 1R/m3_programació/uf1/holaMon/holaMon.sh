#!/bin/sh

# holaMon.sh
#
# Programa "Hola món!" en bash
#
# Copyright 2010 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
# 
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information.
#

echo "Hola món!!!"

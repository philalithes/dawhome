/*
 * ProgramTUI.java        1.0 Oct 30, 2015
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
    
    /**
     * Calculate a total number of supplies
     * 
     * @return the total number 
     */
    public int totalSupplies() {
        //Variable Declaration
        int dia = 0;
        int marca = 0;
        int hores = 0;
        int recanvis = 0;
        int totalRecanvis = 0;
        Scanner sc = new Scanner(System.in);
        //Interacció amb l'usuari
        
        do {
            System.out.print("Dia: ");
            dia = sc.nextInt();
            if (dia != -1) {
                System.out.print("Marca: ");
                marca = sc.nextInt();
                System.out.print("Hores: ");
                hores = sc.nextInt();
                System.out.print("Nombre de recanvis: ");
                recanvis = sc.nextInt();
                totalRecanvis += recanvis;
            }
        } while (dia != -1);
        return totalRecanvis;
    }
    
    public static void main(String[] args) {
        //Variable declaration
        int totalRecanvis = 0;
        ProgramTUI p = new ProgramTUI();
        //calcul
        totalRecanvis = p.totalSupplies();
        System.out.printf("Nombre total de recanvis = %d",totalRecanvis);
    }
}

/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèrica real, calcula el % d'elements que
     * són més grans que la mitjana aritmètica dels elements
     * de la taula.
     * 
     * 
     */
    
    public double meanArray(double[] array) {
        //variable declaration
        double sum = 0;
        //recorregut de l'array
        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }
        double mitja = sum / array.length;
        
        return mitja;
        
    }
    
    public double percentageGreaterThanMean (double[] array) {
        //variable declaration
        int greaters = 0;
        Program p = new Program();
        //calcul
        for (int i = 0; i < array.length; i++) {
            if (array[i] > p.meanArray(array)) {
                greaters++;
            }
        }
        return greaters * 100.0 / array.length;
    }
}

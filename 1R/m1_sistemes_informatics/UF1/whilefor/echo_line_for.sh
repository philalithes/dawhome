#!/bin/bash
# Filename:			echolinefor.sh
# Author:			iaw47951368
# Date:				17/12/15
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./echolinefor.sh [args...]
# Description:		

OLD_IFS=$IFS
IFS=$'\n'
i=1
for line in $(cat $1)
do
	echo -e "$i $line"
	i=$(($i+1))
done

IFS=$OLD_IFS

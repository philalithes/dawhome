package org.escoladeltreball.provam05uf2;

/**
 * Classe usuari de la nostra aplicació
 * 
 * @author iaw47951368
 * @version 1.0
 * @since 1.0
 * 
 */
public class User implements Comparable<User> {

	private long id;
	private String name;
	private long score;

	/**
	 * @param id La id d'usuari
 	 * @param name nom de l'usuari
	 * @param score puntuació de l'usuari
	 */
	public User(long id, String name, long score) {
		this.id = id;
		this.name = name;
		this.score = score;
	}

	/**
	 * @return id de l'usuari
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id la nova id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return nom de l'usuari
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name el nou nom
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return la puntuació de l'usuari
	 */
	public long getScore() {
		return score;
	}

	/**
	 * @param score la nova puntuació
	 */
	public void setScore(long score) {
		this.score = score;
	}
	
	@Override
	public String toString() {
		return String.format("Name : %s\t Score : %s\n", name, score);
	}

	@Override
	public int compareTo(User o) {
		return -new Long(score).compareTo(new Long(o.getScore()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
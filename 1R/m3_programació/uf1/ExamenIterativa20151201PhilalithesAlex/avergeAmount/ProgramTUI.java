/*
 * ProgramTUI.java        1.0 Oct 30, 2015
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
    
    /**
     * Calculate averge amount
     * 
     * @return a string with all the information of the bill
     */
    public String avergeAmount() {
        //Variable Declaration
        int dia = 0;
        int marca = 0;
        int horesAudi = 0;
        int horesHonda = 0;
        int recanvis = 0;
        int sortides = 0;
        int totalHoresAudi = 0;
        int totalHoresHonda = 0;
        int totalRecanvis = 0;
        double importMitja = 0;
        String factura = "*************************************************************\nFactura\n";
        Scanner sc = new Scanner(System.in);
        //Interacció amb l'usuari
        
        do {
            System.out.print("Dia: ");
            dia = sc.nextInt();
            //mentre el dia no sigui -1
            if (dia != -1) {
                //anem formant la cadena factura
                factura += "Dia = " + dia + "\n";
                System.out.print("Nombre de sortides: ");
                sortides = sc.nextInt();
                //nombre de sortides
                for (int i = 1; i <= sortides; i++) {
                    factura += i + ") Sortida\n";
                    System.out.print("Marca: ");
                    marca = sc.nextInt();
                    //marca Honda
                    if (marca == 72) {
                        factura += "\tMarca = Honda ";
                        System.out.print("Hores: ");
                        horesHonda = sc.nextInt();
                        horesHonda *= 20;
                        totalHoresHonda += horesHonda;
                    }
                    //marca Audi
                    else {
                        factura += "\tMarca = Audi ";
                        System.out.print("Hores: ");
                        horesAudi = sc.nextInt();
                        horesAudi *= 10;
                        totalHoresAudi += horesAudi;
                    }
                    //recanvis
                    System.out.print("Nombre de recanvis: ");
                    recanvis = sc.nextInt();
                    factura += "Nombre de recanvis = " + recanvis + "\n";
                    totalRecanvis += recanvis;
                }
                //calcul de l'import mitjà
                importMitja = 1.0 * (totalHoresHonda + totalHoresAudi) / dia;
            }
        } while (dia != -1);
        factura += "*************************************************************\nTOTAL FACTURA\nImport Mitjà = " + importMitja + "\nTotal Recanvis = " + totalRecanvis;
        
        return factura;
        
     }
     
     public static void main(String[] args) {
         ////Variable declaration
        String factura = "";
        ProgramTUI p = new ProgramTUI();
        //calcul
        factura = p.avergeAmount();
        System.out.println(factura);
     }
}

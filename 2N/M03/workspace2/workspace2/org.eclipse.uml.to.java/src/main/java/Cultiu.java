/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Cultiu.
 * 
 * @author iaw47951368
 */
public class Cultiu {
	/**
	 * Description of the property lloc.
	 */
	public String lloc = "";
	
	/**
	 * Description of the property tipus.
	 */
	public String tipus = "";
	
	/**
	 * Description of the property superficie.
	 */
	public String superficie = "";
	
	// Start of user code (user defined attributes for Cultiu)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Cultiu() {
		// Start of user code constructor for Cultiu)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Cultiu)
	
	// End of user code
	/**
	 * Returns lloc.
	 * @return lloc 
	 */
	public String getLloc() {
		return this.lloc;
	}
	
	/**
	 * Sets a value to attribute lloc. 
	 * @param newLloc 
	 */
	public void setLloc(String newLloc) {
	    this.lloc = newLloc;
	}

	/**
	 * Returns tipus.
	 * @return tipus 
	 */
	public String getTipus() {
		return this.tipus;
	}
	
	/**
	 * Sets a value to attribute tipus. 
	 * @param newTipus 
	 */
	public void setTipus(String newTipus) {
	    this.tipus = newTipus;
	}

	/**
	 * Returns superficie.
	 * @return superficie 
	 */
	public String getSuperficie() {
		return this.superficie;
	}
	
	/**
	 * Sets a value to attribute superficie. 
	 * @param newSuperficie 
	 */
	public void setSuperficie(String newSuperficie) {
	    this.superficie = newSuperficie;
	}



}

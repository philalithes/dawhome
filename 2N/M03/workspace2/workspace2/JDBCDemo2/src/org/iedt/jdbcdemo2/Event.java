package org.iedt.jdbcdemo2;

import java.sql.Timestamp;

public class Event {
	private long id;
	private String name;
	private String descripcio;
	private Timestamp data;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public Timestamp getData() {
		return data;
	}

	public void setData(Timestamp data) {
		this.data = data;
	}

	public Event(long id, String name, String descripcio, Timestamp data) {
		super();
		this.id = id;
		this.name = name;
		this.descripcio = descripcio;
		this.data = data;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", name=" + name + ", descripcio=" + descripcio + ", data=" + data + "]";
	}

}

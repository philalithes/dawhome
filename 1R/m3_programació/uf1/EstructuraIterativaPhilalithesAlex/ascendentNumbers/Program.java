/*
 * Program.java        2015/30/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats 2 nombres n1 i n2 amb n1 < n2 escriu per pantalla tots els nombres enters dins l'interval [n1, n2]
     * en ordre creixent
     * 
     */

    public void writeAscNumbers(int n1, int n2) {
        
        while (n1 <= n2) {
            
            System.out.println(n1);
            n1++;
               
        }
    }
    public void writeAscNumbersFor(int n1, int n2) {
        
        for ( ; n1 <= n2; n1++) {
            
            System.out.println(n1);
        }
    }
    
    
}

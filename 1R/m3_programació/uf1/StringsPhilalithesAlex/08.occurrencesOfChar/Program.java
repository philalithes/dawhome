/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donats un caràcter i una cadena,
     * calcula quantes vegades apareix el caràcter dins la cadena.
     * 
     * 
     */
    
    public int occurrencesOfChar (String cadena, char caracter) {
        //vars
        int occurrencesOfChar = 0;
        int i = 0;
        //calc
        for (i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == caracter) {
                occurrencesOfChar++;
            }
        }
        //ret
        return occurrencesOfChar;
    }
}

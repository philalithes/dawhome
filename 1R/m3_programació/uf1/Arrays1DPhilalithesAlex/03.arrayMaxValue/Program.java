/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula de nombres reals, esbrina
     * quin és el nombre més gran dels seus elements.
     * 
     * 
     */
    
    public double findMaxValue(double[] array) {
        //variable declaration
        double max = array[0];
        //recorregut de l'array
        for (int i = 0; i < array.length; i++) {
            //max?
            if (array[i] > max) {
                max = array[i];
            }
        }
        return max;
        
    }
}

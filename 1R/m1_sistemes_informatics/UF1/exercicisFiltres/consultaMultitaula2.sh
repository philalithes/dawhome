#!/bin/bash
# Filename:		consultaMultitaula2.sh
# Author:		Julio Amoros iam0000000
# Date:			01/11/2013
# Version:		0.2
# License:		"This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information."
# Usage:		./consultaMultitaula2.sh
# Description:	Script que pregunta de manera interactiva el numero d'un treballador-repventas,
# 				així com el número de columna de la "taula" oficines i, a continuació,
# 				mostra la informació corresponent a aquest número de columna de la taula d'oficines
# 				del venedor entrat.
# 				Es necessita tenir els fitxers repventas.dat i oficinas.dat
#				al mateix directori on s'executa l'script

# Emmagatzemem els fitxers originals 
taula_repventas=repventas.dat 
taula_oficinas=oficinas.dat

# Establim el delimitador
delimiter=$'\t'

# Demanem a l'usuari el nom del venedor i el capturem
echo "escriu el nombre del treballador de l'oficina"
read num_venedor

# Demanem a l'usuari el camp a mostrar i el capturem
echo "escriu el nombre de l'opcio que vols"
echo "1: nombre de l'oficina"
echo "2: ciutat de l'oficina"
echo "3: nombre del director de l'oficina"
echo "4: nombre de l'oficina"
echo "5: objectiu de l'oficina"
echo "6: vendes de l'oficina"
read camp_oficina

# Per a cada camp  escollit guardem una cadena
# si el camp escollit no és una de les opcions escollides sortim de l'script 
case "$camp_oficina" in
	1)
		cadena_opcio="el nombre de l'oficina"
		;;
	2)
		cadena_opcio="la ciutat de la oficina"
		;;
	3)
		cadena_opcio="la regió de l'oficina"
		;;
	4)
		cadena_opcio="el nombre del director de l'oficina"
		;;
	5)
		cadena_opcio="l'objectiu de l'oficina"
		;;
	6)
		cadena_opcio="les vendes de l'oficina"
		;;
	*)
	echo "escull una opcio vàlida [1-6]"
	exit 1
		;;
esac

# Creem els fitxers ordenats, repventas i oficinas pel 1er camp
cat $taula_repventas | sort -t"$delimiter" -k1,1 > ${taula_repventas}ord1
cat $taula_oficinas | sort -t"$delimiter" -k1,1  > ${taula_oficinas}ord1

# La mateixa consulta que a l'altra versió de l'script es fa en varies etapes aquí la fem en una de sola.
# Trobem el camp escollit per l'usuari
consulta=$(echo $(echo "$num_venedor" |  join   ${taula_repventas}ord1 -  -t"$delimiter" -1 1 -2 1 | cut -f4 ) | join -t"$delimiter" ${taula_oficinas}ord1 - -1 1 -2 1 | cut -f$camp_oficina)

# Eliminem els fitxers creats
rm -rf  ${taula_repventas}ord1  ${taula_oficinas}ord1

# Si la cadena consulta està buida és que o no existeix el venedor o aquest no té oficina 
# en cas contrari mostrem el valor demanat
if [ "$consulta" = "" ]
then
	echo "o $num_venedor no existeix a la taula o no té oficina"
	# podríem pensar a sortir i posar un errolevel aqui diferent de zero
else
	echo "$cadena_opcio del venedor $num_venedor és $consulta"
fi

# Exercici: feu que l'script comprovi que els fitxers repventas.dat i oficinas.dat es troben al directori actual
# i si no és així que informi a l'usuari i surti de l'script informant de l'error també al sistema. 

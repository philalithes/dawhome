/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de tres o més
     * nombres enters acabada en zero i escriu si la seqüència és una progressió aritmètica.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int n1 = 0;
        int n2 = 0;
        int dif = 0;
        int dif2 = 0;
        boolean isProg = true;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una seqüència de tres o més nombres enters acabada en zero \n i et diré si és una progressió aritmètica.");
        System.out.println("Escriu un nombre");
        n1 = sc.nextInt();
        System.out.println("Escriu un nombre");
        n2 = sc.nextInt();
        dif = n2 - n1;
        System.out.println("Escriu un nombre");
        n1 = sc.nextInt();
        dif2 = n1 - n2;
        n2 = n1;
        do {
            if (dif2 != dif){
                isProg = false;
            }
            System.out.println("Escriu un nombre:");
            n1 = sc.nextInt();
            dif2 = n1 - n2;
            n2 = n1;
        }while (n1 != 0);
        if (isProg){
            System.out.printf("Has introduït una progressió aritmètica");
        }else {
            System.out.printf("No és una progressió aritmètica");
        }
    }
}


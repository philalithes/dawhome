#!/bin/bash
# Filename:	header.sh
# Author:	alex philalithes iaw47951368
# Date:		22/10/2015
# Version:	0.2
# License:	This is free software, licensed under the GNU General Public License v3.
# 		See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:	./header.sh  arg1
# Description:	Script to automate the creation of headers.
# 		This program will get the name of the script from the command line
# 		and it will create the header details, with execute permission,
# 		and, finally, the developer will run vim in insert mode.

echo "#!/bin/bash" > $1
echo -e "# Filename:\t\t\t$1" >> $1
echo -e "# Author:\t\t\t$(whoami)" >> $1
echo -e "# Date:\t\t\t\t$(date +%d/%m/%y)" >> $1
echo -e "# Version:\t\t\t0.1" >> $1
echo -e "# License:\t\t\tThis is free software, licensed under the GNU General Public License v3.
#\t\t\t\t\tSee http://www.gnu.org/licenses/gpl.html for more information." >> $1
echo -e "# Usage:\t\t\t./$1 [args...]" >> $1
echo -e "# Description:\t\t\t" >> $1
chmod u+x $1
vim "+normal G$" +startinsert $1

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    /*
     * Donat un nombre enter, determina si és un nombre
     * primer o no. Un nombre enter és primer si es més gran que 1 i només és divisible per 1 i
     * per ell mateix.
     */
    
    public boolean isPrime(int n) {
        //variable declaration
        boolean isPrime = false;
        int contador = 0;
        //càlcul
        for (int i = n; i > 1; i--) {
            if (n % i == 0) {
                contador++;
            }
        }
        if (contador == 1) {
            isPrime = true;
        }
        return isPrime;
            
            
        
    }
}

#!/bin/bash
# Filename:			exercici2.sh
# Author:			iaw47951368
# Date:				01/03/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici2.sh [arg1]
# Description:		mostra la suma dels imports de les comandes d'1 client.

#control d'arguments
if [ $# -ne 1 ]
then
	echo "error: sintaxi incorrecta"
	echo "ús: $0 num_clie"
	echo "on num_clie és un enter que representa el client"
	exit 1
fi

#functions
function get_total_debt {
	cat $fitxer | grep "^[0-9]*:[0-9-]*:$1:" | cut -d":" -f8
}

#variables
fitxer=pedidos.txt

#body
if [ -z "$(get_total_debt $1)" ]
then
	echo "el client $1 no té pendent de pagament cap comanda"
else
	for i in $(get_total_debt $1)
	do
		acumulador=$(($acumulador+$i))
	done
fi
echo $acumulador

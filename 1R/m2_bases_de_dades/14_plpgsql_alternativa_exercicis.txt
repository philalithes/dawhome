-- --------------------------------------------
--    plpgsql - estructura alternativa
-- --------------------------------------------

-- 1.- (sign) Determina si un nombre real és positiu, negatiu o zero. La funció retornarà les cadenes “positiu”, “negatiu” o “zero”, respectivament.

create or replace function sign(num int) returns varchar(7) as $$
  declare ret varchar(7);
  begin
    if num > 0 then 
      ret := 'positiu';
    elsif num < 0 then 
      ret := 'negatiu';
    else 
      ret := 'zero';
    end if;
    return ret;
  end;
$$ language plpgsql;

-- 2.- (triangle) A partir de les llargades de tres segments, esbrina si formen o no un triangle i si formen triangle de quin tipus. Per tal de que tres segments formin un triangle un segment qualsevol ha de ser més petit que la suma de la llargada dels altres dos. Si els tres segments formen un triangle, aquest pot ser equilàter (tres costats iguals), isòsceles (dos costats iguals) o escalè (tots els costats diferents). La funció retornarà "No és triangle", "Triangle equilàter", "Triangle isòsceles" o "Triangle escalè".

-- 3.- Donada una cadena amb un NIF, determina si és un NIF correcte. S'ha de validar que la llargada de la cadena d'entrada sigui correcta (9 caràcters) i que la lletra sigui correcta. El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el residu de la divisió entera del DNI entre 23 . Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 -> T, 1-> R, 2 -> W, etc. Exemple: Al DNI 37721039 li correspon el NIF 37721039G. Si la llargada és incorrecta retornarem un 1, si el problema és la lletra retornarem un 2 i si tot és correcte retornarem un 0.

-- 4.- Donada una comanda, esbrina si es va fer en un any de traspàs. Si era any de traspa retorna TRUE, sinó FALSE. Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de 4 però no de 100.

-- 5.- Crear una funció que insereixi una oficina a la taula oficinas. Per paràmetre arribaran totes les dades d'una oficina (oficina, ciudad, region, dir, objetivo, ventas). S'hauran de fer les següents comprovacions:
  * El codi d'oficina no existeix a la taula oficinas
  * La region es 'Este' o 'Oeste'
  * El codi de director és valid
  * L'objectiu és superior a 0.
Si totes les dades són vàlides, l'oficina s'inserirà i retornarem TRUE. Altrament no s'inserirà i retornarem FALSE.

IMPORTANT: feu totes les comprovacions en funcions separades per poder-les reutilitzar.

-- 6.- Crear una funció que insereixi o modifiqui una oficina a la taula oficinas. Per paràmetre arribaran totes les dades d'una oficina (oficina, ciudad, region, dir, objetivo, ventas). S'hauran de fer les següents comprovacions:
  * La region es 'Este' o 'Oeste'
  * El codi de director és valid
  * L'objectiu és superior a 0.
Si totes les dades són vàlides i el codi d'oficina no existeix, l'oficina s'inserirà i retornarem un 1.
Si totes les dades són vàlides i el codi d'oficina existeix, l'oficina amb aquest codi es modificarà i retornarem un 2. 
Altrament no es farà res i retornarem un -1.


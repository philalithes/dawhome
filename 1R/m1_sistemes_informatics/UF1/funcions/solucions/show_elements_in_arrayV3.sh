#!/bin/bash
# Filename:		show_elements_in_arrayV3
# Author:		Jefferson Paul Robles Calle
# Date:			17/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./show_elements_in_arrayV3
# Description:	Script que utilitzant una funció ens mostra els elements de l'array

function show_elements_array {
	# Create a local array name & local array
	local array_name=$1[*]
	local aux_array=(${!array_name})
	
    # Traverse the array, til the last char, showing the current char
	for x in ${aux_array[*]}
	do
		echo $x
	done
	
}

declare -a numbers=(11 22 33 44 55)
show_elements_array numbers

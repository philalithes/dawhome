package org.escoladeltreball.springdemo3;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@Configuration
//@ComponentScan
@EnableAspectJAutoProxy(proxyTargetClass=true)
@Import(value = {BeansDefinition1.class, BeansDefinition2.class})
public class SpringConfig {

	public SpringConfig() {
		// TODO Auto-generated constructor stub
	}
	
	

}

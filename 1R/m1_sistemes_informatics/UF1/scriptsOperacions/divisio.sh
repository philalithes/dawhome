#!/bin/bash
# Filename:			divisio.sh
# Author:			iaw47951368
# Date:				24/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./divisio.sh [args...]
# Description:		Script per dividir 2 arguments i que mostri 2 decimals.

if test $# -ne 2
	then
	echo "escriu 2 arguments"
	exit 1
else
	echo -e "scale=2\n$1 / $2" | bc -l
fi

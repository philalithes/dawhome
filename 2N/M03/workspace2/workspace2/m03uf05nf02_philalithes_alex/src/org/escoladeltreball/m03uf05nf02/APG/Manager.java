package org.escoladeltreball.m03uf05nf02.APG;

import java.util.List;
import java.util.stream.Collectors;

public class Manager {

	public static void setProject(Projecte p, List<Empleat> empleats) {
		// filtra els que ja tenen projecte assignat
		empleats = empleats.stream().filter(e -> e.isHasProject() == false).collect(Collectors.toList());
		if (!empleats.isEmpty()) {
			if (p.getDificultat() == 3) { // filtrem empleats segons dificultat
											// del projecte
				empleats = empleats.stream().filter(e -> e.getCategoria().equals(Categoria.C))
						.collect(Collectors.toList());
			} else if (p.getDificultat() == 2) {
				empleats = empleats.stream()
						.filter(e -> e.getCategoria().equals(Categoria.C) || e.getCategoria().equals(Categoria.B))
						.collect(Collectors.toList());
			} // si es de dificultat 1, la categoria de l'empleat pot ser
				// cualsevol
				// ordenem per categoria per agafar primer els de categoria A
				// abans que B o C)
			if (!empleats.isEmpty()) {
				empleats = empleats.stream().sorted((e1, e2) -> e1.getCategoria().compareTo(e2.getCategoria()))
						.collect(Collectors.toList());
				List<Empleat> frontEnd = empleats.stream().filter(e -> e.getRol().equals(Rols.FrontEnd))
						.collect(Collectors.toList());
				List<Empleat> backEnd = empleats.stream().filter(e -> e.getRol().equals(Rols.BackEnd))
						.collect(Collectors.toList());
				List<Empleat> projectManager = empleats.stream().filter(e -> e.getRol().equals(Rols.ProjectManager))
						.collect(Collectors.toList());
				if (!frontEnd.isEmpty()) {
					p.setFrontEnd(frontEnd.get(0));
					frontEnd.get(0).setHasProject(true);
				}
				if (!backEnd.isEmpty()) {
					p.setBackEnd(backEnd.get(0));
					backEnd.get(0).setHasProject(true);
				}
				if (!projectManager.isEmpty()) {
					p.setProjectManager(projectManager.get(0));
					projectManager.get(0).setHasProject(true);
				}

			}
		}
	}
}

package org.escoladeltreball.springdemo3;



public interface Engine {

	public abstract void start();
	public abstract void stop();
}

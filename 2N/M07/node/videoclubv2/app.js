/*
# Filename:		app.js
# Author:		Gio Mikee Plata Justiniano iaw1128309
# Date:			02/03/2017
# License:		This is free software, licensed under the GNU General Public License v3.
# 				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		node app
*/

var express = require('express'),
    mongoose = require('mongoose'),
    app = express(),
    bodyParser = require('body-parser');

app.use(express.static(__dirname, + '/public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Opcional, el motor de renderitzat per defecte és jade
// app.engine('jade', require('jade').__express); // for .ejs

app.set('view engine', 'jade');

mongoose.connect('mongodb://localhost/videoclub', function (err, res) {
    if (err)
        console.log('Error al conectarse BBDD pelis');
    else console.log('Conectado a videoclub');
});

require('./routes/controller')(app);

app.listen(3000, function () {
    console.log('Node server running on http://localhost:3000');
});
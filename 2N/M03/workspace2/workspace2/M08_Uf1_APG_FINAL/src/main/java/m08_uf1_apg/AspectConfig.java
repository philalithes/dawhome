package m08_uf1_apg;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class AspectConfig {

//	@Autowired
//	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private CallService callService;
	
	public AspectConfig() {
		// TODO Auto-generated constructor stub
	}
	
	@Pointcut("execution(* call(String))")
	public void callPointcut() {}
	
	@Around("callPointcut()")
	public void checkNum(ProceedingJoinPoint pjp) {
//		jdbcTemplate.update("insert into logs (now(), number) values (?)", "fdsfds");
		try {
			//agafem l'argument de la funció call i comprovem què no és null ni una cadena buida.
			String numero = (String) pjp.getArgs()[0];
			if (numero != null && numero != "") {
				pjp.proceed();
				callService.save(new Log(numero));
				
			}
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	};
}

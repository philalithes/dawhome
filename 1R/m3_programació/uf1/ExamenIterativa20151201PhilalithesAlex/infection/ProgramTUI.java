/*
 * ProgramTUI.java        1.0 Oct 30, 2015
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI
{
    
    /**
     * Calculate the resistance factor of a vaccine for a period of time
     * 
     * @param initial number of infected
     * 
     */
    public void infection (double initialNumberInfected) {
        //Variable declaration
        int any = 0;
        double infectats = initialNumberInfected;
        double resistencia = 1.5;
        double percentResistencia = 0;
        Scanner sc = new Scanner(System.in);
        //calcul
        while (resistencia < 5) {
            percentResistencia = resistencia / 100;
            System.out.println("*************************************************************************");
            any++;
            System.out.printf("El nombre d'infectats al %d any és de %d persones\n", any, (int)infectats);
            System.out.printf("Amb un factor de resistència del %f  \n", resistencia);
            //període de grip
            System.out.println("*************************************************************************");
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Octubre el nombre d'infectats és de %d persones\n", (int)infectats);
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Novembre el nombre d'infectats és de %d persones\n", (int)infectats);
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Desembre el nombre d'infectats és de %d persones\n", (int)infectats);
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Gener el nombre d'infectats és de %d persones\n", (int)infectats);
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Febrer el nombre d'infectats és de %d persones\n", (int)infectats);
            infectats += infectats * percentResistencia;
            System.out.printf("Després del mes de Març el nombre d'infectats és de %d persones\n", (int)infectats);
            //la resistencia augmenta un 1% anual
            resistencia++;
        
        }
        System.out.println("*************************************************************************");
    }
    public static void main(String[] args) {
        //Variable declaration
        int infectatsInici = 0;
        Scanner sc = new Scanner(System.in);
        ProgramTUI p = new ProgramTUI();
        //Interacció
        System.out.print("Digue'm el nombre d'infectats inicialment");
        infectatsInici = sc.nextInt();
        p.infection(infectatsInici);
    }
}

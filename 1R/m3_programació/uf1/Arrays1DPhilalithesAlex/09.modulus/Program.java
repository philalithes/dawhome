/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèrica d'enters que simbolitzen un
     * vector, calcula el seu mòdul. 
     */
    
    public double modulus(double[] array) {
        //variable declaration
        double cuadrat = 0;
        double sum = 0;
        double modulus = 0;
        //calcul
        for (int i = 0; i < array.length; i++) {
            cuadrat = Math.pow(array[i],2);
            sum += cuadrat;
        }
        modulus = Math.sqrt(sum);
        return modulus;
    }
}

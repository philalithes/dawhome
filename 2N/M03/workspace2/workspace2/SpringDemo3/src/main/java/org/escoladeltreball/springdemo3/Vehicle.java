package org.escoladeltreball.springdemo3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Vehicle {

	private String color;
	
	@Autowired
	@Qualifier("classe A")
	private Motor motor;
	
	public Vehicle(String color, Motor motor) {
		super();
		this.color = color;
		this.motor = motor;
	}
	public Motor getMotor() {
		return motor;
	}
	public void setMotor(Motor motor) {
		this.motor = motor;
	}
	public Vehicle() { 
		
	}
	public Vehicle(String color) {
		super();
		this.color = color;
	}
	@Override
	public String toString() {
		return "Vehicle [color=" + color + ", motor=" + motor + "]";
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

}

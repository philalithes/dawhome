package org.escoladeltreball.m08_uf3_apg_final;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {

	private static final Logger logger = 
			LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexPage() {
		return "redirect:index";
	}

	@RequestMapping(value = "/index")
	public String indexPage(Locale locale, Model model) {

		logger.info("Welcome home! The client locale is {}.", locale);

		Date date = new Date();
		DateFormat dateFormat = 
				DateFormat
				.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);

		String formattedDate = dateFormat.format(date);

		model.addAttribute("serverTime", formattedDate);

		return "index";
	}

	@RequestMapping(value = "/user")
//	@Secured("ROLE_USER")
	public String test1(Model model) {
		userService.userMethod();
		return "home";
	}
	
	@RequestMapping(value = "/admin")
//	@Secured("ROLE_ADMIN")
	public String test2(Model model) {
		userService.adminMethod();
		return "home";
	}
}

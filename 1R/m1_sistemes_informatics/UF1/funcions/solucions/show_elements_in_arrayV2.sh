#!/bin/bash
# Filename:		show_elements_in_arrayV2
# Author:		pingui
# Date:			16/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./show_elements_in_arrayV2 [arg1...]
# Description:	Script que utilitzant una funció ens mostra els elements de l'array


function show_elements_array {
	# Calculem la longitud de l'array
	longitud=$(eval echo \${#$1[@]})

	i=0
        # Mentre no arribi a l'últim caràcter de la cadena mostro el caràcter
	while [ $i -lt $longitud ]
	do
		eval echo \${$1[$i]}
		i=$((i+1))
	done
}

# cridem a la funció que mostra els caràcters de l string
numbers_array=( 1 3 5 7 9 45 )
show_elements_array numbers_array



# Més info a:  http://tldp.org/LDP/abs/html/string-manipulation.html


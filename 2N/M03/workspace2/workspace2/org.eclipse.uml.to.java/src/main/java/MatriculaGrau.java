/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of MatriculaGrau.
 * 
 * @author iaw47951368
 */
public class MatriculaGrau extends Matricula {
	/**
	 * Description of the property nCredits.
	 */
	public int nCredits = 0;
	
	// Start of user code (user defined attributes for MatriculaGrau)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public MatriculaGrau() {
		// Start of user code constructor for MatriculaGrau)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for MatriculaGrau)
	
	// End of user code
	/**
	 * Returns nCredits.
	 * @return nCredits 
	 */
	public int getNCredits() {
		return this.nCredits;
	}
	
	/**
	 * Sets a value to attribute nCredits. 
	 * @param newNCredits 
	 */
	public void setNCredits(int newNCredits) {
	    this.nCredits = newNCredits;
	}



}

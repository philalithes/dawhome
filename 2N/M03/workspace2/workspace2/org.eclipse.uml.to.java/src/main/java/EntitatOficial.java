/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of EntitatOficial.
 * 
 * @author iaw47951368
 */
public class EntitatOficial {
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	// Start of user code (user defined attributes for EntitatOficial)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public EntitatOficial() {
		// Start of user code constructor for EntitatOficial)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for EntitatOficial)
	
	// End of user code
	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}



}

package org.edt;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static List<Partido> partidos = new ArrayList<>();
	public static List<String> equipos = new ArrayList<>();
	
	public static String calculResultat(String local, String visitante){
		
		if(!equipos.contains(local)){
			System.out.println("Equip local introduit "+local+" No correspon a ningun d'aquesta temporada.");
			return "Error";
		}
		if(!equipos.contains(visitante)){
			System.out.println("Equip Visitant introduit "+visitante+" No correspon a ningun d'aquesta temporada.");
			return "Error";
		}
		
		int vicLoc = 0;
		int vicVis = 0;
		int emp = 0;
		
		for(Partido p: partidos){			
			if(p.getLocal() == local && p.getVisitante() == visitante){
				if(p.getResultado() != "x" && p.getResultado() != "1" && p.getResultado() != "2"){
					System.out.println("El resultat del partit "+p.getLocal()+" VS "+p.getVisitante() +" de la data "+p.getData()+" es incoherent");
					continue;
				}
				else if(p.getResultado() == "1"){
					vicLoc++;
				}
				else if(p.getResultado() == "2"){
					vicVis++;
				}
				else{
					emp++;
				}
			}
		}
		
		if(vicLoc > vicVis && vicLoc > emp){
			System.out.println("1");
			return "1";
		}
		
		else if(vicVis > emp && vicVis > vicLoc){
			System.out.println("2");
			return "2";
		}
		
		else if(emp > vicLoc && emp > vicVis){
			System.out.println("x");
			return "x";
		}
		
		else if(vicLoc == vicVis && vicLoc == emp){
			System.out.println("Prediccio impresisa, no hi ha resultat clar");
			return "No hi ha un resultat clar";
		}
		
		else if(vicLoc == vicVis){
			System.out.println("1 - 2");
			return "1 - 2";
		}
		
		else if(vicLoc == emp){
			System.out.println("1 - x");
			return "1 - x";
		}
		
		else{
			System.out.println("2 - x");
			return "2 - x";
		}
	}
	
	public static void main(String[] args) {
		
		equipos.add("Barça");
		equipos.add("Madrid");
		equipos.add("Betis");
		equipos.add("Malaga");
		equipos.add("Granada");
		equipos.add("Sevilla");
				
		insertPartidos();
				
		System.out.println("Barça - Madird");
		calculResultat("Barça", "Madrid");
		System.out.println("Betis - Sevilla");
		calculResultat("Betis", "Sevilla");
		System.out.println("Malaga - Granada");
		calculResultat("Malaga", "Granada");
	}
	
	public static void insertPartidos(){
		partidos.add(new Partido("Barça","Madrid","1","1999-10-13"));
		partidos.add(new Partido("Barça","Madrid","2","2000-10-12"));
		partidos.add(new Partido("Barça","Madrid","x","1999-10-16"));
		partidos.add(new Partido("Barça","Madrid","x","1999-10-22"));
		partidos.add(new Partido("Barça","Madrid","3","1998-09-13"));
		partidos.add(new Partido("Barça","Madrid","1","1997-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","1996-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","1995-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","1994-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","1993-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","1992-10-13"));
		partidos.add(new Partido("Barça","Madrid","2","1991-10-13"));
		partidos.add(new Partido("Barça","Madrid","x","1990-10-13"));
		partidos.add(new Partido("Barça","Madrid","2","2006-10-13"));
		partidos.add(new Partido("Barça","Madrid","2","2007-10-13"));
		partidos.add(new Partido("Barça","Madrid","x","2008-10-13"));
		partidos.add(new Partido("Barça","Madrid","x","2009-10-13"));
		partidos.add(new Partido("Barça","Madrid","x","2010-10-13"));
		partidos.add(new Partido("Barça","Madrid","1","2011-10-13"));
		
		partidos.add(new Partido("Betis","Sevilla","1","2006-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","2004-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","2003-10-13"));
		partidos.add(new Partido("Betis","Sevilla","2","2002-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","1999-10-13"));
		partidos.add(new Partido("Betis","Sevilla","2","1998-10-13"));
		partidos.add(new Partido("Betis","Sevilla","1","1997-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","1996-10-13"));
		partidos.add(new Partido("Betis","Sevilla","2","1995-10-13"));
		partidos.add(new Partido("Betis","Sevilla","2","1994-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","1993-10-13"));
		partidos.add(new Partido("Betis","Sevilla","x","1992-10-13"));
		partidos.add(new Partido("Betis","Sevilla","2","1991-10-13"));
		partidos.add(new Partido("Betis","Sevilla","1","1990-10-13"));
		
		partidos.add(new Partido("Malaga","Granada","1","1990-10-13"));
		partidos.add(new Partido("Malaga","Granada","2","1990-10-13"));
		partidos.add(new Partido("Malaga","Granada","x","1990-10-13"));
	}

}

#!/bin/bash
# Filename:		show_elements_in_array
# Author:		pingui
# Date:			16/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./show_elements_in_array [arg1...]
# Description:	Script que utilitzant una funció ens mostra els elements de l'array

function show_elements_array {
	# Save the array in a local variable
	eval local array_temp=(\${$1[@]})

	longitud=${#array_temp[@]}
	i=0
    # Traverse the arry, til the last char, showing the current char
	while [ $i -lt $longitud ]
	do
		echo ${array_temp[$i]}
		i=$((i+1))
	done
}

# Call the function that shows the string chars
numbers_array=( 1 3 5 7 9 45 )
show_elements_array numbers_array

# Més info a:  http://tldp.org/LDP/abs/html/string-manipulation.html


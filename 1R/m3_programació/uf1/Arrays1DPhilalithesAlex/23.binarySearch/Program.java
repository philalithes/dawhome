
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula de nombres enters ordenats
     * de forma creixent i un nombre enter, determina si el nombre és un element de la taula
     * emprant cerca dicotòmica. No usis el mètode Arrays.binarySearch()
     */
    
    public boolean binarySearch(int[] array, int n) {
        //variable declaration
        boolean found = false;
        int low = 0;
        int high = array.length;
        int test = 0;
        //calcul
        while (low + 1 < high && !found) {
            test = (low + high) / 2;
            if (array[test] > n) {
                high = test;
            }
            else {
                low = test;
            }
        }
        if (array[low] == n) {
            found = true;
        }
        return found;
    }
}
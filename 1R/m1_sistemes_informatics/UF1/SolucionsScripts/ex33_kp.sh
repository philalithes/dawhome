#!/bin/bash
# Fitxer: ex33_kp_comments.sh
# Autor: jamoros
# Data: 31/03/2011
# Versio: 0.1
# Descripcio: script que compta el número de caràcters que tenen els noms dels fitxers del directori actual.
#		Suposarem que tenim permís de lectura. No mirarem recursivament, és a dir només examinem al primer nivell de profunditat
#


# Capturarem tots els fitxers del directori actual

tots_fitxers=`ls -1`  # L'opció -1 em possa un fitxer a cada línia

# Per a cada fitxer del directori actual mostrem el numero de caracters que té el nom del fitxer


OLD_IFS=$IFS
IFS=$'\n'


for nom_fitxer in $tots_fitxers
do
	num_caracters=$(echo $nom_fitxer |wc -m)
	echo ${nom_fitxer}:$num_caracters
done

IFS=$OLD_IFS

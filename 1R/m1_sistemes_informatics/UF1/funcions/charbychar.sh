#!/bin/bash
# Filename:			charbychar.sh
# Author:			iaw47951368
# Date:				04/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./charbychar.sh [args...]
# Description:		escriu cada caràcter que entra per stdin en linies diferents.

function funcio {
cadena=$1
var=${#cadena}

for i in $(seq 0 $var)
do
	echo "${cadena:$i:1}"
done
}

var=$(cat)
funcio $var

/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     * Donades dues cadenes, determina en quina posició discrepen
     * per primer cop. Si no discrepen, retornarem -1.
     */
    public int positionDiffers(String cadena1, String cadena2) {
        //vars
        int position = -1;
        int i = 0;
        boolean exit = false;
        //calc
        while (!exit && i < cadena1.length() && i < cadena2.length()) {
            if (cadena1.charAt(i) != cadena2.charAt(i)) {
                position = i;
                exit = true;
            }
            i++;
        }
        if (!exit && cadena1.length() > cadena2.length()) {
            position = cadena2.length();
        } else if (!exit && cadena2.length() > cadena1.length()) {
            position = cadena1.length();
        }
        return position;
    }
    
}
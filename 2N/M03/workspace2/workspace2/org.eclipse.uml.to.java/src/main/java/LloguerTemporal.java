/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of LloguerTemporal.
 * 
 * @author iaw47951368
 */
public class LloguerTemporal extends Lloguer {
	/**
	 * Description of the property dataFinal.
	 */
	public String dataFinal = "";
	
	// Start of user code (user defined attributes for LloguerTemporal)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public LloguerTemporal() {
		// Start of user code constructor for LloguerTemporal)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for LloguerTemporal)
	
	// End of user code
	/**
	 * Returns dataFinal.
	 * @return dataFinal 
	 */
	public String getDataFinal() {
		return this.dataFinal;
	}
	
	/**
	 * Sets a value to attribute dataFinal. 
	 * @param newDataFinal 
	 */
	public void setDataFinal(String newDataFinal) {
	    this.dataFinal = newDataFinal;
	}



}

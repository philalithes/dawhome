package org.escoladeltreball.beansiaspects;

public class Battery {

	private int charge;

	public Battery(int charge) {
		super();
		this.charge = charge;
	}

	public int getCharge() {
		return charge;
	}

	public void setCharge(int charge) {
		this.charge = charge;
	}

	@Override
	public String toString() {
		return "Battery [charge=" + charge + "]";
	}
	
	public void spend() {
		charge--;
		System.out.println("charge - 1");
	}
	
}
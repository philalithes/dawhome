// ******************* REGION VARIABLES GLOBALES ******************//
//variables
var rows = 50;
var cols = 50;
var board = new Board(rows, cols);
var arrObjetos = [];
var classSelected;
var down = true;
var oPlayerBlock = {};

//objetos
var IdClazz {
	SKY: 0,
	FLOOR: 1,
	FLOOR_UNSTABLE: 2,
	STAIRS: 3,
	PLAYER: 4,
	MINE: 5
}

// FIN REGION
// ******************* REGION EVENTOS******************//

$(document).ready(function() {

	updateTable("main", board);


	//left = 37 up = 38 right = 39 down = 40
	$("body").mousedown(function() {
				down = true;
			}).mouseup(function() {
				down = false;
			}).keydown(function(event) {
				if (event.which == 37) {
					interaccion(leftBlock(oPlayerBlock).attr('class'), true);
				} else if (event.which == 38) {
					interaccion(upperBlock(oPlayerBlock).attr('class'), true);
					move(upperBlock(oPlayerBlock), clazz);
				} else if (event.which == 39) {
					interaccion(rightBlock(oPlayerBlock).attr('class'), true);
					move(rightBlock(oPlayerBlock), clazz);
				} else if (event.which == 40) {
					interaccion(lowerBlock(oPlayerBlock).attr('class'), true);
					move(lowerBlock(oPlayerBlock), clazz);
				}
			});

	$('td').click(function() {
				paint($(this));
			}).mouseover(function() {
				if (down) {
					paint($(this));

				}
			});

	$('.tileset').click(function() {
		classSelected = $(this).attr("class").split(" ")[1];
		$('.selected').removeClass().addClass('tileset selected');
		if (classSelected != "selectede")
		$('.selected').toggleClass(classSelected);
		
	});

	
});


// FIN REGION

// ******************* REGION FUNCIONES ******************//

function Item(x, y, tipo, mov) {
	this.x = x;
	this.y = y;
	this.tipo = tipo;
	this.mov = mov;
	this.setPos = function(x, y) {
		this.x = x;
		this.y = y;
	};
	this.setMov = function(mov) {
		this.mov = mov;
	}
}
function paint(thisBlock) {
	if (classSelected == "selected") return;
	thisBlock.removeClass();
	thisBlock.addClass(classSelected);
	if (classSelected == 'player') {
		$('td').off();
		oPlayerBlock = $(".player:eq(2)");
	}
}

function Board(rows, cols) {
	this.name = arguments[2];
	this.nRows = rows;
	this.nCols = cols;
	this.cells = [];
	for (var i = 0; i < this.nRows; i++) {
		this.cells.push([]);
		for (var j = 0; j < this.nCols; j++) {
			this.cells[i].push([]);
		}
	}
}
function updateTable(idDiv, board) {
	var table = '<center><table>';
	for (var i = 0; i < rows; i++) {
		table += '<tr>';
		for (var j = 0; j < cols; j++) {
			table += '<td id='+(i+j)+'>';
			table += board.cells[i][j];
			table += '</td>';
		}
	table+= '</tr>';
	}
	table+= '</table>';
	$("#"+idDiv).html(table);
}
function updateItems(arrObjetos) {
	for (var i = 0; i < arrObjetos; i++) {
		switch (arrObjetos[i].tipo) {
			case IdClazz.SKY:
				break;
			case IdClazz.FLOOR:
				break;
			case IdClazz.FLOOR_UNSTABLE:
				break;
			case IdClazz.PLAYER:
				break;
			case IdClazz.MINE:
				break;
			case IdClazz.STAIRS:
				break;
		}

	}
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function interaccion(item) {
	if (item == 'floor') {
		return 'floor';
	}
	if (item == 'sky') {
		console.log("asdf")
		move(oPlayerBlock);
		return 'sky';
	}
	if (item == 'mine') {
		Player.die();
		return 'mine';
	}
}

function move(block) {
	$(".jugador").attr('class', Player.swap);
	Player.swap = block.attr("class");
	block.removeClass();
	block.addClass("jugador");
}

function gravity() {
	if ($(".jugador").lowerBlock().attr("class") == 'air') {
		move(lowerBlock(), 'air');
	} else if ($(".jugador").lowerBlock().attr("class") == 'cristal') {
		Player.die();
	}
}


//funciones que devuelven el objeto vecino
function upperBlock(thisBlock) {
	blockID = thisBlock.attr('id');
	parent = thisBlock.parent();
	return parent.prev().children("td:eq("+blockID+")");
}

function lowerBlock(thisBlock) {
	blockID = thisBlock.attr('id');
	parent = thisBlock.parent();
	return parent.next().children("td:eq("+blockID+")");
}

function leftBlock(thisBlock) {
	console.log(thisBlock)
	console.log(thisBlock.prev())
	return thisBlock.prev();
}

function rightBlock(thisBlock) {
	return thisBlock.next();
}

// FIN REGION
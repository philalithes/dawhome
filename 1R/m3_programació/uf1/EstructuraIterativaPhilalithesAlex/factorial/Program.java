/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donat un nombre enter positiu calcula el seu factorial.
     * El factorial d’un nombre enter positiu n es defineix com: n!= 1×2×3×...× ( n−1 ) ×n
     * 
     */
    
    public int factorial(int n) {
        //variable declaration
        int i = 1;
        int acumulador = 1;
        //calcul
        while (i <= n) {
            acumulador *= i;
            i++;
        }
        return acumulador;
    }
}

/*
 * Program.java        2015/6/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Calculate seconds from h:m:s
     * 
     * @param h hours
     * @param m minutes
     * @param s seconds
     * 
     * @param a full seconds
     */
    public int hms2s(int h, int m, int s){
        //variable declaration    
        int a;
        //calculate return
        a =  h * 3600 + m * 60 + s;
        //return
        return a;
    }
    
}

package org.iedt.jdbcdemo2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.sql.Timestamp;

public class Main {

	private static final String URL = "jdbc:postgresql://localhost:5432/java";
	
	public static void insertEvent(Event e) {
		Connection connection;
		Statement statement;
		String name = e.getName();
		Timestamp data = e.getData();
		String desc = e.getDescripcio();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			statement.executeUpdate("insert into events (nom, descripcio, data) values ('" + name + "','" + desc + "','" + data + "')");
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void deleteEvent(long id) {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			statement.executeUpdate("delete from events where id = " + id);
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static void updateEvent(Event e) {
		long id = e.getId();
		String name = e.getName();
		String descripcio = e.getDescripcio();
		Timestamp data = e.getData();
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			statement.executeUpdate("update events set name = '" + name + "', descripcio = '" + descripcio
					+ "', data = '" + data + "' where id = " + id);
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	
	}
	public static void main(String[] args) {
		Event e = new Event(1, "any nou", "bon any nou!", Timestamp.valueOf("2017-01-01 00:00:00"));
		
		insertEvent(e);
		System.out.println("ok");
		
	}

}

package org.escoladeltreball.provam05uf2;

import java.util.Random;

/**
 * @author iaw47951368
 * @version 1.0
 * @see org.escoladeltreball.provam05uf2.User
 * @see org.escoladeltreball.provam05uf2.Leaderboard
 */
public class Main {
	
	public static Random rnd = new Random(); 
	
	/**
	 * Crea un objecte leaderboard, genera usuaris aleatoris, anuncia el guanyador i imprimeix els 10 primers
	 * @param args no es fa servir
	 */
	public static void main(String[] args) {
		Leaderboard scores = new Leaderboard();
		for (int i = 0; i < 30; i++) {
			User u = new User(i, "user"+i, (long)(rnd.nextInt(2000)));
			scores.addUser(u);
		}
		
		User u = new User(100L, "user", (long)(rnd.nextInt(2000)));
		System.out.println("The Winner is "+ scores.userWin().getName() + " scores " + scores.userWin().getScore() + "!!!");
		System.out.println("Diferencia entre el segundo usuario y el primero : "+ scores.difSecondFirst());
		System.out.println("******* Ranking de los 10 mejores usuarios!! ********");
		scores.bestTen();
		System.out.println("Usuario "+ u.getName() + " con score " + u.getScore() + " le faltan "+scores.difUserFirst(u) + " para llegar al mejor");
	}

}

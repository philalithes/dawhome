package org.escoladeltreball.jpaprojectdemo1;

import java.io.Serializable;
import java.lang.Double;
import java.lang.Long;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: RecordEntity
 *
 */
@Entity(name="RecordEntity")
@Table(name="record")

public class Record implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Double grade;
	private static final long serialVersionUID = 1L;

	public Record() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Double getGrade() {
		return this.grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}
   
}

package org.escoladeltreball.m03uf06nf02APG;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Entity implementation class for Entity: Jugador
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "selectJugador", query = "SELECT j FROM Jugador j WHERE j.id = ?1"),
	@NamedQuery(name = "selectJugadorByNom", query = "SELECT j FROM Jugador j WHERE j.nom like ?1"),
	@NamedQuery(name = "updateJugador", query = "UPDATE Jugador j SET j.nom = ?1, j.dorsal = ?2, j.posicio = ?3 WHERE j.id = ?4")
})
public class Jugador implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nom;
	private Integer dorsal;
	private String posicio;
	private static final long serialVersionUID = 1L;
	//owner
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Equip equip;

	public Jugador() {
		super();
	}   
	
	@Override
	public String toString() {
		return "Jugador [id=" + id + ", nom=" + nom + ", dorsal=" + dorsal + ", posicio=" + posicio + "]";
	}

	public Equip getEquip() {
		return equip;
	}

	public void setEquip(Equip equip) {
		this.equip = equip;
	}

	public Jugador(String nom, Integer dorsal, String posicio) {
		super();
		this.nom = nom;
		this.dorsal = dorsal;
		this.posicio = posicio;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public Integer getDorsal() {
		return this.dorsal;
	}

	public void setDorsal(Integer dorsal) {
		this.dorsal = dorsal;
	}   
	public String getPosicio() {
		return this.posicio;
	}

	public void setPosicio(String posicio) {
		this.posicio = posicio;
	}
   
}

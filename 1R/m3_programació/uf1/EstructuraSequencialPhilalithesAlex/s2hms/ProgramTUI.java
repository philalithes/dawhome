/*
 * Program.java 2015/5/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * Llegeix un nombre enter de segons i escriu el nombre d’hores, minuts i segons equivalents en format h:m:s.
  * 
  * @param s = segons
  * 
  * @param h = hores
  * @param m = minuts
  * @param ss = segons
  * 
  * @return h:m:s
  */


    
    public String s2hms(int s) {
        int h;
        int m;
        int ss;
        h = s / 3600;
        m = (s - h * 3600) / 60;
        ss = s - h * 3600 - m * 60;
        
        String hms = h + ":" +  m + ":" + ss;
        return hms;
        
    }
    public static void main(String args[]){
        String hms;
        //declaracio de variables
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("introdueix els segons");
        int s = sc.nextInt();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        hms = p.s2hms(s);
        //retorn
        System.out.printf("%s", hms);
    }
}

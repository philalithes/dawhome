package org.escoladeltreball.beansiaspects;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

@Configuration
//@ComponentScan
@EnableAspectJAutoProxy(proxyTargetClass=true)
@Import(value = {BeansDefinition2.class})
public class SpringConfig {

	public SpringConfig() {
		// TODO Auto-generated constructor stub
	}
	
	

}

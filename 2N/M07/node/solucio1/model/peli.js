var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//objecte esquema
var peliculaSchema = new Schema({
    Codpeli: Number,
    Titol: String,
    Sinopsi: String
});

//registrem la col.leccio Pelicules amb un esquema correct
//i l'exportem per a que tots els moduls puguin tenir-lo
module.exports = mongoose.model('pelicules', peliculaSchema);

package org.escoladeltreball.jpaprojectdemo2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

/**
 * Entity implementation class for Entity: Teacher
 *
 */
@Entity

public class Teacher implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	//inverse
	
	@ManyToMany(mappedBy = "teachers")
	private List<Student> students = new ArrayList<Student>();;
	private String name;
	private static final long serialVersionUID = 1L;

	
	@Override
	public String toString() {
		return "Teacher [id=" + id + ", name=" + name + "]";
	}
	public Teacher(String name) {
		super();
		this.name = name;
	}
	public Teacher() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNom() {
		return this.name;
	}

	public void setNom(String name) {
		this.name = name;
	}
   
}

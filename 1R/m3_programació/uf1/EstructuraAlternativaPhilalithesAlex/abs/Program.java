/*
 * Program.java        2015/20/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    
    /**
     * Calcula el valor absolut d'un nombre real (sense usar Math.abs(x)).
     * 
     * @param n1 = nombre real
     * 
     * @retorn valor absolut
     */
    public double abs(double n1){
        double abs;
        if (n1 < 0){
            abs = -n1;
        }
        else {
            abs = n1;
        }    
        return abs;
    }

    
}

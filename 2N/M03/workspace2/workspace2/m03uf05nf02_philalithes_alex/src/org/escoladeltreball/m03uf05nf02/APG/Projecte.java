package org.escoladeltreball.m03uf05nf02.APG;

public class Projecte {
	private String nom;
	private int dificultat;
	private long pressupost;
	private Empleat projectManager;
	private Empleat backEnd;
	private Empleat frontEnd;

	public Empleat getProjectManager() {
		return projectManager;
	}

	public void setProjectManager(Empleat projectManager) {
		this.projectManager = projectManager;
	}

	public Empleat getBackEnd() {
		return backEnd;
	}

	public void setBackEnd(Empleat backEnd) {
		this.backEnd = backEnd;
	}

	public Empleat getFrontEnd() {
		return frontEnd;
	}

	public void setFrontEnd(Empleat frontEnd) {
		this.frontEnd = frontEnd;
	}

	@Override
	public String toString() {
		return "Projecte [nom=" + nom + ", dificultat=" + dificultat + ", pressupost=" + pressupost
				+ ", projectManager=" + projectManager + ", backEnd=" + backEnd + ", frontEnd=" + frontEnd + "]";
	}

	public Projecte(String nom, int dificultat, long pressupost) {
		super();
		this.nom = nom;
		this.dificultat = dificultat;
		this.pressupost = pressupost;
	}

	public Projecte(String registre) {
		super();
		String[] splitRegistre = registre.split(",");
		nom = splitRegistre[0];
		dificultat = Integer.parseInt(splitRegistre[1]);
		pressupost = Long.parseLong(splitRegistre[2]);
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getDificultat() {
		return dificultat;
	}

	public void setDificultat(int dificultat) {
		this.dificultat = dificultat;
	}

	public long getPressupost() {
		return pressupost;
	}

	public void setPressupost(long pressupost) {
		this.pressupost = pressupost;
	}
}

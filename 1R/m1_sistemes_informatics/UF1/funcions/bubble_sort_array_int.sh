#!/bin/bash
# Filename:		bubble_sort_array_int.sh
# Author:		jamoros
# Date:			04/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./bubble_sort_array_int.sh [arg1...]
# Description:	Script que ordena amb el mètode de la bombolla un array d'enters
# 				Farem l'array hardcoded i amb valors aleatoris utilitzant la variable RANDOM


max_number=30 # max number aleatori
declare -a enters	# això declara la variable enters com a array
					# en principi no és necessari però segons (1)
					# en certs casos pot accelerar l'script

# Calcularem 10 vegades un numero aleatori i l'afegirem a l'array 
enters=( $( \
			for i in {1..10};
			do 
				echo -n $(($RANDOM % max_number + 1 ))
				echo -n " "
			done
			)
		)

# Mostrem l'array que acabem de crear
echo ${enters[@]}

# Fem el mètode la bombolla per ordenar de manera descendent.
# descendent: quan comparem 2 parelles de números, volem que el de la dreta sigui més gran o igual que el de l'esquerra 
# Utilitzem el for modern: C-style
for (( i=9; i>0; i--))
{
	for (( j=0; j<i; j++ ))
	{
		# Si aquesta parella no està ordenada descendenment fem un swap
		if (( enters[j] < enters[j+1] )) 
		then
				temp=${enters[j]}
				enters[j]=${enters[j+1]}
				enters[j+1]=$temp

		fi			
	}
}
# Mostrem l'array, ara ja ordenat
echo ${enters[@]}



<<OBSERVACIONS
(1) "Adding a superfluous declare -a statement to an array declaration may speed up execution of subsequent operations on the array."
extract from http://tldp.org/LDP/abs/html/arrays.html

(2) si volguessim posar una variable per la longitud de l'array ho podríem fer de la següent manera:
longitud_enters=10
...
	...		for i in $( eval echo {1..$longitud_enters});
	...
...
ja que si no fem res, no substituiria el valor de la variable "longitud_enters"

(3) El bloc:
                temp=${enters[j]}
                enters[j]=${enters[j+1]}
es podria codificar amb C-style com:
				(( temp=enters[j] ))
				(( enters[j]=enters[j+1] ))

(4) enllaç interessant:
	http://stackoverflow.com/questions/5061100/bash-c-style-if-statement-and-styling-techniques

OBSERVACIONS



/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donades dues taules numèriques reals, construeixi una tercera
     * taula amb els elements de la primera taula seguits dels elements de la segona i retorna
     * questa tercera taula
     */
    
    public double[] join(double[] array1, double[] array2) {
        //variable declaration
        int n = 0;
        int i = 0;
        //esbrinar la longitud del tercer array:
        for (i = 0; i < array1.length; i++) {
            n++;
        }
        for (i = 0; i < array2.length; i++) {
            n++;
        }
        //declarar tercer array
        double[] array3 = new double[n];
        for (i = 0; i < array3.length; i++) {
            if (i < array1.length) {
                array3[i] = array1[i];
            } else {
                array3[i] = array2[i-array1.length];
            }
        }
        return array3;
            
    }
}

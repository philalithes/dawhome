package org.escoladeltreball.m03uf06nf02APG;

public class NonUniqueNomException extends Exception {

	public NonUniqueNomException() {
		super();
	}
	
	public NonUniqueNomException(String message) {
		super(message);
	}
}

<?php
    $function = $_POST['accio'];
    $doc = new DOMDocument('1.0', 'UTF-8');
    // print
    $doc -> formatOutput = true;
    $doc -> prserveWhiteSpace = false;
    $doc->load('../xml/gym.xml');
    if ($function == 'INS') {
        $nom = $_POST['new_name'];
        $club = $_POST['new_club'];
        $barra = $_POST['new_barra'];
        $asimetriques = $_POST['new_asimetriques'];
        $terra = $_POST['new_terra'];
        $potro = $_POST['new_potro'];

        
        $root = $doc->firstChild;

        $new_nena = $doc->createElement('nena');
        $new_nena = $root->appendChild($new_nena);

        $new_nom = $doc->createElement('nom');
        $new_nom = $new_nena->appendChild($new_nom);
        $text = $doc->createTextNode($nom);
        $text = $new_nom->appendChild($text);

        $new_club = $doc->createElement('club');
        $new_club = $new_nena->appendChild($new_club);
        $text = $doc->createTextNode($club);
        $text = $new_club->appendChild($text);

        $new_fase1 = $doc->createElement('fase1');
        $new_fase1 = $new_nena->appendChild($new_fase1);

        $new_barra = $doc->createElement('barra');
        $new_barra = $new_fase1->appendChild($new_barra);
        $text = $doc->createTextNode($barra);
        $text = $new_barra->appendChild($text);

        $new_asimetriques = $doc->createElement('asimetriques');
        $new_asimetriques = $new_fase1->appendChild($new_asimetriques);
        $text = $doc->createTextNode($asimetriques);
        $text = $new_asimetriques->appendChild($text);

        $new_terra = $doc->createElement('terra');
        $new_terra = $new_fase1->appendChild($new_terra);
        $text = $doc->createTextNode($terra);
        $text = $new_terra->appendChild($text);

        $new_potro = $doc->createElement('potro');
        $new_potro = $new_fase1->appendChild($new_potro);
        $text = $doc->createTextNode($potro);
        $text = $new_potro->appendChild($text);
    } else {
        $noms = $doc->getElementsByTagName('nom');
        $nom = utf8_encode($_POST['fnom']);
        $new_nom = utf8_encode($_POST['gnom']);
        $club = utf8_encode($_POST['gclub']);
        $barra = utf8_encode($_POST['gbarra']);
        $potro = utf8_encode($_POST['gpotro']);
        $asim  = utf8_encode($_POST['gasimetriques']);
        $terra  = utf8_encode($_POST['gterra']);
        
        for ($i = 0; $i < $noms->length; $i++) {
            if ($noms->item($i)->nodeValue == $nom) {
                // Save data
                $npare=$noms->item($i)->parentNode;
                $n=$npare->getElementsByTagName("nom");
                $n->item(0)->nodeValue=$new_nom;
                $n=$npare->getElementsByTagName("club");
                $n->item(0)->nodeValue=$club;
                $n=$npare->getElementsByTagName("barra");
                $n->item(0)->nodeValue=$barra;
                $n=$npare->getElementsByTagName("terra");
                $n->item(0)->nodeValue=$terra;
                $n=$npare->getElementsByTagName("asimetriques");
                $n->item(0)->nodeValue=$asim;
                $n=$npare->getElementsByTagName("potro");
                $n->item(0)->nodeValue=$potro;
            }
        }
        
    }

    $doc->save('../xml/gym.xml');
    header('Location: ../practica_14_add_nodes_xml.html');
?>
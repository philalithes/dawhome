/*
 * ProgramTUI.java        1.0 Oct 30, 2016
 * Models the program.
 *
 * Copyright 2016 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.*;
import java.util.Scanner;
public class ProgramTUI
{
    /**
     * Count blank spaces from a string
     * 
     * @param telegram's string
     * @param initial positon to count spaces
     * @param final position to count spaces
     * @return number of blank spaces
     * 
     */
    public int countBlankSpace(String telegrams,int start,int end) {
        //variables
        int i = 0;
        int blankSpaces = 0;
        //comptar
        for (i = start; i <= end; i++) {
            if (telegrams.charAt(i) == ' ') {
                blankSpaces++;
            }
        }
        return blankSpaces;
    }
    /**
     * Count words from a string
     * 
     * @param telegram's string
     * @param initial positon to count words
     * @param final position to count words
     * @return word's number
     * 
     */
    public int countWords(String telegrams,int start,int end) { 
        //variables
        int i = 0;
        int words = 0;
        //comptar
        for (i = start + 1; i <= end; i++) {
            if (telegrams.charAt(i) == ' ' && telegrams.charAt(i - 1) != ' ') {
                words++;
            }
        }
        return words;
    }
   /**
    * Count telegrams from a string
    * 
    * @param telegram's string
    * @return telegram's number
    * 
    */
 public int countTelegrams(String telegrams) {
     //variables
     int i = 0;
     int countTelegrams = 1;
     //comptar
     for (i = 0; i < telegrams.length(); i++) {
         if (telegrams.charAt(i) == '*') {
             countTelegrams++;
         }
     }
     return countTelegrams;
 }
 /**
  * Indicates the start and end position telegrams and save these positions in an array of strings
  * EXAMPLE : telgrams = "HOLA MARIA TINC MOLTA FEINA*JOAN PLEGARÉ TARD *ENS VEÏEM DEMÀ ."
  *  locationPhrase[0] = 0
  * locationPhrase[1] = 26
  * locationPhrase[2] = 28
  * locationPhrase[3] = 45
  * locationPhrase[4] = 47
  * locationPhrase[5] = 61
  * @param telegram's string
  * @return The array of strings with the positions of telegrams
  * 
  */
 public String [] locationTelegrams(String telegrams) {
     //variables
     int contador = 2;
     int j = 0;
     int i = 0;
     for (i = 0; i < telegrams.length(); i++) {
         if (telegrams.charAt(i) == '*') {
             contador += 2;
         }
     }
     String[] locationPhrase = new String[contador];
     for (i = 0; i < telegrams.length(); i++) {
         if (i == 0) {
             locationPhrase[j] = Integer.toString(i);
             j++;
         } else if (telegrams.charAt(i) == '*') {
             locationPhrase[j] = Integer.toString(i-1);
             j++;
             locationPhrase[j] = Integer.toString(i+1);
             j++;
         } else if (telegrams.charAt(i) == '.') {
             locationPhrase[j] = Integer.toString(i-1);
         }
     }
     return locationPhrase;
 }
 /**
  * Displays billing's telegrams
  * 
  * @param telegram's string
  */
 public void telegramsBilling(String telegrams) {
     //variables
     int numberOfTelegrams = countTelegrams(telegrams);
     int i = 0;
     String parsableInt = "";
     double preuTotal = 0;
     double preuWords = 0;
     double preuBlanks = 0;
     int blankSpaces = 0;
     int words = 0;
     String[] locationPhrase = new String[numberOfTelegrams * 2];
     locationPhrase = locationTelegrams(telegrams);
     int start = 0;
     int end = 0;
     int contador = 0; //servira per determinar start i end.
     System.out.println(Arrays.toString(locationPhrase));
     //print
     System.out.println("******************* FACTURACIÓ DELS TELEGRAMES *******************");
     System.out.println("Nombre de telegrames = " + numberOfTelegrams);
     for (i = 0; i < numberOfTelegrams; i++) {
         parsableInt = locationPhrase[contador];
         start = Integer.parseInt(parsableInt);
         contador++;
         parsableInt = locationPhrase[contador];
         end = Integer.parseInt(parsableInt);
         contador++;
         blankSpaces = countBlankSpace(telegrams, start, end);
         words = countWords(telegrams, start, end);
         preuWords = words * 0.1;
         preuBlanks = blankSpaces * 0.05;
         System.out.println("telegrama " + (i + 1));
         System.out.println("\t Nombre de paraules = " + words + " Preu: " + preuWords + " EUR");
         System.out.println("\t Nombre d'espais en blanc = " + blankSpaces + " Preu: " + preuBlanks + " EUR");
         preuTotal += preuWords + preuBlanks;
     }
     System.out.println("Preu total = " + preuTotal + " EUR");
 }
 /**
  * The user enters the keyboard telegrams
  * 
  * @return telegram's string
  */
 public  String buildTelegrams() {
     //variables
     Scanner sc = new Scanner(System.in);
     String telegrams = "";
     //usuari
     System.out.println("Escriu els telegrames:");
     telegrams = sc.next();
     return telegrams;
 }
 
 /**
  * Program's menu
  */
 public void menu() {
     //variables
     Scanner sc = new Scanner(System.in);
     int resposta = 0;
     String telegrams = "";
     //interacció amb l'usuari
     System.out.println("Què vol fer?");
     System.out.println("1) Escriure un telegrama");
     System.out.println("2) Veure factura");
     System.out.println("3) Sortir");
     resposta = sc.nextInt();
     switch (resposta) {
         case 1 :
             telegrams = buildTelegrams();
             menu();
         case 2 :
             telegramsBilling(telegrams);
             menu();
         case 3 :
             break;
     }
 }
 public static void main(String[] args) {
     //variables
     ProgramTUI p = new ProgramTUI();
     p.menu();
 }
}
 

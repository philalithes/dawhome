/*
 * Program.java        2016/1/12
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donada una cadena calcula quants d’ells
     * són vocals. S'ha de considerar que tant el caràcter a com el caràcter A són vocals.
     * 
     * 
     */
    
    public int numberOfVowels (String cadena) {
        //variable declaration
        int nombreVocals = 0;
        //calcul
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'A' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'E' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'I' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'O' || cadena.charAt(i) == 'u' || cadena.charAt(i) == 'U') {
                nombreVocals++;
            }
        }
        //retorn
        return nombreVocals;
    
        
    }
}

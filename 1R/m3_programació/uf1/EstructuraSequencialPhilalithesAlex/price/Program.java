/*
 * Program.java        2015/6/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Calculate price of a product
     * 
     * @param price = preu tarifa
     * @param iva = increment impost valor afegit, valor entre 0 i 100
     * @param discount = descompte, valor entre 0 i 100
     * 
     * @param preufinal = preu total a pagar
     */
    public double price(double price, double iva, double discount){
        //variable declaration    
        double preufinal;
        //calculate return
        preufinal = price + price / 100 * iva - price / 100 * discount;
        //return
        return preufinal;
    }
    
}

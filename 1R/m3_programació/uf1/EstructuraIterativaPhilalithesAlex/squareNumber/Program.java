/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    /*
     * Donat un nombre enter, determina si és
     * un quadrat perfecte o no. Un nombre enter és quadrat
     * perfecte si existeix un altre nombre
     * enter tal que el seu quadrat és igual a ell.
     */
    
    public boolean isSquareNumber(int n) {
        //variable declaration
        boolean isSquare = false;
        //càlcul
        for (int i = n; i > 0; i--) {
            if (i * i == n) {
                isSquare = true;
            }
        }
        
        return isSquare;
            
            
        
    }
}

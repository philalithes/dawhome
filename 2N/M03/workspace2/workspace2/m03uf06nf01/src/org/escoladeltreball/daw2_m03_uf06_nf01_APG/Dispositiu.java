package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

import java.sql.Timestamp;

public class Dispositiu {
	private int id;
	private String nom;
	private boolean wifi;
	private Timestamp alta;
	private double preu;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alta == null) ? 0 : alta.hashCode());
		result = prime * result + id;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		long temp;
		temp = Double.doubleToLongBits(preu);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + (wifi ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dispositiu other = (Dispositiu) obj;
		if (alta == null) {
			if (other.alta != null)
				return false;
		} else if (!alta.equals(other.alta))
			return false;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (Double.doubleToLongBits(preu) != Double.doubleToLongBits(other.preu))
			return false;
		if (wifi != other.wifi)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean isWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}

	public Timestamp getAlta() {
		return alta;
	}

	public void setAlta(Timestamp alta) {
		this.alta = alta;
	}

	public double getPreu() {
		return preu;
	}

	@Override
	public String toString() {
		return "Dispositiu [id=" + id + ", nom=" + nom + ", wifi=" + wifi + ", alta=" + alta + ", preu=" + preu + "]";
	}

	public void setPreu(double preu) {
		this.preu = preu;
	}

	public Dispositiu(int id, String nom, boolean wifi, Timestamp alta, double preu) {
		super();
		this.id = id;
		this.nom = nom;
		this.wifi = wifi;
		this.alta = alta;
		this.preu = preu;
	}

	/**
	 * constructor per posar la alta en string
	 */
	public Dispositiu(int id, String nom, boolean wifi, String alta, double preu) {
		super();
		this.id = id;
		this.nom = nom;
		this.wifi = wifi;
		this.alta = Timestamp.valueOf(alta);
		this.preu = preu;
	}
}

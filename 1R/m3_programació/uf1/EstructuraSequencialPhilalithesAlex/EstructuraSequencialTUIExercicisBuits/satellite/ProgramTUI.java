/*
 * ProgramTUI.java        VERSION DATE
 *
 * Models the program.
 *
 * Copyright YEAR FULL_NAME <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {

    /**
     * Calculates the rotation period of a satellite around the planet.
     * 
     * @param h the orbit height of a satellite, in km.
     * @return a string with the format h:m, where h:m is the rotation period.
     */
    public String rotationPeriod(double h) {

    }

    /**
     * Modelizes the UI as a TUI.
     * 
     * @param args not used.
     */
    public static void main(String[] args) {

    }

}

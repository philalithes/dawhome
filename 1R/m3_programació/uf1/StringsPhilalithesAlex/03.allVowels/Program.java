/*
 * Program.java        2016/1/12
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donada una cadena, determina si apareixen totes les
     * vocals.
     * 
     * 
     */
    
    public boolean allVowels (String cadena) {
        //variable declaration
        boolean a = false;
        boolean e = false;
        boolean i = false;
        boolean o = false;
        boolean u = false;
        int j = 0;
        boolean allVowels = false;
        //calcul
        while (!allVowels && j < cadena.length())  {
            if (cadena.charAt(j) == 'a' || cadena.charAt(j) == 'A') {
                a = true;
            }
            if (cadena.charAt(j) == 'e' || cadena.charAt(j) == 'E') {
                e = true;
            }
            if (cadena.charAt(j) == 'i' || cadena.charAt(j) == 'I') {
                i = true;
            }
            if (cadena.charAt(j) == 'o' || cadena.charAt(j) == 'O') {
                o = true;
            }
            if (cadena.charAt(j) == 'u' || cadena.charAt(j) == 'U') {
                u = true;
            }
            if (a && e && i && o && u) {
                allVowels = true;
            }
            j++;
        }
        //retorn
        return allVowels;
        
        
    }
}

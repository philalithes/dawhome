/*
 * Program.java        2015/7/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine digit from digit position
     * 
     * @param a int number
     * @param b digit position
     * 
     * @return digit from digit position
     */
    public int digitPosition(int a, int b){
        //variable declaration    
        int digit;
        //calculate return
        digit = (int)(a / Math.pow(10,b -1) % 10);
        //return
        return digit;
    }
    
}

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats dos nombre enters 
     * n1 i n2 amb n1 < n2 , calcula quants parells hi ha dins l’interval [n1 , n2].
     * 
     */
    
    public int numberOfEvenNumbers(int n1, int n2) {
        //variable declaration
        int even = 0;
        //calcul
        for (int n = n1; n <= n2; n++) {
            if (n % 2 == 0) {
                even++;
            }
        }
        return even;
    }
}
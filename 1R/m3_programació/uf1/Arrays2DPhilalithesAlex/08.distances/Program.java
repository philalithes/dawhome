/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     *  Suposem la següent taula amb distàncies
     * quilomètriques:
     *           Barcelona Girona Lleida Tarragona Saragossa Terol
     * Barcelona           100    156    98        296       409
     * Girona                     256    198       396       509
     * Lleida                            91        140       319
     * Tarragona                                   231       311
     * Saragossa                                             181
     * Terol
     * Donats els noms de dos poblacions, calcula la seva distància.
     * 
     */
    
    public int distance(String poblacio1, String poblacio2) {
        //variables
        int i = 0;
        int j = 0;
        int[][] distancies = {{0,100,156,98,296,409},{100,0,256,198,396,509},{156,256,0,91,140,319},{98,198,91,0,231,311},{296,396,140,231,0,181},{409,509,319,311,181,0}};
        if (poblacio1.equals("Barcelona")) {
            i = 0;
        }
        else if (poblacio1.equals("Girona")) {
            i = 1;
        }
        else if (poblacio1.equals("Lleida")) {
            i = 2;
        }
        else if (poblacio1.equals("Tarragona")) {
            i = 3;
        }
        else if (poblacio1.equals("Saragossa")) {
            i = 4;
        }
        else if (poblacio1.equals("Terol")) {
            i = 5;
        }
        if (poblacio2.equals("Barcelona")) {
            j = 0;
        }
        else if (poblacio2.equals("Girona")) {
            j = 1;
        }
        else if (poblacio2.equals("Lleida")) {
            j = 2;
        }
        else if (poblacio2.equals("Tarragona")) {
            j = 3;
        }
        else if (poblacio2.equals("Saragossa")) {
            j = 4;
        }
        else if (poblacio2.equals("Terol")) {
            j = 5;
        }
        //calcul
        return distancies[i][j];
    }
}


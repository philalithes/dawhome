/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de nombres no negatius
     * acabada en zero i escriu quants nombres parells hi ha.
     * El 0 compta com a nombre parell.
     */
    
    public static void main(String[] args) {
        //variable declaration
        int n = 0;
        int pars = 0;
        //interacció amb l'usuari
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una sequència de nombres acabada en 0 i et diré quants d'aquests són parells.");
        //càlculs
        do {
            System.out.println("Escriu un nombre: ");
            n = s.nextInt();
            if (n % 2 == 0) {
                pars++;
            }
        }
        while (n != 0);
        System.out.printf("Has introduït %d nombres parells.",pars);
        
       
    }
}


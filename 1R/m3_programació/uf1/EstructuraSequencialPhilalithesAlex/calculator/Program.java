/*
 * Program.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * add 2 numbers
     * 
     * @param a = number 
     * @param b = number
     * 
     * @return the sum of the numbers
     */
    public double add(double a, double b){
        //return    
        return a + b;
    }
    
    /**
     * substract 2 numbers
     * 
     * @param a = number 
     * @param b = number
     * 
     * @return the substract of a number
     */
    public double substract(double a, double b){
        //return    
        return a - b;
    }
    
    /**
     * multiply 2 numbers
     * 
     * @param a = number 
     * @param b = number
     * 
     * @return the product of the numbers
     */
    public double multiply(double a, double b){
        //return    
        return a * b;
    }
    
    /**
     * divide 2 numbers
     * 
     * @param a = number 
     * @param b = number
     * 
     * @return the division of the numbers
     */
    public double divide(double a, double b){
        //return    
        return a / b;
    }
    
}

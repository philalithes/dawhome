/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if a number is in open interval between a and b
     * 
     * @param a = start of interval
     * @param b = end of interval
     * @param c = number to determine if it is in the open interval
     * @return true = is in interval, false = is not in interval
     */
    public boolean isInOpenInterval(int a, int b, int c){
        //variable declaration    
        boolean is;
        //calculate return
        is = a < c && b > c;
        //return
        return is;
    }
    
}

#!/bin/bash
# Fitxer: ex29_kp.sh
# Autor: jamoros
# Data: 24/03/2011
# Versio: 0.1
# Descripcio: Donats dos arguments els suma si el 1er argument és menor que el segon i els resta en cas contrari.



# Si no tinc dos arguments mostro missatge d'error, ajuda i surto amb un valor que indiqui que hi ha hagut un error
if [ $# -ne 2 ]
then
	echo "error: numero d'arguments diferent de 2"
	echo "ús: $0 argument1 argument2"
	echo "    on argument1 i argument2 són els arguments a restar o sumar"
	exit 1
fi



# En cas contrari, si el primer argument és menor que el segon els sumo

if [ $1 -lt $2 ]
then
	OPERADOR='+'
	RESULT=$(($1+$2))
# En cas constrari els resto
else
    OPERADOR='-'
    RESULT=$(($1-$2))
fi

# Mostro el resultat

echo "$1 $OPERADOR $2 =  $RESULT"

 


<<*
Una altra manera de fer la 2a part:

#Si el primer argument és menor que el segon els sumo
if [ $1 -lt $2 ]
then
	operacio='+'
# En cas constrari els resto
else
	operacio='-'
fi

# Mostro el resultat
echo "$1 $operacio $2 = $(( $1 $operacio $2 ))"


*









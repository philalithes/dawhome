package org.iedt.streamsdemo1;

public interface Operador {
	public abstract double opera(double n1, double n2);
}

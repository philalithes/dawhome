/*
 * Program.java        1.0 10/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Arrays;

public class Program {

    /**
     * Determines if two arrays have the same elements.
     * 
     * @param a an array
     * @param b an array
     * @return true if they have the same elements, false otherwise.
     */
    public boolean sameNumbers(int[] arrayA, int[] arrayB) {
        //variable declaration
        boolean sameNumbers = false;
        int i = 0;
        boolean diferents = false;
        //boolean diferents = false;
        
        //calcul
        if (arrayA.length == arrayB.length) {
            //ordenem els arrays
            Arrays.sort(arrayA);
            Arrays.sort(arrayB);
            while (!diferents && i < arrayA.length) {
                if (arrayA[i] != arrayB[i]) {
                    diferents = true;
                }
                i++;
            }
            if (!diferents) {
                sameNumbers = true;
            }
            
        }
        return sameNumbers;
    }

}

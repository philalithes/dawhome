package org.escoladeltreball.jpaprojectdemo2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAProjectDemo2");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		ClassRoom a1 = new ClassRoom("A1");
		ClassRoom a2 = new ClassRoom("A2");
		
		Record r1 = new Record(1.3);
		Record r2 = new Record(4.3);
		Record r3 = new Record(7.3);
		Record r4 = new Record(8.3);
		
		Teacher t1 = new Teacher("Julio");
		Teacher t2 = new Teacher("Monica");
		Teacher t3 = new Teacher("Rafa");
		Teacher t4 = new Teacher("Carlos");
		
		Student s1 = new Student("Joan", 19, r1, a1);
		Student s2 = new Student("Anna", 39, r2, a1);
		Student s3 = new Student("Pep", 29, r3, a2);
		Student s4 = new Student("Mey", 59, r4, a2);
		
		s1.addTeacher(t1);
		s1.addTeacher(t2);
		s1.addTeacher(t3);
		s1.addTeacher(t4);
		
		s2.addTeacher(t1);
		s2.addTeacher(t2);
		
		s3.addTeacher(t3);
		s3.addTeacher(t4);
		s3.addTeacher(t1);
		
		s4.addTeacher(t2);
		s4.addTeacher(t3);
		s4.addTeacher(t4);
		
		a1.add(s1);
		a1.add(s2);
		a2.add(s3);
		a2.add(s4);
		em.persist(s1);
		em.persist(s2);
		em.persist(s3);
		em.persist(s4);
		
		em.getTransaction().commit();
		
		
//		ClassRoom rs = em.find(ClassRoom.class, 1l);
//		rs.getStudents().forEach(System.out::println);
	
		Query query = em.createNamedQuery("StudentGrade");
		List<Object[]> list = query.getResultList();
		for (Object[] o : list) {
			for (Object o2 : o) {
				System.out.print(o2 + " ");
			}
			System.out.println();
		}
		
//		
//		em.getTransaction().begin();
//		Student s = em.find(Student.class, 2l);
//		ClassRoom rs = em.find(ClassRoom.class, 1l);
//		s.setClassRoom(rs);
//		em.merge(s);
//		em.remove(s);
//		em.getTransaction().commit();
		
//		query = em.createNamedQuery("AllStudents");
//		query.getResultList().forEach(System.out::println);
//		rs = em.find(ClassRoom.class, 1l);
//		rs.getStudents().forEach(System.out::println);
		em.close();
		emf.close();
		System.out.println("ok");
	}

}

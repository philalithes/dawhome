/*
 * ProgramTUI.java        2015/7/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Determine velocity with known distance and time
     * 
     * @param d = distancia (metros)
     * @param t = temps (segons)
     *
     * 
     * @return velocity
     */
    public double velocity(double d, double t){
        //variable declaration    
        double velocity;
        //calculate return
        velocity = (d / 1000) / (t / 3600);
        //retorn
        return velocity;
        
    }
}

/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèric d'enters i un
     * nombre enter x, escriu els x primers elements de la taula i retorna la seva suma.
     * 
     * retorn = suma;
     */
    
    public double writeSquareCube(double[] array, int x) {
        //variable declaration
        double suma = 0;
        //calcul
        for (int i = 0; i < x; i++) {
            suma += array[i];
            System.out.println(array[i]);
        
        }
        return suma;
    }
}

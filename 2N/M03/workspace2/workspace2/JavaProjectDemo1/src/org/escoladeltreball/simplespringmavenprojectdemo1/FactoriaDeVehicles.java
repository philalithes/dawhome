package org.escoladeltreball.simplespringmavenprojectdemo1;

/**
 * @author iaw47951368
 *	Singleton
 */
public class FactoriaDeVehicles {
//	eager instantiation
	private static FactoriaDeVehicles instance = new FactoriaDeVehicles();
	
	private FactoriaDeVehicles() {
		
		
	}
	
	/**
	 * lazy instantiation
	 * @return
	 */
	public static FactoriaDeVehicles getInstance() {
//		if (instance == null) {
//			instance = new FactoriaDeVehicles();
//		}
		return instance;
	}
	/**
	 * Factory Method
	 * @param tipusConductor
	 * @return
	 */
	public Vehicle getVehicle(TipusConductor tipusConductor) {
		switch (tipusConductor) {
		case FITIPALDI:
			return new Esportiu("BMW", new Motor(360, 3460), 360);
		case CARROZA:
			return new Classic("Peugeot", new Motor(3, 24), 30);
		case PAREDEFAMILIA:
			return new Turisme("Renault", new Motor(260, 140), 5);
		}
		return null;
	}
}

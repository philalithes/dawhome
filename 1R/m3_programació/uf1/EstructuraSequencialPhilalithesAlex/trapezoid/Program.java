/*
 * ProgramTUI.java        2015/7/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculate area of a trapezoide
     * 
     * @param b = base 1
     * @param B = base 2
     * @param h = altura
     * 
     * @return area = area of the trapezoid
     */
    public double area(double b, double B, double h){
        //variable declaration    
        double area;
        //calculate return
        area = ((b + B) * h) / 2;
        //retorn
        return area;
        
    }
}

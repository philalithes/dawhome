/*
 * Program.java        2016/1/12
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donada una cadena, determina si hi ha més vocals
que altres caràcters.
     * 
     * 
     */
    
    public boolean moreVowels (String cadena) {
        //variable declaration
        int vocals = 0;
        int noVocals = 0;
        boolean moreVowels = false;
        //calcul
        for (int i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'A' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'E' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'I' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'O' || cadena.charAt(i) == 'u' || cadena.charAt(i) == 'U') {
                vocals++;
            } else {
                noVocals++;
            }
        }
        if (vocals > noVocals) {
            moreVowels = true;
        }
        //retorn
        return moreVowels;
            
        
        
    }
}

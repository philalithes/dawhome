var mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;


// instància de l'esquema
var userSchema = new Schema( {
	user: String,
	password: String
});



// registrem la col·lecció Users amb un esquema concret i exportem
module.exports = mongoose.model('Users', userSchema);
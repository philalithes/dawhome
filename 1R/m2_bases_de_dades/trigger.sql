create or replace function emp_stamp() returns trigger as $$
	begin
		-- check empname and salary are given.
		if new.empname is null then
			raise exception 'empname cannot be null';
		end if;
		if new.salary is null then
			raise exception '% cannot have null salary', new.empname;
		end if;
		if new.salary < 0 then
			raise exception '% no negative salary', new.salary;
		end if;
		new.last_date := localtimestamp;
		new.last_user := user;
		return new;
	end;
$$ language plpgsql;
			

create trigger emp_stamp_trigger 
before insert or update on emp
for each row 
execute procedure emp_stamp();

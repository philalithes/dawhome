package org.escoladeltreball.simplespringmavenprojectdemo1;

public class Esportiu extends Vehicle {
	private int velocitatPunta;

	public Esportiu() {
		// TODO Auto-generated constructor stub
	}

	public Esportiu(String marca, Motor motor, int velocitatPunta) {
		super(marca, motor);
		this.velocitatPunta = velocitatPunta;	
	}

	public int getVelocitatPunta() {
		return velocitatPunta;
	}

	public void setVelocitatPunta(int velocitatPunta) {
		this.velocitatPunta = velocitatPunta;
	}

	@Override
	public String toString() {
		return "Esportiu [velocitatPunta=" + velocitatPunta + "]";
	}

}


/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class ProgramTUI {
    /*
     * Cada alumne d'una classe té un codi incremental
     * començant pel número 1. Fer un programa que demani la nota dels alumnes d'una classe
     * per ordre de codi i posteriorment mostri els codis dels alumnes de la classe que han tret
     * insuficient, un aprovat, un bé, un notable i un excel.lent. Els intervals per calcular la nota
     * són:
     * a. [0,5) Insuficient
     * b. [5,6) Aprovat
     * c. [6,7) Bé
     * d. [7,9) Notable
     * e. [9,10] Excel.lent
     * S'ha de tenir en compte que una classe té 35 alumnes.
     */
    
    public static void main(String[] args) {
        //variable declaration
        Scanner sc = new Scanner(System.in);
        int i = 0;
        double[] grades = new double[35];
        //introduir notes
            for (i = 0; i < grades.length; i++) {
            System.out.printf("Introdueix la nota de l'alumne %d", i+1);
            grades[i] = sc.nextDouble();
        }
        for (i = 0; i < grades.length; i++) {
            if (grades[i] < 5) {
                System.out.printf("L'alumne %d ha tret un Insuficient\n", i+1);
            } else if (grades[i] < 6) {
                System.out.printf("L'alumne %d ha tret un Aprovat\n", i+1);
            } else if (grades[i] < 7) {
                System.out.printf("L'alumne %d ha tret un Bé\n", i+1);
            } else if (grades[i] < 9) {
                System.out.printf("L'alumne %d ha tret un Notable\n", i+1);
            } else {
                System.out.printf("L'alumne %d ha tret un Excel.lent\n", i+1);
            }
        }
        
    }
}

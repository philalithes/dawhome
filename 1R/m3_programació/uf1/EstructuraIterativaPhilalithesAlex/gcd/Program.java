/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    
    /*
     * Donats dos nombres enters positius, calcula el seu màxim comú
     * divisor. El MCD és el nombre enter positiu més gran que els divideix al dos.
     */
    
    public int gcd(int n1, int n2) {
        //variable declaration
        int gcd = 0;
        if (n1 > n2) {
            int i = n2;
            int maxD = 1;
            boolean found = false;
            //càlcul
            while (i >= 1 && !found) {
                if (n2 % i == 0) {
                    maxD = i;
                    if (n1 % maxD == 0)  {
                        found = true;
                        gcd = maxD;
                    }
                }
                i--;
            }
        }
        else {
            int i = n1;
            int maxD = 1;
            boolean found = false;
            //càlcul
            while (i >= 1 && !found) {
                if (n1 % i == 0) {
                    maxD = i;
                    if (n2 % maxD == 0)  {
                        found = true;
                        gcd = maxD;
                    }
                }
                i--;
            }
        }
        
        
        return gcd;
        
    }
}

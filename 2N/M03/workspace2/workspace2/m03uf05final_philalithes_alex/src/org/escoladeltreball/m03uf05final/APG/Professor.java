package org.escoladeltreball.m03uf05final.APG;

import java.time.LocalTime;

public class Professor {
	private long id;
	private String hDilluns;
	private String hDimarts;
	private String hDimecres;
	private String hDijous;
	private String hDivendres;

	public Professor(String registre) {
		String[] regSplit = registre.split(",");
		id = Long.parseLong(regSplit[0]);
		hDilluns = regSplit[1];
		hDimarts = regSplit[2];
		hDimecres = regSplit[3];
		hDijous = regSplit[4];
		hDivendres = regSplit[5];
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String gethDilluns() {
		return hDilluns;
	}

	public void sethDilluns(String hDilluns) {
		this.hDilluns = hDilluns;
	}

	public String gethDimarts() {
		return hDimarts;
	}

	public void sethDimarts(String hDimarts) {
		this.hDimarts = hDimarts;
	}

	public String gethDimecres() {
		return hDimecres;
	}

	public void sethDimecres(String hDimecres) {
		this.hDimecres = hDimecres;
	}

	public String gethDijous() {
		return hDijous;
	}

	public void sethDijous(String hDijous) {
		this.hDijous = hDijous;
	}

	public String gethDivendres() {
		return hDivendres;
	}

	public void sethDivendres(String hDivendres) {
		this.hDivendres = hDivendres;
	}

	@Override
	public String toString() {
		return "Professor [id=" + id + ", hDilluns=" + hDilluns + ", hDimarts=" + hDimarts + ", hDimecres=" + hDimecres
				+ ", hDijous=" + hDijous + ", hDivendres=" + hDivendres + "]";
	}

	public Professor(long id, String hDilluns, String hDimarts, String hDimecres, String hDijous, String hDivendres) {
		super();
		this.id = id;
		this.hDilluns = hDilluns;
		this.hDimarts = hDimarts;
		this.hDimecres = hDimecres;
		this.hDijous = hDijous;
		this.hDivendres = hDivendres;
	}

	/*
	 * Agafar horaris en forma de LocalTime
	 */
	public LocalTime getHDillunsIn() {
		String hIn = hDilluns.split("-")[0];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDillunsOut() {
		String hIn = hDilluns.split("-")[1];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDimartsIn() {
		String hIn = hDimarts.split("-")[0];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDimartsOut() {
		String hIn = hDimarts.split("-")[1];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDimecresIn() {
		String hIn = hDimecres.split("-")[0];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDimecresOut() {
		String hIn = hDimecres.split("-")[1];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDijousIn() {
		String hIn = hDijous.split("-")[0];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDijousOut() {
		String hIn = hDijous.split("-")[1];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Professor other = (Professor) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public LocalTime getHDivendresIn() {
		String hIn = hDivendres.split("-")[0];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

	public LocalTime getHDivendresOut() {
		String hIn = hDivendres.split("-")[1];
		String hourIn = hIn.substring(0, 2);
		String minuteIn = hIn.substring(2, 4);
		int hour = Integer.parseInt(hourIn);
		int minute = Integer.parseInt(minuteIn);
		return LocalTime.of(hour, minute);
	}

}

/*
 * ProgramTUI.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculate area and volume of a cylinder
     * 
     * @param r radius
     * @param h height
     * 
     *
     * @return al = lateral area of the cylinder
     * @return v = volume of the cylinder
     */
    public double area(double h, double r) {
        //variable declaration    
        double al;
        //calculate return
        al = Math.PI * Math.pow(r,2) * h;
        //retorn
        return al;
    }
    public double volume(double h, double r) {
        //variable declaration    
        double v;
        //calculate return
        v = 2 * Math.PI * r * h;
        //retorn
        return v;
    }
}

/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Grup.
 * 
 * @author iaw47951368
 */
public class Grup {
	/**
	 * Description of the property maxEstudiants.
	 */
	public int maxEstudiants = 0;
	
	/**
	 * Description of the property assignaturas.
	 */
	public HashSet<Assignatura> assignaturas = new HashSet<Assignatura>();
	
	/**
	 * Description of the property codi.
	 */
	public String codi = "";
	
	// Start of user code (user defined attributes for Grup)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Grup() {
		// Start of user code constructor for Grup)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Grup)
	
	// End of user code
	/**
	 * Returns maxEstudiants.
	 * @return maxEstudiants 
	 */
	public int getMaxEstudiants() {
		return this.maxEstudiants;
	}
	
	/**
	 * Sets a value to attribute maxEstudiants. 
	 * @param newMaxEstudiants 
	 */
	public void setMaxEstudiants(int newMaxEstudiants) {
	    this.maxEstudiants = newMaxEstudiants;
	}

	/**
	 * Returns assignaturas.
	 * @return assignaturas 
	 */
	public HashSet<Assignatura> getAssignaturas() {
		return this.assignaturas;
	}

	/**
	 * Returns codi.
	 * @return codi 
	 */
	public String getCodi() {
		return this.codi;
	}
	
	/**
	 * Sets a value to attribute codi. 
	 * @param newCodi 
	 */
	public void setCodi(String newCodi) {
	    this.codi = newCodi;
	}



}

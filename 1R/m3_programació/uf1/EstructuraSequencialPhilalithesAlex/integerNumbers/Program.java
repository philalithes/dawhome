/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if a number is integer
     * 
     * @param a = number
     * 
     * 
     * @return true = is integer, false = is not integer
     */
    public boolean isInteger(double a){
        //variable declaration    
        boolean is;
        //calculate return
        is = a == (int)a;
        //return
        return is;
    }
    
}

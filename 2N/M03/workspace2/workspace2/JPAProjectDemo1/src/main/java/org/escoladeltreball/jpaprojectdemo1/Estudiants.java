package org.escoladeltreball.jpaprojectdemo1;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Estudiants
 *
 */
@Entity
@Table(name = "estudiants")

/* Java Persistence Query Language (JPQL) */
@NamedQueries({ @NamedQuery(name = "select students", query = "select s from Estudiants s"),
		@NamedQuery(name = "select students age > 20", query = "select s from Estudiants s where s.age > 20"),
		@NamedQuery(name = "select students age > param1", query = "select s from Estudiants s where s.age > ?1"),
		@NamedQuery(name = "select students age > :edat", query = "select s from Estudiants s where s.age > :edat"),
		@NamedQuery(name = "select students nom like :nom", query = "select s from Estudiants s where s.name LIKE :nom"),
		@NamedQuery(name = "select students avgAge aula", query = "select s.aula, avg(s.age), max(s.age), min(s.age) from Estudiants s group by s.aula having avg(s.age) > 30 order by avg(s.age) desc"), })
public class Estudiants implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(length = 32, nullable = false)
	private String name;
	// @Transient
	// private String passwd;
	private Integer age;
	private String aula;

	public String getAula() {
		return aula;
	}

	public void setAula(String aula) {
		this.aula = aula;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estudiants other = (Estudiants) obj;
		if (id != other.id)
			return false;
		return true;
	}

	private static final long serialVersionUID = 1L;

	public Estudiants() {
		super();
	}

	@Override
	public String toString() {
		return "Estudiants [id=" + id + ", name=" + name + ", age=" + age + ", aula=" + aula + "]";
	}

	public Estudiants(long id, String name, Integer age, String aula) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.aula = aula;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

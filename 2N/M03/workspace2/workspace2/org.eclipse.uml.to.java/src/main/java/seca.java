/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of seca.
 * 
 * @author iaw47951368
 */
public class seca extends Cultiu {
	/**
	 * Description of the property usComercial.
	 */
	public String usComercial = "";
	
	// Start of user code (user defined attributes for seca)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public seca() {
		// Start of user code constructor for seca)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for seca)
	
	// End of user code
	/**
	 * Returns usComercial.
	 * @return usComercial 
	 */
	public String getUsComercial() {
		return this.usComercial;
	}
	
	/**
	 * Sets a value to attribute usComercial. 
	 * @param newUsComercial 
	 */
	public void setUsComercial(String newUsComercial) {
	    this.usComercial = newUsComercial;
	}



}

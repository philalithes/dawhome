#!/bin/bash
# Filename:			install_postgresql.sh
# Author:			iaw47951368
# Date:				29/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./install_postgresql.sh [args...]
# Description:		Instala postgres.

echo -n "Root "
su -c "yum install postgresql-server -y; postgresql-setup initdb; systemctl start postgresql; systemctl enable postgresql; echo 'postgres' | passwd postgres --stdin"
echo -n "Postgres "
su postgres -c "psql template1 -c \"create user `whoami` createdb;\""
cp training.sql /tmp
psql template1 -c "create database training;";
psql training -f /tmp/training.sql;


#!/bin/bash
# Filename:		exercici39.sh
# Author:		phila
# Date:			08/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici39.sh [arg1...]
# Description:		Script que serveix per copiar, moure o esborrar arxius.

#control d'arguments
if [ "$1" != -c ] && [ "$1" != -m ] && [ "$1" != -d ]
then
	echo "el primer argument ha de ser -c (copiar), -m (moure) o -d (borrar)."
	exit 1
fi
#comprovem que el arxiu existeix
if [ -e "$2" ]
then
	#si l'argument 1 és -c
	if [ "$1" = -c ]
	then
		cp "$2" "$3"
	#si l'argument és -m
	elif [ "$1" = -m ]
	then
		mv "$2" "$3"
	#si l'argument és -d
	elif [ "$1" = -d ]
	then
		rm "$2"
	fi
fi

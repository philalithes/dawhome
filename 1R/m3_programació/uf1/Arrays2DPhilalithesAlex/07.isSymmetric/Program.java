/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula quadrada numèrica d'enters de
     * dimensió n, determina si és simètrica. Una taula quadrada és simètrica si el elements
     * simètrics respecte de la diagonal principal són iguals.
     * 
     */
    
    public boolean isSymmetric(int[][] matrix) {
        //variables
        int i, j = 0;
        boolean isSymmetric = false;
        boolean isNotSymmetric = false;
        //calcul
        for (i = 0; i < matrix.length && !isNotSymmetric; i++) {
            for (j = 0; j < matrix[0].length && !isNotSymmetric; j++) {
                if (matrix[i][j] != matrix[j][i]) {
                    isNotSymmetric = true;
                }
            }
            
        }
        if (isNotSymmetric == false) {
            isSymmetric = true;
        }
        return isSymmetric;
    }
}


/*
 * Program.java        2015/2/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombres enters n1 i n2 amb n1 < n2 i un nombre enter x,
     * escriu x nombres enters aleatoris dins l’interval [n1 , n2].
     * NOTA: Math.random() retorna un valor aleatori major o igual a 0 i menor a 1.
     * 
     */
    
    public void writeRandomNumbers(int n1, int n2, int x) {
        //variable declaration
        int numbers = 0;
        int random = 0;
        //algoritme
        while (numbers < x) {
            random = (int)(Math.random() * n2);
            if (random >= n1 && random <= n2) {
                System.out.println(random);
                numbers++;
                
            }
        }
        
    }
    }

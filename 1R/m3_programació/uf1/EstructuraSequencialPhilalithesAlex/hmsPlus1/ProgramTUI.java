/*
 * Program.java 2015/21/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * A partir de tres enters que representen hores,
  * minuts i segons, suma un segon i doni el resultat en el format “h:m:s”.
  * 
  * @param s = segons
  * @param h = hores
  * @param m = minuts
  *
  * 
  * @return h:m:s+1
  */


    
    public String hmsPlus1(int h, int m, int s) {
        int segons = h * 3600 + m * 60 + s;
        segons++;
        h = segons / 3600;
        int rest = segons % 3600;
        m = rest / 60;
        s = m % 60;
        
        
        String hms = h + ":" + m + ":" + s;
        return hms;
        
    }
    public static void main(String args[]){
        String hms;
        //declaracio de variables
        int h;
        int m;
        int s;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("introdueix els segons");
        s = sc.nextInt();
        System.out.println("introdueix els minuts");
        m = sc.nextInt();
        System.out.println("introdueix les hores");
        h = sc.nextInt();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        hms = p.hmsPlus1(h, m, s);
        //retorn
        System.out.printf("%s", hms);
    }
}

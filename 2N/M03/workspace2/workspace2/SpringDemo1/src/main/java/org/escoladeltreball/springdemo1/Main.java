package org.escoladeltreball.springdemo1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("springconfig.xml");
		Vehicle seat = context.getBean("opel", Vehicle.class);
		System.out.println(seat);

	}

}

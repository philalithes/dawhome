
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula amb paraules, ordena l'array i
     * escriu l'array ordenat.
     */
    
    public char[] sortWords(char[] array) {
        //variable declaration
        double aux;
        int i = 0;
        int k = 0;
        //calcul
        for (k = 1; k < array.length; k++) {
            for (i = 0; i < array.length-k; i++) {
                
                if (array[i] > array[i+1]) {
                    aux = array[i];
                    array[i] = array[i+1];
                    array[i+1] = aux;
                }
            }
        }
        return array;
        
    }
    
    public char[] sortWords(char[] array) {
        //variable declaration
        
        //calcul
       
        
    }
    
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import static java.lang.System.out;

public class Main {

	public static int totalCervezas (Bar bar) {
		int totalCervezas = 0;
		
		for (Cerveza c: bar.getCervezas()) {
			totalCervezas++;
		}
		return totalCervezas;
	}
	
	public static List<Cerveza> leer(String filein) {
		List<Cerveza> lista = new ArrayList<Cerveza>();

		try (BufferedReader reader = new BufferedReader(new FileReader(filein))) {
			String line = null;
			String[] parts;
			Random rnd = new Random();
			while ((line = reader.readLine()) != null) {
				parts = line.split(",");
				Cerveza beer = new Cerveza();
				beer.setNombre(parts[0]);
				beer.setMarca(parts[1]);
				beer.setLikes(Integer.parseInt(parts[2]));
				beer.setDislikes(Integer.parseInt(parts[3]));
				lista.add(beer);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lista;
	}
	
	public static List<Cerveza> popus(List<Cerveza> cervezas) {
		List<Cerveza> lista = new ArrayList<Cerveza>();
		for(Cerveza c : cervezas) {
			if(c.getLikes() > 6000) {
				lista.add(c);
			}
		}
		return lista;
	}
	
	public static void main(String[] args) {

		Bar manolo = new Bar();
		Cerveza damm = new Cerveza();
		
		manolo.setCervezas(leer("cervezas"));
		totalCervezas(manolo);
		List<Cerveza> popust = popus(manolo.getCervezas());
		popust.stream().forEach(out::println);
	}
	
}

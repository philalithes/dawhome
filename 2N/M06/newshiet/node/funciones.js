function f1() {
	console.log("f1")
}
function f2() {
	console.log("f2")
}
function f3() {
	console.log("f3")
}

module.exports = {
	f1: f1,
	f2: f2,
	f3: f3

}
module.exports.PI = Math.PI;
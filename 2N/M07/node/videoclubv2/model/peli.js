/*
 # Filename:	peli.js
 # Author:		Gio Mikee Plata Justiniano iaw1128309
 # Date:		02/03/2017
 # License:		This is free software, licensed under the GNU General Public License v3.
 # 				See http://www.gnu.org/licenses/gpl.html for more information.
 */
var mongoose=require('mongoose');

//Schema object
var Schema = mongoose.Schema;

//Schema instance
var peliSchema = new Schema({
    Codpeli: Number,
    Titol: String,
    Sinopsi: String,
    Dvds: [{type: Number}],
    DvdsPrestats: Number,
    Prestecs: [{
        DataPres: Date,
        Soci: String,
        Coddvd: Number,
        DataDev: Date
    }]
});

// Register Pelicules collection with a schema and export (FINDS IN LOWERCASE)
module.exports = mongoose.model('Pelicules', peliSchema);
/*
 * Program.java 2015/21/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * Llegeix la distància entre dos aeroports en 
  * km i la velocitat mitjana de l’avió en km/h, i escriu el temps estimat de vol en format h:m.
  * 
  * @param d = distància en km
  * @param v = velocitat en km/h
  * 
  *
  * 
  * @return t = temps en h:m
  */


    
    public String flightTime(double d, double v) {
        double tempsh = d / v;
        int h = (int)tempsh;
        double m = (tempsh - h) * 60;
        String hm = h + ":" + (int)m;
        return hm;
        
    }
    public static void main(String args[]){
        //declaracio de variables
        double d;
        double v;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("escriu la distància entre aeroports en km");
        d = sc.nextDouble();
        System.out.println("escriu la velocitat de l'avió en km/h");
        v = sc.nextDouble();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        String t = p.flightTime(d, v);
        //retorn
        System.out.printf("temps estimat: %s",t);
    }
}

/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if 2 numbers are equal
     * 
     * @param a = number1
     * @param b = number2
     * 
     * 
     * 
     * @return true = are equal, false = are not equal
     */
    public boolean areEquals(double a, double b){
        //variable declaration    
        boolean is;
        //calculate return
        is = a == b;
        //return
        return is;
    }
    
}

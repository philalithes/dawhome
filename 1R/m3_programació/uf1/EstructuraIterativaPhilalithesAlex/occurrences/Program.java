/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Determina el nombre de xifres d'un nombre
     * 
     * return xifres = nombre de xifres
     */
    
    public static int numberOfDigits(int n) {
        //variable declaration
        int xifres = 1;
        //calcul
        while (n > 9 || n < -9) {
            n = n / 10;
            xifres++;
        }
        return xifres;
    }
    /*
     * 
     * Donat un nombre i una posició retorna el digit d'aquella posició del nombre.
     * 
     */
    
    public int digitPosition(int n, int d){
        //variable declaration    
        int digit;
        //calculate return
        digit = (int)(n / Math.pow(10,d -1) % 10);
        //return
        return digit;
    }
    /*
     * Donat un nombre enter positiu i un dígit, calcula 
     * quantes vegades el dígit apareix dins del nombre.
     * 
     * 
     * 
     * 
     */
    public int occurrences(int n, int d) {
        //variable declaration
        int xifres = numberOfDigits(n);
        int pos = 1;
        int digitn = 0;
        int occurrences = 0;
        //calcul
        while (pos <= xifres) {
            digitn = digitPosition(n,pos);
            if (digitn == d) {
                occurrences++;
            }
            pos++;
        }
        return occurrences;
    }
}

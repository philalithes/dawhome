package org.escoladeltreball.beansiaspects;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) { 
		AnnotationConfigApplicationContext ctx = 
				new AnnotationConfigApplicationContext();
		ctx.getEnvironment().setActiveProfiles("production");
		ctx.register(SpringConfig.class);
		ctx.refresh();
		Mobil mobil = ctx.getBean("Samsung", Mobil.class);
		mobil.turnOn();
		mobil.AAcall("Juan");
		
		System.out.println(mobil);
		System.out.println("ok");
		
//		Motor motor = new Motor(100);
//		ProxyEngine proxy = new ProxyEngine(motor);
//		proxy.start();
	}

}

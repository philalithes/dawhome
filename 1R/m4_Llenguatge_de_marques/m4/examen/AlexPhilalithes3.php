<!doctype html>
<html>
<body>
<?php

$doc = new DOMDocument('1.0', 'utf-8');
$doc->formatOutput = true;
$doc->preserveWhiteSpace = false;
$doc->load('notes-departament.xml');

$nom = $_POST['nom'];
$cognom = $_POST['cognom'];
$cognom2 = $_POST['cognom2'];
$m4 = $_POST['m4'];
$m3 = $_POST['m3'];
$m2 = $_POST['m2'];

$noms=$doc->getElementsByTagName("nom");

    //afegir alumne
    $arrel = $doc->firstChild;
    $alumnes = $arrel->firstChild;
    
    $alumne = $doc->createElement('alumne');
    $alumne = $alumnes->appendChild($alumne);

    //nom
    $nouNom = $doc->createElement('nom');
    $nouNom = $alumne->appendChild($nouNom);

    $txt = utf8_encode($nom);
    $text = $doc->createTextNode($txt);
    $text = $nouNom->appendChild($text);
    //cognom
    $nouCognom = $doc->createElement('cognom');
    $nouCognom = $alumne->appendChild($nouCognom);

    $txt = utf8_encode($cognom);
    $text = $doc->createTextNode($txt);
    $text = $nouCognom->appendChild($text);
    //cognom2
    $nouCognom2 = $doc->createElement('cognom2');
    $nouCognom2 = $alumne->appendChild($nouCognom2);

    $txt = utf8_encode($cognom2);
    $text = $doc->createTextNode($txt);
    $text = $nouCognom2->appendChild($text);
    //m4
    $credits = $doc->createElement('credits');
    $credits = $alumne->appendChild($credits);

    $credit1 = $doc->createElement('credit');
    $credit1 = $credits->appendChild($credit1);

    $nomcredit = $doc->createElement('nom');
    $nomcredit = $credit1->appendChild($nomcredit);

    $txt = utf8_encode('Llenguatge de marques');
    $text = $doc->createTextNode($txt);
    $text = $nomcredit->appendChild($text);

    $nota = $doc->createElement('nota');
    $nota = $credit1->appendChild($nota);

    $txt = utf8_encode($m4);
    $text = $doc->createTextNode($txt);
    $text = $nota->appendChild($text);

    //m3

    $credit2 = $doc->createElement('credit');
    $credit2 = $credits->appendChild($credit2);

    $nomcredit = $doc->createElement('nom');
    $nomcredit = $credit2->appendChild($nomcredit);

    $txt = utf8_encode('Programació');
    $text = $doc->createTextNode($txt);
    $text = $nomcredit->appendChild($text);

    $nota = $doc->createElement('nota');
    $nota = $credit2->appendChild($nota);

    $txt = utf8_encode($m3);
    $text = $doc->createTextNode($txt);
    $text = $nota->appendChild($text);
    //m2

    $credit3 = $doc->createElement('credit');
    $credit3 = $credits->appendChild($credit3);

    $nomcredit = $doc->createElement('nom');
    $nomcredit = $credit3->appendChild($nomcredit);

    $txt = utf8_encode('Base de dades');
    $text = $doc->createTextNode($txt);
    $text = $nomcredit->appendChild($text);

    $nota = $doc->createElement('nota');
    $nota = $credit3->appendChild($nota);

    $txt = utf8_encode($m2);
    $text = $doc->createTextNode($txt);
    $text = $nota->appendChild($text);

$doc->save('notes-departament.xml');

$doc->saveXML();
header("Location: AlexPhilalithes1.html");
?>
</body>
</html>
/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     *  Donada una cadena, crea una segona cadena amb els
     * caràcters que estaven en minúscula passats a majúscula. Fes dues versions del programa:
     * una usant el mètode toUpperCase() i una altra versió sense usar aquest mètode. 
     */
    public String toUpper(String cadena) {
        //vars
        String cadenaUpper = "";
        //calc
        for (int i = 0; i < cadena.length(); i++) {
            cadenaUpper += (char) ((int) cadena.charAt(i) - 32);
        }
        //ret
        return cadenaUpper;
    }
    
}
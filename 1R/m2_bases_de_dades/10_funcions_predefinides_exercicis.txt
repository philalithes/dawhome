-- --------------------------------------------
--    Funcions predefinides
-- --------------------------------------------

-- 10.1- Mostreu la longitud de la cadena "hola que tal"

training=> SELECT char_length('hola que tal');
 char_length 
-------------
          12
(1 row)


-- 10.2- Mostreu la longitud dels valors del camp "id_producto".

SELECT char_length(id_producto) from productos;

-- 10.3- Mostrar la longitud dels valors del camp "descripcion".

SELECT char_length(descripcion) from productos;


-- 10.4- Mostreu els noms dels venedors en majúscules.

SELECT upper(nombre) from repventas;
     upper     
---------------
 BILL ADAMS
 MARY JONES
 SUE SMITH
 SAM CLARK
 BOB SMITH
 DAN ROBERTS
 TOM SNYDER
 LARRY FITCH
 PAUL CRUZ
 NANCY ANGELLI
(10 rows)


-- 10.5- Mostreu els noms dels venedors en minúscules.

SELECT lower(nombre) from repventas;
     lower     
---------------
 bill adams
 mary jones
 sue smith
 sam clark
 bob smith
 dan roberts
 tom snyder
 larry fitch
 paul cruz
 nancy angelli
(10 rows)

-- 10.6- Trobeu on és la posició de l'espai en blanc de la cadena 'potser 7'.

training=> select position(' ' in 'potser 7');
 position 
----------
        7
(1 row)

-- 10.7- Volem mostrar el nom, només el nom dels venedors sense el cognom, en majúscules.

training=> select substring(nombre from '#"% #"%' for '#') from repventas;
 substring 
-----------
 Bill 
 Mary 
 Sue 
 Sam 
 Bob 
 Dan 
 Tom 
 Larry 
 Paul 
 Nancy 
(10 rows)


-- 10.8- Crear una vista que mostri l'identificador dels representants de vendes i en columnes separades el nom i el cognom.

training=> create view venedors as select num_empl, substring(nombre from '#"% #"%' for '#') AS nombre, substring(nombre from '% #"%#"' for '#') AS apellido from repventas;
CREATE VIEW
training=> select * from venedors;
 num_empl | nombre | apellido 
----------+--------+----------
      105 | Bill   | Adams
      109 | Mary   | Jones
      102 | Sue    | Smith
      106 | Sam    | Clark
      104 | Bob    | Smith
      101 | Dan    | Roberts
      110 | Tom    | Snyder
      108 | Larry  | Fitch
      103 | Paul   | Cruz
      107 | Nancy  | Angelli
(10 rows)

-- 10.9- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'B. Adams'.
 
training=> select replace(nombre, substring(nombre from '#"% #"%' for '#'), substring(nombre from '#"_#"%' for '#')|| '. ') from repventas;
  replace   
------------
 B. Adams
 M. Jones
 S. Smith
 S. Clark
 B. Smith
 D. Roberts
 T. Snyder
 L. Fitch
 P. Cruz
 N. Angelli
(10 rows)

-- 10.10- Mostreu els valors del camp nombre de manera que 'Bill Adams' sorti com 'Adams, Bill'.

SELECT format(' %1$s, %2$s', split_part(nombre, ' ', 2),split_part(nombre, ' ', 1)) FROM repventas;

-- 10.11- Volem mostrar el camp descripcion de la taula productos però que en comptes de sortir espais en blanc, volem subratllats ('_').

training=> SELECT replace(descripcion, ' ', '_')FROM productos;

-- 10.12- Volem treure per pantalla una columna, que conté el nom i les vendes, amb els següent estil:
--   vendes dels empleats
-- ---------------------------
--  Bill Adams..... 367911,00
--  Mary Jones..... 392725,00
--  Sue Smith...... 474050,00
--  Sam Clark...... 299912,00
--  Bob Smith...... 142594,00
--  Dan Roberts.... 305673,00
--  Tom Snyder.....  75985,00
--  Larry Fitch.... 361865,00
--  Paul Cruz...... 286775,00
--  Nancy Angelli.. 186042,00
-- (10 rows)

training=> select translate(format('%-15s', nombre), ' ', '.') || ' ' || format('%9s', ventas) AS "vendes dels empleats" from repventas;
   vendes dels empleats    
---------------------------
 Bill.Adams..... 367911.00
 Mary.Jones..... 392725.00
 Sue.Smith...... 474050.00
 Sam.Clark...... 299912.00
 Bob.Smith...... 142594.00
 Dan.Roberts.... 305673.00
 Tom.Snyder.....  75985.00
 Larry.Fitch.... 361865.00
 Paul.Cruz...... 286775.00
 Nancy.Angelli.. 186042.00
(10 rows)


-- 10.13- Treieu per pantalla el temps total que fa que estan contractats els treballadors, ordenat pels cognoms dels treballadors amb un estil semblant al següent:
--       nombre  |     tiempo_trabajando
--    -----------+-------------------------
--    Mary Jones | 13 years 4 months 6 days

 training=> select nombre, age(contrato) as "temps treballant" from repventas;
    nombre     |     temps treballant     
---------------+--------------------------
 Bill Adams    | 27 years 11 mons 9 days
 Mary Jones    | 26 years 3 mons 9 days
 Sue Smith     | 29 years 1 mon 11 days
 Sam Clark     | 27 years 7 mons 7 days
 Bob Smith     | 28 years 8 mons 2 days
 Dan Roberts   | 29 years 3 mons 1 day
 Tom Snyder    | 26 years 8 days
 Larry Fitch   | 26 years 3 mons 9 days
 Paul Cruz     | 28 years 10 mons 20 days
 Nancy Angelli | 27 years 2 mons 7 days


-- 10.14- Cal fer un llistat dels productes dels quals les existències són inferiors al total d'unitats venudes d'aquell producte els darrers 60 dies, a comptar des de la data actual. Cal mostrar els codis de fabricant i de producte, les existències, i les unitats totals venudes dels darrers 60 dies.

SELECT 
id_fab, 
id_producto, 
existencias, 
sum(cant) AS "ventas_recientes" 
FROM 
productos 
JOIN 
pedidos ON id_fab = fab AND id_producto = producto 
WHERE 
fecha_pedido > current_date - interval '60 days'
GROUP BY 
id_fab, 
id_producto 
HAVING 
sum(cant) > existencias;


Is dado

SELECT id_fab, id_producto, existencias, sum(cant) 
FROM productos, pedidos 
WHERE id_fab = fab 
        AND id_producto = producto 
        AND age(fecha_pedido) <= '60 days' 
GROUP BY id_fab, id_producto 
HAVING existencias < sum(cant);
Gio

SELECT id_fab, id_producto, existencias, SUM(cant) 
FROM productos 
JOIN pedidos ON id_fab = fab AND id_producto = producto 
WHERE fecha_pedido BETWEEN current_date - interval '60 days' AND current_date 
GROUP BY id_fab, id_producto 
HAVING existencias < SUM(cant);

-- 10.15- Com l'exercici anterior però en comptes de 60 dies ara es volen aquells productes venuts durant el mes actual o durant l'anterior.

Gio Mikee Plata

Malament: No és en la data actual - 2 mesos, sinó que es volen els productes venuts durant tot el mes actual i durant tot el mes anterior

SELECT id_fab, id_producto, existencias, sum(cant) 
FROM productos, pedidos 
WHERE id_fab = fab 
        AND id_producto = producto 
        AND age(fecha_pedido) <= '2 mons' 
GROUP BY id_fab, id_producto 
HAVING existencias < sum(cant);

versión 2:
SELECT id_fab, id_producto, fecha_pedido, existencias, sum(cant) 
FROM productos, pedidos 
WHERE id_fab = fab 
        AND id_producto = producto 
        AND fecha_pedido IN (date_trunc('month',current_date), date_trunc('month',current_date) + interval '1 month' - interval '1 day') 
GROUP BY id_fab, id_producto, fecha_pedido ;
Bé:

SELECT id_fab, id_producto, existencias, SUM(cant) 
FROM productos 
JOIN pedidos ON id_fab = fab AND id_producto = producto 
WHERE fecha_pedido 
BETWEEN date_trunc('month', current_date) - interval '1 month' 
AND date_trunc('month', current_date) + interval '1 month' - interval '1 day'  
GROUP BY id_fab, id_producto 
HAVING existencias < SUM(cant);

-- 10.16- Per fer un estudi previ de la campanya de Nadal es vol un llistat on, per cada any del qual hi hagi comandes a la base de dades, el nombre de clients diferents que hagin fet comandes en el mes de desembre d'aquell any. Cal mostrar l'any i el número de clients, ordenat ascendent per anys.

SELECT extract('year' from fecha_pedido), num_clie, empresa 
FROM pedidos 
JOIN clientes ON clie = num_clie
 WHERE extract('month' from fecha_pedido) = 12 
 ORDER BY date_part ASC;
 
 Meyling

SELECT extract(year from date_trunc('month', fecha_pedido)) as any, count(distinct clie) as numero_clientes 
FROM pedidos 
WHERE extract(month from fecha_pedido) = 12 
GROUP BY any
ORDER BY any;
gio

SELECT date_part('year', fecha_pedido) as any, count(DISTINCT clie) as numero_clientes 
FROM pedidos 
WHERE date_part('month', fecha_pedido) = 12 
GROUP BY any 
ORDER BY any;

-- 10.17- Llisteu codi(s) i descripció dels productes. La descripció ha d'aperèixer en majúscules. Ha d'estar ordenat per la longitud de les descripcions (les més curtes primer).

SELECT id_fab, id_producto, upper(descripcion) FROM productos ORDER BY char_length(descripcion);

-- 10.18- LListar el nom dels treballadors i la data del seu contracte mostrant-la amb el següent format:
-- Dia_de_la_setmana dia_mes_numeric, mes_text del any_4digits
-- per exemple:
-- Bill Adams    | Friday    12, February del 1988

select nombre, to_char(contrato, 'Day') || '   '  ||  date_part('day', contrato) || ',   '  || initcap(to_char(contrato, 'month')) || ' del ' || date_part('year', contrato) from repventas;

Alberto Micolau

SELECT nombre, to_char(contrato, 'Day DD, Month "del" yyyy') AS contrato 
FROM repventas;
Meyling

-- 10.19- Modificar els imports de les comandes que s'han realitzat durant l'estiu augmentant-lo un 20% i arrodonint a l'alça el resultat.

Joan Gregori

UPDATE pedidos 
SET importe = ceil(importe + (importe * 0.20)) 
FROM pedidos
WHERE fecha_pedido between '21-06-89' AND '20-09-89';

UPDATE pedidos
SET importe = ceil(importe + (importe * 0.20)) 
FROM pedidos
WHERE date_part('month', fecha_pedido) BETWEEN 6 AND 9;


UPDATE pedidos
SET importe = ceil(importe+(importe*0.20)) 
WHERE extract('month' from fecha_pedido) 
BETWEEN 6 and 9;

training=> update pedidos set importe = ceil(importe+(importe*0.20)) where date_part('month', fecha_pedido) BETWEEN 6 and 9

Meyling

UPDATE pedidos
SET importe = ceil(importe + importe * 0.2)
WHERE ((extract (month from fecha_pedido)>= 6 AND extract(day from fecha_pedido) >= 21) 
                AND (extract(month from fecha_pedido) <= 9 AND extract(day from fecha_pedido) <= 20)) 
        OR (extract(month from fecha_pedido) IN (7,8));
GIO

UPDATE pedidos 
SET importe = ceil(importe + importe * 0.2) 
WHERE fecha_pedido BETWEEN date(date_part('year', fecha_pedido) || '-06-21') 
                   AND date(date_part('year', fecha_pedido) || '-09-21');

-- 10.20- Mostar les dades de les oficines llistant en primera instància aquelles oficines que tenen una desviació entre vendes i objectius més gran.

SELECT * FROM oficinas 
ORDER BY objetivo - ventas DESC;

SELECT * FROM oficinas 
ORDER BY (AVG(objetivo) - objetivo) - (AVG(ventas) - ventas) DESC;

-- 10.21- Llistar les dades d'aquells representants de vendes que tenen un identificador senar i són directors d'algun representants de vendes.

SELECT * FROM repventas 
WHERE mod(num_empl, 2) <> 0 
AND num_empl IN (SELECT d.director FROM repventas d);

Meyling Garcia



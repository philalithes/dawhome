var fs = require("fs");
var fun = require("./funciones.js")

fun.f1();
fun.f2();
fun.f3();
console.log(fun.PI);
fs.readFile('file1.txt', function(err, data) {
	if(err) return console.error(err);
	console.log(data.toString());
})
var data = fs.readFileSync('file1.txt');
console.log(data.toString() + "hola");

fs.readFile('file2.txt', function(err, data) {
	if(err) return console.error(err);
	console.log(data.toString());
})
var data = fs.readFileSync('file2.txt');
console.log(data.toString() + "hola");

fs.readFile('file3.txt', function(err, data) {
	if(err) return console.error(err);
	console.log(data.toString());
})
var data = fs.readFileSync('file3.txt');
console.log(data.toString() + "hola");


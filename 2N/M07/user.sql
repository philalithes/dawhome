CREATE TABLE usuari (
id SERIAL PRIMARY KEY,
username character varying(32),
password_hash character varying(32),
first_name character varying(64),
last_name character varying(64));

INSERT INTO usuari(username, password_hash, first_name, last_name) 
  VALUES('ed', md5('berkhamsted'), 'Ed', 'Lecky-Thompson');
INSERT INTO usuari(username, password_hash, first_name, last_name) 
  VALUES('steve', md5('newyork'), 'Steve', 'Nowicki');
INSERT INTO usuari(username, password_hash, first_name, last_name) 
  VALUES('marie', md5('leicester'), 'Marie', 'Ellis');
INSERT INTO usuari(username, password_hash, first_name, last_name)
  VALUES('harriet', md5('cambridge'), 'Harriet', 'Frankland');

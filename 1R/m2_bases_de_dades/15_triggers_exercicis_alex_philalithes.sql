-- --------------------------------------------
--    triggers
-- --------------------------------------------


-- 1.- Es vol fer un control dels canvis produïts a la taula repventas. Per tal de fer-ho es crearà la taula cambios_repventas amb les següents columnes:
--  * usuario: usuari de postgres que ha fet el canvi a la taula
--  * fecha: data en la qual s'ha fet la modificaió
--  * num_empl_anterior: codi de treballador que hi havia abans del canvi
--  * num_empl_nuevo: codi de treballador que hi ha després del canvi
--  * nombre_anterior: nom del treballador que hi havia abans del canvi
--  * nombre_nuevo: nom del treballador que hi ha després del canvi
--
-- Crea un trigger que insereixi una fila en aquesta taula cada cop que s'insereixi, es modifiqui o s'esborri un treballador. Si s'insereix un treballador els camps num_empl_anterior i nombre_anterior seran NULL. Si s'esborra un treballador els camps num_empl_nuevo i nombre_nuevo seran NULL.

create table cambios_repventas (
	usuario varchar (20),
	fecha timestamp,
	num_empl_anterior smallint,
	num_empl_nuevo smallint,
	nombre_anterior varchar(15),
	nombre_nuevo varchar(15)
);

create or replace function repventas_fingerprint() returns trigger as $$
	declare
		old_num_empl smallint;
		new_num_empl smallint;
		old_nombre varchar(15);
		new_nombre varchar(15);
	begin
		if TG_OP = 'INSERT' then
			old_num_empl = null;
			old_nombre = null;
			new_num_empl = new.num_empl;
			new_nombre = new.nombre;
		elsif TG_OP = 'DELETE' then
			old_num_empl = old.num_empl;
			old_nombre = old.nombre;
			new_num_empl = null;
			new_nombre = null;
		else
			old_num_empl = old.num_empl;
			old_nombre = old.nombre;
			new_num_empl = new.num_empl;
			new_nombre = new.nombre;
		end if;
		insert into cambios_repventas values
		(user, localtimestamp, old_num_empl, new_num_empl, old_nombre, new_nombre);
		return null;
	end;
$$ language plpgsql;

create trigger repventas_control after insert or update or delete on repventas
	for each row execute procedure repventas_fingerprint();

-- 2.- Crea un trigger que faci que no es permeti que un treballador pugui ser director de més de 5 treballadors.
create or replace function director_control() returns trigger as $$
	declare
		dir smallint;
		i smallint := 0;
	begin
		for dir in select director from repventas loop
			if dir = new.num_empl then
				i := i + 1;
			end if;
			if i >= 5 then
				raise exception 'aquest director ja té 5 empleats';
			end if;
		end loop;
		return new;
	end;
$$ language plpgsql;

create trigger dir_control before update or insert on repventas
	for each row execute procedure director_control();
-- 3.- Crea un trigger que faci que no es permeti que un treballador pugui ser director de més d'una oficina.
create or replace function director_ofi_control() returns trigger as $$
	declare
		dire smallint;
		i smallint := 0;
	begin
		for dire in select dir from oficinas loop
			if dire = new.dir then
				i := i + 1;
			end if;
			if i >= 1 then
				raise exception 'aquest director ja té 1 oficina';
			end if;
		end loop;
		return new;
	end;
$$ language plpgsql;

create trigger dir_ofi_control before update or insert on oficinas
	for each row execute procedure director_ofi_control();

-- 4.- Crea un trigger que faci que no es permeti que es pugi el preu d'un producte més d'un 20% del preu actual.
create or replace function exCuatro() returns trigger as $$
declare
        nuevoPrecio numeric = new.precio;
        maximoPermitido numeric;
begin
        maximoPermitido := old.precio + old.precio * 0.2;
        if nuevoPrecio > maximoPermitido then
                raise exception 'ERR! Esta aplicacion no contempla procesos hiperinflacionarios';
        end if;
        return new;
end;
$$ language plpgsql;

create trigger triggerCuatro before update  on productos 
for each row execute procedure exCuatro();
-- 5.- Crea una taula de nom oficinas_baja amb la següent estructura:
--  * oficina smallint
--  * ciudad   character varying(15)
--  * region   character varying(10)
--  * dir      smallint
--  * objetivo numeric(9,2)
--  * ventas   numeric(9,2)
--  * usuario  character varying(15)
--  * fecha    date
--
-- Crea un trigger que insereixi una fila a la taula oficinas_baja cada cop que s'esborri una oficina. Les dades que s'inseriran són les corresponents a l'oficina que es dóna de baixa excepte les columnes usuario i fecha on es desaran l'usuari que dóna de baixa l'oficina i la data actual.

create or replace function funcio_oficines_baixa() returns trigger as $$
                begin
                        insert into oficinas_bajas values
                                (old.oficina,old.ciudad,old.region,old.dir,old.objetivo,old.ventas,current_user,current_timestamp);
                        return old;
                end;
        $$ language plpgsql;
        
create trigger trigger_oficina_baixa after delete on oficinas
for each row execute procedure funcio_oficines_baixa();
-- 6.- Crea un trigger que impedeixi que la quota total dels venedors d'una oficina (suma de les quotes de tots els seus venedors) sigui inferior a 200000.
create or replace function quota_oficina_limit() returns trigger as $$
	declare
		cuotas numeric;
		cuota_oficina numeric;
	begin
	for cuotas in select cuota from repventas where oficina = old.oficina loop
		cuota_oficina := cuota_oficina + cuotas;
	end loop;
	if cuota_oficina < 200000 then
		raise exception 'la cuota de la oficina no pot baixar de 200000';
	end if;
	return new;
$$ language plpgsql;

create trigger oficina_cuota before update or delete on repventas
execute procedure quota_oficina_limit();

-- 7.- Crea un trigger que no permeti inserir treballadors a l'oficina de Denver.

create or replace function deny_insert_denver() returns trigger as $$
	declare
		oficina_denver int; 
	begin
	select oficina into oficina_denver from oficinas where ciudad = 'Denver';
		if new.oficina_rep = oficina_denver then
			RAISE EXCEPTION 'no es poden inserir treballadors a l oficina de Denver';
		end if;
		return new;
	end;
$$ language plpgsql;

create trigger deny_insert_emp before insert or update on repventas
	for each row execute procedure deny_insert_denver();

-- 8.- L'objectiu de l'exercici és fer que no s'eliminin mai les comandes. Per fer això farem les següents passes:
--
-- a) Modifica la taula pedidos afegint una columna de nom baja de tipus boolean. Si la comanda s'ha de donat de baixa tindra TRUE, altrament FALSE. Fes que quan s'insereixi una comanda aquest camp estigui per defecte a FALSE.

ALTER TABLE pedidos
    ADD COLUMN baja boolean DEFAULT false;

-- b) Crea un trigger que faci que quan s'esborri una comanda no l'esborri, però posi el camp baja de la comanda a TRUE.
CREATE OR REPLACE FUNCTION delete_pedido_trigger() RETURNS TRIGGER AS $$
        BEGIN
            UPDATE pedidos
            SET baja = true
            WHERE num_pedido = OLD.num_pedido;
            RETURN NULL;
        END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER delete_pedido
    BEFORE DELETE ON pedidos
        FOR EACH ROW
        EXECUTE PROCEDURE delete_pedido_trigger();


-- c) No permetis que es modifiqui un registre que s'ha esborrat, a no ser que es tregui la marca d'esborrat.
CREATE OR REPLACE FUNCTION update_pedido_trigger() RETURNS TRIGGER AS $$
        BEGIN 
            IF OLD.baja = true THEN
                    RAISE EXCEPTION 'CANNOT MODIFY DELETED ORDER - (%)', OLD.num_pedido;
                END IF;
            RETURN NEW;
        END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_pedido
        BEFORE UPDATE ON pedidos
        FOR EACH ROW
        WHEN (OLD.baja = NEW.baja)
        EXECUTE PROCEDURE update_pedido_trigger();

-- 9.- Fer un trigger que actualitzi el camp existencias de la taula productos cada cop que s'insereixi o actualitzi una comanda. Es suposa que quan es ven una quantitat d'un producte, les seves existències baixen.


CREATE FUNCTION actualiza_existencias() RETURNS trigger AS $$
    DECLARE
        old_value int := CASE
                            WHEN TG_OP = 'INSERT' THEN 0
                            ELSE OLD.cant
                         END;
        cant_prod int := (SELECT
                              existencias
                          FROM productos
                          WHERE
                             id_fab = NEW.fab AND 
                             id_producto = NEW.producto);
        cant_incr int := NEW.cant - old_value;
    BEGIN
        IF (cant_prod - cant_incr < 0 AND cant_incr >= 0) THEN
                RAISE EXCEPTION 'no puedes vender cantidades que no tienes';
        END IF; 
        UPDATE 
            productos 
        SET
            existencias = cant_prod - cant_incr 
        WHERE
            id_fab = NEW.fab AND 
            id_producto = NEW.producto;
        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER actualiza_existencias_trigger 
    BEFORE UPDATE OR INSERT ON pedidos
    FOR EACH ROW 
        EXECUTE PROCEDURE actualiza_existencias();
-- 10.- Crea els triggers necessaris per tal de fer les actualitzacions que consideris adients a la vista clientes_vip (exercici 4 del llistat d'exercicis de vistes).

-- --------------------------------------------
--    plpgsql - estructura repetitiva
-- --------------------------------------------


-- 1.- Funció spread(text) que converteix el text d'entrada en un altre, passat a majúscules, i amb un espai en blanc afegit entre cada dos caràcters de la cadena original. Així "Hola, que tal?" donaria com a sortida "H O L A ,   Q U E   T A L ?".

create or replace function spread(t text) returns text as $$
	declare
		cur_letter text;
		i int := 1;
		ret text := '';
	begin
		loop
			cur_letter := substring(t, i, 1);
			ret := ret || upper(cur_letter) || ' ';
			i := i + 1;
			if i > length(t) then
				EXIT;
			end if;
		end loop;
		return ret;
	end;
$$ language plpgsql;

-- 2.- Crear una funció que donat l'identificador d'un representant de vendes calculi la seva comissió. La comissió de cada representant de vendes es calcula de la següent manera:
-- La comissió d'un representant de vendes es composa de la comissions que cada representant de vendes obté de cada un dels encàrrecs que ha atès.
-- Per cada encàrrec la comissió és un percentatge de l'import sense descompte de l'encàrrec.
-- L'import sense descompte de l'encàrrec és el resultat de multiplicar el preu del producte per la quantitat de producte de l'encàrrec.
-- Si l'import sense descompte de l'encàrrec és superior o igual a 1500 el percentatge de comissió és 15%.
-- Si l'import sense descompte de l'encàrrec és inferior a 1500 però superior o igual a 500 el percentatge de comissió és del 10%.
-- Si l'import sense descompte de l'encàrrec és inferior a 500 el percentatge de comissió és 5%.

create or replace function comisio(reparg repventas.num_empl%type) returns numeric as $$
	declare
		ret numeric := 0;
		percent int;
		i int = 1;
		import numeric;
	begin
		for import in select cant * precio from pedidos join productos on fab=id_fab and producto = id_producto where rep = reparg loop
			if import >= 1500 then
				percent := 15;
			elsif import < 1500 and import >= 500 then
				percent := 10;
			else
				percent := 5;
			end if;
			ret := ret + import * percent / 100;
			i := i + 1;
		end loop;
		return ret;
	end;
$$ language plpgsql;

-- 3.- Volem generar llistats de venedors ordenats pels màxims períodes entre comandes consecutives de cada venedor. Per aquest motiu necessitem una funció que donat un identificador de representant de vendes en retorni l'interval de temps més gran que hi ha hagut entre dos comandes consecutives d'aquest representant de vendes.

create or replace function intervalComanda(reparg smallint) returns interval as $$
	declare
		fechaAnterior date;
		inter interval;
		interMax interval;
		interAnterior interval := 0;
		ret interval;
		fecha date;
		i int := 1;
	begin
		for fecha in select fecha_pedido from pedidos where rep = reparg order by fecha_pedido loop
			if i = 1 then
				fechaAnterior = fecha;
				i := i+1;
			end if;
			inter := age(fecha, fechaAnterior);
			if inter > interAnterior then
				ret := inter;
			end if;
			fechaAnterior := fecha;
			interAnterior := inter;
		end loop;
		return ret;
	end;
$$ language plpgsql;

-- 4.- Crear una funció que donat un identificador de client mostri per pantalla la seva factura. La factura ha de contenir tots els productes que ha demanat el client amb la data en que els ha demanat. En cas de tenir descompte calcular el descompte aplicat a cada comanda. Calcular el l'IVA i el total. Crear les funcions necessàries per crear un codi modular. La factura ha de tenir el següent aspecte:

Client (id): Ace International (2107)
=====================================
Data: 10/5/2012

Descripció del producte (id)  Data de la comanda  Quantitat  Preu Unitari  Descompte         Import
---------------------------------------------------------------------------------------------------
Manivela (bic, 41003)                   1/4/2012         10         54.00                    540.00
Perno Riostra (imm,887p)               12/4/2012         10         10.00        10%          90.00
Montador (aci,4100z)                   25/5/2012          1       1000.00        50%         500.00
===================================================================================================
                                                                           Base:            1130.00
                                                                       IVA(18%):             203.40
                                                                          Total:            1333.40
===================================================================================================

CREATE OR REPLACE FUNCTION factura(cliente smallint) RETURNS text AS $$
        DECLARE
                factura text := 'Client (id): ' || (SELECT empresa || ' (' || num_clie || ')' FROM clientes WHERE num_clie = cliente);
                base real := 0;
                data date;
                prod text;
                quantitat int;
                price real;
                discount numeric;
                priceT real;
                nom_prod character(5);
                nom_fab character(3);
        BEGIN
                factura := factura || E'\n' || repeat('=', 37);
                factura := factura || E'\nData: ' || current_date;
                factura := factura || E'\n';
                factura := factura || E'\n' || format('%-30s%18s%11s%14s%11s%15s','Descripció del producte (id)','Data de la comanda','Quantitat','Preu Unitari','Descompte','Import');
                factura := factura || E'\n' || repeat('-', 100);
                FOR data IN SELECT fecha_pedido 
                                        FROM pedidos 
                                        WHERE clie = cliente 
                                LOOP
                                        prod := (SELECT descripcion || ' (' || id_fab || ', ' || id_producto || ')' FROM productos JOIN pedidos ON id_fab = fab AND id_producto = producto WHERE data = fecha_pedido LIMIT 1);
                                        nom_prod := (SELECT id_producto FROM productos JOIN pedidos ON id_fab = fab AND id_producto = producto WHERE data = fecha_pedido);
                                        nom_fab := (SELECT id_fab FROM productos JOIN pedidos ON id_fab = fab AND id_producto = producto WHERE data = fecha_pedido);
                                        quantitat := (SELECT cant FROM pedidos WHERE fecha_pedido = data AND clie = cliente);
                                        price := (SELECT precio FROM productos JOIN pedidos ON id_fab = fab AND id_producto = producto WHERE data = fecha_pedido);
                                        discount := descompte(cliente, nom_prod, nom_fab, quantitat);
                                        priceT := quantitat * price * (1 - discount);
                                        factura := factura || E'\n' || format('%-30s%18s%11s%14s%11s%15s', prod, data, quantitat, price, discount * 100 || '%', priceT);
                                        base := (SELECT base + priceT);
                END LOOP;
                factura := factura || E'\n' || repeat('=', 100);
                factura := factura || E'\n' || format('%81s%18s', 'Base: ', base);
                factura := factura || E'\n' || format('%81s%18s', 'IVA(18%): ', base * 0.18);
                factura := factura || E'\n' || format('%81s%18s', 'Total: ', base * 1.18);
                factura := factura || E'\n' || repeat('=', 100);
                RETURN factura;
        END;
$$ LANGUAGE plpgsql;


-- 5.- Crear una funció que donat un client, un producte i una quantitat retorni el descompte que se li podria aplicar a la comanda.
-- En general els clients gaudeixen d'un descompte del 10% si el preu de la comanda és superior a 1000.
-- També gaudeixen d'un descompte del 20% en cas que el preu de la comanda sigui superior a 5000.
-- A més a més gaudeixen d'un descompte del 40% en cas que el preu de la comanda sigui superior a 10000.
-- A les comandes de 2 o més productes del fabricant amb identificador imm i un preu unitari superior a 500 tenen un descompte del 35%.  
-- Si el client ha acumulat un import superior a 20000 amb les comandes dels últims 2 mesos aquest client és un client VIP.
-- Els clients VIP gaudeixen d'un descompte del 30% en comandes amb un preu superior a 2000, en comandes amb preus inferiors el descompte serà del 20%.
-- Sempre s'aplicarà el descompte més beneficiós pel client.

create or replace function descompte(cli smallint, idfab character(3), idprod character(5), cant int) returns int as $$
	declare
		preu numeric;
		descompte_vip int;
		descompte_norm int;
		comandes2mesos numeric;
		import int;
		isvip boolean;
		preu_imm numeric;
	begin
		--esbrinar preu producte
		select precio into preu from productos where id_fab = idfab and id_producto = idprod;
		--import comanda
		import := preu * cant;
		--descompte normal
		if import > 10000 then
			descompte_norm := 40;
		elsif import > 5000 then
			descompte_norm := 20;
		elsif import > 1000 then
			descompte_norm := 10;
		end if;
		--descompte imm?
		if idfab = "imm" and cant > 2 and precio > 500 and descompte_norm < 40 then
			descompte_norm := 35;
		end if;
		--vip?
		select sum(importe) into comandes2mesos from pedidos where age(fecha_pedido) <= '2 months'::interval and clie = cli;
		if comandes2mesos > 20000 then
			isvip := true;
		end if;
		if isvip then
			if import > 2000 then
				descompte_vip := 30;
			else
				descompte_vip := 20;
			end if;
		end if;
		if descompte_vip > descompte_norm then
			descompte_norm := descompte_vip;
		end if;
		return descompte_norm;
	end;
$$ language plpgsql;
		
		

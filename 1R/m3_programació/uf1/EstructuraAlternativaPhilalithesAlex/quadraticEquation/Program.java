/*
 * Program.java        2015/27/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Calculates the solutions of a quadratic equation.
     *
     * @param a the first parameter of the equation.
     * @param b the second parameter of the equation.
     * @param c the third parameter of the equation.
     * @return "No té solució" if there are no solutions, "x = sol1", where sol1
     * is the solution, if there is only one solution and
     * "x1 = sol1, x2 = sol2", where sol1 and sol2 are the solutions, if
     * there are two solutions.
     */
    public String calculateSolutions(double a, double b, double c) {
      //variable declaration
        String solution;
        double discriminant = Math.pow(b,2) - 4 * a * c;
      //calculate
        if (discriminant < 0) {
            solution = "No té solució";
        }
        else if (discriminant == 0) {
            double sol1 = (-b + Math.sqrt(discriminant)) / 2 * a;
            
            solution = "x = "+ sol1;
        }
        else {
            double arrel = Math.sqrt(discriminant);
            double sol1 = (-b + arrel) / 2 * a;
            double sol2 = (-b - arrel) / 2 * a;
            solution = "x1 = " + sol1 + ", x2 = " + sol2;
        }
        return solution;
    }
    
    
}

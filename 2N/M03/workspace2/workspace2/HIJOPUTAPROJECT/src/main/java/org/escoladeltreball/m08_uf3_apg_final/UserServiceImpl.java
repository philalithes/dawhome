/**
 * 
 */
package org.escoladeltreball.m08_uf3_apg_final;

import java.util.logging.Logger;

import org.springframework.stereotype.Service;

/**
 * @author root
 *
 */
@Service
public class UserServiceImpl implements UserService {

	Logger logger = Logger.getLogger(UserService.class.getName());
	/**
	 * 
	 */
	public UserServiceImpl() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.escoladeltreball.springmvcsecuritydemo1.UserService#userMethod()
	 */
	@Override
	public void userMethod() {
		logger.warning("in method userMethod");

	}

	/* (non-Javadoc)
	 * @see org.escoladeltreball.springmvcsecuritydemo1.UserService#adminMethod()
	 */
	@Override
	public void adminMethod() {
		logger.warning("in method adminMethod");
	}

}

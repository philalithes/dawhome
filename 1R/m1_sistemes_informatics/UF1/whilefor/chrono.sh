#!/bin/bash
# Filename:			chrono.sh
# Author:			iaw47951368
# Date:				15/12/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./chrono.sh [args...]
# Description:		cuenta atrás desde 10.

num=$1

if [ $# -ne 1 ]
then
	echo "introdueix un número"
	exit 1
fi

while [ $num -gt 0 ]
do
	echo -ne  "\r$num"
	sleep 1

	num=$(($num-1))
done
echo 

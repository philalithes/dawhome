#!/bin/bash
# Filename:		exercici38.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici38.sh [arg1...]
# Description:		Script que suma tots els pesos dels fitxers d'un directori que es passa com a argument.

#declaració de variables
acumulador=0
#control d'arguments i comprovació de que $1 sigui un directori
if [ $# -eq 1 ] && [ -d $1 ] 
then
	pes=$(ls -l $1 | tr -s ' ' | cut -d" " -f5) 
	for i in $pes
	do
		acumulador=$(($acumulador+$i)) #suma tots els resultats.
	done
else
	echo "has de passar almenys un directori com a argument"
	echo "ajuda: ./exercici37.sh [arg1...]"
	exit 1
fi
#mostrar acumulador
echo $acumulador

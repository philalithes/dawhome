#!/bin/bash
# Filename:			macFab.sh
# Author:			iaw47951368
# Date:				22/04/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./macFab.sh [args...]
# Description:		Script que et demana una mac adress i et dona informacio sobre el fabricant.

macregex="^[0-9a-zA-Z]{2}:[0-9a-zA-Z]{2}:[0-9a-zA-Z]{2}:[0-9a-zA-Z]{2}:[0-9a-zA-Z]{2}:[0-9a-zA-Z]{2}$"

echo "comprovant conexió..."
if ping -c1 8.8.8.8 &> /dev/null 
then
	echo "ok"
else
	echo "no tens conexió a internet"
fi
if [ -n "$DISPLAY" ]
then
	mac=$(zenity --entry \
	--title="macFab.sh" \
	--text="Introdueix una adreça MAC" \
	--entry-text="MACaddress")
	if [[ "$mac" =~ $macregex ]] 
	then
		mac=$(echo $mac | sed 's/:/-/g')
		echo "obtenint informació..."
		zenity --list \
		--column="startHex" \
		--column="endHex" \
		--column="startDec" \
		--column="endDec" \
		--column="company" \
		--column="addressL1" \
		--column="addressL2" \
		--column="addressL3" \
		--column="country" \
		--column="type" \
		--text="info:" \
		$(curl http://www.macvendorlookup.com/api/v2/$mac | sed 's/,/ /g')
	else
		echo "adreça mac no vàlida"
	fi
fi


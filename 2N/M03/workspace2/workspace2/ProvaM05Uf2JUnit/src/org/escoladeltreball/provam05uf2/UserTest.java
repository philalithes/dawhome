/**
 * 
 */
package org.escoladeltreball.provam05uf2;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author iaw47951368
 *
 */
public class UserTest {
	public static User user1;
	public static User user2;
	@BeforeClass
	public static void testBefore() {
		user1 = new User(1l, "alex", 1500);
		user2 = new User(2l, "carlos", 1599);
	}
	@Ignore
	public void testIgnore() {
		user1.setScore(0);
		assertEquals(0, user1.getScore());
	}
	/**
	 * Testeja el funcionament dels getters i setters
	 */
	@Test
	public void test() {
		long score1 = user1.getScore(); 
		assertEquals("error en get", 1500, score1);
		user1.setScore(3000);
		score1 = user1.getScore();
		assertEquals("error en set", 3000, score1);
	}

}

//package org.escoladeltreball.springmvcsecuritydemo1;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.Ordered;
//import org.springframework.web.servlet.ViewResolver;
//import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;
//import org.thymeleaf.spring4.SpringTemplateEngine;
//import org.thymeleaf.spring4.view.ThymeleafViewResolver;
//import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
//import org.thymeleaf.templateresolver.TemplateResolver;
//import org.thymeleaf.templateresolver.UrlTemplateResolver;
//
//@Configuration
//@PropertySource("classpath:thymeleaf.properties")
//public class MyThymeLeafConfig {
//	
//	@Bean
//	public TemplateResolver templateResolver() {
//		ServletContextTemplateResolver s = new ServletContextTemplateResolver();
//		s.setPrefix("WEB-INF/thymeleaf/");
//		s.setSuffix(".html");
//		s.setTemplateMode("HTML5");
//		s.setCacheable(false);
//		return s;
//	}
//	
//	@Bean
//	public UrlTemplateResolver urlTemplateResolver() {
//		return new UrlTemplateResolver();
//	}
//	
//	@Bean
//	public SpringTemplateEngine templateEngine() {
//		SpringTemplateEngine s = new SpringTemplateEngine();
//		s.setTemplateResolver(templateResolver());
//		s.addDialect(new SpringSecurityDialect());
//		return s;
//	}
//	
//	@Bean
//	public ViewResolver thymeleafViewResolver() {
//		ThymeleafViewResolver v = new ThymeleafViewResolver();
//		v.setTemplateEngine(templateEngine());
//		v.setCharacterEncoding("UTF-8");
//		v.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return v;
//	}
//
//}

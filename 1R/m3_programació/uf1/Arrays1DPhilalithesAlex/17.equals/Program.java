
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donades dues taules de nombres enters, determina si són
     * iguals o no. Dues taules són iguals si tenen els mateixos elements en les mateixes
     * posicions.
     */
    
    public boolean equals(int[] array1, int[] array2) {
        //variable declaration
        boolean areEquals = false;
        int i = 0;
        //calcul
        if (array1.length == array2.length) {
            while (i < array1.length && array1[i] == array2[i]  ) {
                i++;
                
            }
            if (i == array1.length) {
                areEquals = true;
            }
        }
        return areEquals;
        
    }
}

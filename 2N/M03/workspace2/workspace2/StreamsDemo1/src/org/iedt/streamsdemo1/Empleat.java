package org.iedt.streamsdemo1;

public class Empleat {
	private long id;
	private String alias; //inicials
	private String departament; //informatica o electronica
	private double salari; //1000 - 3000

	@Override
	public String toString() {
		return "Empleat [id=" + id + ", alias=" + alias + ", departament=" + departament + ", salari=" + salari + "]";
	}

	public Empleat(long id, String alias, String departament, double salari) {
		super();
		this.id = id;
		this.alias = alias;
		this.departament = departament;
		this.salari = salari;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getDepartament() {
		return departament;
	}

	public void setDepartament(String departament) {
		this.departament = departament;
	}

	public double getSalari() {
		return salari;
	}

	public void setSalari(double salari) {
		this.salari = salari;
	}

}

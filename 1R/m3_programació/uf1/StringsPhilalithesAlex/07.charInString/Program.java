/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donats un caràcter i una cadena, determina si el
     * caràcter es troba dins de la cadena o no. Fes dues versions del programa: una usant el
     * mètode indexOf(c) i una altra versió sense usar aquest mètode.
     * 
     * 
     */
    
    public boolean isCharInString (String cadena, char caracter) {
        //vars
        boolean isCharInString = false;
        int i = 0;
        //calc
        while (!isCharInString && i < cadena.length()) {
            if (cadena.charAt(i) == caracter) {
                isCharInString = true;
            }
            i++;
        }
        //ret
        return isCharInString;
        
        
    }
}

package org.escoladeltreball.comparabledemo;


/**
 * @author iaw47951368
 *
 */
public class Student implements Comparable<Student> {

	private String nom;
	private String cognoms;
	private int id;
	private String grup;
	private double nota;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGrup() {
		return grup;
	}

	public void setGrup(String grup) {
		this.grup = grup;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognoms == null) ? 0 : cognoms.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (cognoms == null) {
			if (other.cognoms != null)
				return false;
		} else if (!cognoms.equals(other.cognoms))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}



	public Student(String nom, String cognoms, int id, String grup, double nota) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.id = id;
		this.grup = grup;
		this.nota = nota;
	}



	@Override
	public String toString() {
		return "Student [nom=" + nom + ", cognoms=" + cognoms + ", id=" + id + ", grup=" + grup + ", nota=" + nota
				+ "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getCognoms() {
		return cognoms;
	}

	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	/*
	 * Implementa l'ordre natural de la classe
	 * 
	 */
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Student student) {
		// serà negatiu si aquest estudiant és més petit que l'argument.
		// 0 si són iguals.s
		// positiu si aquest estudiant és més gran que l'argument.
		return cognoms.compareTo(student.getCognoms());
	}

	

}

-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.1
-- PostgreSQL version: 9.3
-- Project Site: pgmodeler.com.br
-- Model Author: ---


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database
-- ;
-- -- ddl-end --
-- 

-- object: public.camisetas | type: TABLE --
-- DROP TABLE IF EXISTS public.camisetas CASCADE;
CREATE TABLE public.camisetas(
	artista varchar(30),
	modelo varchar(20),
	id_camiseta smallint NOT NULL,
	CONSTRAINT camisetapk PRIMARY KEY (id_camiseta)

);
-- ddl-end --
ALTER TABLE public.camisetas OWNER TO postgres;
-- ddl-end --

-- object: public.entradas | type: TABLE --
-- DROP TABLE IF EXISTS public.entradas CASCADE;
CREATE TABLE public.entradas(
	fecha date,
	lugar text,
	artista varchar(30),
	disponibles smallint,
	id_entrada smallint NOT NULL,
	CONSTRAINT conciertopk PRIMARY KEY (id_entrada)

);
-- ddl-end --
ALTER TABLE public.entradas OWNER TO postgres;
-- ddl-end --

-- object: public.pedidos | type: TABLE --
-- DROP TABLE IF EXISTS public.pedidos CASCADE;
CREATE TABLE public.pedidos(
	cantidad smallint NOT NULL,
	num_pedido serial NOT NULL,
	fecha_pedido date,
	cliente smallint NOT NULL,
	empleado smallint NOT NULL,
	importe double precision NOT NULL,
	id_producto smallint NOT NULL,
	CONSTRAINT pedidospk PRIMARY KEY (num_pedido)

);
-- ddl-end --
ALTER TABLE public.pedidos OWNER TO postgres;
-- ddl-end --

-- object: public.tallas | type: TABLE --
-- DROP TABLE IF EXISTS public.tallas CASCADE;
CREATE TABLE public.tallas(
	talla varchar(6) NOT NULL,
	CONSTRAINT tallaspk PRIMARY KEY (talla)

);
-- ddl-end --
ALTER TABLE public.tallas OWNER TO postgres;
-- ddl-end --

-- object: public.camiseta_talla | type: TABLE --
-- DROP TABLE IF EXISTS public.camiseta_talla CASCADE;
CREATE TABLE public.camiseta_talla(
	id_camiseta smallint,
	talla varchar(6),
	unidades smallint
);
-- ddl-end --
ALTER TABLE public.camiseta_talla OWNER TO postgres;
-- ddl-end --

-- object: public.cds | type: TABLE --
-- DROP TABLE IF EXISTS public.cds CASCADE;
CREATE TABLE public.cds(
	id_cd smallint NOT NULL,
	artista varchar(30),
	titulo varchar(30),
	duracion interval MINUTE ,
	unidades smallint,
	CONSTRAINT cdpk PRIMARY KEY (id_cd)

);
-- ddl-end --
ALTER TABLE public.cds OWNER TO postgres;
-- ddl-end --

-- object: public.productos | type: TABLE --
-- DROP TABLE IF EXISTS public.productos CASCADE;
CREATE TABLE public.productos(
	id smallint NOT NULL,
	precio double precision NOT NULL,
	CONSTRAINT productospk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.productos OWNER TO postgres;
-- ddl-end --

-- object: public.producto_stock | type: TABLE --
-- DROP TABLE IF EXISTS public.producto_stock CASCADE;
CREATE TABLE public.producto_stock(
	id smallint NOT NULL,
	CONSTRAINT prodstockpk PRIMARY KEY (id)

);
-- ddl-end --
ALTER TABLE public.producto_stock OWNER TO postgres;
-- ddl-end --

-- object: public.proveedores | type: TABLE --
-- DROP TABLE IF EXISTS public.proveedores CASCADE;
CREATE TABLE public.proveedores(
	id_proveedor smallint NOT NULL,
	telefono integer,
	direccion text,
	producto smallint,
	precio double precision,
	CONSTRAINT proveedorpk PRIMARY KEY (id_proveedor)

);
-- ddl-end --
ALTER TABLE public.proveedores OWNER TO postgres;
-- ddl-end --

-- object: public.clientes | type: TABLE --
-- DROP TABLE IF EXISTS public.clientes CASCADE;
CREATE TABLE public.clientes(
	id_cliente smallint NOT NULL,
	direccion text,
	telefono integer,
	puntos smallint,
	CONSTRAINT clientepk PRIMARY KEY (id_cliente)

);
-- ddl-end --
ALTER TABLE public.clientes OWNER TO postgres;
-- ddl-end --

-- object: public.empleados | type: TABLE --
-- DROP TABLE IF EXISTS public.empleados CASCADE;
CREATE TABLE public.empleados(
	num_empl smallint NOT NULL,
	turno varchar(20),
	nombre varchar(20),
	CONSTRAINT empleadospk PRIMARY KEY (num_empl)

);
-- ddl-end --
ALTER TABLE public.empleados OWNER TO postgres;
-- ddl-end --

-- object: public.compras | type: TABLE --
-- DROP TABLE IF EXISTS public.compras CASCADE;
CREATE TABLE public.compras(
	num_compra serial NOT NULL,
	empleado smallint NOT NULL,
	producto smallint,
	cantidad smallint,
	fecha date,
	importe double precision,
	CONSTRAINT comprapk PRIMARY KEY (num_compra)

);
-- ddl-end --
ALTER TABLE public.compras OWNER TO postgres;
-- ddl-end --

-- object: stockfk | type: CONSTRAINT --
-- ALTER TABLE public.camisetas DROP CONSTRAINT IF EXISTS stockfk CASCADE;
ALTER TABLE public.camisetas ADD CONSTRAINT stockfk FOREIGN KEY (id_camiseta)
REFERENCES public.producto_stock (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: productofk | type: CONSTRAINT --
-- ALTER TABLE public.entradas DROP CONSTRAINT IF EXISTS productofk CASCADE;
ALTER TABLE public.entradas ADD CONSTRAINT productofk FOREIGN KEY (id_entrada)
REFERENCES public.productos (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: productofk | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS productofk CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT productofk FOREIGN KEY (id_producto)
REFERENCES public.productos (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: clientesfk | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS clientesfk CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT clientesfk FOREIGN KEY (cliente)
REFERENCES public.clientes (id_cliente) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: emplfk | type: CONSTRAINT --
-- ALTER TABLE public.pedidos DROP CONSTRAINT IF EXISTS emplfk CASCADE;
ALTER TABLE public.pedidos ADD CONSTRAINT emplfk FOREIGN KEY (empleado)
REFERENCES public.empleados (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: camifk | type: CONSTRAINT --
-- ALTER TABLE public.camiseta_talla DROP CONSTRAINT IF EXISTS camifk CASCADE;
ALTER TABLE public.camiseta_talla ADD CONSTRAINT camifk FOREIGN KEY (id_camiseta)
REFERENCES public.camisetas (id_camiseta) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: tallafk | type: CONSTRAINT --
-- ALTER TABLE public.camiseta_talla DROP CONSTRAINT IF EXISTS tallafk CASCADE;
ALTER TABLE public.camiseta_talla ADD CONSTRAINT tallafk FOREIGN KEY (talla)
REFERENCES public.tallas (talla) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: stockfk | type: CONSTRAINT --
-- ALTER TABLE public.cds DROP CONSTRAINT IF EXISTS stockfk CASCADE;
ALTER TABLE public.cds ADD CONSTRAINT stockfk FOREIGN KEY (id_cd)
REFERENCES public.producto_stock (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: prodfk | type: CONSTRAINT --
-- ALTER TABLE public.producto_stock DROP CONSTRAINT IF EXISTS prodfk CASCADE;
ALTER TABLE public.producto_stock ADD CONSTRAINT prodfk FOREIGN KEY (id)
REFERENCES public.productos (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: productofk | type: CONSTRAINT --
-- ALTER TABLE public.proveedores DROP CONSTRAINT IF EXISTS productofk CASCADE;
ALTER TABLE public.proveedores ADD CONSTRAINT productofk FOREIGN KEY (producto)
REFERENCES public.producto_stock (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: emplfk | type: CONSTRAINT --
-- ALTER TABLE public.compras DROP CONSTRAINT IF EXISTS emplfk CASCADE;
ALTER TABLE public.compras ADD CONSTRAINT emplfk FOREIGN KEY (empleado)
REFERENCES public.empleados (num_empl) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: prodfk | type: CONSTRAINT --
-- ALTER TABLE public.compras DROP CONSTRAINT IF EXISTS prodfk CASCADE;
ALTER TABLE public.compras ADD CONSTRAINT prodfk FOREIGN KEY (producto)
REFERENCES public.producto_stock (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --



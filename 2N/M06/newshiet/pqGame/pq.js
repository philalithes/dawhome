// ******************* REGION VARIABLES GLOBALES ******************//

var Player = {
	isAlive: true,
	die: function() {
		if (isAlive) {
			isAlive = false;
			alert("Has muerto");
		} 
	}
}
var rows = 50;
var cols = 50;
var board = new genBoard(rows, cols);
var classSelected;
var down = true;

// FIN REGION
// ******************* REGION EVENTOS******************//

$(document).ready(function() {

	updateTable("main");

	$("body").mousedown(function() {
		down = true;
	}).mouseup(function() {
		down = false;
	});
	$('td').click(function() {
		if(classSelected != "selected") 
			paint($(this));
	})
	$('td').mouseover(function() {
		if(down && classSelected != "selected")
			paint($(this));
	})


	$('.tileset').click(function() {
		classSelected = $(this).attr("class").split(" ")[1];
		console.log(classSelected);
		if(classSelected != "selected") {
			$('.selected').removeClass().addClass('tileset selected');
			$('.selected').toggleClass(classSelected);
		}
	});


	//left = 37 up = 38 right = 39 down = 40
	$(".jugador").keypress(function(event) {
		var clazz;
		if (event.which == 37) {
			clazz = interaccion($(leftBlock).attr('class'), true);
			move($(leftBlock), clazz);
		} else if (event.which == 38) {
			interaccion($(upperBlock).attr('class'), true);
			move($(upperBlock), clazz);
		} else if (event.which == 39) {
			interaccion($(rightBlock).attr('class'), true);
			move($(rightBlock), clazz);
		} else if (event.which == 40) {
			interaccion($(lowerBlock).attr('class'), true);
			move($(lowerBlock), clazz);
		}
	});
});


// FIN REGION

// ******************* REGION FUNCIONES ******************//


function paint(thisBlock) {
	thisBlock.removeClass();
	thisBlock.addClass(classSelected);
}

function genBoard(rows, cols) {
	this.name = arguments[2];
	this.nRows = rows;
	this.nCols = cols;
	this.cells = [];
	var x = 0;
	for (var i = 0; i < this.nRows; i++) {
		this.cells.push([]);
		for (var j = 0; j < this.nCols; j++) {
			this.cells[i].push([]);
			x++;
		}
	}
}
function updateTable(idDiv) {
	var table = '<center><table>';
	for (var i = 0; i < rows; i++) {
		table += '<tr>';
		for (var j = 0; j < cols; j++) {
			table += '<td>';
			table += board.cells[i][j];
			table += '</td>';
		}
	table+= '</tr>';
	}
	table+= '</table>';
	$("#"+idDiv).html(table);
}
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function interaccion(item, returnClazz) {
	if (item == 'air') {
		move()
		if (returnClazz) {
			return 'air';
		}
	}
	if (item == 'mina') {
		Player.die();
		if (returnClazz) {
			return 'mina';
		}
	}
}

function move(block, clazz) {
	$(".jugador").attr('class') = clazz;
	block.addClass("jugador");
}

function gravity() {
	if ($(".jugador").lowerBlock().attr("class") == 'air') {
		move(lowerBlock, 'air');
	} else if ($(".jugador").lowerBlock().attr("class") == 'cristal') {
		Player.die();
	}
}


//funciones que devuelven el objeto vecino
function upperBlock(thisBlock) {

}

function lowerBlock(thisBlock) {

}

function leftBlock(thisBlock) {

}

function rightBlock(thisBlock) {

}

// FIN REGION
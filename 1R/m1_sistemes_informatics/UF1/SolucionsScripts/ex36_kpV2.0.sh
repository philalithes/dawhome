#!/bin/bash
# Fitxer: ex36_kp_commentsV2.0.sh
# Autor: jamoros
# Data: 04/04/2011
# Versio: 0.2
# Descripcio:	Script que ens mostrarà per pantalla els fitxers i directoris que hi ha al directori actual. 
#			 	Però ho farà amb el següent format:
# 				Si amb la comanda ls obtenim la següent sortida
#				fit1   prog1   prog2   joc
#				Amb el nostre script ens ho mostrarà així:
#			    |_fit1
#			    .|_prog1
#				..|_prog2
#				...|_joc

#   Aquest script difereix de l'anterior versió en la utilització de les taules (estructures compostes: tipus arrai)



OLD_IFS=$IFS  # Això ho fem perquè el separador d'elements a la taula sigui només \n 
IFS=$'\n'

# Emmagatzemem la sortida de l'ordre ls a una taula (podem pensar en un arrai o vector)

files=(`ls`) 



# Per a cada fitxer creem la cadena de sortida i el mostrem
# Creem una cadena de sortida inicial
cadena='|_'
i=0 # variable comptadora, per recorrer el bucle
max=${#files[@]} # longitud de la taula
while [ $i -lt $max ]
do 
	echo "$cadena${files[$i]}" #concateno les dues cadenes
	cadena=.${cadena}  # Afegeixo un punt a la cadena
	i=$((i+1))
done

IFS=$OLD_IFS



<<*
una altra manera de trobar la longitud:
${#array_name[*]}

*


/*
 * Program.java        2015/12/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * L'objectiu d'aquest mètode és escriure per pantalla
     * un histograma amb els pesos d'un conjunt de persones.
     * Pes Nombre de persones
     * --------------------------------------
     * 55 **
     * 61 ****
     * 65 *********
     * 70 *****
     * ... ...
     * El nombre d'asteriscs es correspon amb el nombre de persones del pes especificat.
     * El mètode rebrà com a paràmetre un array que llegirà els pesos i escriurà l'histograma
     * corresponent. Suposarem que els pesos estan compresos entre els valors de 1 i 100 kg. A
     * l'histograma només apareixeran els pesos que corresponen a 1 o més persones.
     */
    
    public void histogram(int[] array) {
        //variable declaration
        int i = 0;
        int n = 0;
        int k = 0;
        int[] pesos = new int[100];
        //calcul
        for (i = 0; i < array.length; i++) {
            pesos[array[i]]++;
        }
        System.out.println("Pes\tNombre de persones\n--------------------------------------");
        for (i = 0; i < pesos.length; i++) {
            if (pesos[i] > 0) {
                System.out.print(i + "\t");
                for (k = 0; k < pesos[i]; k++) {
                    System.out.print("*");
                }
                System.out.print("\n");
            }
        }
    }
}
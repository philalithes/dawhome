#!/bin/bash
# Filename:			suma.sh
# Author:			iaw47951368
# Date:				12/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./suma.sh arg1 arg2
# Description:		suma aritmetica dels 2 arguments

if test $# -eq 2 
then 
	expr $1 + $2
else
	echo "escriu 2 arguments per sumarlos"
	exit 1
fi

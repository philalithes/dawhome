/*
 * Program.java        2015/23/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determina si un nombre real és positiu, negatiu o zero. El mètode
     * retornarà les cadenes “positiu”, “negatiu” o “zero”, respectivament.
     * 
     * @param n = nombre real
     * 
     * 
     *
     * 
     * @retorn "positiu" "negatiu" o " zero"
     */
    public String sign(double n){
        String sign;
        if(n > 0) {
        sign = "positiu";
        }
        else if(n == 0) {
        sign = "zero";
        }
        else {
        sign = "negatiu";
        }
        return sign;
    }
    
    
}

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats el primer terme, la diferència i el nombre de termes d'una progressió aritmètica,
     * escriu els seus elements, la seva suma i el seu producte. 
     * En una progressió aritmètica cada terme és igual a l'anterior més la diferència.
     * 
     * n1 = primer nombre
     * nn = nombre de nombres
     */
    
    public void writeArithmeticProgression(int n1, int diferencia, int nn) {
        //variable declaration
        int i;
        int acumulador = n1;
        int suma = 0;
        int producte = 1;
        //calcul
        for (i = 0; i < nn; i++) {
            suma += acumulador;
            producte *= acumulador;
            System.out.println(acumulador);
            acumulador += diferencia;
            
        }
        System.out.printf("suma: %d \n",suma);
        System.out.printf("producte: %d \n",producte);
        
    }
}

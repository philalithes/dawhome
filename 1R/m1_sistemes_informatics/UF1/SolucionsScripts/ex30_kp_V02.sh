
#!/bin/bash
# Fitxer: ex30_kp_comments.sh
# Autor: jamoros
# Data: 29/03/2011
# Versio: 0.1
# Descripcio: Entrarem un caràcter i direm si és un número, lletra o diferent d'aquests. Tot i que l'exercici no diu res al respecte, farem un petit control d'errors: si hi ha més o mensy d'un caràcter surtirem.

# Demanem un caràcter i l'emmagatzemem
echo "Sisplau entra un únic caràcter"
read caracter
# Si no hem entrat exactament un caracter surtim amb misstage d'error
num_caracters=$(echo -en $caracter | wc -m)

if [ $num_caracters -ne 1 ]
then
	echo "error: no has entrat un únic caràcter"
	exit 1
fi

# En cas contrari, si és una lletra mostrem un missatge indicant-lo
echo -en $caracter | grep '[[:alpha:]]' > /dev/null 2> /dev/null 
if echo -en $caracter | grep '[[:alpha:]]' > /dev/null 2> /dev/null 
then
	echo "És una lletra"
	
	# Altrament, si és un nombre  mostrem un missatge indicant-lo
elif echo -en $caracter | grep '[[:digit:]]' > /dev/null 2> /dev/null
then
	echo "És un dígit"

# Si no, mostrem un missatge indicant que no és ni lletra ni nombre
else
	echo "No és ni una lletra ni un dígit"
fi


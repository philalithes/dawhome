/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Finca.
 * 
 * @author iaw47951368
 */
public class Finca {
	/**
	 * Description of the property carrers.
	 */
	public Carrer carrers = null;
	
	/**
	 * Description of the property numero.
	 */
	public int numero = 0;
	
	// Start of user code (user defined attributes for Finca)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Finca() {
		// Start of user code constructor for Finca)
		super();
		// End of user code
	}
	
	/**
	 * Description of the method abrir.
	 */
	public void abrir() {
		// Start of user code for method abrir
		// End of user code
	}
	 
	/**
	 * Description of the method cerrar.
	 */
	public void cerrar() {
		// Start of user code for method cerrar
		// End of user code
	}
	 
	/**
	 * Description of the method acumularPolvo.
	 */
	public void acumularPolvo() {
		// Start of user code for method acumularPolvo
		// End of user code
	}
	 
	// Start of user code (user defined methods for Finca)
	
	// End of user code
	/**
	 * Returns carrers.
	 * @return carrers 
	 */
	public Carrer getCarrers() {
		return this.carrers;
	}
	
	/**
	 * Sets a value to attribute carrers. 
	 * @param newCarrers 
	 */
	public void setCarrers(Carrer newCarrers) {
	    this.carrers = newCarrers;
	}

	/**
	 * Returns numero.
	 * @return numero 
	 */
	public int getNumero() {
		return this.numero;
	}
	
	/**
	 * Sets a value to attribute numero. 
	 * @param newNumero 
	 */
	public void setNumero(int newNumero) {
	    this.numero = newNumero;
	}



}

/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.*;

public class Program {
    /*
     *  Donades dues cadenes, genera una altra cadena
     * amb els caràcters de les anteriors intercalats començant pel primer caràcter de la primera
     * cadena. Si els caràcters d’una de les cadenes s'esgoten, s’afegeixen tots els caràcters de
     * l’altra.
     * 
     * 
     */
    public String insertChars(String cadena1, String cadena2) {
        //vars
        String cadena3 = "";
        int i = 0;
        //calc
        while (i < cadena1.length() || i < cadena2.length()) {
            if (i < cadena1.length() && i < cadena2.length()) {
            cadena3 += String.valueOf(cadena1.charAt(i));
            cadena3 += String.valueOf(cadena2.charAt(i));
            } else if (i >= cadena1.length()) {
                cadena3 += String.valueOf(cadena2.charAt(i));
            } else if (i >= cadena2.length()) {
                cadena3 += String.valueOf(cadena1.charAt(i));
            }
            i++;
        }
        return cadena3;
    }
    
}
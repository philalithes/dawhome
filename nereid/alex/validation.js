$(function() {
	$.validator.addMethod('strongPassword', function(value, element) {
		return this.optional(element)
			|| value.length >= 6
			&& /\d/.test(value)
			&& /[a-z]/.test(value)
			&& /[A-Z]/.test(value);
	}, 'Your password must be at least 6 characters long and contain at least one number and one upper case.')

	$.validator.addMethod('checkDob', function(value, element) {
		if (this.optional(element)) return true;
		if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(value)) return false;
		var arr_date = value.split("/");

		//format dd/mm/yyyy (spanish)
		day = arr_date[0];
		month = arr_date[1];
		year = arr_date[2];

		//format mm/dd/yyyy (english)
		/*day = arr_date[1];
		month = arr_date[0];
		year = arr_date[2]*/

		now = new Date();
		d1950 = new Date(1950, 0, 1);
		if (Date.UTC(year,month-1,day) > Date.UTC(now.getFullYear(),now.getMonth(),now.getDate())
			|| Date.UTC(year,month-1,day) < Date.UTC(d1950.getFullYear(),d1950.getMonth(),d1950.getDate())) return false;
		return true;
	}, 'Date of birth must be between 01/01/1950 and today.')

	/*$.validator.addMethod('string20', function(value, element) {
		if (value.length > 20) return false;
		return true;
	}, 'Max. 20 characters')
	
	$.validator.addMethod('string100', function(value, element) {
		if (value.length > 100) return false;
		return true;
	}, 'Max. 100 characters')*/

	$("#register-form").validate({
		rules: {
			email: {
				required: true,
                maxlength:100,
				email: true
			},
			password: {
				required: true,
				maxlength: 20,
				strongPassword: true
			},
			password2: {
				required: true,
				equalTo: "#password"
			},
			firstName: {
				required: true,
				nowhitespace: true,
				maxlength: 20,
				lettersonly: true
			},
			dob: {
				required: true,
				checkDob: true
			}
		}
	});

});
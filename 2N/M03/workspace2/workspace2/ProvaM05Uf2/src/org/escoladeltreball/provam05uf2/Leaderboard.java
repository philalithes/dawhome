package org.escoladeltreball.provam05uf2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe que es fa servir per veure les puntuacions dels usuaris.
 * 
 * @author iaw47951368
 * @version 1.0
 * @since 1.0
 * @see org.escoladeltreball.provam05uf2.User
 */
public class Leaderboard {
	/** Llista d'usuaris de la taula **/
	private List<User> users = new ArrayList<>();

	/**
	 * Constructor que utilitza una llista d'usuaris que ja està creada
	 * 
	 * @param users Llista d'usuaris
	 */
	public Leaderboard(List<User> users) {
		this.users = users;
	}
	
	/**
	 * Constructor per defecte
	 */
	public Leaderboard() {
	}

	/**
	 * Retorna la llista d'usuaris de l'objecte Leaderboard
	 * 
	 * @return La llista d'usuaris
	 */
	public List<User> getUsers() {
		return users;
	}

	/**
	 * Posa una nova llista d'usuaris
	 * 
	 * @param users La nova llista d'usuaris
	 */
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	/**
	 * Afegeix un usuari nou a la llista
	 * 
	 * @param user nou usuari que afegeix a la llista d'usuaris
	 * @return true si ha tingut èxit, si no, false
	 */
	public boolean addUser(User user) {
		if (users.contains(user)) {
			int index = users.indexOf(user);
			if (user.getScore() > users.get(index).getScore()) {
				users.get(index).setScore(user.getScore());
				return true;
			} 
			return false;
		} 
		return users.add(user);		
	}
	
	/**
	 * Mètode per veure qui és l'usuari amb més puntuació
	 * 
	 * @return l'usuari amb la puntuació més alta
	 */
	public User userWin(){
		Collections.sort(users);
		return users.get(0);
		
	}
	
	/**
	 * Mètode per treure per pantalla els 10 millors usuaris
	 * 
	 *  Printa els primers 10 usuaris
	 */
	public void bestTen(){
		Collections.sort(users);
		String s = "";
		for (int i = 0; i < 10; i++){
			s += "Name : "+ users.get(i).getName() + "\t Score : "+ users.get(i).getScore()+"\n";
		}
		System.out.println(s);
	}
	
	/**
	 * Mètode per veure la diferència de puntuació entre el primer i el segon usuari
	 * @return la diferència de puntuació entre el primer i el segon usuari
	 */
	public long difSecondFirst(){
		Collections.sort(users);
		return users.get(0).getScore() - users.get(1).getScore();
	}
	 
	/**
	 * Mètode per veure quina puntuació t'ha faltat per arribar a la puntuació máxima
	 * 
	 * @param u un usuari
	 * @return la diferència de puntuació entre l'usuari paràmetre i l'usuari amb màxima puntuació
	 */
	public long difUserFirst(User u){
		Collections.sort(users);
		return users.get(0).getScore() - u.getScore();
	}
	
	@Override
	public String toString() {
		Collections.sort(users);
		String s = "";
		for (User u : users) {
			s += "Name : "+ u.getName() + "\t Score : "+ u.getScore()+"\n";
		}
		return s;
	}
	
}
package org.escoladeltreball.m03uf06nf02APG;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Entity implementation class for Entity: Equip
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "selectEquip", query = "SELECT e FROM Equip e WHERE e.id = ?1"),
	@NamedQuery(name = "selectEquipByNom", query = "SELECT e FROM Equip e WHERE e.nom like ?1"),
	@NamedQuery(name = "updateEquip", query = "UPDATE Equip e SET e.nom = ?1, e.nCompeticionsGuanyades = ?2, e.capital = ?3 WHERE e.id = ?4")
})
public class Equip implements Serializable {

	private String nom;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private Integer nCompeticionsGuanyades;
	private Double capital;
	private static final long serialVersionUID = 1L;
	@OneToMany(cascade = CascadeType.PERSIST, mappedBy = "equip")
	private List<Jugador> jugadors = new ArrayList<>();
	//owner
	@OneToOne(cascade = CascadeType.PERSIST)
	private Estadi estadi;
	@ManyToMany(cascade = CascadeType.PERSIST, mappedBy = "equips")
	private List<Competicio> competicions = new ArrayList<>();

	
	@Override
	public String toString() {
		return "Equip [nom=" + nom + ", id=" + id + ", nCompeticionsGuanyades=" + nCompeticionsGuanyades + ", capital="
				+ capital + "]";
	}

	public Equip(String nom, Integer nCompeticionsGuanyades, Double capital) {
		super();
		this.nom = nom;
		this.nCompeticionsGuanyades = nCompeticionsGuanyades;
		this.capital = capital;
	}

	public void addCompeticio(Competicio c) {
		competicions.add(c);
	}

	public void removeCompeticio(Competicio c) {
		competicions.remove(c);
	}

	public Integer getnCompeticionsGuanyades() {
		return nCompeticionsGuanyades;
	}

	public void setnCompeticionsGuanyades(Integer nCompeticionsGuanyades) {
		this.nCompeticionsGuanyades = nCompeticionsGuanyades;
	}

	public Estadi getEstadi() {
		return estadi;
	}

	public void setEstadi(Estadi estadi) {
		this.estadi = estadi;
	}

	public void addJugador(Jugador j) {
		jugadors.add(j);
	}

	public void removeJugador(Jugador j) {
		jugadors.remove(j);
	}

	public Equip() {
		super();
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Double getCapital() {
		return this.capital;
	}

	public void setCapital(Double capital) {
		this.capital = capital;
	}

}

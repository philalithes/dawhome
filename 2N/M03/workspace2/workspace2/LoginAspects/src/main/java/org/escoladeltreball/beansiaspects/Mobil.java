package org.escoladeltreball.beansiaspects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import ch.qos.logback.core.net.SyslogOutputStream;

public class Mobil {

	private String marca;
	private boolean power;
	
	@Autowired
	@Qualifier("AAA")
	private Battery battery;

	public Mobil(String marca, Battery battery) {
		super();
		this.marca = marca;
		this.battery = battery;
	}

	public Mobil(String marca) {
		super();
		this.marca = marca;
	}
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public Battery getBattery() {
		return battery;
	}

	public void setBattery(Battery battery) {
		this.battery = battery;
	}
	
	public void AAcall(String who) {
		System.out.println("calling " + who);
	}

	public void AAsendTxt(String txt, String who) {
		System.out.println("To: " + who);
		System.out.println("Message: " + txt);
	}
	
	@Override
	public String toString() {
		return "Mobil [marca=" + marca + ", power=" + power + ", battery=" + battery + "]";
	}

	public boolean isPower() {
		return power;
	}

	public void setPower(boolean power) {
		this.power = power;
	}

	public void turnOn() {
		if (!isPower()) {
			setPower(true);
			System.out.println("Mobile On");
		}
	}
	
	public void turnOff() {
		if (isPower()) {
			setPower(false);
			System.out.println("Mobile Off");
		}
	}
}

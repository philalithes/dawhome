#!/bin/bash
# Filename:		exercici3.sh
# Author:		pingui
# Date:			03/03/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici3.sh [arg1...]
# Description:	script que mostra quines parelles de partits poden aconseguir la majoria per investir un president


# funció que rep la id d'un partit polític i retorna el seu número d'escons
function get_escons {
	cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f3 
}

# funció que rep la id d'un partit polític i retorna el seu nom
function get_nom {
	cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f2
}

# function que rep la id d'un partit polític i mostra el id de la seva incompatibilitat
function get_incompat {
	cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f4
}

# funció que rep una id d'un partit i mostra les parelles que sumen majoria 
# amb el partit rebut, sempre que no siguin incompatibles
function mostra_parelles {
	# Guardem el IFS actual i el canvien (de tots els blanks a només el salt de línia)
	local OLD_IFS=$IFS
	IFS=$'\n'
	# Guardem el nom del partit consultat i el seu número d'escons
	local nom_partit_consultat=$(get_nom $1)
	local escons_partit_consultat=$(get_escons $1)
	# De tots els partits que NO són el partit consultat
	# mostro els que sumen majoria amb el consultat i que no són incompatibles
	for partit in $(cat $resultats_eleccions_fitxer | grep -v "^$1:")
	do
		local id_parella=$(echo $partit | cut -d: -f1)
		local escons_parella=$(echo $partit | cut -d: -f3)
		local suma_escons=$(($escons_parella + $escons_partit))

		if [ $suma_escons -ge $majoria_escons ] && [ $(get_incompat $1) -ne $id_parella ] && [ $(get_incompat $id_parella) -ne $1 ] # exercici 4
		then
			echo $nom_partit_consultat $escons_partit_consultat
			echo $(echo $partit | cut -d: -f2) $(echo $partit | cut -d: -f3)
			echo "TOTAL: $suma_escons"
		else
			IFS=$OLD_IFS
			return # Aquesta sortida no és trivial *
		fi
		echo
	done

	# tornem a deixar el IFS com estava
	IFS=$OLD_IFS
}

####### main  ##########################

escons=350
majoria_escons=$(($escons/2+1)) # Més de la meitat dels escons
resultats_eleccions_fitxer=investidura_episodi1.txt

# Si no hi ha un únic argument informem de l'error i sortim informant al sistema
if [ $# -ne 1 ] 
then
	echo "error: número de argumentos incorrecto"
	echo "$0 id"
	echo "on id és el número del partit polític"
	exit 1
fi

# Si l'argument no és un enter informem de l'error i sortim informant al sistema
if ! echo $1 | grep '^[0-9]\+$' >& /dev/null
then
	echo "error: l'argument $1 no és un número enter positiu"
	exit 2
fi
escons_partit=$(get_escons $1)

# Mirem si la id es troba al fitxer
if [ -z "$escons_partit" ]
then
	echo "la id $1 no pertany a cap partit"
	exit 3
fi

mostra_parelles $1

# *	Com que el fitxer està ordenat pel nombre d'escons de manera descendent,
#	quan arribi a un partit amb el qual no sumi la majoria, 
#	amb els següents tampoc (ja que tindran un número igual o més petit d'escons)

# exercici 4: que es podria millorar en aquesta instrucció? Pista a sota

















# Penseu si hi ha una crida a una funció que estem repetint tota l'estona 

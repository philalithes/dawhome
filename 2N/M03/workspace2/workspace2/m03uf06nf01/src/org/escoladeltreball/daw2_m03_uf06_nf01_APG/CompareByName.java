package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

import java.util.Comparator;

public class CompareByName implements Comparator<Dispositiu>{
	public CompareByName() {
		
	}
	
	public int compare(Dispositiu d1, Dispositiu d2) {
		return d1.getNom().compareTo(d2.getNom());
	}
}

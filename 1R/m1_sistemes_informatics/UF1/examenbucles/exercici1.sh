#!/bin/bash
# Filename:			exercici1.sh
# Author:			iaw47951368
# Date:				04/03/16
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici1.sh [args...]
# Description:			Script que mostra el nom i el numero d'escons del partit polític passat com a argument amb id corresponent a l'arxiu $fitxer. L'script treballa amb un fitxer que	conté aquesta informació guardat a la variable $fitxer.

#Variable del fitxer, si en un futur es canvia el nom, s'ha de canviar aquesta variable.
fitxer=investidura_episodi1.txt

#funcions
function get_nom_escons {
cat $fitxer | grep "^$1:" | cut -d":" -f2-3 --output-delimiter=" " #mostra els camps 2 i 3 de les línies que coincideixen amb la id passada com a argument.
}

#control d'arguments
if [ "$#" -ne 1 ] #si no hi ha només un argument...
then
	echo "error: número d'arguments incorrecte."
	echo "ús: $0 id."
	exit 1
fi

if [ ! $(echo "$1" | egrep "^[0-9]+$") ] #si hi ha un argument però no és un enter positiu...
then
	echo "error: l'argument $1 no és un número enter positiu"
	echo "ús: $0 id"
	exit 1
fi

if [ ! "$(cat $fitxer | grep "^$1:" | cut -d":" -f1)" ] #si l'argument no coincideix amb cap id del fitxer...
then
	echo "error: la id $1 no pertany a cap partit"
	exit 1
else	#si hem arribat aquí és que tot ha anat bé.
	get_nom_escons "$1"	#crida a la funció.		
fi

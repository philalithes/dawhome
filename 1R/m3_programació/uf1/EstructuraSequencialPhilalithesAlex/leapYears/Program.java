/*
 * Program.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if a number is a leap year
     * 
     * @param n = number
     * 
     * 
     * 
     * 
     * @return true = is a leap year, false = isn't a leap year
     */
    public boolean isLeapYear(int n){
        //variable declaration    
        boolean is;
        //calculate return
        is = n % 4 == 0 && n % 100 != 0 || n % 400 == 0;
        //return
        return is;
    }
    
}

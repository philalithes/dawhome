package org.escoladeltreball.m03uf05final.APG;

import java.time.LocalDateTime;

public class Registre {
	private long id;
	private LocalDateTime dateTime;
	
	public Registre(String registre) {
		String[] regSplit = registre.split(",");
		id = Long.parseLong(regSplit[0]);
		dateTime = LocalDateTime.parse(regSplit[1]);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}

	@Override
	public String toString() {
		return "Registre [id=" + id + ", dateTime=" + dateTime + "]";
	}

	public Registre(long id, LocalDateTime dateTime) {
		super();
		this.id = id;
		this.dateTime = dateTime;
	}
	
}

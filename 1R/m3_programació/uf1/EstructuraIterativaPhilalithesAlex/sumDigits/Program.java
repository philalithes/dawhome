/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Determina el nombre de xifres d'un nombre
     * 
     * return xifres = nombre de xifres
     */
    
    public static int numberOfDigits(int n) {
        //variable declaration
        int xifres = 1;
        //calcul
        while (n > 9 || n < -9) {
            n = n / 10;
            xifres++;
        }
        return xifres;
    }
    
    /**
     * Determina la xifra de la posició donada
     * 
     * @param n int number
     * @param d digit position
     * 
     * @return digit from digit position
     */
    public int digitPosition(int n, int d){
        //variable declaration    
        int digit;
        //calculate return
        digit = (int)(n / Math.pow(10,d -1) % 10);
        //return
        return digit;
    }
    
    
    /*
     * Donat un nombre enter positiu, calcula la suma de les
     * seves xifres.
     * No posar zeros a l'esquerra.
     */
    
    public int sumDigits(int n){
        //variable declaration
        int acumulador = 0;
        int xifres = numberOfDigits(n);
        //calcul
        for (int i = 1; i <= xifres; i++) {
            acumulador += digitPosition(n,i);
        }
        return acumulador;
            
        
    }
}


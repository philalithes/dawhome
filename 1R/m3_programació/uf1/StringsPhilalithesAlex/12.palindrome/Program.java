/*
 * Program.java        2016/1/22
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     * Donada una cadena determina si és un palíndrom.
     * 
     * 
     */
    
    public boolean palindrome(String cadena) {
        //vars
        boolean palindrome = false;
        int i = 0;
        int indexOfLastChar = cadena.length() -1;
        boolean exit = false;
        //calc
        while (!exit && i < cadena.length() && i < indexOfLastChar) {
            if (cadena.charAt(i) != cadena.charAt(indexOfLastChar)) {
                exit = true;
            } else {
                i++;
                indexOfLastChar--;
            }
        }
        if (indexOfLastChar <= i) {
            palindrome = true;
        }
        return palindrome;
    }
    
}
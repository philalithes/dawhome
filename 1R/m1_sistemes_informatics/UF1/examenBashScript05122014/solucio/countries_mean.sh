#!/bin/bash 
# Fitxer: countries_mean.sh
# Autor: ordinari
# Data: 02/01/2015
# Versio: 0.1
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio: Script que calcula la mitjana aritmètica de la 2a i3a columna numèrica d'un fitxer estadístic


unsorted_file=countries.txt # emmagatzemem el nom del fitxer a ordenar


echo -n "La mitjana aritmètica de la columna 2 és: "
echo  "scale=2;($(cat $unsorted_file | cut -f2 |  paste -s -d+ )) / $(cat $unsorted_file | wc -l)" | bc -l
echo -n "La mitjana aritmètica de la columna 3 és: "
echo  "scale=2;($(cat $unsorted_file | cut -f3 |  paste -s -d+ )) / $(cat $unsorted_file | wc -l)" | bc -l

# Anem per la primera part: la suma de tots els valors numèrics de la 2a columna
# Amb cut -f 2 trobo aquests valors 
# Amb el paste que m'ensenya l'enunciat construeixo una cadena que expressa la suma de tots els valors.

# Amb wc -l tinc les línies d'un fitxer que en aquest cas coincideix amb la quantitat de registres (paisos)
# Amb un echo i uns parèntesis pel numerador li envio al bc perquè m'ho calculi
# Afegeixo el scale=2 perquè només em mostri dos decimals


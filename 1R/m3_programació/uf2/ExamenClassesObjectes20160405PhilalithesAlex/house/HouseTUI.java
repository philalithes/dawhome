/*
 * HouseTUI.java        1.0 March 14, 2016
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package house;
import org.joda.time.DateTime;

public class HouseTUI {

     public static void main(String[] args) {
          House h1 = new House("01/02/1975");
          House h2 = new House("11/07/1901");
          House h3 = new House("29/11/2008");
          Room bedroom = new Room("BEDROOM",10,8);
          Room wc2 = new Room("WC2",3,5);
          Room wc1 = new Room("WC1",6,8);
          Room kitchen = new Room("KITCHEN",10,8);
          Room pantry = new Room("PANTRY",6,6);
          Room living_room = new Room("LIVING ROOM",19,8);
          h1.setAddress("carrer Diputació 64 Barcelona");
          h2.setAddress("carrer Mallorca 125 Barcelona");
          h3.setAddress("carrer Bonaplata 6 Barcelona");
          
          //house
          bedroom.setHouse(h1);
          wc2.setHouse(h1);
          wc1.setHouse(h1);
          kitchen.setHouse(h1);
          pantry.setHouse(h1);
          living_room.setHouse(h1);
          //exits
          bedroom.setRoomNorthExit(wc2);
          bedroom.setRoomEastExit(kitchen);
          wc2.setRoomSouthExit(bedroom);
          wc1.setRoomSouthExit(kitchen);
          kitchen.setRoomNorthExit(pantry);
          kitchen.setRoomEastExit(living_room);
          kitchen.setRoomWestExit(bedroom);
          pantry.setRoomSouthExit(kitchen);
          living_room.setRoomWestExit(kitchen);
          
          //report
          System.out.println("***********************************************");
          System.out.println("*********** ITV CASA **************************");
          System.out.println("***********************************************");
          h1.reportHouse();
          h2.reportHouse();
          h3.reportHouse();
          System.out.println("***********************************************");
          System.out.println("*********** MÉS GRAN QUE **********************");
          System.out.println("***********************************************");
          living_room.isBigger(bedroom);
          wc1.isBigger(wc2);
          System.out.println("***********************************************");
          System.out.println("*********** LLISTAT HABITACIONS ***************");
          System.out.println("***********************************************");
          bedroom.reportRoom();
          System.out.println("***********************************************");
          wc2.reportRoom();
          System.out.println("***********************************************");
          wc1.reportRoom();
          System.out.println("***********************************************");
          kitchen.reportRoom();
          System.out.println("***********************************************");
          pantry.reportRoom();
          System.out.println("***********************************************");
          living_room.reportRoom();
          System.out.println("***********************************************");
          System.out.println("***********************************************");
          System.out.println("*********** METRES ****************************");
          System.out.println("***********************************************");
          bedroom.fewMetersBetweenTwoJointRooms(living_room);
          bedroom.fewMetersBetweenTwoJointRooms(wc2);
          living_room.fewMetersBetweenTwoJointRooms(kitchen);
     }
}
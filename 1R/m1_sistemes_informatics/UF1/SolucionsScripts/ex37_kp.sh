#!/bin/bash
# Fitxer: ex37_kp_comments.sh
# Autor: jamoros
# Data: 05/04/2011
# Versio: 0.1
# Descripcio: script que suma les mides de tots els fitxers que se li passen com a arguments
#			donant un error per a tots aquells arguments que no existeixin
#			o que siguin directoris.



# Suposarem el següent: 
#			Li estem proporcionant la ruta, ja sigui absoluta o relativa, per tant no ha de fer búsquedes.
#			Enviarem l'error a stderr
# 			Mostrarem error per a tots els fitxers que no siguin regulars, nonomés directoris


# Inicialitzo a zero una variable acumuladora on tindré les sumes parcials de les mides dels fitxers

mida_total_fitxers=0  # unitat de mida dels fitxers: 1 byte

# Mentre hi hagi arguments, emmagatzemo el primer argument
while [ $# -gt 0 ]
do
 	# Si no el trobo, mostro missatge d'error
 	if [ ! -e $1 ]
 	then
 		echo "el fitxer $1 no existeix" 2> /dev/stderr # Envio a la sortida stàndard de l'error
 		echo "repassa la trajectòria o la sintaxi" 2> /dev/stderr
 		
	# altrament, si no és un fitxer regular, mostro missatge d'error
	
 	elif [ ! -f $1 ]
 	then
 		echo "el fitxer $1 no és un fitxer regular" 2> /dev/stderr 
 		echo "no sumarem la seva mida" 2> /dev/stderr


	# altrament, calculo la seva mida i l'afegeixo al total
 	else
 		 mida_fitxer=$(wc -c $1 | cut -f1 -d' ')
 		 mida_total_fitxers=$((mida_total_fitxers + mida_fitxer))
	fi
	
	shift # desplaçament 
done

# Mostro el total
echo "la mida total és: $mida_total_fitxers bytes"



<<*
 Un bon test de prova per a cassos sense errors en els arguments seria el següent:

 ls   *.sh | xargs wc -c  # fent-ho en un directori on existix uns quants scripts
 La  darrera línia ens mostra el total
 Executem el nostre script a veure si coincideix

No oblidem de fer proves també per als cassos on no existeix el fitxer o no és regular

*

var Users = require('../model/users.js');



module.exports = function(app, passport, LocalStrategy) {
	login = function(req, res){
		console.log("Formulari login");
		res.render('login.jade');
	}

	app.get('/', login);
	app.get('/login', login);

	app.post('/login', passport.authenticate('local', {
    	successRedirect: '/loginSuccess',
    	failureRedirect: '/loginFailure'
  		})
	);

	app.get('/loginFailure', function(req, res, next) {
	  res.send('Failed to authenticate');
	});

	app.get('/loginSuccess', function(req, res, next) {
	  res.send('Successfully authenticated');
	});

	passport.serializeUser(function(user, done) {
	  done(null, user);
	});

	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});

	
	passport.use(new LocalStrategy(function(username, password, done) {
	console.log(username, password);
	  process.nextTick(function() {
	    Users.findOne({
	      'user': username,
	    }, function(err, user) {
	    	console.log("user "+user);
	      if (err) {
	        return done(err);
	      }

	      if (!user) {
	        return done(null, false);
	      }

	      if (user.password != password) {
	      	console.log(password);
	        return done(null, false);
	      }
	      return done(null, user);
	    });
	  });
	}));

}

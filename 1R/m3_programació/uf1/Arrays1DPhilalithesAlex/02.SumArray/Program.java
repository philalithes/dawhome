/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula de nombres reals, calcula la suma
     * dels seus elements.
     * 
     * 
     */
    
    public double sumArray(double[] taula) {
        //variable declaration
        double sum = 0;
        for (double x : taula) {
            sum += x;
        }
        return sum;
        
    }
}

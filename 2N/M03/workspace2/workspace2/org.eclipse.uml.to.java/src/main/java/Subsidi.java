/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Subsidi.
 * 
 * @author iaw47951368
 */
public class Subsidi {
	/**
	 * Description of the property import.
	 */
	public double import = 0D;
	
	// Start of user code (user defined attributes for Subsidi)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Subsidi() {
		// Start of user code constructor for Subsidi)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Subsidi)
	
	// End of user code
	/**
	 * Returns import.
	 * @return import 
	 */
	public double getImport() {
		return this.import;
	}
	
	/**
	 * Sets a value to attribute import. 
	 * @param newImport 
	 */
	public void setImport(double newImport) {
	    this.import = newImport;
	}



}

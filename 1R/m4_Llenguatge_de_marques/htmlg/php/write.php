<?php
    header ('Content-type: text/html; charset=UTF-8');
    // Create file
    $file = fopen("../data/file.txt", "w");
    fwrite($file, "first text line" . PHP_EOL);
    
    fwrite($file, "second file" . PHP_EOL);
    
    fclose($file);

    // Read file
    $file = fopen("../data/file.txt", "r");
    while (!feof($file)) {
        echo fgets($file) . "<br>";
    }
    fclose($file);

    // Append lines
    $file = fopen("../data/file.txt", "a");
    
    fwrite($file, "third line" . PHP_EOL);
    
    fwrite($file, "fourth line", PHP_EOL);

    fclose($file);
?>
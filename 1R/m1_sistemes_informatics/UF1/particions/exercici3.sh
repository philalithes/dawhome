#!/bin/bash
# Filename:			exercici3.sh
# Author:			iaw47951368
# Date:				01/03/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici3.sh [args...]
# Description:		Script que calcula el limit de credit, suma els imports i informa si s'ha excedit el limit o no.

#functions
function get_total_debt {
    cat $fitxer | grep "^[0-9]*:[0-9-]*:$1:" | cut -d":" -f8 
}

function get_credit_limit { #rep el num_clie i retorna el limit de credit-
cat $fitxer | grep "^$1:" | cut -d":" -f4 
}

#variables
clientes=clientes.txt
pedidos=pedidos.txt

#control d'arguments
if [ $# -ne 1 ]
then
	echo "error: sintaxi incorrecta"
	echo "ús: $0 num_clie"
	echo "on num_clie és un enter que representa el client"
	exit 1
fi

if [ -z $(echo "$1"|egrep "^[0-9]+$") ]
then
	 #l'argument no és un número enter
	echo "l'argument no és un número enter positiu"
	exit 2
fi

if [ -z $(get_credit_limit) ]
then
	echo "el client $1 no existeix a la nostra base de dades"
	exit 3
fi

if [ -z $(get_total_debt) ]
	echo "el client $1 no té pendent de pagament cap comanda"
	exit 4
fi

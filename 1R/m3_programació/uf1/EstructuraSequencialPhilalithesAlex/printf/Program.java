/*
 * ProgramTUI.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * pràctica de printf al moodle.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variables
        int a = 3;
        double b = 3.75;
        char c = 'j';
        double d = 15.6897;
        double e = 15.689754;
            
        System.out.printf("Variable a = %d\n", a);
        System.out.printf("Variable b = %f\n", b);
        System.out.printf("Variable c = %c\n", c);
        System.out.printf("Variable d = %f\n", d);
        System.out.printf("Variable e = %f\n\n", e);
        
        System.out.printf("EXEMPLES D'ÚS DE print, println I printf\n");
        
        System.out.printf("Variable 'a' .Nombre enter: %d\n", a);
        System.out.printf("Variable 'b' .Nombre amb decimals: %f\n", b);
        System.out.printf("Variable 'c' .Nombre amb decimals: %c\n", c);
        System.out.printf("Variable 'd' .Nombre amb 15 espais: %+15f", d);
        System.out.printf("Variable 'd' .Nombre amb 15 espais omplint amb 0: %015f\n", d);
        System.out.printf("Variable 'e' .Nombre amb 2 decimals: %.2f\n", e);
        System.out.printf("Variable 'd' .Nombre amb 15 espais i 2 decimals: %+15f", d);
        System.out.printf("Variable 'd' .Nombre amb 15 espais i 2 decimals omplint amb 0: %+15f", d);
        System.out.printf("Alineació a la dreta Més text. (30 espais)", );
        System.out.printf("Alineació a l'esquerra Més text. (30 espais)", );
        
    }
}

/*
 * Program.java        2015/6/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Convert celsius to fahrenheit
     * 
     * @param c celsius
     * 
     * 
     * @return f fahrenheit
     */
    public double celsius2fahrenheit(int c){
        //variable declaration    
        double f;
        //calculate return
        f =  c * 9.0 / 5 + 32;
        //return
        return f;
    }
    
}

-------------------------------------------------------------------------------
--          Modificacions
-------------------------------------------------------------------------------

-- 6.1- Inserir un nou venedor amb nom "Enric Jimenez" amb identificador 1012, oficina 18, títol "Dir Ventas", contracte d'1 de febrer del 2012, director 101 i vendes 0.

insert into repventas (num_empl, nombre, oficina_rep, titulo, contrato, director, ventas)
values (1012, 'Enric Jimenez', 18, 'Dir Ventas', '2012-02-01', 101, 0);

-- 6.2- Inserir un nou client "C1" i una nova comanda pel venedor anterior.

insert into clientes (num_clie, empresa, rep_clie) values (2200, 'C1', 1012);

insert into pedidos values (120000, '2012-02-01', 2200, 1012, 'rei', '2A45C', 1, 79);


-- 6.3- Inserir un nou venedor amb nom "Pere Mendoza" amb identificador 1013, contracte del 15 de agost del 2011 i vendes 0.

insert into repventas (num_empl, nombre, contrato, ventas) values (1013, 'Pere Mendoza', '2011-08-15', 0);


-- 6.4- Inserir un nou client "C2" omplint els mínims camps.

insert into clientes values (2201, 'C2', 1013, NULL);

-- 6.5- Inserir una nova comanda del client "C2" al venedor "Pere Mendoza" sense especificar la llista de camps pero si la de valors.

insert into pedidos values (120001, '2011-08-15', 2201, 1013, 'rei', '2A45C', 1, 79);


-- 6.6- Esborrar de la còpia de la base de dades el venedor afegit anteriorment anomenat "Enric Jimenez".

delete from repventas where nombre = 'Enric Jimenez';
   
-- 6.7- Eliminar totes les comandes del client "C1" afegit anteriorment.

delete from pedidos where clie = 2200;

-- 6.8- Esborrar totes les comandes d'abans del 15-11-1989.

 delete from pedidos where fecha_pedido < '1989-11-15';

-- 6.9- Esborrar tots els clients dels venedors: Adams, Jones i Roberts.

 delete from clientes where rep_clie IN (select num_empl from repventas where nombre ilike '%Adams' or nombre ilike '%Jones' or nombre ilike '%Roberts');

-- 6.10- Esborrar tots els venedors contractats abans del juliol del 1988 que encara no se'ls ha assignat una quota.

delete from repventas where contrato < '1988-07-1' and cuota is null;

-- 6.11- Esborrar totes les comandes.

delete from pedidos;

-- 6.12- Esborrar totes les comandes acceptades per la Sue Smith (cal tornar a disposar  de la taula pedidos).

delete from pedidos where rep = (select num_empl from repventas where nombre = 'Sue Smith');

-- 6.13- Suprimeix els clients atesos per venedors les vendes dels quals són inferiors al 80% de la seva quota.

delete from clientes where rep_clie in (select num_empl from repventas where ventas < 0.8 * cuota);

-- 6.14- Suprimir els venedors els quals el seu total de comandes actual (imports) és menor que el 2% de la seva quota.

delete from repventas where (select sum(importe) from pedidos where rep = num_empl) < 0.02 * cuota;


-- 6.15- Suprimeix els clients que no han realitzat comandes des del 10-11-1989.

delete from clientes where num_clie in (select clie from pedidos where fecha_pedido < '1989-11-10') or num_clie not in (select clie from pedidos);


-- 6.16- Eleva el límit de crèdit de l'empresa Acme Manufacturing a 60000 i la reassignes a Mary Jones.

update clientes set limite_credito=60000, rep_clie=(Select num_empl from repventas where nombre ilike 'Mary Jones') where empresa ilike 'Acme Mfg.';

-- 6.17- Transferir tots els venedors de l'oficina de Chicago (12) a la de Nova York (11), i rebaixa les seves quotes un 10%.

update repventas set oficina_rep=11, cuota=cuota-cuota*0.1 where oficina_rep = 12;
 
-- 6.18- Reassigna tots els clients atesos pels empleats 105, 106, 107, a l'emleat 102.

update clientes set rep_clie=102 where rep_clie in (105, 106, 107);

-- 6.19- Assigna una quota de 100000 a tots aquells venedors que actualment no tenen quota.

update repventas set cuota=100000 where cuota is null;

-- 6.20- Eleva totes les quotes un 5%.

update repventas set cuota=cuota+cuota*0.05;


-- 6.21- Eleva en 5000 el límit de crèdit de qualsevol client que ha fet una comanda d'import superior a 25000.

update clientes set limite_credito = limite_credito+5000 where exists (select * from pedidos where importe > 25000 and clie = num_clie);


-- 6.22- Reassigna tots els clients atesos pels venedors les vendes dels quals són menors al 80% de les seves quotes. Reassignar al venedor 105.

update clientes set rep_clie = 105 where rep_clie in (select num_empl from repventas where ventas < 0.8*cuota);

  
-- 6.23- Fer que tots els venedors que atenen a més de tres clients estiguin sota de les ordres de Sam Clark (106).

update repventas set director = (select num_empl where nombre ilike 'Sam Clark') where (select count(rep_clie) from clientes where rep_clie = num_empl group by rep_clie) > 3;

-- 6.24- Augmentar un 50% el límit de credit d'aquells clients que totes les seves comandes tenen un import major a 30000.

update clientes set limite_credito=limite_credito*0.5+limite_credito where not exists (select * from pedidos where importe <= 30000 and clie=num_clie) and 1 <= (select count(clie) from pedidos where clie=num_clie);


-- 6.25- Disminuir un 2% el preu d'aquells productes que tenen un estoc superior a 200 i no han tingut comandes.


-- 6.26- Establir un nou objectiu per aquelles oficines en que l'objectiu actual sigui inferior a les vendes. Aquest nou objectiu serà el doble de la suma de les vendes dels treballadors assignats a l'oficina.


-- 6.27- Modificar la quota dels directors d'oficina que tinguin una quota superior a la quota d'algun empleat de la seva oficina. Aquests directors han de tenir la mateixa quota que l'empleat de la seva oficina que tingui una quota menor.

update repventas d set cuota = (select min(cuota) from repventas e where oficina_rep in (select oficina from oficinas where dir = d.num_empl)) where num_empl in (select dir from oficinas) and d.cuota > any (select cuota from repventas e where oficina.rep in (select oficina from oficinas where dir=d.num_empl))

-- 6.28- Cal que els 5 clients amb un total de compres (cantidad) més alt siguin transferits a l'empleat Tom Snyder i que se'ls augmenti el límit de crèdit en un 50%.


-- 6.29- Es volen donar de baixa els productes dels que no tenen existències i alhora no se n'ha venut cap des de l'any 89.

delete from productos where existencias = 0 and not id_producto in (select producto from pedidos where id_fab = fab and fecha_pedido > '1989-01-01');

-- 6.30- Afegir una oficina de la ciutat de "San Francisco", regió oest, el director ha de ser "Larry Fitch", les vendes 0, l'objectiu ha de ser la mitja de l'objectiu de les oficines de l'oest i l'identificador de l'oficina ha de ser el següent valor després del valor més alt.

 insert into oficinas (oficina, ciudad, region, dir, objetivo, ventas) values ((select max(oficina + 1) from oficinas), 'San Francisco', 'Oeste',(select num_empl from repventas where nombre ilike 'Larry Fitch'),(select avg(objetivo) from oficinas), 0);

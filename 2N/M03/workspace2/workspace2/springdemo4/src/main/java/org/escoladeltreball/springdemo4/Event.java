package org.escoladeltreball.springdemo4;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "events")
public class Event {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date eventDate;
	@Column(length = 64)
	private String comment;
	
	public Event() {
		// TODO Auto-generated constructor stub
	}

	public Event(String comment) {
		this(null, new Date(System.currentTimeMillis()), comment);
	}
	
	public Event(Long id, Date eventDate, String comment) {
		super();
		this.id = id;
		this.eventDate = eventDate;
		this.comment = comment;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", eventDate=" + eventDate + ", comment=" + comment + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}

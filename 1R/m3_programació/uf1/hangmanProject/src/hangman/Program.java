/*
 * Program.java        1.0 17/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package hangman;

import java.util.Random;
import java.util.Scanner;

public class Program {

    /**
     * Builds a string with as many "-" as a secretWord length.
     * 
     * @param secretWord a word
     * @return the hidden word
     */
    public String hide(String secretWord) {
        String hidden = "";
        for (int i = 0; i < secretWord.length(); i++)
            hidden += "-";
        return hidden;
    }

    /**
     * Builds a string with the hiddenWord refresehed if the letter is in secret word.
     * 
     * @param secretWord a secret word
     * @param hiddenWord teh corresponding hiddwen word
     * @param letter a letter
     * @return a new refreshed hidden word
     */
    public String refresh(String secretWord, String hiddenWord, String letter) {
        String hintString = "";
        for (int i = 0; i < secretWord.length(); i++) {
            char c = secretWord.charAt(i);
            if (String.valueOf(c).equals(letter)) {
                hintString += c;
            } else {
                hintString += hiddenWord.charAt(i);
            }
        }
        return hintString;
    }

    /**
     * Main program.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        Program p = new Program();
        Scanner sc = new Scanner(System.in);
        String[] words = { "gat", "gos", "gallina", "vaca" };
        Random r = new Random();
        int secretPosition = r.nextInt(words.length);
        String secretWord = words[secretPosition];
        String hiddenWord = p.hide(secretWord);
        int maxGuesses = 10, i = 1;
        boolean win = false;
        System.out.println("JOC DEL PENJAT");
        System.out.println("==============\n");
        System.out.println(hiddenWord);
        System.out.print("Lletra ? ");
        String letter = sc.next();
        while (i < maxGuesses && !win) {
            if (secretWord.contains(letter)) {
                hiddenWord = p.refresh(secretWord, hiddenWord, letter);
            }
            if (secretWord.equals(hiddenWord)) {
                win = true;
            } else {
                System.out.println();
                System.out.println(hiddenWord);
                System.out.print("Lletra ? ");
                letter = sc.next();
                i++;
            }
        }
        if (win)
            System.out.println("\nHas guanyat amb " + i + " intents.");
        else
            System.out.println("\nHas perdut. La paraula secreta era: " + secretWord);
        sc.close();

    }

}

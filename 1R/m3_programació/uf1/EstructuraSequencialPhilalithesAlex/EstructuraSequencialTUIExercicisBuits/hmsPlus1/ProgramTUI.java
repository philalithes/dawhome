/*
 * ProgramTUI.java        2015/6/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramArea {

    /**
     * Calculate area of a triangle
     * 
     * @param a length of side a
     * @param b length of side b
     * @param c length of side c
     * @return area of the triangle
     */
    public double add(double a, double b){
        //variable declaration    
        double retorno;
        //calculate return
        retorno = a * b / 2;
    }

    /**
     * Modelizes the UI as a TUI.
     * 
     * @param args not used.
     */
    public static void main(String[] args) {
        
    }
}

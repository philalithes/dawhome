create or replace function capicua(cadena varchar(20)) returns boolean as $$
	declare
		i int;
	begin
		--recorrem caracters
		for i in 1..char_length(cadena) loop
			--comparem el caracter de la posició i amb el de la mateixa posició comptant pel final.
			if upper(substring(cadena from i for 1)) != upper(substring(cadena from length(cadena) +1 -i for 1)) then
				--si no coincideixen surt false.
				return false;
			end if;
		end loop;
		--arribat aquí tots els caràcters coincideixen.
		return true;
	end;
$$ language plpgsql;

#!/bin/bash
# Filename:			uninstall_postgresql.sh
# Author:			iaw47951368
# Date:				29/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./uninstall_postgresql.sh [args...]
# Description:		Desinstala postgres.

su -c "systemctl stop postgresql; yum remove -y postgresql-server postgresql; rm -rf /var/lib/pgsql/data"


/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donat un nombre enter positiu, determina si és un nombre perfecte o no. 
     * Un nombre enter és perfecte si és positiu i és igual a la 
     * suma dels seus divisors positius, excepte ell mateix.
     * 
     * int n = nombre
     * 
     * return true = es perfecte
     * return false = no es perfecte
     */
    
    public boolean isPerfectNumber(int n) {
        //variable declaration
        boolean perfect = false;
        int acumulador = 0;
        int i = 0;
        //calcul
        if (n > 0) {
            for (i = n-1; i >= 1; i--) {
                if (n % i == 0) {
                    acumulador += i;
                }
            }
            if (n == acumulador) {
                perfect = true;
            }
            else {
                perfect = false;
            }
        }
        else if (n == 0) {
            System.out.println("0 no és divisible");
        }
        else {
            System.out.println("escriu un nombre positiu");
        }
        return perfect;
    }
}

/*
 * HolaMon.java
 * 
 * Programa "Hola món!" en Java
 * 
 * Copyright 2010 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 * 
 */


public class HolaMon {
	public static void main (String args[]) {
		System.out.println("Hola món!");		
	}
}

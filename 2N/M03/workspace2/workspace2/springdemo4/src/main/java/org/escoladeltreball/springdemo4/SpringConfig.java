package org.escoladeltreball.springdemo4;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan
@EnableAspectJAutoProxy(proxyTargetClass=true)
@PropertySource("classpath:/application.properties")
@EnableJpaRepositories(basePackages = "org.escoladeltreball.springdemo4") //no cal dir
public class SpringConfig {

	public SpringConfig() {
		// TODO Auto-generated constructor stub
	}

}

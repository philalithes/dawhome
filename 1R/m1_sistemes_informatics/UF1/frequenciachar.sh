#!/bin/bash
# Filename:			frequenciachar.sh
# Author:			iaw47951368
# Date:				04/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./frequenciachar.sh [args...]
# Description:		Script que detecta la frequència de cada caràcter de l'entrada estàndar.

declare -a array
array=$(echo {a..z})
text=$(cat)
size=${#text}
echo "text=$text  size=$size"
echo -e "CHAR\tFREQ\n"
for i in $array
do
repeticions=0
#La letra es $i	
	for j in $(seq 0 $size)
	do
	
#Si $i == ${text:j:1}
		if [ "$i" = "${text:$j:1}" ]
		then
			repeticions=$(($repeticions+1))
		fi
	done
	if [ "$repeticions" -gt 0 ]
	then
		echo -e "$i\t$repeticions"
	fi
done


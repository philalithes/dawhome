package org.escoladeltreball.jpaprojectdemo2;

import java.io.Serializable;
import java.lang.Double;
import java.lang.Long;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Record
 *
 */
@Entity
@Table(name="record")

public class Record implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Double grade;
	//inversa
	@OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REFRESH, CascadeType.MERGE},mappedBy = "record")
	private Student student;
	private static final long serialVersionUID = 1L;
	

	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}

	public Record(Double grade) {
		super();
		this.grade = grade;
	}
	@Override
	public String toString() {
		return "Record [id=" + id + ", grade=" + grade + ", student=" + student + "]";
	}
	public Record() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public Double getGrade() {
		return this.grade;
	}

	public void setGrade(Double grade) {
		this.grade = grade;
	}
   
}

package org.escoladeltreball.m03uf06nf02.APG;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("JPAProjectDemo2");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.getTransaction().commit();
		
		em.close();
		emf.close();
		System.out.println("ok");
	}

}

directoriactual=$(echo "0") # variable que farem servir per saber si hem de crear el fitxer clase.txt en el directori actual o en un altre
fitxer=detall_animals.txt

if [ $# -eq 1 ] || [ $# -gt 1 ] #mirem si el nombre de arguments es 1, si es aixi tot be
then 
	if [ $# -gt 1 ]
	then
		echo "nomes es mirara el primer parametre"
	fi
	if find -maxdepth 0 -type f -name $fitxer 2>/dev/null #busquem el fitxer en el directeri actual, en el cas que existeixi podrem seguir amb el script
	then
		echo "el fitxer clase.txt es creara al actual directori [s/n]" # li comuniquem que el fitxer es creara en el directori actual
		read afavor # guardem la seva resposta
		if [ $afavor = n ] || [ $afavor = N ] #mirem si el vol guardar o no, en cas que no vulgui li preguntem a on el vol guardar i ho guardem
		then
			echo "a quin directori el vols?"
			directoriactual=$(echo "1")  
			read nomdirectori # mirem el nom del directori, si no hi ha introduit un nom es mostrara un error, si ho ha fet seguira el script
			if test -z "$nomdirectori"
			then
				echo "no hi ha cap directori"
				exit 3
		
			fi
		fi
		#$directoriactual  ara sera 1 o 0, si es 0 vol dir que el fitxer s'ha de crear en el directori actual si es 1 en un altre directori
		if test $directoriactual -eq 0
		then
			ruta=$(pwd) # per guardar el directori per mostrar-lo despres
			cat $fitxer | cut -d":" -f1,2 | grep -w $1 | sort > seguro.txt 
			echo " ja esta creat el fixter"
		else # com es a un altre directori, busquem la ruta absoluta i la guardem
			ruta=$(find / -type d -name "$nomdirectori")  2> /dev/null #busquem la ruta del director  a on ho vol guardar	
			cat $fitxer | cut -d":" -f1,2 | grep -w $1 | sort > $ruta/seguro.txt #ordenem el fitxer i fem un nou
			echo "fitxer creat"	
		fi	
		# ara mostrarem el numero de animals, a on esta guardat el script i la clase rebuda
		echo "clase rebuda $1"
		animals=seguro.txt
		animals=$(cat $animals | wc -l | cut -d" " -f1)
		echo "el numero de animals es $animals"
		echo "la ruta a on s'ha guardatr el arxiu es $ruta"
	else
		echo "no es troba el fitxer" # en el cas que no el trobi, sortirem del script i ho notificarem
		exit 2
	fi
	
elif [ -z $# ] #com per força nomes queda que no envii res, li notifiquem que esta malament i li donem una ajuda
then
	echo "nombre malament"
	echo "./animals.sh arg1"
	exit 1
fi


	


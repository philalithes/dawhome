/*
 * Program.java        2015
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * boolean isSubstring (str1, str2)
     * Determina si str2 pertenece a str1.
     * Devuelve true si pertenece. Devuelve false si NO pertenece.
     * 
     * Este metodo recorre str1 y compara con los chars de str2.
     */
      public int isSubstring(String str1, String str2){
            int ret = -1;
            //Descomponer strs en char arrays
            char[] cstr1 = str1.toCharArray();
            char[] cstr2 = str2.toCharArray();
            //Recorrer str1 salvaguardando el limite final
            for(int i = 0; i < str1.length() - str2.length(); i++){
                  boolean parcial = true;
                  //Recorrido interno de la substring
                  for(int j = 0; j < str2.length(); j++) {
                        if(cstr1[i + j] != cstr2[j]){
                              parcial = parcial && false;
                        }
                  }
                  if(parcial == true){
                        ret = i;
                  }
            }
            return ret;
      }      
}

package org.escoladeltreball.springdemo3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) { 
		AnnotationConfigApplicationContext ctx = 
				new AnnotationConfigApplicationContext();
		ctx.getEnvironment().setActiveProfiles("production");
		ctx.register(SpringConfig.class);
		ctx.refresh();
		Vehicle vehicle = ctx.getBean("vermell", Vehicle.class);
		vehicle.getMotor().start();
		
		System.out.println(vehicle);
		System.out.println("ok");
		
//		Motor motor = new Motor(100);
//		ProxyEngine proxy = new ProxyEngine(motor);
//		proxy.start();
	}

}

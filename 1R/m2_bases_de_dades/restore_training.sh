#!/bin/bash

if [ -z "$1" ]; then
    echo "USAGE: restore_training.sh user"
    exit
fi

psql template1 -c 'DROP DATABASE IF EXISTS training';
psql template1 -c 'CREATE DATABASE training';
psql training < ./training.sql;

/*
 * ProgramTUI.java        1.0 Oct 30, 2016
 *
 * Models the program.
 *
 * Copyright 2016 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Arrays;
import java.util.Scanner;

public class ProgramTUI {

    /**
    * creates white character
    * 
    * @param array containing white character 
    */
    public void whiteLetter(char letter[][]) {
        //Omplir l'array amb caràcters en blanc
        //variables
        int i = 0;
        int j = 0;
        //recorregut
        for (i = 0; i < letter.length; i++) { //Recorregut per files
            for (j = 0; j < letter[0].length; j++) { //columnes
                letter[i][j] = ' ';
            }
        }
     }
    
     
    /**
    * shows a letter
    * 
    * @param array containing a letter 
    */ 
    public void viewLetter(char letter[][]) {
        //variables
        int i = 0;
        //Mostrar l'array per pantalla
        for (i = 0; i < letter.length; i++) { //recorre les files per mostrar les column
            System.out.println(Arrays.toString(letter[i]));
        }
        //posem noves linies per separar les lletres.
        System.out.println();
     
     }
    
    /**
    * builds the letter L
    * 
    * @param array containing a letter L 
    */  
    public void letterL(char letter[][]) {
        //Construir la lletra L amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[4][j] = '*';
             }
             
         }
     }
    
    /**
    * builds the letter O
    * 
    * @param array containing a letter O 
    */ 
    public void letterO(char letter[][]) {
        //Construir la lletra O amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[0][j] = '*';
                 letter[4][j] = '*';
             }
             
         }
     }
     
    /**
    * builds the letter H
    * 
    * @param array containing a letter H 
    */
     public void letterH(char letter[][]) {
         //Construir la lletra H amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[2][j] = '*';
             }
             
         }
     }
     
    /**
    * builds the letter A
    * 
    * @param array containing a letter A 
    */
     public void letterA(char letter[][]) {
       //Construir la lletra H amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[0][j] = '*';
                 letter[3][j] = '*';
             }
             
         }
     }
     
     /**
     * generates a 4 letter word
     * 
     * @param the first letter of the word
     * @param the second letter of the word
     * @param the third letter of the word
     * @param the fourth letter of the word
     * 
     * @return an array with de word
     */
     public char[][] insertWordWithoutDivison(char letter1[][],char letter2[][],char letter3[][],char letter4[][]) {
         //variables
         char[][] word = new char[5][20];
         int i = 0;
         int j = 0;
         //construcció de la paraula.
         for (i = 0; i < word.length; i++) {
             for (j = 0; j < word[0].length; j++) {
                 if (j < 5) {
                     word[i][j] = letter1[i][j]; //col.loquem la lletra H
                 }
                 else if (j < 10) {
                     word[i][j] = letter2[i][j-5]; //col.loquem la lletra O
                 }
                 else if (j < 15) {
                     word[i][j] = letter3[i][j-10]; //col.loquem la lletra L
                 }
                 else {
                     word[i][j] = letter4[i][j-15]; //col.loquem la lletra A
                 }
             }
         }
         return word;
     }
    
    
    /**
    * generates a 4 letter word with a space between letters
    * 
    * @param the first letter of the word
    * @param the second letter of the word
    * @param the third letter of the word
    * @param the fourth letter of the word
    * 
    * @return an array with de word with a space between letters
    */ 
     public char[][] insertWordWithOneDivision(char letter1[][],char letter2[][],char letter3[][],char letter4[][]) {
         //variables
         char[][] word = new char[5][23];
         int i = 0;
         int j = 0;
         //construcció de la paraula.
         for (i = 0; i < word.length; i++) {
             for (j = 0; j < word[0].length; j++) {
                 if (j < 5) {
                     word[i][j] = letter1[i][j]; //col.loquem la lletra H
                 }
                 else if (j == 5) {
                     word[i][j] = ' '; //col.loquem espais.
                 }
                 else if (j < 11) {
                     word[i][j] = letter2[i][j-6]; //col.loquem la lletra O
                 }
                 else if (j == 11) {
                     word[i][j] = ' '; //col.loquem espais.
                 }
                 else if (j < 17) {
                     word[i][j] = letter3[i][j-12]; //col.loquem la lletra L
                 }
                 else if (j == 17) {
                     word[i][j] = ' '; //col.loquem espais.
                 }
                 else {
                     word[i][j] = letter4[i][j-18]; //col.loquem la lletra A
                 }
             }
         }
         return word;
     }
     
     /**
      * shows a word
      * 
      * @param array containing a word 
      */
     public void viewWord (char word[][]) {
         //variables
         int i = 0;
         int j = 0;
         //Mostrar l'array per pantalla
         for (i = 0; i < word.length; i++) {
             for (j = 0; j < word[0].length; j++) {
                 System.out.print(word[i][j]);
                 if (j == word[0].length-1) {
                     System.out.println();
                 }
             }
         }
     }
     
     public static void main(String[] args) {
         ProgramTUI p = new ProgramTUI();
         Scanner sc = new Scanner(System.in);
         //constant que determina el tamany de les matrius.
         int SIZE = 5;
         //variables.
         char letterH [][] = new char [SIZE][SIZE];
         char letterO [][] = new char [SIZE][SIZE];
         char letterL [][] = new char [SIZE][SIZE];
         char letterA [][] = new char [SIZE][SIZE];
         char word [][];
         int opcio = 0;
         //construcció de lletres.
         p.whiteLetter(letterH);
         p.letterH(letterH);
         p.whiteLetter(letterO);
         p.letterO(letterO);
         p.whiteLetter(letterL);
         p.letterL(letterL);
         p.whiteLetter(letterA);
         p.letterA(letterA);
         //Pregunta: de quina manera vol mostrar la paraula?
         System.out.println("Com vols mostrar la paraula 'HOLA'?");
         System.out.println("1) Sense espais entre lletres");
         System.out.println("2) Amb espais entre lletres");
         //resposta
         opcio = sc.nextInt();
         //Mostrar la paraula segons la opció escollida
         if (opcio == 1) { //Mostrar HOLA sense espais.
             //es construeix l'array sense espais.
             word = p.insertWordWithoutDivison(letterH,letterO,letterL,letterA);
             //mostra l'array.
             p.viewWord(word);
         } else { //mostrar HOLA amb espais
             //es construeix l'array amb espais.
             word = p.insertWordWithOneDivision(letterH,letterO,letterL,letterA);
             //mostra l'array
             p.viewWord(word);
         }
         
     }
}

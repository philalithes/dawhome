/*
 * ProgramTUI.java        VERSION DATE
 *
 * Models the program.
 *
 * Copyright YEAR FULL_NAME <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {

    /**
     * Convert a number of seconds into hours, minutes and seconds.
     * 
     * @param s a number of seconds.
     * @return a string with the format hours:minutes:seconds
     */
    public String s2hms(int s) {
        
    }

    /**
     * Modelizes the UI as a TUI.
     * 
     * @param args not used.
     */
    public static void main(String[] args) {
        
    }

}

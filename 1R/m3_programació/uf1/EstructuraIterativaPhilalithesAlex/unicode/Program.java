/*
 * Program.java        2015/2/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombres enters positius n1 i n2 amb n1 < n2, 
     * escriu els caràcters Unicode de dins de l'interval [n1, n2] en ordre creixent.
     * 
     */

    public void writeUnicode(int n1, int n2) {
        //variable declaration
        
        //algoritme
        while (n1 <= n2) {
            System.out.println(n1);
            System.out.println((char)n1);
            n1++;
            
        }
    }
    
    
}

/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèric d'enters, escriu
     * cada element de la taula junt amb el seu quadrat i el seu cub.
     * 
     * 
     */
    
    public void writeSquareCube(double[] array) {
        //variable declaration
        String sqcu = "";
        double square = 0;
        double cube = 0;
        //calcul
        for (int i = 0; i < array.length; i++) {
            square = Math.pow(array[i],2);
            cube = Math.pow(array[i],3);
            sqcu = "num " + i + ": " + array[i] + " cuadrat: " + square + " cub: " + cube;
            System.out.println(sqcu);
        }
    }
}

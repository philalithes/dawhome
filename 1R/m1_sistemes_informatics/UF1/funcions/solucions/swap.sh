#!/bin/bash
# Filename:		swap.sh
# Author:		pingui
# Date:			16/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./swap.sh [arg1...]
# Description:	Script que crida a diverses funcions swap per intercanviar 2 valors fixos
# 				L'script ens mostrarà els valors abans i després de cada swap.



function swap_1 {
	local temp=${!1}
	eval $1=${!2}
	eval $2=$temp
}

function swap_2 {
	eval local temp=\$$1
	eval $1=\$$2
	eval $2=$temp
}


function swap_3 {
	read a b <<< "${!2} ${!1}"
}

function swap_4 {
	read a b <<< $(eval echo \$$2 \$$1)
}

a=1
b=30
echo "a=$a b=$b"
swap_1 a b
echo "a=$a b=$b"
swap_2 a b
echo "a=$a b=$b"
swap_3 a b
echo "a=$a b=$b"
swap_4 a b
echo "a=$a b=$b



# TAGS: eval, indirect expansion, HERE STRING
# <<< HERE STRING
# a <<< 


#!/bin/bash 
# Fitxer: ordena.sh
# Autor: ordinari
# Data: 02/01/2015
# Versio: 0.1
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio:	Script que ordena alfabèticament (a,b,c,...) els registres del fitxer countries.txt
#		Si rep com a argument la cadena --desc ordenarà el fitxer en ordre invers o descendent (z,y,x...)
#		Fem control d'errors a l'entrada


unsorted_file=countries.txt # emmagatzemem el nom del fitxer a ordenar


# Fem un control dels arguments d'entrada
if [ $# -gt 1 ]     # Si el nombre d'arguments és superior a 1 ---> missatge d'error, ajuda i informem al sistema sortint
then
	echo "error: invalid arguments number" 
	echo "Ús: ./ordena.sh  [--desc]"
	exit 1
elif [ $# -eq 0 ]
then
	order_ascend=true # Hem escollit ordre ascendent (és l'opció per defecte)
elif [ "$1" = "--desc" ]  # si arribem a aquesta línia per força hi ha un únic argument
then
	order_ascend=false # Hem escollit ordre descendent
else
	echo "error: invalid option $1"    # Si l'únic argument que li passem és diferent de la cadena '--desc' --->  1 missatge d'error, ajuda i informem al sistema sortint
	echo "Ús: ./ordena.sh  [--desc]"
	exit 2
fi


# Si hem rebut l'argument --desc ordenarem a l'inversa, en cas contrari l'ordre per defecte (ascendent)

if  $order_ascend
then
	sort $unsorted_file
else
	sort -r $unsorted_file
fi
	

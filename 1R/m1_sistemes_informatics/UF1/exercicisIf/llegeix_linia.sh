#!/bin/bash
# Filename:			exercici3
# Author:			iaw47951368
# Date:				10/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici3 [args...]
# Description:		llegeix una linea i mostra el missatge.

echo "escriu el que vuilguis i prem intro"
read string
if [ $? -eq 0 ]
then
echo $string
else
echo "no s'ha introduit cap línia"
fi

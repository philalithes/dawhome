package org.escoladeltreball.springdemo3;

public class ProxyEngine implements Engine {

	private Motor motor;
	
	public ProxyEngine(Motor motor) {
		this.motor = motor;
	}

	public void start() {
		System.out.println("comprovant temperatura");
		motor.start();
		System.out.println("comprovant oli");
		
	}

	public void stop() {
		

		motor.stop();
		
	}

}

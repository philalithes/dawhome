package org.escoladeltreball.bcryptpasswordencoderdemo;

public class Main {

	public static void main(String[] args) {
		BCryptPasswordEncoder encoder = new BcryptPasswordEncoder();
		String s = encoder.encode("password");
		System.out.println(s);
	}

}

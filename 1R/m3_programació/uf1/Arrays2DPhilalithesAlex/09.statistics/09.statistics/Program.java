/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Un alumne de DAM desitja realitzar una estadística de les
     * hores d'estudi mensuals dedicades a cadascuna de les seves assignatures. Fer un mètode
     * que ens permeti calcular:
     * a. El total anual d'hores dedicades a cada assignatura
     * b. El total mensual d'hores dedicades a estudiar
     * El mètode rebrà com a paràmetre un array bidimensional on les files representen les
     * assigantures i les columnes les hores estudiades cada mes per a cada assignatura. El
     * mètode haurà d'escriure la següent taula per pantalla:
     *       gener febrer març abril maig juny juliol agost setembre octubre novembre desembre total
     * SO
     * PROG
     * BBDD
     * XML
     * FOL
     * EMP
     * total
     * 
     */
    
    public void statistics(int[][] matrix) {
        //variables
        int i, j = 0;
        int[][] taula = new int[7][13];
        int totalColumn = 0;
        String materia = "";
        //calcul
        for (i = 0; i < matrix.length; i++) {
            for (j = 0; j < matrix[0].length; j++) {
                taula[i][j] = matrix[i][j];
                taula[i][12] += matrix[i][j];
                taula[6][j] += matrix[i][j];
            }
        }
        //totalHores
        
        
        for (j = 0; j < taula.length; j++) {
            totalColumn += taula[j][12];
        }
        taula[6][12] = totalColumn;
        
        
        System.out.println("      g, f, m, a, m, j, j, a, s, o, n, d, total");
        for (i = 0; i < taula.length; i++) {
            if (i == 0) {
            materia = "SO   ";
        }
        else if (i == 1) {
            materia = "PROG ";
        }
        else if (i == 2) {
            materia = "BBDD ";
        }
        else if (i == 3) {
            materia = "XML  ";
        }
        else if (i == 4) {
            materia = "FOL  ";
        }
        else if (i == 5) {
            materia = "EMP  ";
        }
        else {
            materia = "total";
        }
            System.out.println(materia + Arrays.toString(taula[i]));
        }
    }
}


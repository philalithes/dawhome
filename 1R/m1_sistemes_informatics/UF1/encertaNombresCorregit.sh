#!/bin/bash
# Filename:		encertaNombresCorregit.sh
# Author:		jamoros
# Date:			21/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./encertaNombresCorregit.sh
# Description:		Un senzill joc per encertar nombres. L'usuari entra números fins que l'encerta
# 			amb un màxim fix d'intents.
# 			L'script li diu si l'ha encertat (acabant)
# 			o és més petit o més gran fins que no arribi al màxim d'intents.

maxim=100	# nombre màxim
intent=0	# nombre que proposa el jugador
num_intents=0	# nombre d'intents fets perl jugador
nombre=$(( $RANDOM % $maxim ))	# nombre aleatori entre 1 i $maxim


echo "Has d'endevinar en quin nombre enter estic pensant entre 0 i $maxim"
read intent

while [ $intent -ne $nombre ]
do

	if [ "$intent" -lt $nombre ]
	then
		echo "el nombre és més gran que $intent!"
	elif [ "$intent" -gt $nombre ]
	then
		echo "el nombre és més petit que $intent!"
	fi

	echo -n "Intenta -ho, de nou "
	read intent
	num_intents=$(($num_intents + 1))
done
echo "Correcte!! $nombre acertat en $num_intents intents."
exit 0


# No controlem si el que entra l'usuari és un número o no
# Es podria fer

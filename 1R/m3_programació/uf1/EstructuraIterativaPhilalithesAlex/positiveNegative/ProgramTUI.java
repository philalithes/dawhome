/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de nombres enters
     * acabada en zero i escriu si hi ha més nombres positius que negatius.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int pos = 0;
        int neg = 0;
        int n = 0;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una seqüència de nombres enters acabada en 0 i diré si hi ha més negatius o positius");
        //càlculs
        do {
            System.out.println("Escriu un nombre enter:");
            n = sc.nextInt();
            if (n > 0) {
                pos++;
            }
            if (n < 0) {
                neg++;
            }
        } while (n != 0);
        if (pos > neg) {
            System.out.println("Hi ha més nombres positius que negatius.");
        }
        else if (pos < neg) {
            System.out.println("Hi ha més nombres negatius que positius.");
        }
        else {
            System.out.println("Hi ha tants nombres negatius com positius.");
        }
        
        
        
    }
}


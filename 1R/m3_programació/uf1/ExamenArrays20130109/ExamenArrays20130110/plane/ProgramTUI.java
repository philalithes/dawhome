/*
 * Program.java        1.0 10/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
    
    /**
     * Books the seat with minimum number respecting the user preferences (if he/she want to be next to a window or
     * not).
     * 
     * @param plane an array where each cell represents a seat. If the cell is true, the seat is booked, if the cell is
     *        false, the seat is free.
     * @param window true if the user wants to be next to a window, false if the user doesn't want to be next to a
     *        window.
     * @return the number of the booked seat if a reservation is possible respecting the user preferences or -1 if a
     *         reservation is not possible.
     */
    public int bookSeat(boolean[] plane, boolean window) {
        //variables
        int bookSeat = -1;
        boolean booked = false;
        int i = 0;
        //calcul
        if (window) {
            while (!booked && i < plane.length) {
                if (i % 6 == 0 || i % 6 == 5) {
                    if (!plane[i]) {
                        bookSeat = i;
                        plane[i] = true;
                        booked = true;
                    }
                }
                i++;
            }
        } else {
            while (!booked && i < plane.length) {
                if (i % 6 != 0 && i % 6 != 5) {
                    if (!plane[i]) {
                        bookSeat = i;
                        plane[i] = true;
                        booked = true;
                    }
                }
                i++;
            }
        }
    
    return bookSeat;
}

/**
 * Prints the plane showing the booked seats.
 * 
     * @param plane an array where each cell represents a seat. If the cell is true, the seat is booked, if the cell is
     *        false, the seat is free.
     */
    public void showPlane(boolean[] plane) {
        for (int i = 0; i < plane.length; i++) {
            String seat = plane[i] ? "X" : "-";
            System.out.print(seat + " ");
            if (i % 6 == 2) {
                System.out.print("  ");
            } else if (i % 6 == 5) {
                System.out.println("");
            }
        }
    }

    /**
     * Main program.
     * 
     * @param args Not used
     */
    public static void main(String[] args) {
        int N_SEATS = 54;
        boolean[] plane = new boolean[N_SEATS];
        ProgramTUI p = new ProgramTUI();
        Scanner sc = new Scanner(System.in);
        System.out.println("SEIENTS AVIÓ");
        System.out.println("============");
        boolean bookMore = true;
        while (bookMore) {
            System.out.print("Pot estar a la finestra (s/n) ? ");
            String windowS = sc.next();
            boolean window = windowS.equals("s") ? true : false;
            int seat = p.bookSeat(plane, window);
            if (seat != -1)
                System.out.println("Seient reservat: " + seat);
            else
                System.out.println("No hi ha seients disponibles");
            p.showPlane(plane);
            System.out.print("Vol reservar més seients (s/n) ? ");
            String moreS = sc.next();
            bookMore = moreS.equals("s") ? true : false;
        }
        sc.close();
    }

}

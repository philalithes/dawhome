/*
 * Program.java        2015/2/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Escriu la seqüència ascendent dels codis i caràcters del codi
     * ASCII estès.
     * 
     */

    public void writeASCII(int n1) {
        //variable declaration
        //algoritme
        while (n1 <= 128) {
            System.out.println(n1);
            System.out.println((char)n1);
            n1++;
            
        }
    }
    
    
}

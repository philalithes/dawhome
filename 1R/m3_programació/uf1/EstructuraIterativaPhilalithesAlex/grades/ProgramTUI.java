/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de notes obtingudes per un grup 
     * d’alumnes acabada en -1 i escriu quants alumnes han obtingut les qualificacions MD, I, S,
     * B, N, E. La correspondència entre notes i qualificacions és: MD=[0, 3) I=[3, 5) S=[5, 6)
     * B=[6, 7) N=[7, 9) E=[9, 10]
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int nota = 0;
        int md = 0;
        int i = 0;
        int s = 0;
        int b = 0;
        int n = 0;
        int e = 0;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una seqüència de notes obtingudes per un grup d’alumnes acabada en -1.");
        //càlculs
        do {
            System.out.println("Escriu la nota de l'alumne");
            nota = sc.nextInt();
            if (nota >= 0 && nota < 3) {
                md++;
            }
            else if (nota >= 3 && nota < 5) {
                i++;
            }
            else if (nota >= 5 && nota < 6) {
                s++;
            }
            else if (nota >= 6 && nota < 7) {
                b++;
            }
            else if (nota >= 7 && nota < 9) {
                n++;
            }
            else if (nota >= 9 && nota <= 10) {
                e++;
            }
            
        } while (nota != -1);
        System.out.printf("El nombre de qualificacions amb MD és %d \n:",md);
        System.out.printf("El nombre de qualificacions amb I és %d \n:",i);
        System.out.printf("El nombre de qualificacions amb S és %d \n:",s);
        System.out.printf("El nombre de qualificacions amb B és %d \n:",b);
        System.out.printf("El nombre de qualificacions amb N és %d \n:",n);
        System.out.printf("El nombre de qualificacions amb E és %d \n:",e);
    }
}


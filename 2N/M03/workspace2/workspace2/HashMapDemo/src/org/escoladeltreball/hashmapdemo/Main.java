package org.escoladeltreball.hashmapdemo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author iaw47951368
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//hashmap de students
		HashMap<Long,Student> students = new HashMap<>();
		students.put(1l, new Student("Joan", "Guardiola", "12345678A", "1WIAW"));
		students.put(2l, new Student("Alex", "Philalithes", "47951368W", "2WIAW"));
		students.put(3l, new Student("Pep", "Salles", "98765432D", "2WIAW"));
		students.put(4l, new Student("Anna", "Martinez", "14729803F", "2WIAW"));
		students.put(5l, new Student("Luis", "Franco", "59680433H", "1WIAW"));
		students.put(6l, new Student("Maria", "Carballo", "76589035L", "1WIAW"));
		
		//llista de students per grups
		List<Student> grup1 = new ArrayList<>();
		List<Student> grup2 = new ArrayList<>();
		
		//omplim les llistes per grups
		for (Long s : students.keySet()) {
			if (students.get(s).getGrup().equals("1WIAW")) {
				grup1.add(students.get(s));
			} else {
				grup2.add(students.get(s));
			}
		}
		//posem les llistes de grups a un hashmap de llistes
		HashMap<String,List<Student>> grups = new HashMap<>();
		grups.put("1WIAW", grup1);
		grups.put("2WIAW", grup2);
		
		//busquem dintre del hashmap de llistes
		System.out.println(Student.find(grups, "2WIAW", "98765432D"));
		
		//Quants students hi ha en un grup?
		System.out.println(Student.getNStudentsInGroup(grups, "2WIAW"));

		//Quants grups hi ha?
		
		System.out.println(Student.getNGroups(grups));
		
		
		
	}

}

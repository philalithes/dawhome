package m08_uf1_apg;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "logs")
public class Log {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date callDate;
	@Column(length = 32)
	private String number;
	
	public Log() {
		// TODO Auto-generated constructor stub
	}

	public Log(String number) {
		this(null, number, new Date(System.currentTimeMillis()));
	}
	
	public Log(Long id, String number, Date callDate) {
		super();
		this.id = id;
		this.callDate = callDate;
		this.number = number;
	}

	@Override
	public String toString() {
		return "Log [id=" + id + ", callDate=" + callDate + ", number=" + number + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCallDate() {
		return callDate;
	}

	public void setCallDate(Date callDate) {
		this.callDate = callDate;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

}

/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donades dues taules de nombres enters, esbrina
     * si tenen la mateixa mida.
     */
    
    public boolean equalLength(double[] array1, double[] array2) {
        //variable declaration
        boolean areEqual = false;
        //calcul
        if (array1.length == array2.length) {
            areEqual = true;
        
        }
        return areEqual;
    }
}

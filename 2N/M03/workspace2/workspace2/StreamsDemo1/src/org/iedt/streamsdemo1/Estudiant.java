package org.iedt.streamsdemo1;

public class Estudiant implements Comparable<Estudiant>{
	private long id;
	private Sexe sexe;
	private String alias;
	private String grup;
	private double nota;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGrup() {
		return grup;
	}

	public void setGrup(String grup) {
		this.grup = grup;
	}

	public Sexe getSexe() {
		return sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	/**
	 * @param id
	 * @param sexe
	 * @param alias
	 * @param grup
	 * @param nota
	 */
	public Estudiant(long id, Sexe sexe, String alias, String grup, double nota) {
		super();
		this.id = id;
		this.sexe = sexe;
		this.alias = alias;
		this.grup = grup;
		this.nota = nota;
	}

	/**
	 * @param registre (id,sexe,alias,grup,nota)
	 */
	public Estudiant(String registre) {
		super();
		String[] regist = registre.split(",");
		this.id = Long.parseLong(regist[0]);
		sexe = Sexe.valueOf(regist[1]);
		this.alias = regist[2];
		this.grup = regist[3];
		this.nota = Double.parseDouble(regist[4]);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estudiant other = (Estudiant) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Estudiant [id=" + id + ", sexe=" + sexe + ", alias=" + alias + ", grup=" + grup + ", nota=" + nota
				+ "]";
	}

	@Override
	public int compareTo(Estudiant o) {
		// TODO Auto-generated method stub
		
		return new Long(id).compareTo(new Long(o.getId()));
	}

}

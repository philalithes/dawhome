#!/bin/bash
# Filename:			regex_exercici2.sh
# Author:			iaw47951368
# Date:				22/01/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./regex_exercici2.sh [args...]
# Description:		Script que rep un número com a argument i diu si és parell o senar.

#control d'arguments
if [ $# -ne 1 ]
then
	echo "escriu 1 sol argument i que sigui un nombre."
	exit 1

#parell o senar
elif echo $1 | grep ^[[:digit:]]*$ > /dev/null
then
	if [ $(($1 % 2)) -eq 0 ]
	then
		echo "$1 és parell"
	else
		echo "$1 és senar"
	fi
else
	echo "introdueix un nombre enter"
	exit 1
fi

package org.escoladeltreball.comparabledemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author iaw47951368
 *
 */
public class StudentManager {
	List<Student> students;

	public StudentManager(List<Student> students) {
		super();
		this.students = students;
	}
	
	/**
	 * @param course
	 * @return llista d'estudiants del curs course
	 */
	public List<Student> fromCourse(String course) {
		List<Student> returnList = new ArrayList<Student>();
		Collections.sort(students, new Comparator<Student>() {
			@Override
			public int compare(Student s1, Student s2) {
				if (s1.getGrup().compareTo(s2.getGrup()) == 1) {
					return 1;
				} else if (s1.getGrup().compareTo(s2.getGrup()) == 0){
					return 0;
				} else {
					return -1;
				}
			}
		});
		for (Student s : students) {
			if (s.getGrup() == course) {
				returnList.add(s);
			}
		}
		return returnList;
	}
	
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	/**
	 * @param course
	 * @return la mitjana de la nota dels alumnes del curs course
	 */
	public double average(String course) {
		List<Student> fromCourse = new ArrayList<>(fromCourse(course));
		double result = 0;
		int i = 0;
		for (Student s : fromCourse) {
			result += s.getNota();
			i++;
		}
		return result / i;
	}
	
	/**
	 * @param course
	 * @return el nom de l'alumne amb nota més alta del curs course
	 */
	public String nameMaxNota(String course) {
		List<Student> fromCourse = new ArrayList<>(fromCourse(course));
		double maxNota = 0;
		String name = "";
		for (Student s : fromCourse) {
			if (s.getNota() > maxNota) {
				maxNota = s.getNota();
				name = s.getNom();
			}
		}
		return name;
	}
}

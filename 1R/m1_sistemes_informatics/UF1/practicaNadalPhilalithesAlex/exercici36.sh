#!/bin/bash
# Filename:		exercici36.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici36.sh
# Description:		Script que fa un ls en forma d'arbre de nadal.

#nombre de fitxers del directori actual
numfiles=$(ls | wc -l)
#guardem |_ en una variable
pipebarra=$(echo "|_")
#mostrem el primer fitxer
echo "$pipebarra$(ls | sed -n 1p)"
#bucle per posar puntets
for i in $(seq 2 $numfiles)
do
	j=$i #contador de puntets que tindrà cada arxiu
	while [ $j -gt 1 ]
	do
		echo -n "."
		j=$(($j-1))
	done
echo "$pipebarra$(ls | sed -n ${i}p)"
done
	

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * 
     * Mètode per multiplicar sense usar *:
     * 
     */
    
    public int producte(int n1, int n2) {
        int producte = 0;
        for (int i = 0; i < n2; i++) {
            producte += n1;
        }
        return producte;
    }
    /*
     * Llegeix un nombre enter positiu n i n nombres enters i
     * escriu el seu producte. No fer ús de l'operador *.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int cant = 0;
        int i = 0;
        int n1 = 0;
        int n2 = 0;
        int producte = 0;
        int naux = 0;
        
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu un nombre n i n nombres enters i els multiplicaré.");
        System.out.println("Quants nombres introduiràs?");
        cant = sc.nextInt();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        System.out.println("Escriu un nombre:");
        n1 = sc.nextInt();
        naux = n1;
        for (i = 1; i < cant; i++) {
             System.out.println("Escriu un nombre:");
             n2 = sc.nextInt();
             producte = p.producte(naux,n2);
             naux = producte;
             
        }
        
        System.out.printf("producte = %d", producte);
             
             
    }
}

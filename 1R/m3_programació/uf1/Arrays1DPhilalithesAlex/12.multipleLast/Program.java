
/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula de nombres enters, retorna
     * una altra taula amb els elements de la taula que són múltiples del darrer element de la taula.
     */
    
    public int[] multipleLast(int[] array) {
        //variable declaration
        int lastNum = array[array.length-1];
        int n = 0;
        int j = 0;
        //calcul
        for (int i = 0; i < array.length; i++) {
            if (array[i] % lastNum == 0) {
                n++;
            }
        }
        int[] multiples = new int[n];
        for (int i = 0; i < array.length; i++) {
            if (array[i] % lastNum == 0) {
                multiples[j] = array[i];
                j++;
            }
        }
        return multiples;
        
    }
}

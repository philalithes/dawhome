var mongoose = require('mongoose');

var bcrypt   = require('bcrypt-nodejs');
// objecte esuqema

var Schema = mongoose.Schema;


// instància de l'esquema
var sociSchema = new Schema( {
	user: String,
	password: String,
	codsoci: Number,
	nom: String,
	cognoms : String,
	recarrecs : [{
	    codpeli : Number,
	    import: Number,   
	    cobrat: Number,
	}]
});

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// registrem la col·lecció Pelicules amb un esquema concret i exportem
module.exports = mongoose.model('Socis', sociSchema);
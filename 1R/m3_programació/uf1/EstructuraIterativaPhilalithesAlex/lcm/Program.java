/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    /*
     * Donats dos nombres enters positius, calcula el seu mínim comú
     * múltiple. El MCM és el nombre enter positiu múltiple d'ambdós més petit.
     */
    
    public int lcm(int n1, int n2) {
        //variable declaration
        int lcm = 0;
        if (n1 > n2) {
            int i = n1;
            int minM = 1;
            boolean found = false;
            //càlcul
            while (!found) {
                if (i % n2 == 0) {
                    minM = i;
                    if (minM % n1 == 0)  {
                        found = true;
                        lcm = minM;
                    }
                }
                i++;
            }
        }
        else {
            int i = n2;
            int minM = 1;
            boolean found = false;
            //càlcul
            while (!found) {
                if (i % n1 == 0) {
                    minM = i;
                    if (minM % n2 == 0)  {
                        found = true;
                        lcm = minM;
                    }
                }
                i++;
            }
        }
        return lcm;
        
        
    }
}

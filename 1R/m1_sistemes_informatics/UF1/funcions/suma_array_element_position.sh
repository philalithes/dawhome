#!/bin/bash
# Filename:		suma_array_element_position
# Author:		jamoros
# Date:			17/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./suma_array_element_position index increment
# Description:	Script que rep dos arguments per la línia d'ordres, i que agafarà el primer com la posició d'un array,
# 				i el segon com l'increment que haurà d'afegir a l'element que es troba en la posició esmentada.

function suma_array_position {
}

# If there are more or less than 2 arguments we exit from the script inform everybody about it.
# No error control about the first argument nature
if [ $# -ne 2 ]
then
	echo "error: incorrect arguments number"
	echo "us: $0 index increment"
	exit 1
fi

array_body=(70 60 50 40)
position=$1
increment=$2
echo "The original array ${array_body[*]}"
suma_array_position array_body position increment
echo "The array after increment the element in the $1-position with $2: ${array_body[*]}"


# We can add some error checks: 
# $1 is an integer?
# $2 is an integer?
# $1, the position,  is an integer less than the array's length? 

# A shorter function:
#function suma_array_position {
#	let $1[$2]=$(($1[$2]+$3))
#}

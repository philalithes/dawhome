package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @autor Alex Philalithes
 */
public class Main {

	private static final String URL = "jdbc:postgresql://localhost:5432/java";

	public static List<Dispositiu> findAll() {
		Connection connection;
		Statement statement;
		List<Dispositiu> dispositius = new ArrayList<>();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			ResultSet resultSet = statement.executeQuery("select * from dispositius order by id");
			// Recorrer ResultSet
			while (resultSet.next()) {
				String nom = resultSet.getString("nom");
				int id = resultSet.getInt("id");
				double preu = resultSet.getDouble("preu");
				Timestamp alta = resultSet.getTimestamp("alta");
				boolean wifi = resultSet.getBoolean("wifi");
				dispositius.add(new Dispositiu(id, nom, wifi, alta, preu));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispositius;
	}

	public static Dispositiu findById(int id) throws DispositiuNotFoundException {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			ResultSet rs = statement.executeQuery("select id, nom, wifi, alta, preu from dispositius where id = " + id);
			connection.close();
			if (!rs.next()) {
				throw new DispositiuNotFoundException("No s'ha trobat el dispositiu amb id " + id);
			}

			id = rs.getInt("id");
			String nom = rs.getString("nom");
			double preu = rs.getDouble("preu");
			boolean wifi = rs.getBoolean("wifi");
			Timestamp alta = rs.getTimestamp("alta");
			Dispositiu d = new Dispositiu(id, nom, wifi, alta, preu);

			return d;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void save(Dispositiu dispositiu) {
		Connection connection;
		Statement statement;
		String nom = dispositiu.getNom();
		double preu = dispositiu.getPreu();
		boolean wifi = dispositiu.isWifi();
		Timestamp alta = dispositiu.getAlta();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			statement.executeUpdate("insert into dispositius (nom, wifi, alta, preu) values ('" + nom + "'," + wifi
					+ ",'" + alta + "'," + preu + ")");
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void remove(int id) throws DispositiuNotFoundException {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			// 1 = la comanda retorna algo
			if (statement.executeUpdate("delete from dispositius where id = " + id) != 1) {
				throw new DispositiuNotFoundException("No s'ha trobat el dispositiu amb id " + id);
			}
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void remove(Dispositiu dispositiu) throws DispositiuNotFoundException {
		int id = dispositiu.getId();
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			if (statement.executeUpdate("delete from dispositius where id = " + id) != 1) {
				throw new DispositiuNotFoundException("No s'ha trobat el dispositiu amb id " + id);
			}
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static List<Dispositiu> find(boolean teWifi) {
		Connection connection;
		Statement statement;
		List<Dispositiu> dispositius = new ArrayList<>();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			ResultSet resultSet = statement
					.executeQuery("select * from dispositius where wifi = " + teWifi + " order by id");
			// Recorrer ResultSet
			while (resultSet.next()) {
				String nom = resultSet.getString("nom");
				int id = resultSet.getInt("id");
				double preu = resultSet.getDouble("preu");
				Timestamp alta = resultSet.getTimestamp("alta");
				boolean wifi = resultSet.getBoolean("wifi");
				dispositius.add(new Dispositiu(id, nom, wifi, alta, preu));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispositius;
	}

	public static List<Dispositiu> find(double preuInit, double preuFinal) {
		Connection connection;
		Statement statement;
		List<Dispositiu> dispositius = new ArrayList<>();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			ResultSet resultSet = statement.executeQuery(
					"select * from dispositius where preu between " + preuInit + " and " + preuFinal + " order by id");
			// Recorrer ResultSet
			while (resultSet.next()) {
				String nom = resultSet.getString("nom");
				int id = resultSet.getInt("id");
				double preu = resultSet.getDouble("preu");
				Timestamp alta = resultSet.getTimestamp("alta");
				boolean wifi = resultSet.getBoolean("wifi");
				dispositius.add(new Dispositiu(id, nom, wifi, alta, preu));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dispositius;
	}

	public static void main(String[] args) {
		// aquesta part està comentada perquè provis els mètodes save() remove(int id)
		// i remove(Dispositiu dispositiu)
		// i puguis esborrar dispositius que no s'han esborrat ja.
		// save(new Dispositiu(21,"disp21", false, "2011-01-01 00:00:00", 11.5));
		// save(new Dispositiu(22,"disp22", false, "2012-01-01 00:00:00", 12.5));
		// save(new Dispositiu(23,"disp23", true, "2012-05-01 00:00:00", 13.5));
		// save(new Dispositiu(24,"disp24", true, "2015-01-01 00:00:00", 14.5));
		// save(new Dispositiu(25,"disp25", false, "2017-01-01 00:00:00", 15.5));
		// save(new Dispositiu(26,"disp26", true, "2010-01-01 00:00:00", 16.5));
//		System.out.println(findById(21));
//		System.out.println(findById(22));
		// remove(21)
//		System.out.println(findById(21));
		// remove(new Dispositiu(22, "disp22", true, "2001-01-01 00:00:00", 10));
//		System.out.println(findById(22));
		List<Dispositiu> dispos = findAll();
		// ordre normal (id)
		System.out
		.println("************************************* Dispositius ordenats per ID **********************************");
		dispos.stream().forEach(System.out::println);
		// Comparators
		CompareByName cName = new CompareByName();
		CompareByAlta cAlta = new CompareByAlta();
		CompareByPreu cPreu = new CompareByPreu();
		System.out
				.println("************************************* Comparator amb nom **********************************");
		// nom
		Collections.sort(dispos, cName);
		dispos.stream().forEach(System.out::println);
		System.out
				.println("************************************* Comparator amb alta *********************************");
		// alta
		Collections.sort(dispos, cAlta);
		dispos.stream().forEach(System.out::println);
		System.out
				.println("************************************* Comparator amb preu *********************************");
		// preu
		Collections.sort(dispos, cPreu);
		dispos.stream().forEach(System.out::println);
		System.out.println(
				"************************************* Dispositius amb wifi *********************************");
		// dispositius amb wifi
		List<Dispositiu> dispositiusWifi = find(true);
		dispositiusWifi.stream().forEach(System.out::println);
		System.out.println(
				"********************************* Dispositius amb preu entre 12 i 15 ***********************");
		// preu entre 12 i 15
		List<Dispositiu> entre12i15 = find(12, 15);
		entre12i15.stream().forEach(System.out::println);System.out.println(
				"********************************* Array Buit (dispositius amb preu entre 0 i 1) *******************");
		// preu entre 0 i 1
		List<Dispositiu> entre0i1 = find(0, 1);
		System.out.println(entre0i1);
		System.out.println(
				"********************************* DispositiuNotFoundException ***********************");
		// DispositiuNotFoundException
		Dispositiu dispNull = findById(9999);
		remove(new Dispositiu(-1, "dispI", false, "2017-01-01 00:00:00", 14.5));
		remove(0);
		System.out.println(dispNull); 
		System.out.println("************************************ Find 12 *************************************");
		System.out.println(findById(12));
		
	}

}

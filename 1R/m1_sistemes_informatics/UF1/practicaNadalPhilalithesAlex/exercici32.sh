#!/bin/bash
# Filename:		exercici32.sh
# Author:		phila
# Date:			05/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici32.sh [arg1...]
# Description:		Script que suma tots els números que se li passen com a arguments.

#control d'arguments (hi ha d'haber almenys un)
if [ -z $1 ]
then
	echo "Hi ha d'haber almenys un argument i ha de ser un nombre"
	echo "execució: ./exercici32.sh [arg1...]"
	exit 1
fi

#expressió que suma tots els arguments
echo $(echo "$*" | tr ' ' '+' | bc -l)





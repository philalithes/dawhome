package org.escoladeltreball.comparabledemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Student> students = new ArrayList<>();
		students.add(new Student("Ana", "Fernandez Torres",1,"2WIM", 9.0));
		students.add(new Student("Teresa", "Martinez Fernandez",2,"2WIM", 7.0));
		students.add(new Student("Ivan", "Estrada Montoro",3,"2WIW", 6.0));
		students.add(new Student("Eva", "Ferrer Catala",4,"2WIW", 6.30));
		students.add(new Student("Joan", "Menendez Adam",5,"1WIM", 9.90));
		students.add(new Student("Ilma", "Sotillos Jucar",6,"1WIM", 9.23));
		students.add(new Student("Gonzalez", "Fernandez Pujol",7,"WIW", 5.40));
		
		StudentManager sm = new StudentManager(students);
		List<Student> wim2 = new ArrayList<>(sm.fromCourse("2WIM"));
		for (Student s : wim2) {
			System.out.println(s);

		}
		System.out.println(sm.average("2WIM"));
		System.out.println(sm.nameMaxNota("2WIM"));
	}

}

#!/bin/bash
# file: bigOldFilesUsers.sh
# versio: v0.1
# Script fet en una distribució Fedora Core 8. Shell: bash 
# Contingut del fitxer /etc/sysconfig/i18n en el sistema treballat: 
#			LANG="ca_ES.UTF-8"
#			SYSFONT="latarcyrheb-sun16"
# Script fet escoltant Vampire Weekend (amb l'ajuda de gnomad2)
# Farem l'script una mica més genèric afegint una quantitat variable de megas i de mesos 
# Si no hi ha arguments es considerarà l'opció demanada, es a dir, més de 10 megas i més de 3 mesos d'antigüitat
# Si es pasa només un argument es considerarà que es refereix al tamany en megas del fitxer
# Si es pasen dos arguments el primer es considerarà que es refereix al tamany en megas del fitxer i el segon al temps d'antigüitat (en mesos)
# No fem control d'errors, en quant a que els arguments passats per la línia de comandes siguin, per exemple, strings...
# L'únic que fem és en cas de pasar 3 arguments mostrar missatge d'ajuda i sortida d'error
# També mostrem l'us de la comanda en cas de que es passi com a primer argument un "-h" o "--help" 

# Algunes consideracions que s'han fet:
# Per trobar els usuaris ordinaris suposarem que tots els usuaris tenen el seu $HOME per defecte penjant de la variable
# HOME que en Fedora es troba al fitxer /etc/default/useradd (normalment "/home")
# Encara que podria ser que l'administrador utilitzes la comanda adduser amb algun parametre i canvies això. 
# Però com se suposa que el root som nosaltres, doncs col·locarem sempre els usuaris seguint aquesta politica.
# Una altra opció seria utilitzar la variable $SHELL per comprovar si tenen shell al últim camp del /etc/passwd, però hauriem de discriminar encara
# usuaris com per exemple root, postgres ....

# Una altra opció seria xequejar el UID de l'usuari i veure si és més gran que una certa quantitat (moltes vegades >= 500)
# Encara que també hauriem de discriminar alguna entrada més del /etc/passwd
# Utilitzarem mail del paquet mailx
# No farem variable el temps donat (48 hores ) per enviar el email (més que res per que amb tanta opció l'script està començant a crèixer cada cop més)
# Encara que seria interesant fer-ho.




if [ $# -eq 0 ]
then
	#Fem l'opció de l'enunciat
	FILE_SIZE=10	#tamany en megas del fitxer 
	OLD_FILE=3	#antigüitat en messos del fitxer
elif [ "$1" = "-h" -o "$1" = "--help" ] 
then
	#Mostrem l'ajuda demanada
	#Sortirem amb error level 0, no canviem res per tant
	echo "us: $0 [ tamany_fitxer [ antiguitat_fitxer] ]"
	echo -e "\t on tamany_fitxer és el tamany mínim en megas dels fitxers a buscar"
	echo -e "\t on antiguitat_fitxer és l'antigüitat mínima en messos dels fitxers a buscar"
	exit 0
elif [ $# -gt 2 ]
then
	echo "$0: error de sintaxi"
        echo "us: ./$0 [ tamany_fitxer [ antiguitat_fitxer] ]"
        echo -e "\t on tamany_fitxer és el tamany en megas dels fitxers a buscar"
        echo -e "\t on antiguitat_fitxer és l'antigüitat mínima en messos dels fitxers a buscar"
	exit 1
elif [ $# -eq 2 ]
then
	# Tant en aquest cas com en el de sota no controlem que els arguments siguin numèrics
	FILE_SIZE=$1
	OLD_FILE=$2
else
	#Només queda una possibilitat: un argument
	FILE_SIZE=$1
	OLD_FILE=3
fi

#echo  FILE_SIZE=$FILE_SIZE
#echo  OLD_FILE=$OLD_FILE



# Fixem el directori d'on penjaran els "homes" dels nostres usuaris ordinaris  
HOME_BASE=`cat /etc/default/useradd |grep "HOME"|cut -d"=" -f2`

# Busquem aquest directori al fitxer /etc/passwd, retallem la info que es troba al camp 1 i 6 que són respectivament el nom de l'usuari
# i el seu home i enmagatzem aquesta info a un fitxer temporal que després esborrarem
cat /etc/passwd | grep $HOME_BASE | cut -d: -f1,6 > usuarisHomeTemp 



#Passem els messos a dies + 1
#Suposarem que els messos són de 30 dies
OLD_FILE_M=$[$OLD_FILE*30 + 1]
#Subjecte del missatge
SUBJECT_EMAIL="Avis eliminació fitxers antics i grans"
#Capçalera del cos del missatge 
HEADER_EMAIL="Sisplau faci una còpia de seguretat (cd/DVD) dels següents fitxers, ja que s'eliminaran del sistema:\n\n"

# Agafarem cada línia del fitxer creat amb els usuaris i els seus directoris i...
for REGISTRE in `cat usuarisHomeTemp`
do
	USUARI=`echo $REGISTRE | cut -d: -f1`
	HOME_USUARI=`echo $REGISTRE | cut -d: -f2`
	
	# echo usuari:  $USUARI
	# echo els seu directori home és: $HOME_USUARI
	
	# La següent ordre cerca fitxers regulars (-type f) 
	# amb antigüitat + gran que els dies que li passem (opció -mtime i utilitzant el + davant del número)
	# l'opció exec ens permet executar una comanda sobre cada un dels fitxers trobats (aquest argument es '{}')
	# En aquest cas li passem al exec un ls
	# I finalment redirigim a un fitxer temporal emmagatzemant els fitxers grans i vells
	


	find $HOME_USUARI -type f -mtime 0            -size +${FILE_SIZE}M -exec ls '{}' \; > fitxersTemp$USUARI		
#	find $HOME_USUARI -type f -mtime +$OLD_FILE_M -size +${FILE_SIZE}M -exec ls '{}' \; > fitxersTemp$USUARI
	

	# Si el fitxer no està buit serà que l'usuari té fitxers que acompleixen la condició
	TAMANY_FITXER=`ls -s fitxersTemp$USUARI | cut -f1 -d" "`
	if [ $TAMANY_FITXER -ne 0 ]
	then
		#Enviem amb l'ajuda de l'ordre mail la info. El body el passem redireccionant amb echo.
		echo -e "$HEADER_EMAIL""`cat fitxersTemp$USUARI`" | mail -s "$SUBJECT_EMAIL" $USUARI

		# Aquesta variable de temps (48 hores ) la podriem fer variable
		# A l'ordre "at" li podem passar un fitxer script o l'entrada estàndard (optem per aquesta segona opció)
		# Volem, a més, canviar el '\n' per un separador diferent (espai en blanc) 
		# Podriem haber jugat amb la variable IFS (Input Field Separator) però optem per jugar amb la comanda tr 
		echo "rm -f  `cat fitxersTemp$USUARI | tr  '\n' ' '` " | at now + 1 minutes	
	#		echo "rm -f  `cat fitxersTemp$USUARI | tr  '\n' ' '` " | at now + 48 hours
		# Ens carreguem també el fitxer temporal, però 1 hora després (no sigui que trigui una mica en carregar-se els altres fitxers) 
		echo "rm -f  fitxersTemp$USUARI" | at now + 2 minutes
	#	echo "rm -f  fitxersTemp$USUARI" | at now + 49 hours	

	else
	#En cas que el tamany del fitxer sigui 0, només ens el carreguem
		rm -f fitxersTemp$USUARI
	fi
done

# Eliminem el fitxer temporal
rm -f usuarisHomeTemp

exit 0

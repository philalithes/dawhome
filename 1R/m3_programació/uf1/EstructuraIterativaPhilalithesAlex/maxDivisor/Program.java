/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    /*
     * Donat un nombre enter positiu, calcula el seu
     * divisor positiu més gran diferent d'ell mateix.
     */
    
    public int maxDivisor(int n) {
        //variable declaration
        int i = n;
        int maxD = 1;
        //càlcul
        while (maxD != i) {
            i--;
            if (n % i == 0) {
                maxD = i;
            }
            
        }
        return maxD;
            
            
        
    }
}

/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Assignatura.
 * 
 * @author iaw47951368
 */
public class Assignatura {
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	// Start of user code (user defined attributes for Assignatura)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Assignatura() {
		// Start of user code constructor for Assignatura)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Assignatura)
	
	// End of user code
	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}



}

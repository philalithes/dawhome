-- functions && triggers for practica.sql

-- 1 mostrar el valor total del stock
create function stockValue() returns double precision as $$
	declare
		stockValue double precision := 0;
		producto record;
		unidadesCamiseta smallint;
	begin
		-- preu cds
		for producto in select * 
				from productos
				where id IN (select id_cd
						from cds) loop
			stockValue := stockValue + producto.precio * (select unidades 
									from cds
									where id_cd = producto.id);
		end loop;
		-- preu samarretes
		for producto in select *
				from productos
				where id IN (select id_camiseta
						from camisetas) loop
			unidadesCamiseta := 0;
			-- sumar unitats de totes les talles de la mateixa samarreta
			for unidades IN (select unidades
						from camiseta_talla
						where producto.id = id_camiseta) loop
				unidadesCamiseta := unidadesCamiseta + unidades;
			end loop;
			stockValue := stockValue + producto.precio * unidadesCamiseta;
		end loop;
	return stockValue;
	end;
$$ language plpgsql;

-- 2 insertar nova camiseta
create function newCamiseta(idCami smallint, artist varchar(30), model varchar(20), price double precision) returns void as $$
	begin
		insert into productos
			values (idCami, price);
		insert into producto_stock
			values (idCami);
		insert into camisetas
			values (artist, model, idCami);
	end;
$$ language plpgsql;

-- 3 insertar nou cd
create or replace function newCD(idCD smallint, artist varchar(30), title varchar(30), duracion interval, unidades smallint, price double precision) returns void as $$
	begin
		insert into productos
			values (idCD, price);
		insert into producto_stock
			values (idCD);
		insert into cds
			values (idCD, artist, title, duracion, unidades);
	end;
$$ language plpgsql;

-- 4 insertar nova entrada de concert
create function newEntrada(idEntrada smallint, artist varchar(30), fecha date, lugar text, unidades smallint, price double precision) returns void as $$
	begin
		insert into productos
			values (idEntrada, price);
		insert into entradas
			values (fecha, lugar, artist, unidades, idEntrada);
	end;
$$ language plpgsql;

-- 5 afegir samarretes a stock
create function addCamisetas(idCami smallint, addUnidades smallint, talla varchar(6)) returns void as $$
	begin
		update camiseta_talla
		set unidades = unidades + addUnidades
		where id_camiseta = idCami;
	end;
$$ language plpgsql;

-- 6 agefir cds a stock
create function addCds(idCD smallint, addUnidades smallint) returns void as $$
	begin
		update cds
		set unidades = unidades + addUnidades
		where id_cd = idCD;
	end;
$$ language plpgsql;

-- 7 calcular import del pedido
create function importePedido(idProducto smallint, cant smallint) returns double precision as $$
	declare
		import double precision := 0;
	begin
		import := (select precio
			from productos
			where id = idProducto) * cant;
		return import;
	end;
$$ language plpgsql;

-- 8 afegir nou pedido
create or replace function newPedido(clie smallint, empl smallint, product smallint, cant smallint) returns void as $$
	begin
		insert into pedidos (cantidad, fecha_pedido, cliente, empleado, importe, id_producto)
			values (cant, localtimestamp, clie, empl, importePedido(product, cant), product);
	end;
$$ language plpgsql;

-- 9 punts client (els punts són un 10% del import dels pedidos de cada client)
create or replace function addPuntos() returns trigger as $$
	declare
		percent smallint := 10;
	begin
			update clientes
			set puntos = puntos + new.importe * percent / 100
			where id_cliente = new.cliente;
	return new;
	end;
$$ language plpgsql;

-- trigger punts
create trigger puntosCliente after insert on pedidos
	for each row
	execute procedure addPuntos();

-- 10 calcular importe compra
create function importeCompra(id smallint, cant smallint) returns double precision as $$
	declare
		import double precision := 0;
	begin
		import := (select precio
			from proveedores
			where id = producto) * cant;
		return import;
	end;
$$ language plpgsql;

-- 11 afegir nova compra
create function newCompra(empl smallint, prod smallint, cant smallint) returns void as $$
	begin
		insert into compras
			values (empl, prod, cant, localtimestamp, importecompra(prod, cant));
	end;
$$ language plpgsql;

-- afegir nou client
create function newClient(id smallint, dire text, telefono int) returns void as $$
	declare
		puntsInici smallint:= 100;
	begin
		insert into clientes
			values (id, dire, telefono, puntsInici);
	end;
$$ language plpgsql;

-- no tocar els punts
create function block() returns trigger as $$
	begin
		raise exception 'Operation not permitted';
	end;
$$ language plpgsql;

create trigger blockPunts before update of puntos on clientes
	execute procedure block();

-- vista dels proveidors de cds
create view proveedoresCds as select * from proveedores where producto IN (select id_cd from cds);

-- vista dels proveidors de samarretes
create view proveedoresCamisetas as select * from proveedores where producto IN (select id_camiseta from camisetas);

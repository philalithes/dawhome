/*
 * Program.java 2015/5/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * Intercanvia el valor de dues variables
  * 
  * @param nombre1 = nombre real
  * @param nombre2 = nombre real
  *
  * @return swap
  */
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);
        System.out.println("Escriu un nombre per posar a la variable nombre1");
        double nombre1 = input.nextDouble();
        System.out.println("Escriu un altre nombre per posar a la variable nombre2");
        double nombre2 = input.nextDouble();
        System.out.println("s'estàn intercanviant els nombres!");
        //swap
        double nombreaux;
        nombreaux = nombre1;
        nombre1 = nombre2;
        nombre2 = nombreaux;
        System.out.printf("la variable nombre1 conté %.2f i la variable nombre2 conté %.2f",nombre1,nombre2);
    }
}

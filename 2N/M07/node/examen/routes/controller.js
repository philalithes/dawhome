// Fitxer: controller.js
var Pelicules = require('../model/peli.js');
var Socis = require('../model/soci.js');
var sess;

module.exports = function(app, passport, LocalStrategy) {
	
/********** LOGIN **********/

	login = function(req, res){	
		res.render('login.jade');
	}

	app.use(function(req, res, next) {
		if(req.url == '/login' || (sess && req.session.userId)) {
			next();
		} else {
			res.render('login.jade');
		}
	})

	//app.get('/', login);
	app.get('/login', login);
	
	app.post('/login', function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
			if(err) return next(err);
			if(!user) return res.redirect('/login');
			req.logIn(user, function(err){
				if(err) return next(err);
				sess = req.session;
				sess.userId = user.id;
				
				sess.save(function(){
				 	res.redirect('/index');
				});

			});
		})(req, res, next);
	})

	app.get('/loginFailure', function(req, res, next) {
	  res.send('Failed to authenticate');
	});

	app.get('/loginSuccess', function(req, res, next) {
	  	res.send('Successfully authenticated');
	});

	passport.serializeUser(function(user, done) {
	  done(null, user);
	});

	passport.deserializeUser(function(user, done) {
	  done(null, user);
	});

	passport.use(new LocalStrategy(function(username, password, done) {
	  process.nextTick(function() {
	    Socis.findOne({
	      'user': username, 
	    }, function(err, user) {

	      if (err) return done(err);

	      if (!user) {
	        return done(null, false);
	      }

	      if (user.password != password) {
	        return done(null, false);
	      }
	      return done(null, user);
	    });
	  });
	}));
	/* END LOGIN */
	/* LOGOUT */
	app.get('/logout',function(req,res){
		req.session.destroy(function(err) {
		  if(err) {
		    console.log(err);
		  } else {
		    res.redirect('/');
		  }
		});
	});
/**** END LOGIN ****/

	// Pagina inicial
	index = function(req, res){
		//res.cookie("user", userSession);

		res.render('index.jade');

	}

	// Llistar totes les pelis
	llistarPelis = function(req, res){
		return  Pelicules.find(function(err, pelis){
			if(err) throw err;
			//return res.send(pelis);
			res.render('mostrarPelis',{title : 'Pel.licules', pelis: pelis});
		});
	}

	// Busca i mostra la peli pasada per parametre
	llistarPeli = function(req, res) {
		var id = parseInt(req.params.CodPeli);
		return  Pelicules.findOne({"Codpeli" : id},
			function(err, pelis){
			if(err) throw err;
			res.render('mostrarPeli',{title : 'Pel.licules', pelis: pelis});

		});
	}

	formAltaPeli = function(req, res){
			res.render('formPeli.jade', {action : "/alta", accio: "Insertar"});
	}

	// funció modificar peli
	modificarPeli = function(req, res) {
		var id = parseInt(req.params.CodPeli);
		return  Pelicules.findOne({"Codpeli" : id},
			function(err, pelis){
			if(err) throw err;
			res.render('formPeli.jade',{action : "/modificar", accio: "Modificar", pelis: pelis});

		});
	}
	/*
	* Funció per modificar una peli
	*/
	modificar = function(req, res) {
		var peli = new  Pelicules(req.body);		 
		Pelicules.findOneAndUpdate({"Codpeli" : peli.Codpeli}, 
			{ "Titol" : peli.Titol, "Sinopsi" : peli.Sinopsi }, function(err, peli) {
		 	if (err) throw err;
		  	res.redirect("/llistarPelis/"+peli.Codpeli);
		});
	}

	/* Funció que dona d'alta una nova pelicula, a partir d'un formulari
	*  Si la pelicula ja existeix es sobre escriurà la ja existent
	*/
	altaPeli = function (req, res) {
		var peli = new  Pelicules(req.body);
		Pelicules.find({"Codpeli" : peli.Codpeli}, function(err, data) {
			if (err) throw err;
			if(data.length == 0) {
				peli.save();
				res.redirect("/llistarPelis/"+peli.Codpeli);
			} else {
				res.render('formPeli.jade');
			}
		});
	}

	esborrarPeli = function (req, res) {
		var id = parseInt(req.params.CodPeli);
		Pelicules.remove({"Codpeli": id}, function(err, pelis){
			if(err){
				res.redirect("/llistarPelis");
			} else {
				res.redirect("/llistarPelis");
			}
		});
	}

	app.get('/', index); 
	app.get('/index', index);
	app.get('/llistarPelis', llistarPelis);
	app.get('/llistarPelis/:CodPeli', llistarPeli);
	app.get('/altaPeli', formAltaPeli);
	app.get('/modificarPeli/:CodPeli', modificarPeli);
	app.get('/esborrarPeli/:CodPeli', esborrarPeli);
	app.post('/alta', altaPeli);
	app.post('/modificar', modificar);

	app.get('/rent', function(req, res) {
		var id = req.query.Codpeli;
		var soci = req.query.Soci;
		Pelicules.findOne({"Codpeli" : id}, function(err, peli) {
			if (err) throw err;


            var dvdsInRent = [], 
            	coddvd;

            // Go through prestecs array and get the dvds that are currently in rent
            peli.prestecs.forEach(function (prestec) {
                if (!prestec.dataDev) { // Get the current dvd if datadev field doesn't exist
                    dvdsInRent.push(prestec.coddvd);
                }
            });

            // Go through the dvds of a movie and find one that is available for rent
            peli.dvds.forEach(function (dvd) {
                // Get index of current dvd
                var isRented = dvdsInRent.indexOf(dvd);

                if (isRented == -1) {   // Get this dvd if it's not in the dvdsInRent array
                    coddvd = dvd;
                    return;
                }
            })
			if( dvdsInRent.length >= peli.dvdsPrestats && coddvd) {
				//var prestats = peli.dvdsPrestats + 1;
				//console.log(prestats);
				Pelicules.update({"Codpeli" : peli.Codpeli}, 
				{"$inc" : {"dvdsPrestats" : +1}, 
				$push : {"prestecs" : { "dataPres": new Date, 
										"soci": soci,
										"coddvd" : coddvd}}},
				{returnNewDocument : true}, function(err, peli) {
		 			if (err) throw err;
		  		});
		  		res.redirect("/llistarPelis");
			} else {
				res.redirect("/llistarPelis");
			}
		})
	})


	app.get('/return', function(req, res) {
		var id = req.query.Codpeli;
		var soci = req.query.Soci;
		Pelicules.findOne({"Codpeli" : id, 
			"prestecs.soci" : soci, 
			"prestecs.$.dataDev" : {"$exists" : false}}, function(err, peli) {
			if (err) throw err;
			if (peli == null) {
				res.redirect("/llistarPelis");
			}else {

                // The Dvd to be returned
                var coddvd;
                // Go through prestecs array and get the rented dvd of the specified user
                peli.prestecs.forEach(function (prestec) {
                    if (prestec.soci == soci && !prestec.dataDev) {
                        coddvd = prestec.coddvd; // Save the coddvd
                        return; // Break the loop
                    }
                });

				Pelicules.update(
					{"Codpeli" : peli.Codpeli, 
					 "prestecs" : {	
					 	"$elemMatch" : {"dataPres" : {"$exists" : true},
					 					"soci": soci,
					 					"coddvd": coddvd,
					 					"dataDev" : {"$exists" : false}}
					}}, 
					{"$inc" : {"dvdsPrestats" : -1},
					 "$set" : {"prestecs.$.dataDev": new Date}
					}, function(err, peli) {
			 			if (err) throw err;
			  		});
		  		res.redirect("/llistarPelis");
			}



		})
	});	

	//********** EXERCICI 1 EXAMEN *************///

	app.use(function(req, res) {
		console.log(req);
		res.render("pageNotFound", { url: req.headers.host + req.url });
	});

	//******************************************///

}

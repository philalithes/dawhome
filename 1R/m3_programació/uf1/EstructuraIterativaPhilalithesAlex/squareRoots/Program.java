/*
 * Program.java        2015/2/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombre enters n1 i n2 amb n1 < n2,
     * escriu les arrels quadrades dels nombres enters dins l'interval [n1, n2] en ordre creixent.
     * 
     */

    public void writeSquareRoots(int n1, int n2) {
        //variable declaration
        double sqrt;
        //algoritme
        while (n1 <= n2) {
            sqrt = Math.sqrt(n1);            
            System.out.println(sqrt);
            n1++;
               
        }
    }
    public void writeSquareRootsFor(int n1, int n2) {
        //variable declaration
        double sqrt;
        //algoritme
        for ( ; n1 <= n2; n1++) {
            sqrt = Math.sqrt(n1);
            System.out.println(sqrt);
        }
    }
    
    
}

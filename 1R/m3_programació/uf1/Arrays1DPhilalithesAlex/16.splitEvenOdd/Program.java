
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèrica entera,
     * descompon-la en dos, una formada pels valors senars i una altra pels valors parells i escriu
     * aquestes dues taules.
     */
    
    public void splitEvenOdd(int[] array) {
        //variable declaration
        int evenLength = 0;
        int i = 0;
        int oddLength = 0;
        int j = 0;
        int k = 0;
        //esbrinar la longitud dels arrays
        for (i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                evenLength++;
            } else {
                oddLength++;
            }
        }
        //declarar els arrays
        int[] arrayEven = new int[evenLength];
        int[] arrayOdd = new int[oddLength];
        //calcul
        for (i = 0; i < array.length; i++) {
            if (array[i] % 2 == 0) {
                arrayEven[j] = array[i];
                j++;
            } else {
                arrayOdd[k] = array[i];
                k++;
            }
        }   
        System.out.println(Arrays.toString(arrayEven));
        System.out.println(Arrays.toString(arrayOdd));
    }
}

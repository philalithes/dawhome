/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if realnumber is positive
     * 
     * @param a = realnumber
     * param 0 is positive
     * 
     * @return true = positive, false = negative
     */
    public boolean isPositive(double a){
        //variable declaration    
        boolean positive;
        //calculate return
        positive = a >= 0;
        //return
        return positive;
    }
    
}

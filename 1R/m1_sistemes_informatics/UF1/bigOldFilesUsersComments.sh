#!/bin/bash
# file: bigOldFilesUsers.sh
# versio: v0.1
# Si no hi ha arguments es considerarà més de 10 megas i més de 3 mesos d'antigüitat.
# Si es pasa només un argument es considerarà que es refereix al tamany en megas del fitxer
# Si es pasen dos arguments el primer es considerarà que es refereix al tamany en megas del fitxer i el segon al temps d'antigüitat (en mesos)
# No fem control d'errors, en quant a que els arguments passats per la línia de comandes siguin, per exemple, strings...
# L'únic que fem és en cas de pasar 3 arguments mostrar missatge d'ajuda i sortida d'error
# També mostrem l'us de la comanda en cas de que es passi com a primer argument un "-h" o "--help" 

# Algunes consideracions que s'han fet:
# Per trobar els usuaris ordinaris suposarem que tots els usuaris tenen el seu $HOME per defecte penjant de la variable
# HOME que en Fedora es troba al fitxer /etc/default/useradd (normalment "/home")
# Encara que podria ser que l'administrador utilitzes la comanda adduser amb algun parametre i canvies això. 
# Però com se suposa que el root som nosaltres, doncs col·locarem sempre els usuaris seguint aquesta politica.
# Una altra opció seria utilitzar la variable $SHELL per comprovar si tenen shell al últim camp del /etc/passwd, però hauriem de discriminar encara
# usuaris com per exemple root, postgres ....

# Una altra opció seria xequejar el UID de l'usuari i veure si és més gran que una certa quantitat (moltes vegades >= 500)
# Encara que també hauriem de discriminar alguna entrada més del /etc/passwd
# Utilitzarem mail del paquet mailx
# No farem variable el temps donat (48 hores ) per enviar el email (més que res per que amb tanta opció l'script està començant a crèixer cada cop més)
# Encara que seria interesant fer-ho.




	#Fem l'opció de l'enunciat


	#Mostrem l'ajuda demanada
	#Sortirem amb error level 0, no canviem res per tant
	





	# Tant en aquest cas com en el de sota no controlem que els arguments siguin numèrics

	#Només queda una possibilitat: un argument





# Fixem el directori d'on penjaran els "homes" dels nostres usuaris ordinaris  



# Busquem aquest directori al fitxer /etc/passwd, retallem la info que es troba al camp 1 i 6 que són respectivament el nom de l'usuari
# i el seu home i enmagatzem aquesta info a un fitxer temporal que després esborrarem




#Passem els messos a dies + 1
#Suposarem que els messos són de 30 dies

#Subjecte del missatge

#Capçalera del cos del missatge 


# Agafarem cada línia del fitxer creat amb els usuaris i els seus directoris i...


	
	# La següent ordre cerca fitxers regulars (-type f) 
	# amb antigüitat + gran que els dies que li passem (opció -mtime i utilitzant el + davant del número)
	# l'opció exec ens permet executar una comanda sobre cada un dels fitxers trobats (aquest argument es '{}')
	# En aquest cas li passem al exec un ls
	# I finalment redirigim a un fitxer temporal emmagatzemant els fitxers grans i vells
	


	

	# Si el fitxer no està buit serà que l'usuari té fitxers que acompleixen la condició


		#Enviem amb l'ajuda de l'ordre mail la info. El body el passem redireccionant amb echo.


		# Aquesta variable de temps (48 hores ) la podriem fer variable
		# A l'ordre "at" li podem passar un fitxer script o l'entrada estàndard (optem per aquesta segona opció)
		# Volem, a més, canviar el '\n' per un separador diferent (espai en blanc) 
		# Podriem haber jugat amb la variable IFS (Input Field Separator) però optem per jugar amb la comanda tr 


		# Ens carreguem també el fitxer temporal, però 1 hora després (no sigui que trigui una mica en carregar-se els altres fitxers) 




	#En cas que el tamany del fitxer sigui 0, només ens el carreguem




# Eliminem el fitxer temporal




/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donades dues taules quadrades numèriques
     * enteres de dimensió n, calcula la seva suma. La suma de les taules s'efectua assignant a
     * cada element de la nova taula la suma dels elements que ocupen la mateix fila i la
     * mateixa columna.
     * 
     * 
     */
    
    public int[][] sumMatrices(int[][] matrix1, int[][] matrix2) {
        //variables
        int i, j = 0;
        int n = matrix1.length;
        int[][] sumMatrix = new int[n][n];
        //calcul
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                sumMatrix[i][j] = matrix1[i][j] + matrix2[i][j];
            }
            
        }
        return sumMatrix;
    }
}


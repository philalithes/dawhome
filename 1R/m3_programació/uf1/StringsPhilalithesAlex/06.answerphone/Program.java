/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donades les dades d'una persona que marxa de
     * viatge (nom, sexe(H/D), telèfon i ciutat on ha marxat), crea la cadena que hauria de dir un
     * contestador automàtic:
     * a. Si el sexe és 'H' la cadena serà: “Hola! Sóc el [nom]. Has trucat al [telèfon] i ara no
     * estic a casa perquè estic a [ciutat] de vacances!!!”
     * b. Si el sexe és 'D' la cadena serà: “Hola! Sóc la [nom]. Has trucat al [telèfon] i ara no
     * estic a casa perquè estic a [ciutat] de vacances!!!”
     * 
     * 
     */
    
    public String answerphone (String nom, char sexe, int telefon, String ciutat) {
        //variables
        String contestador = "";
        //calcul
        if (sexe == 'H') {
            contestador = "Hola! Sóc el " + nom + ". Has trucat al " + telefon + " i ara no estic a casa perquè estic a " + ciutat + " de vacances!!!";
        } else {
            contestador = "Hola! Sóc la " + nom + ". Has trucat al " + telefon + " i ara no estic a casa perquè estic a " + ciutat + " de vacances!!!";
        }
        //retorn
        return contestador;
        
        
    }
}

package org.escoladeltreball.springdemo2;

public class Cpu {

	private String marca;
	private double frequencia;
	
	@Override
	public String toString() {
		return "Cpu [marca=" + marca + ", frequencia=" + frequencia + "]";
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public double getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(double frequencia) {
		this.frequencia = frequencia;
	}

	public Cpu() {
		// TODO Auto-generated constructor stub
	}
	public Cpu(String marca, double frequencia) {
		this.marca = marca;
		this.frequencia = frequencia;
	}

}

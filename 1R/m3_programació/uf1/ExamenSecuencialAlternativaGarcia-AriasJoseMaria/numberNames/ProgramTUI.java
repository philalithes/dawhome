/*
 * ProgramTUI.java        1.0 Oct 30, 2014
 *
 * Programa que te escribe en catalán un número introducido del 0 al 99
 *
 * Copyright 2014 Chema García-Arias García-Morato <mrdelayman@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Write a number on Catalan
     * 
     * @param number The number to write
     * @return The number written in catalan
     */
    public String numberNames(int number) {
        // Declaración de variables
        String stringUnidades = "", stringDecenas = "";
        int unidades, decenas;
        //Codificación del algoritmo
        unidades = number % 10;
        decenas = (number - unidades) / 10;
        if (decenas == 0){
            stringDecenas = "";
            if (unidades == 0){
                stringUnidades = "zero";
            }else if (unidades == 1){
                stringUnidades = "u";
            }else if (unidades == 2){
                stringUnidades = "dos";
            }else if (unidades == 3){
                stringUnidades = "tres";
            }else if (unidades == 4){
                stringUnidades = "quatre";
            }else if (unidades == 5){
                stringUnidades = "cinc";
            }else if (unidades == 6){
                stringUnidades = "sis";
            }else if (unidades == 7){
                stringUnidades = "set";
            }else if (unidades == 8){
                stringUnidades = "vuit";
            }else if (unidades == 9){
                stringUnidades = "nou";
            }else {
                stringUnidades = "deu";
            }
        }
        if (decenas == 1){
            stringDecenas = "";
            if (number == 11){
                stringUnidades = "onze";
            }else if (number == 12){
                stringUnidades = "dotze";
            }else if (number == 13){
                stringUnidades = "tretze";
            }else if (number == 14){
                stringUnidades = "catorze";
            }else if (number == 15){
                stringUnidades = "quize";
            }else if (number == 16){
                stringUnidades = "setze";
            }else if (number == 17){
                stringUnidades = "disset";
            }else if (number == 18){
                stringUnidades = "divuit";
            }else {
                stringUnidades = "dinou";
            }
        }if (decenas == 2){
            stringDecenas = "vint ";
            if (unidades != 0){
                stringUnidades = "i ";
            }
            if (unidades == 1){
                stringUnidades += "u";
            }else if (unidades == 2){
                stringUnidades += "dos";
            }else if (unidades == 3){
                stringUnidades += "tres";
            }else if (unidades == 4){
                stringUnidades += "quatre";
            }else if (unidades == 5){
                stringUnidades += "cinc";
            }else if (unidades == 6){
                stringUnidades += "sis";
            }else if (unidades == 7){
                stringUnidades += "set";
            }else if (unidades == 8){
                stringUnidades += "vuit";
            }else if (unidades == 9){
                stringUnidades += "nou";
            }else {
                stringUnidades = "";
            }
        }if (decenas !=1 && decenas !=2 && decenas !=0 ){
            if (unidades != 0){
                stringUnidades = "-";
            }
            
            if (decenas == 3){
                stringDecenas = "trenta";
            }else if(decenas == 4){
                stringDecenas = "quaranta";     
            }else if(decenas == 5){
                stringDecenas = "cinquanta";     
            }else if(decenas == 6){
                stringDecenas = "seixanta";     
            }else if(decenas == 7){
                stringDecenas = "setanta";     
            }else if(decenas == 8){
                stringDecenas = "vuitanta";     
            }else{
                stringDecenas = "noranta";     
            }if (unidades == 1){
                stringUnidades += "u";
            }else if (unidades == 2){
                stringUnidades += "dos";
            }else if (unidades == 3){
                stringUnidades += "tres";
            }else if (unidades == 4){
                stringUnidades += "quatre";
            }else if (unidades == 5){
                stringUnidades += "cinc";
            }else if (unidades == 6){
                stringUnidades += "sis";
            }else if (unidades == 7){
                stringUnidades += "set";
            }else if (unidades == 8){
                stringUnidades += "vuit";
            }else if (unidades == 9){
                stringUnidades += "nou";
            }else {
                stringUnidades = "";
            }
        }
        return stringDecenas + stringUnidades;
        
    }
    
      /**
     * Main program.
     * 
     * @param args Not used
     */
    public static void main(String[] args) {
         // Declaración de variables
        int number;
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        //Construimos un objeto de la clase program para llamar al método numberNames0To10
        ProgramTUI p = new ProgramTUI();
        System.out.printf("\nESTE PROGRAMA ESCRIBE EN CATALÁN UN NÚMERO DEL 0 AL 99 \n\n");  
        System.out.printf("\nIntroduzca el número que desea escribir: ");
        //Guardamos en la variable "number" el número que el usuario introduzca por teclado mediante scanner
        number = s.nextInt();
        //Llamamos al método y mostramos por pantalla el resultado que nos devuelve
        System.out.printf("\nEl número escrito en catalán es es: %s\n", p.numberNames(number));
    }
    
}

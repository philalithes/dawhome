#!/bin/bash
# Filename:		search_char_position_array.sh
# Author:		jamoros
# Date:			18/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./search_char_position_array.sh char
# Description:	Script que utiliza una funció que rep un array de caràcters com a primer argument
# 				i un caracter com a segon.
# 				retorna un 0 per al cas on el caràcter no s'ha trobat a l'array
# 				i retorna i+1 si el caràcter es troba a la posició i de l'array.
#				No funcionaria per arrais de més de 254 elements 

function search_char_position_array {
	local cont=0
	declare -a alfabet
	echo ${!1}
	
	for i in ${alfabet[@]}
	do
  		cont=$(($cont+1))
	  	if [ $i = $2 ]
		then
			return $cont
		fi
	done
	return 0 
	
}

# If there are more or less than 1 argument we exit from the script inform everybody about it.
# No error control about the first argument nature
if [ $# -ne 1 ]
then
	echo "error: incorrect arguments number"
	echo "us: $0 char"
	exit 1
fi


declare -a alfabet=({a..z})
echo "Original chars array: ${alfabet[*]}"
char="$1"
search_char_position_array alfabet char
errorlevel=$?
if [ $errorlevel -eq 0 ]
then
	echo "The char $1 was not found in the char's array"
else
	echo "The char $1 was found in the $(($errorlevel-1)) array's index"
fi

# Remember the return from a function has the value's range: 0-255 

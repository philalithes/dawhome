/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * A partir de dia, mes i any, retorna el dia següent amb el
     * format “dd/mm/aaaa”.
     * 
     * @param d = dia
     * @param m = mes
     * @param a = any
     *
     * 
     * @retorn datePlus1 = dia seguent.
     */
    public String dayPlus1(int d, int m, int a){
        //declaració de variables
        String datePlus1;
        boolean traspas = (m == 2 && a % 4 == 0 && a % 100 != 0 || a % 400 == 0);
        //anys de traspàs al febrer, canvi de mes
        if (traspas && d == 29) {
            d = 1;
            m = 3;
            datePlus1 = d + "/" + m + "/" + a;
        }
        //dates invalides de febrer en any de traspàs
        else if (traspas && d > 29) {
            datePlus1 = "invalid date";
        }
        //febrer canvi de mes sense ser any de traspàs
        else if (m == 2 && d == 28 && !traspas) {
            d = 1;
            m = 3;
            datePlus1 = d + "/" + m + "/" + a;
        }
        //dates invalides en febrer sense ser any de traspàs
        else if (m == 2 && d > 28 && !traspas) {
            datePlus1 = "invalid date";
        }
        //canvi de mes en mesos de 31 dies
        else if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10) && d == 31) {
            d = 1;
            m = m + 1;
            datePlus1 = d + "/" + m + "/" + a;
        }
        //canvi d'any
        else if (m == 12 && d == 31) {
            d = 1;
            m = 1;
            a = a + 1;
            datePlus1 = d + "/" + m + "/" + a;
        }
        //dates invalides en mesos de 31 dies
        else if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d > 31) {
            datePlus1 = "invalid date";
        }
        //canvi de mes en mesos de 30 dies
        else if ((m == 4 || m == 6 || m == 9 || m == 11) && d == 30) {
            d = 1;
            m = m +1;
            datePlus1 = d + "/" + m + "/" + a;
        }
        //dates invalides en mesos de 30 dies
        else if ((m == 4 || m == 6 || m == 9 || m == 11) && d > 30) {
            datePlus1 = "invalid date";
        }
        //altres dates invalides
        else if (d < 1 || m < 1 || m > 12) {
            datePlus1 = "invalid date";
        }
        //cualsevol dia
        else {
            d = d + 1;
            datePlus1 = d + "/" + m + "/" + a;
        }
        return datePlus1;
    }
    
    
}

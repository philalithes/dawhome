create or replace function benefici(cli clientes.num_clie%TYPE) returns numeric as $$
	declare
		benefici_brut numeric := 0;
		benefici_brut_comanda numeric;
		benefici_sense_descompte numeric;
		comandes record;
		marge_benefici int;
		descompte numeric;
	begin
		--recorrem les comandes del client
		for comandes in select precio, cant, fab, importe
						from pedidos 
						join productos 
						on id_fab = fab and id_producto = producto
						where clie = cli
						loop
		--mirar fabricant
		if comandes.fab = 'imm' then
			marge_benefici := 60;
		elsif comandes.fab = 'aci' or comandes.fab = 'rei' then
			marge_benefici := 40;
		else
			marge_benefici := 20;
		end if;
		--calcul del benefici
		benefici_sense_descompte := comandes.precio * comandes.cant * marge_benefici;
		descompte := comandes.precio * comandes.cant - comandes.importe;
		benefici_brut_comanda := benefici_sense_descompte - descompte;
		benefici_brut := benefici_brut + benefici_brut_comanda;
		end loop;
	return benefici_brut;
	end;
$$ language plpgsql;

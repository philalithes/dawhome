package org.escoladeltreball.m03_uf05_nf01_apg;

import java.util.ArrayList;
import java.util.List;

/**
 * @author iaw47951368 Classe candidat
 */
public class Candidat {
	private int codi;
	private String nom;
	private char sexe;
	private int edat;
	private char fumador;
	private int orientacio;
	private char mascotes;
	private int pes;
	private List<Candidat> compatibles;

	public int getCodi() {
		return codi;
	}

	public void setCodi(int codi) {
		this.codi = codi;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public char isFumador() {
		return fumador;
	}

	public void setFumador(char fumador) {
		this.fumador = fumador;
	}

	public char isMascotes() {
		return mascotes;
	}

	public void setMascotes(char mascotes) {
		this.mascotes = mascotes;
	}

	public List<Candidat> getCompatibles() {
		return compatibles;
	}

	public void setCompatibles(List<Candidat> l) {
		this.compatibles = l;
	}

	public void addCompatible(Candidat c) {
		this.compatibles.add(c);
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	public int getPes() {
		return pes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codi;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Candidat other = (Candidat) obj;
		if (codi != other.codi)
			return false;
		return true;
	}

	public int getOrientacio() {
		return orientacio;
	}

	public void setOrientacio(int orientacio) {
		this.orientacio = orientacio;
	}

	public Candidat(int codi, String nom, char sexe, int edat, char fumador, char mascotes, int orientacio) {
		super();
		this.codi = codi;
		this.nom = nom;
		this.sexe = sexe;
		this.edat = edat;
		this.fumador = fumador;
		this.orientacio = orientacio;
		this.mascotes = mascotes;
		this.compatibles = new ArrayList<Candidat>();
	}

	public int pes(Candidat c) {
		int pes = 0;
		// fum
		if (this.fumador == c.isFumador()) {
			pes += 20;
		}
		// mascotes
		if (this.mascotes == c.isMascotes()) {
			pes += 10;
		}
		// edat
		if (this.edat == c.getEdat()) {
			pes += 30;
		} else if (Math.abs(this.edat - c.getEdat()) <= 5) {
			pes += 20;
		} else if (Math.abs(this.edat - c.getEdat()) <= 10) {
			pes += 10;
		} else if (Math.abs(this.edat - c.getEdat()) <= 15) {
			pes += 10;
		} else {
			pes += 1;
		}
		return pes;
	}

	/*
	 * 1=hetero 2=homo 3=bisexual
	 */
	public boolean isCompatible(Candidat c) {
		// si this és hetero, la parella ha de ser del sexe oposat i hetero
		if (this.orientacio == 1 && c.getSexe() != this.sexe && c.getOrientacio() == 1) {
			return true;

		}
		// si this és homo, la parella ha de ser del mateix sexe i no hetero
		if (this.orientacio == 2 && c.getOrientacio() != 1 && this.sexe == c.getSexe()) {
			return true;
		}
		/*if (this.orientacio == 3) { // si this és bi
			// si la parella és del mateix sexe, no pot ser hetero
			if (this.sexe == c.getSexe() && c.getOrientacio() != 1) {
				return true;
				// si la parella és del sexe oposat, no pot ser homo
			} else if (this.sexe != c.getSexe() && c.getOrientacio() != 2) {
				return true;
			}
		}*/
		if (this.orientacio == 3 && c.getOrientacio() != 1) {
			return true;
		}
		return false;
	}

}

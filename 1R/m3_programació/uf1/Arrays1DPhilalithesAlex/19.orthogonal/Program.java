
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donats dos vectors numèrics reals amb el
     * mateix nombre d'elements, determina si són ortogonals o no. Dos vectors són ortogonals
     * si la seva contracció és igual a zero.
     */
    
    public boolean areOrthogonal(double[] array1, double[] array2) {
        //variable declaration
        boolean areOrthogonal = false;
        int suma = 0;
        //calcul
        for (int i = 0; i < array1.length; i++) {
            suma += array1[i] * array2[i];
        }
        if (suma == 0) {
            areOrthogonal = true;
        }
        return areOrthogonal;
        
    }
}

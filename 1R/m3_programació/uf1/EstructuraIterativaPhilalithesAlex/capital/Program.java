/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats el capital inicial, el % d'interès i el 
     * nombre d’anys de durada d’una inversió bancària capitalitzada segons interès compost,
     * calcula el capital obtingut al finalitzar l'últim any. El capital acumulat des de l'inici d'un any
     * i l'inici de l'any següent es determina per la fórmula: C2 = C1 + C1 * interes / 100
     * 
     */
    
    public double calculateFinalCapital(double ci, double interes, int anys) {
        //variable declaration
        double c1 = ci;
        
        int i = 1;
        //calcul
        while (i <= anys) {
            double c2 = c1 + c1 * interes / 100;
            c1 = c2;
            i++;
        }
        return c1;
    }
}

/*
 * Program.java        2016/1/12
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donada una cadena, determina si només conté dígits.
     * 
     * 
     */
    
    public boolean onlyDigits (String cadena) {
        //variable declaration
        boolean noDigit = false;
        int i = 0;
        boolean onlyDigits = false;
        //calcul
        while (!noDigit && i < cadena.length()) {
            if (cadena.charAt(i) < 48 || cadena.charAt(i) > 57) {
                noDigit = true;
            }
            i++;
        }
        if (!noDigit) {
            onlyDigits = true;
        }
        
        //retorn
        return onlyDigits;
        
        
        
    }
}

package org.iedt.jdbcdemo1;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Main {

	private static final String URL = "jdbc:postgresql://localhost:5432/java";

	static {

	}

	public static void createEstudiant(Estudiant e) {
		Connection connection;
		Statement statement;
		String name = e.getName();
		double nota = e.getNota();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			statement.executeUpdate("insert into estudiants (name, nota) values ('" + name + "'," + nota + ")");
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void delete(long id) {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			statement.executeUpdate("delete from estudiants where id = " + id);
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void update(Estudiant e) {
		long id = e.getId();
		String name = e.getName();
		double nota = e.getNota();
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			statement
					.executeUpdate("update estudiants set name = '" + name + "', nota = " + nota + " where id = " + id);
			connection.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static Estudiant findById(long id) {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			ResultSet rs = statement.executeQuery("select id, name, nota from estudiants where id = " + id);
			connection.close();
			rs.next();
			id = rs.getLong("id");
			String name = rs.getString("name");
			double nota = rs.getDouble("nota");
			Estudiant e = new Estudiant(id, name, nota);
			return e;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static List<Estudiant> getAll() {
		Connection connection;
		Statement statement;
		List<Estudiant> estudiants = new ArrayList<>();
		try {
			// Connection
			connection = DriverManager.getConnection(URL, "postgres", "");
			// Statement
			statement = connection.createStatement();
			// Execute query ResultSet
			ResultSet resultSet = statement.executeQuery("select * from estudiants order by id");
			// Recorrer ResultSet
			// printResultSetMetadata(resultSet);
			while (resultSet.next()) {
				String nom = resultSet.getString(2);
				long id = resultSet.getLong(1);
				double nota = resultSet.getDouble(3);
				estudiants.add(new Estudiant(id, nom, nota));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return estudiants;
	}

	public static void printDatabaseMetadata() {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");) {
			DatabaseMetaData dbm = connection.getMetaData();
			System.out.printf("Soporta basic ANSI 92 %b%n", dbm.supportsANSI92EntryLevelSQL());
			System.out.printf("Soporta intermidiate ANSI 92 %b%n", dbm.supportsANSI92IntermediateSQL());
			System.out.printf("Soporta full ANSI 92 %b%n", dbm.supportsANSI92FullSQL());

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// llista dels estudiants que superan una nota
	public static List<Estudiant> listSuperiorNota(double nota) {
		try (Connection connection = DriverManager.getConnection(URL, "postgres", "");
				Statement statement = connection.createStatement();) {
			// Execute query ResultSet
			ResultSet rs = statement.executeQuery("select id, name, nota from estudiants where nota > " + nota);
			connection.close();
			List<Estudiant> estudiants = new ArrayList<>();
			while (rs.next()) {
				long id = rs.getLong("id");
				String name = rs.getString("name");
				double enota = rs.getDouble("nota");
				estudiants.add(new Estudiant(id, name, enota));
			}
			return estudiants;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}
	// llista dels estudiants entre dos notes

	// llista dels estudiants amb un nom determinat

	// llista dels estudiants amb una nota per sobre de la mitjana

	public static void printResultSetMetadata(ResultSet rs) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		while (rs.next()) {
			for (int i = 1; i <= columns; i++) {
				System.out.printf(rs.getString(i) + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		listSuperiorNota();
	}

}

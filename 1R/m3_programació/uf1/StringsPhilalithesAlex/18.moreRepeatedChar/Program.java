/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     * Donats un caràcter i una cadena,
     * calcula quantes vegades apareix el caràcter dins la cadena.
     * 
     * 
     */
    
    public int occurrencesOfChar (String cadena, char caracter) {
        //vars
        int occurrencesOfChar = 0;
        int i = 0;
        //calc
        for (i = 0; i < cadena.length(); i++) {
            if (cadena.charAt(i) == caracter) {
                occurrencesOfChar++;
            }
        }
        //ret
        return occurrencesOfChar;
    }
    /*
     * Donada una cadena, determina
     * quin és el caràcter que apareix més cops. Retornarem un array de dues cel·les on la
     * primera cel·la contindrà el codi ASCII del caràcter més repetit i en la segona cel·la quants
     * cops apareix
     */
    public int[] moreRepeatedChar(String cadena) {
        //vars
        int[] repeatedChar = new int[2];
        int i = 0;
        int contador = 0;
        char mesRepetit = cadena.charAt(0);
        //calc
        for (i = 0; i < cadena.length(); i++) {
            if (occurrencesOfChar(cadena, cadena.charAt(i)) > contador) {
                contador = occurrencesOfChar(cadena, cadena.charAt(i));
                mesRepetit = cadena.charAt(i);
            }
        }                                          
        repeatedChar[0] = (int) mesRepetit;
        repeatedChar[1] = contador;
        return repeatedChar;
             
    }
    
}
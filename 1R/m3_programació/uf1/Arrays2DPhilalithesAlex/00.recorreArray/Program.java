/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Recorre un vector 2D per files i mostra'l
     * 
     * 
     */
    
    public int[][] recorreFiles(int[][] array) {
        //variables
        int i, j = 0;
        //calcul
        for (i = 0; i < array.length; i++) {
            for (j = 0; j < array[0].length; j++) {
                array[i][j] = 0;
            }
        }
        return array;
        
    }
    /*
     * Recorre un vector 2D per columnes i mostra'l
     * 
     * 
     */
    
    public int[][] recorreColumnes(int[][] array) {
        //variables
        int i, j = 0;
        //calcul
        for (i = 0; i < array[0].length; i++) {
            for (j = 0; j < array.length; j++) {
                array[i][j] = 0;
            }
        }
        return array;
        
    }
}

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de més de dos nombres
     * enters acabada en zero i escriu si hi ha dos nombres consecutius iguals (interrompent la
     * lectura de la seqüència si el trobes).
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int n0 = 0;
        int n1 = 1;
        int naux = 0;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una sequència de més de 2 nombres acabada en zero. \n Si hi ha 2 nombres consecutius iguals t'interrompiré!");
        while (n1 != n0 && n1 != 0) {
            n1 = naux;
            System.out.println("Escriu un nombre");
            n0 = sc.nextInt();
            if (n1 != n0 && n0 != 0) {
                System.out.println("Escriu un nombre");
                n1 = sc.nextInt();
                naux = n1;
            }
        }
        if (n1 == 0 || n0 == 0) {
            System.out.println("Has escrit zero; seqüència acabada.");
        }
        else {
        System.out.println("Has escrit 2 nombres consecutius iguals!");
        }
        
    }
}


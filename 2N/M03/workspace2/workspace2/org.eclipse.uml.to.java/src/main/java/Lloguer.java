/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Lloguer.
 * 
 * @author iaw47951368
 */
public class Lloguer {
	/**
	 * Description of the property preu.
	 */
	public double preu = 0D;
	
	/**
	 * Description of the property dataInici.
	 */
	public String dataInici = "";
	
	// Start of user code (user defined attributes for Lloguer)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Lloguer() {
		// Start of user code constructor for Lloguer)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Lloguer)
	
	// End of user code
	/**
	 * Returns preu.
	 * @return preu 
	 */
	public double getPreu() {
		return this.preu;
	}
	
	/**
	 * Sets a value to attribute preu. 
	 * @param newPreu 
	 */
	public void setPreu(double newPreu) {
	    this.preu = newPreu;
	}

	/**
	 * Returns dataInici.
	 * @return dataInici 
	 */
	public String getDataInici() {
		return this.dataInici;
	}
	
	/**
	 * Sets a value to attribute dataInici. 
	 * @param newDataInici 
	 */
	public void setDataInici(String newDataInici) {
	    this.dataInici = newDataInici;
	}



}

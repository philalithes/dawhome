/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Matricula.
 * 
 * @author iaw47951368
 */
public class Matricula {
	/**
	 * Description of the property codi.
	 */
	public String codi = "";
	
	// Start of user code (user defined attributes for Matricula)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Matricula() {
		// Start of user code constructor for Matricula)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Matricula)
	
	// End of user code
	/**
	 * Returns codi.
	 * @return codi 
	 */
	public String getCodi() {
		return this.codi;
	}
	
	/**
	 * Sets a value to attribute codi. 
	 * @param newCodi 
	 */
	public void setCodi(String newCodi) {
	    this.codi = newCodi;
	}



}

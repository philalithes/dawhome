# Modul passport #

1. Instal·lar modul passport
2. Configurar autenticació (fitxer index.js)
   a. Inclourem modul passport i creem la variable passport
   b. Crearem la funció passportConfig i li passem l'app // mes endevant farem referència local.js passant-li la app
	- passport.initialize()
	- passport.session()
	- passport.serializeUser
	- passport.deserializeUser
   c. Exportarem la funció
3.Configurar la app amb els mòduls
	cookie-parser
	express-session
	(npmjs.com)
4. Triar l'estrategia d'autenticació
	- login & password -> mòdul passport-local
	- Xarxa Social Facebook -> mòdul passport-facebook
	..........
5. Crear local.js l'estratègia (funció local) i definir la validació d'usuari(find a mongo)
   Crear l'endpoint que volem validar
   Exportar la funció
6. Al fitxer index requerirem el fitxer.js passant-li la app


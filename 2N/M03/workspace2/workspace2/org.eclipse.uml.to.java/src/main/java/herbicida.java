/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of herbicida.
 * 
 * @author iaw47951368
 */
public class herbicida {
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	/**
	 * Description of the property composicio.
	 */
	public String composicio = "";
	
	/**
	 * Description of the property preu.
	 */
	public double preu = 0D;
	
	// Start of user code (user defined attributes for herbicida)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public herbicida() {
		// Start of user code constructor for herbicida)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for herbicida)
	
	// End of user code
	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}

	/**
	 * Returns composicio.
	 * @return composicio 
	 */
	public String getComposicio() {
		return this.composicio;
	}
	
	/**
	 * Sets a value to attribute composicio. 
	 * @param newComposicio 
	 */
	public void setComposicio(String newComposicio) {
	    this.composicio = newComposicio;
	}

	/**
	 * Returns preu.
	 * @return preu 
	 */
	public double getPreu() {
		return this.preu;
	}
	
	/**
	 * Sets a value to attribute preu. 
	 * @param newPreu 
	 */
	public void setPreu(double newPreu) {
	    this.preu = newPreu;
	}



}

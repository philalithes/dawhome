package org.escoladeltreball.springrestdemo1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClientController {
	
	@Autowired
	private ClientDAO clientDAO;
	
	@RequestMapping("/clients/{id}")
	public Client findClientById(@PathVariable("id") long id) {
		return clientDAO.findById(id);
	}
	
	@RequestMapping("/clients/findByName/{nom}")
	public Client findClientByName(@PathVariable("nom") String nom) {
		return clientDAO.findByNom(nom);
	}
	
	@RequestMapping("/clients")
	public List<Client> findAll() {
		return clientDAO.findAll();
	}
	
	@RequestMapping(value = "/clients/insert/{nom}", method = RequestMethod.GET)
	public boolean insertClient(@PathVariable String nom) {
		return clientDAO.insertClient(nom);
	}
	
	@RequestMapping(value = "/clients/delete/{id}", method = RequestMethod.GET)
	public boolean deleteClient(@PathVariable int id) {
		return clientDAO.deleteClient(id);
	}
}

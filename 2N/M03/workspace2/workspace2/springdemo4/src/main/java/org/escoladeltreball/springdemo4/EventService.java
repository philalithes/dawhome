package org.escoladeltreball.springdemo4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {

	@Autowired
	private EventRepository eventRepository;
	
	public EventService() {
		
	}
	
	public void save(Event event) {
		eventRepository.saveAndFlush(event);
	}

}

#!/bin/bash
# Filename:		show_chars_in_string.sh
# Author:		pingui
# Date:			17/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./show_chars_in_string.sh arg1
# Description:	Script que utilitzant una funció ens mostra els diferents caracters de l'string	que es passa per la línia d'ordres


function show_chars_string {
	local cadena1=${!1}
	local length=${#cadena1}
	for i in $(seq 0 $(($length-1)))
	do
		echo "${cadena1:$i:1}"
	done
}

# If there is no arguments from the command line we inform everybody and exit from the program
if [ $# -ne 1 ]
then
	echo "error: sintaxi incorrecta"
	echo "us: $0 cadena"
	exit 1
fi


cadena=$1 
show_chars_string cadena


# More info at:  http://tldp.org/LDP/abs/html/string-manipulation.html
# Alternative way to calculate a string length:
# expr length "$string"


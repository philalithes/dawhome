/*
 * Program.java        2015/9/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donat un nombre enter positiu, escriu els seus divisors positius.
     * 
     */
    
    public void writeDivisors(int n) {
        //variable declaration
        double d = 0;
        while (d <= n){
            d++;
            if (n % d == 0) {
                System.out.println(d);
            }
        }
    }
}
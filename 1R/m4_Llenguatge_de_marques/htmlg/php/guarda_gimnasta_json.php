<?php
	// Llefirm el fitxer gym2.json, visualitzem les dades d'objectes json, modifiquem una dada i guardem el fitxer
    $function = $_POST['accio'];
	$file='../UF3/gym3.json';
	$dades = json_decode(file_get_contents($file),TRUE);
	//var_dump($dades);
	
    if ($function == 'INS') {
        $nom = $_POST['new_name'];
        $club = $_POST['new_club'];
        
        $barra1 = $_POST['new_barra1'];
        $asimetriques1 = $_POST['new_asimetriques1'];
        $terra1 = $_POST['new_terra1'];
        $potro1 = $_POST['new_potro1'];
        
        $barra2 = $_POST['new_barra2'];
        $asimetriques2 = $_POST['new_asimetriques2'];
        $terra2 = $_POST['new_terra2'];
        $potro2 = $_POST['new_potro2'];
        
        $barra3 = $_POST['new_barra3'];
        $asimetriques3 = $_POST['new_asimetriques3'];
        $terra3 = $_POST['new_terra3'];
        $potro3 = $_POST['new_potro3'];
        
        $dades['gimnastes'][$nom]['Club'] = $club;
        
        $dades['gimnastes'][$nom]['Fase1']['Barra'] = $barra1;
        $dades['gimnastes'][$nom]['Fase1']['Terra'] = $terra1;
        $dades['gimnastes'][$nom]['Fase1']['Asimetriques'] = $asimetriques1;
        $dades['gimnastes'][$nom]['Fase1']['Potro'] = $potro1;
        
        $dades['gimnastes'][$nom]['Fase2']['Barra'] = $barra2;
        $dades['gimnastes'][$nom]['Fase2']['Terra'] = $terra2;
        $dades['gimnastes'][$nom]['Fase2']['Asimetriques'] = $asimetriques2;
        $dades['gimnastes'][$nom]['Fase2']['Potro'] = $potro2;
        
        $dades['gimnastes'][$nom]['Fase3']['Barra'] = $barra3;
        $dades['gimnastes'][$nom]['Fase3']['Terra'] = $terra3;
        $dades['gimnastes'][$nom]['Fase3']['Asimetriques'] = $asimetriques3;
        $dades['gimnastes'][$nom]['Fase3']['Potro'] = $potro3;
    }
	/*// Visualitzem un valor
	echo "<br>";
	echo $dades['gimnastes'][0]['Nom'];
	echo "<br>";
	
	// Visualitzem totes les puntuacions de la Fase1 de la gimnasta 0
	foreach ($dades['gimnastes'][0]['Fase1'] as $index => $valor) 
	{
		echo $index . " : " . $valor."<br>";
	}
	echo "<br>";
	
	// Visualitzem totes les puntuacions de la Fase1 de totes les gimnastes
	for ($i=0; $i<count($dades['gimnastes']);$i++)
	{
		echo $dades['gimnastes'][$i]['Nom']."<br>";
		foreach ($dades['gimnastes'][$i]['Fase1'] as $index => $valor) 
		{
			echo $index . " : " . $valor."<br>";
		}
	}
	echo "<br>";
	
	// Visualitzem totes les puntuacions de totes les fases de totes les gimnastes
	for ($i=0; $i<count($dades['gimnastes']);$i++)
	{
		echo $dades['gimnastes'][$i]['Nom']."<br>";
		for ($x=1; $x<4;$x++)
		{
			foreach ($dades['gimnastes'][$i]['Fase'.$x] as $index => $valor) 
			{
				echo $index . " : " . $valor."<br>";
			}
		}
	}
	echo "<br>";
	
	// Modifiquem un valor
	$dades['gimnastes'][0]['Fase1']['Potro']=7000;*/

	file_put_contents($file, json_encode($dades,TRUE));
    header('Location: ../UF3/practica_16_json.html');
?>


package org.escoladeltreball.abuelamaria;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {

				List<Integer> dentsUp = new ArrayList<>(); // dents superiors
				List<Integer> dentsDown = new ArrayList<>(); // dents inferiors
				for (int k = 0; k < 6; k++) {
					dentsUp.add(sc.nextInt());
				}

				for (int k = 0; k < 6; k++) {
					dentsDown.add(sc.nextInt());
				}
				int[] suma = new int[6];

				for (int k = 0; k < dentsUp.size(); k++) {
					suma[k] = dentsUp.get(k) + dentsDown.get(k);
				}
				if (suma[0] == suma[1] && suma[0] == suma[2] && suma[0] == suma[3] && suma[0] == suma[4] && suma[0] == suma[5]) {
					System.out.println("SI");
				} else {
					System.out.println("NO");
				}
			

			
		}
		sc.close();
	}

}

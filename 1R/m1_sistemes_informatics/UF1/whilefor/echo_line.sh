#!/bin/bash
# Filename:			echo_line.sh
# Author:			iaw47951368
# Date:				17/12/15
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./echo_line.sh [< archivo]
# Description:		

i=1

while read line
do
	echo "$i $(echo "$line | wc -m") $line"
	i=$(($i+1))
done

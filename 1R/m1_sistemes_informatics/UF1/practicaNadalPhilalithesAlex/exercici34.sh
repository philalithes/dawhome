#!/bin/bash
# Filename:		exercici34.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici34.sh [arg1...]
# Description:		Script que determina si els números que se li passen són parells o senars.

#control d'arguments
if [ $# -eq 0 ]
then
	echo "Almenys hi ha d'haver un argument"
	echo "ajuda: ./exercici34.sh [arg1...]"
	exit 1
fi

#comanda per determinar si el primer argument és parell o senar
echo "scale=0;$1%2" | bc -l | sed s/0/$1\ parell/ | sed s/1/$1\ senar/

#bucle per fer el mateix amb la resta d'arguments
while shift && [ $# -gt 0 ]
do
	echo "scale=0;$1%2" | bc -l | sed s/0/$1\ parell/ | sed s/1/$1\ senar/
done	

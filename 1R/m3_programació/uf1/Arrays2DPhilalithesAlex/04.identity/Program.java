/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donat un nombre enter major que zero, crea la matriu
     * identitat de dimensió el nombre enter donat. Per exemple, si el número és 5 la matriu
     * identitat de dimensió 5 és:
     * 1 0 0 0 0
     * 0 1 0 0 0
     * 0 0 1 0 0
     * 0 0 0 1 0
     * 0 0 0 0 1
     * 
     * 
     */
    
    public int[][] identity(int n) {
        //variables
        int i, j = 0;
        int[][] identity = new int[n][n];
        //calcul
        for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
                if (j == i) {
                    identity [i][j] = 1;
                } else {
                    identity [i][j] = 0;
                }
            }
            
        }
        return identity;
    }
}

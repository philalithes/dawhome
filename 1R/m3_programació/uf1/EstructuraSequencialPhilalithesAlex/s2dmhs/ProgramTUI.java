/*
 * Program.java 2015/21/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * Llegeix un nombre enter que designa un període de temps expressats en segons, escriu l'equivalent en dies,
  * hores, minuts i segons escrivint la següent frase “s segons seran d dies, h hores, m minuts i s segons.”
  * 
  * @param s = segons
  * 
  * @param d = dies
  * @param h = hores
  * @param m = minuts
  * @param ss = segons
  * 
  * @return string
  */


    
    public String s2dhms(int s) {
        int d;
        int h;
        int m;
        int ss;
        d = s / 86400;
        h = (s - d * 86400) / 3600;
        m = (s - h * 3600 - d * 86400) / 60;
        ss = s - h * 3600 - m * 60 - d * 86400;
        
        String hms = s + " segons seràn " + d + " dies, " + h + " hores, " + m + " minuts i " + ss + " segons";
        return hms;
        
    }
    public static void main(String args[]){
        String hms;
        //declaracio de variables
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("introdueix els segons");
        int s = sc.nextInt();
        //càlcul
        ProgramTUI p = new ProgramTUI();
        hms = p.s2dhms(s);
        //retorn
        System.out.printf("%s", hms);
    }
}

/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Prestec.
 * 
 * @author iaw47951368
 */
public class Prestec {
	/**
	 * Description of the property data.
	 */
	public String data = "";
	
	/**
	 * Description of the property nDies.
	 */
	public Integer nDies = Integer.valueOf(0);
	
	// Start of user code (user defined attributes for Prestec)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Prestec() {
		// Start of user code constructor for Prestec)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Prestec)
	
	// End of user code
	/**
	 * Returns data.
	 * @return data 
	 */
	public String getData() {
		return this.data;
	}
	
	/**
	 * Sets a value to attribute data. 
	 * @param newData 
	 */
	public void setData(String newData) {
	    this.data = newData;
	}

	/**
	 * Returns nDies.
	 * @return nDies 
	 */
	public Integer getNDies() {
		return this.nDies;
	}
	
	/**
	 * Sets a value to attribute nDies. 
	 * @param newNDies 
	 */
	public void setNDies(Integer newNDies) {
	    this.nDies = newNDies;
	}



}

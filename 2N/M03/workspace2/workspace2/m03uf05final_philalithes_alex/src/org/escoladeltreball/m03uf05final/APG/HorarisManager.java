package org.escoladeltreball.m03uf05final.APG;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HorarisManager {
	private List<Professor> professors;
	private List<Registre> registres;

	@Override
	public String toString() {
		return "HorarisManager [professors=" + professors + ", registres=" + registres + "]";
	}

	public List<Registre> getRegistres() {
		return registres;
	}

	public void setRegistres(ArrayList<Registre> registres) {
		this.registres = registres;
	}

	public HorarisManager(List<Professor> professors, List<Registre> registres) {
		super();
		this.professors = professors;
		this.registres = registres;
	}

	public List<Professor> getProfessors() {
		return professors;
	}

	public void setProfessors(ArrayList<Professor> professors) {
		this.professors = professors;
	}

	public void solve() {
		// ordenem els professors per id.
		professors = professors.stream().sorted((p1, p2) -> new Long(p1.getId()).compareTo(new Long(p2.getId())))
				.collect(Collectors.toList());
		// variables
		String out = "";
		DayOfWeek lastDay = null;
		// per cada professor
		for (Professor p : professors) {
			long id = p.getId();
			List<Registre> registresDelProfessor = registres.stream().filter(r -> r.getId() == id)
					.collect(Collectors.toList());
			// per cada registre de cada professor
			for (Registre r : registresDelProfessor) {
				//variables del registre
				boolean isExit = false;
				LocalDate date = r.getDateTime().toLocalDate();
				LocalTime hour = r.getDateTime().toLocalTime();
				DayOfWeek day = r.getDateTime().getDayOfWeek();
				if (day.equals(lastDay)) { // si estem mirant el mateix dia que
											// l'anterior registre, el professor
											// s'en va.
					isExit = true;
				}
				lastDay = day;

				// construcció de sortida
				if (!isExit) { // un cop per cada entrada i sortida
					out += id + "," + date + ",";
				}

				// segons el dia de la setmana, agafem l'horari del professor i
				// el comparem amb l'hora d'arribada o sortida corresponent
				if (day == DayOfWeek.MONDAY && !isExit) {
					if (p.getHDillunsIn().isBefore(hour)) { // si el professor
															// ha arribat tard
						Duration d = Duration.between(p.getHDillunsIn(), hour);
						out += getRetard(d) + ",";
					} else {
						out += "00:00:00,";
					}
				} else if (day == DayOfWeek.MONDAY && isExit) {
					if (p.getHDillunsOut().isAfter(hour)) { // si el professor
															// s'ha marxat abans
															// d'hora
						Duration d = Duration.between(hour, p.getHDillunsOut());
						out += getRetard(d);
					} else {
						out += "00:00:00";
					}
				} else if (day == DayOfWeek.TUESDAY && !isExit) {
					if (p.getHDimartsIn().isBefore(hour)) {
						Duration d = Duration.between(p.getHDimartsIn(), hour);
						out += getRetard(d) + ",";
					} else {
						out += "00:00:00,";
					}
				} else if (day == DayOfWeek.TUESDAY && isExit) {
					if (p.getHDimartsOut().isAfter(hour)) {
						Duration d = Duration.between(hour, p.getHDimartsOut());
						out += getRetard(d);
					} else {
						out += "00:00:00";
					}
				} else if (day == DayOfWeek.WEDNESDAY && !isExit) {
					if (p.getHDimecresIn().isBefore(hour)) {
						Duration d = Duration.between(p.getHDimecresIn(), hour);
						out += getRetard(d) + ",";
					} else {
						out += "00:00:00,";
					}
				} else if (day == DayOfWeek.WEDNESDAY && isExit) {
					if (p.getHDimecresOut().isAfter(hour)) {
						Duration d = Duration.between(hour, p.getHDimecresOut());
						out += getRetard(d);
					} else {
						out += "00:00:00";
					}
				} else if (day == DayOfWeek.THURSDAY && !isExit) {
					if (p.getHDijousIn().isBefore(hour)) {
						Duration d = Duration.between(p.getHDijousIn(), hour);
						out += getRetard(d) + ",";
					} else {
						out += "00:00:00,";
					}
				} else if (day == DayOfWeek.THURSDAY && isExit) {
					if (p.getHDijousOut().isAfter(hour)) {
						Duration d = Duration.between(hour, p.getHDijousOut());
						out += getRetard(d);
					} else {
						out += "00:00:00";
					}
				} else if (day == DayOfWeek.FRIDAY && !isExit) {
					if (p.getHDivendresIn().isBefore(hour)) {
						Duration d = Duration.between(p.getHDivendresIn(), hour);
						out += getRetard(d) + ",";
					} else {
						out += "00:00:00,";
					}
				} else if (day == DayOfWeek.FRIDAY && isExit) {
					if (p.getHDivendresOut().isAfter(hour)) {
						Duration d = Duration.between(hour, p.getHDivendresOut());
						out += getRetard(d);
					} else {
						out += "00:00:00";
					}
				}
				if (isExit) {
					out += "\n";
				}
			}
		}
		System.out.println(out);
	}

	/*
	 * retorna la duració d en format HH:MM:SS
	 */
	public String getRetard(Duration d) {
		int horaTard = (int) d.toHours();
		d = d.minusHours(horaTard);
		int minTard = (int) d.toMinutes();
		d = d.minusMinutes(minTard);
		int segonsTard = (int) d.toMillis() / 1000;
		String out = "";
		if (horaTard < 10) {
			out += "0" + horaTard + ":";
		} else {
			out += horaTard + ":";
		}
		if (minTard < 10) {
			out += "0" + minTard + ":";
		} else {
			out += minTard + ":";
		}
		if (segonsTard < 10) {
			out += "0" + segonsTard;
		} else {
			out += segonsTard;
		}
		return out;
	}

}

package org.escoladeltreball.iodemo8;

public class Moviment {
	private String descripcio;
	private String date;
	private int cant;

	public Moviment(String descripcio, String date, int cant) {
		super();
		this.descripcio = descripcio;
		this.date = date;
		this.cant = cant;
	}

	public Moviment(String moviment) {
		this.descripcio = moviment.substring(0, moviment.indexOf("/")-3);
		this.date = moviment.substring(moviment.indexOf("/")-2,moviment.indexOf("/")+8);
		this.cant = Integer.parseInt(moviment.substring(moviment.indexOf("/") + 9, moviment.length()));
	}
	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getCant() {
		return cant;
	}

	public void setCant(int cant) {
		this.cant = cant;
	}

}

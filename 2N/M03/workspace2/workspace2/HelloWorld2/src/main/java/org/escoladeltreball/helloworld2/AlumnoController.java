package org.escoladeltreball.helloworld2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AlumnoController {

	@Autowired
	private AlumnoService service;
	
	public AlumnoController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping("/alumno/{id}")
	public ModelAndView findAlumno(@PathVariable Integer id) {
		//model.addAttribute("id", id);
		
		ModelAndView model = new ModelAndView("alumno");
		
		Alumno alumno = service.FindById(id);
		model.addObject("alumno", alumno);
		return model;
	}
}

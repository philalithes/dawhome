#!/bin/bash
# Filename:			chronfor.sh
# Author:			iaw47951368
# Date:				15/12/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./chronfor.sh [args...]
# Description:		chrono

for num in 1 2 3 4 5 6 7 8 9 10
do
	echo -en "/r$num "
done

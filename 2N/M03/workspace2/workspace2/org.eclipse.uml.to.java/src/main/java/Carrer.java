/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Carrer.
 * 
 * @author iaw47951368
 */
public class Carrer {
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	/**
	 * Description of the property fincas.
	 */
	public HashSet<Finca> fincas = new HashSet<Finca>();
	
	// Start of user code (user defined attributes for Carrer)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Carrer() {
		// Start of user code constructor for Carrer)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Carrer)
	
	// End of user code
	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}

	/**
	 * Returns fincas.
	 * @return fincas 
	 */
	public HashSet<Finca> getFincas() {
		return this.fincas;
	}



}

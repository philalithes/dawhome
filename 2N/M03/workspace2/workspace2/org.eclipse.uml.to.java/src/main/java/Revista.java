/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Revista.
 * 
 * @author iaw47951368
 */
public class Revista extends Material audiovisual {
	/**
	 * Description of the property autor.
	 */
	public String autor = "";
	
	/**
	 * Description of the property titol.
	 */
	public String titol = "";
	
	// Start of user code (user defined attributes for Revista)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Revista() {
		// Start of user code constructor for Revista)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Revista)
	
	// End of user code
	/**
	 * Returns autor.
	 * @return autor 
	 */
	public String getAutor() {
		return this.autor;
	}
	
	/**
	 * Sets a value to attribute autor. 
	 * @param newAutor 
	 */
	public void setAutor(String newAutor) {
	    this.autor = newAutor;
	}

	/**
	 * Returns titol.
	 * @return titol 
	 */
	public String getTitol() {
		return this.titol;
	}
	
	/**
	 * Sets a value to attribute titol. 
	 * @param newTitol 
	 */
	public void setTitol(String newTitol) {
	    this.titol = newTitol;
	}



}

var Pelicula = require('../model/peli.js');
bodyParser = require('body-parser');

module.exports = function (app) {

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    // Creo un endpoint al formulario
    app.get('/', function (req, res) {
        res.render('home');
    });

    app.get('/list', llistarPelis = function (req, res) {
        return Pelicula.find(function (err, pelis) {
            if (!err) {
                console.log('Mostrant pelis....');
                return res.render('mostrarPelis', {
                    title: 'Pel.licules',
                    pelis: pelis
                });
            } else {
                return console.log('Error: ' + err);
            }
        });
    });

    app.get('/list/:id', function (req, res) {
        return Pelicula.find({
            'Codpeli': req.params.id
        }, function (err, peli) {
            if (!err) {
                if (!peli.length) {
                    return res.render('mostrarPeli', {error: "Ese código de pelicula no existe"});
                }
                console.log('Mostrant peli....');
                return res.render('mostrarPeli', {
                    title: 'Pel.licules',
                    peli: peli
                });
            } else {
                return console.log('Error: ' + err);
            }
        });
    });

    // Creo un endpoint para añadir una pelicula
    app.get('/add', function (req, res) {
        res.render('form');
    });

    // Añado una pelicula
    app.post('/add', function (req, res) {
        // Busco si la pelicula ya existe
        Pelicula.find({
            'Codpeli': req.body.codpeli
        }, function (err, peli) {
            // En el caso de que ya exista le paso el Codpeli para mostrar un error
            if (peli.length) {
                return res.render('form', {
                    codpeli: req.body.codpeli
                });
                // Si el codpeli no existe, inserto en la bbdd mongo
            } else {
                var pelicula = new Pelicula({
                    Codpeli: req.body.codpeli,
                    Titol: req.body.titol,
                    Sinopsi: req.body.sinopsi
                });

                pelicula.save(function (err) {
                    if (err) throw err;
                });
                return res.render('form', {
                    pelicula: "Pelicula añadida correctamente"
                });
            }
        });
    });

    app.get('/update', function (req, res) {
        Pelicula.find({}, {Codpeli: 1, _id: 0}, function (err, codPelis) {
            if (!err) {
                console.log('Mostrant pelis....');
                return res.render('formUpdate', {
                    title: 'Pel.licules',
                    codPelis: codPelis
                });
            } else {
                return console.log('Error: ' + err);
            }
        });
    });

    // Modifico una pelicula
    app.post('/update', function (req, res) {
        Pelicula.findOneAndUpdate({Codpeli: req.body.codpeli}, {
                Titol: req.body.titol,
                Sinopsi: req.body.sinopsi
            }, function (err, pelicula) {
                if (err) throw err;
                Pelicula.find({}, {Codpeli: 1, _id: 0}, function (err, codPelis) {
                    if (!err) {
                        return res.render('formUpdate', {
                            codPelis: codPelis,
                            error: "Pelicula modificada correctamente"
                        });
                    } else {
                        return console.log('Error: ' + err);
                    }
                });
            }
        );
    });

    app.get('/delete', function (req, res) {
        Pelicula.find({}, {Codpeli: 1, Titol: 1, _id: 0}, function (err, pelicules) {
            if (!err) {
                console.log('Mostrant pelis....');
                return res.render('formDelete', {
                    title: 'Pel.licules',
                    pelicules: pelicules
                });
            } else {
                return console.log('Error: ' + err);
            }
        });
    });

    // Elimino una pelicula
    app.post('/delete', function (req, res) {
        Pelicula.findOneAndRemove({Codpeli: req.body.codpeli}, function (err, pelicula) {
                if (err) throw err;
                Pelicula.find({}, {Codpeli: 1, Titol: 1, _id: 0}, function (err, pelicules) {
                    if (!err) {
                        return res.render('formDelete', {
                            pelicules: pelicules,
                            error: "Pelicula eliminada correctamente"
                        });
                    } else {
                        return console.log('Error: ' + err);
                    }
                });
            }
        );
    });
};

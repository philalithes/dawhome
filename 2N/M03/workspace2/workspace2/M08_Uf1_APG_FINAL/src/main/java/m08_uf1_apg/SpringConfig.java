package m08_uf1_apg;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan
@EnableAspectJAutoProxy(proxyTargetClass=true)
@PropertySource("classpath:/application.properties")
@EnableJpaRepositories
public class SpringConfig {

	public SpringConfig() {
		// TODO Auto-generated constructor stub
	}

}

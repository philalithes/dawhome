var fs = require("fs");


fs.readFile("file1.txt", function(err, data) {
	if (err) return console.log(err);
	console.log(data.toString());
});

var data = fs.readFileSync("file1.txt");
console.log(data.toString());


fs.readFile("file2.txt", function(err, data) {
	if (err) return console.log(err);
	console.log(data.toString());
});

var data = fs.readFileSync("file3.txt");
console.log(data.toString());

fs.readFile("file2.txt", function(err, data) {
	if (err) return console.log(err);
	console.log(data.toString());
});

var data = fs.readFileSync("file3.txt");
console.log(data.toString());
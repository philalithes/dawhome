<?php
    $function = $_POST['accio'];
    $doc = new DOMDocument('1.0', 'UTF-8');
    // print
    $doc->formatOutput = true;
    $doc->prserveWhiteSpace = false;
    $doc->load('notes-departament.xml');

    if ($function == 'INS') {
        $nom = $_POST['new_name'];
        $cognom1 = $_POST['new_cognom1'];
        $cognom2 = $_POST['new_cognom2'];
        $nota_programacio = $_POST['nota_programacio'];
        $nota_xml = $_POST['nota_xml'];
        $nota_bd = $_POST['nota_bd'];
        
        // departament
        $root = $doc->firstChild;
        
        // alumnes
        

        $new_alumne = $doc->createElement('alumne');
        $new_alumne = $root->appendChild($new_alumne);

        $new_nom = $doc->createElement('nom');
        $new_nom = $new_alumne->appendChild($new_nom);
        $text = $doc->createTextNode($nom);
        $text = $new_nom->appendChild($text);
        
        $new_cognom1 = $doc->createElement('cognom');
        $new_cognom1 = $new_alumne->appendChild($new_cognom1);
        $text = $doc->createTextNode($cognom1);
        $text = $new_cognom1->appendChild($text);
        
        $new_cognom2 = $doc->createElement('cognom2');
        $new_cognom2 = $new_alumne->appendChild($new_cognom2);
        $text = $doc->createTextNode($cognom2);
        $text = $new_cognom2->appendChild($text);
        
        $credits = $doc->createElement('credits');
        $credits = $new_alumne->appendChild($credits);
        
        $credit1 = $doc->createElement('credit');
        $credit1 = $credits->appendChild($credit1);
        $new_creditnom = $doc->createElement('nom');
        $new_creditnom = $credit1->appendChild($new_creditnom);
        $text = $doc->createTextNode('Programació');
        $text = $new_creditnom->appendChild($text);
        $new_nota = $doc->createElement('nota');
        $new_nota = $credit1->appendChild($new_nota);
        $text = $doc->createTextNode($nota_programacio);
        $text = $new_nota->appendChild($text);
        
        $credit2 = $doc->createElement('credit');
        $credit2 = $credits->appendChild($credit2);
        $new_creditnom = $doc->createElement('nom');
        $new_creditnom = $credit2->appendChild($new_creditnom);
        $text = $doc->createTextNode('Llenguatges de marques');
        $text = $new_creditnom->appendChild($text);
        $new_nota = $doc->createElement('nota');
        $new_nota = $credit2->appendChild($new_nota);
        $text = $doc->createTextNode($nota_xml);
        $text = $new_nota->appendChild($text);
        
        $credit3 = $doc->createElement('credit');
        $credit3 = $credits->appendChild($credit3);
        $new_creditnom = $doc->createElement('nom');
        $new_creditnom = $credit3->appendChild($new_creditnom);
        $text = $doc->createTextNode('Base de dades');
        $text = $new_creditnom->appendChild($text);
        $new_nota = $doc->createElement('nota');
        $new_nota = $credit3->appendChild($new_nota);
        $text = $doc->createTextNode($nota_bd);
        $text = $new_nota->appendChild($text);
    } /*else {
        $noms = $doc->getElementsByTagName('nom');
        $nom = utf8_encode($_POST['fnom']);
        $new_nom = utf8_encode($_POST['gnom']);
        $club = utf8_encode($_POST['gclub']);
        $barra = utf8_encode($_POST['gbarra']);
        $potro = utf8_encode($_POST['gpotro']);
        $asim  = utf8_encode($_POST['gasimetriques']);
        $terra  = utf8_encode($_POST['gterra']);
        
        for ($i = 0; $i < $noms->length; $i++) {
            if ($noms->item($i)->nodeValue == $nom) {
                // Save data
                $npare=$noms->item($i)->parentNode;
                $n=$npare->getElementsByTagName("nom");
                $n->item(0)->nodeValue=$new_nom;
                $n=$npare->getElementsByTagName("club");
                $n->item(0)->nodeValue=$club;
                $n=$npare->getElementsByTagName("barra");
                $n->item(0)->nodeValue=$barra;
                $n=$npare->getElementsByTagName("terra");
                $n->item(0)->nodeValue=$terra;
                $n=$npare->getElementsByTagName("asimetriques");
                $n->item(0)->nodeValue=$asim;
                $n=$npare->getElementsByTagName("potro");
                $n->item(0)->nodeValue=$potro;
            }
        }
        
    }*/

    $doc->save('notes-departament.xml');
    header('Location: ./GioPlataJustiniano.html');
?>
var express = require('express'),
    mongoose = require('mongoose'),
    app = express();


mongoose.connect('mongodb://localhost/videoclub', function(err, res){
    if(err) {
        console.log("Error al conectar-se a la BBDD pelis...");
    } else {
    	console.log("Conectat a videoclub");
    }
});

app.use(express.static(__dirname + '/public'));

// Linea opcional para los .ejs
//app.engine('jade', require('jade').__express);

app.set('view engine', 'jade');

require('./routes/controller')(app);

app.listen(3000, function(){
    console.log('Node server running on http://localhost:3000');
});

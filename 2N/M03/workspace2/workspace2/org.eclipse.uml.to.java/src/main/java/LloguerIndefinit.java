/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of LloguerIndefinit.
 * 
 * @author iaw47951368
 */
public class LloguerIndefinit extends Lloguer {
	/**
	 * Description of the property incrementAnual.
	 */
	public double incrementAnual = 0D;
	
	// Start of user code (user defined attributes for LloguerIndefinit)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public LloguerIndefinit() {
		// Start of user code constructor for LloguerIndefinit)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for LloguerIndefinit)
	
	// End of user code
	/**
	 * Returns incrementAnual.
	 * @return incrementAnual 
	 */
	public double getIncrementAnual() {
		return this.incrementAnual;
	}
	
	/**
	 * Sets a value to attribute incrementAnual. 
	 * @param newIncrementAnual 
	 */
	public void setIncrementAnual(double newIncrementAnual) {
	    this.incrementAnual = newIncrementAnual;
	}



}

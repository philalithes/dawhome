<!doctype html>
<html>
<body>
<?php

$doc = new DOMDocument('1.0', 'utf-8');
$doc->formatOutput = true;
$doc->preserveWhiteSpace = false;
$doc->load('gym.xml');

$funcio = $_POST['accio'];
$nom = $_POST['nom'];
$club = $_POST['club'];
$barra = $_POST['barra'];
$asim = $_POST['asimetriques'];
$terra = $_POST['terra'];
$potro = $_POST['potro'];

$noms=$doc->getElementsByTagName("nom");
if($funcio == "MOD")
{
    //modificar nena
}
else
{
    //afegir nena
    $arrel = $doc->firstChild;

    $nena = $doc->createElement('nena');
    $nena = $arrel->appendChild($nena);

    $nouNom = $doc->createElement('nom');
    $nouNom = $nena->appendChild($nouNom);

    $txt = utf8_encode($nom);
    $text = $doc->createTextNode($txt);
    $text = $nouNom->appendChild($text);

    $nouClub = $doc->createElement('club');
    $nouClub = $nena->appendChild($nouClub);

    $txt = utf8_encode($_POST['club']);
    $text = $doc->createTextNode($txt);
    $text = $nouClub->appendChild($text);

    $fase = $doc->createElement('fase1');
    $fase = $nena->appendChild($fase);

    $nouBarra = $doc->createElement('barra');
    $nouBarra = $fase->appendChild($nouBarra);

    $txt = utf8_encode($_POST['barra']);
    $text = $doc->createTextNode($txt);
    $text = $nouBarra->appendChild($text);

    $nouAsimetriq = $doc->createElement('asimetriques');
    $nouAsimetriq = $fase->appendChild($nouAsimetriq);

    $txt = utf8_encode($_POST['asimetriques']);
    $text = $doc->createTextNode($txt);
    $text = $nouAsimetriq->appendChild($text);

    $nouTerra = $doc->createElement('terra');
    $nouTerra = $fase->appendChild($nouTerra);

    $txt = utf8_encode($_POST['terra']);
    $text = $doc->createTextNode($txt);
    $text = $nouTerra->appendChild($text);

    $nouPotro = $doc->createElement('potro');
    $nouPotro = $fase->appendChild($nouPotro);

    $txt = utf8_encode($_POST['potro']);
    $text = $doc->createTextNode($txt);
    $text = $nouPotro->appendChild($text);
}

$doc->save('gym.xml');

$stringXML = $doc->saveXML();
echo $stringXML;
?>
</body>
</html>
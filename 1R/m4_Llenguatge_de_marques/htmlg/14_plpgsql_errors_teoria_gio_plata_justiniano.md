# Captura d'errors

Sintaxi: 

```
[ DECLARE
    declarations ]
BEGIN
    statements
EXCEPTION
    WHEN condition [ OR condition ... ] THEN
        handler_statements
    [ WHEN condition [ OR condition ... ] THEN
          handler_statements
      ... ]
END;
```

Els possibles errors els trobem a: [http://www.postgresql.org/docs/9.3/static/errcodes-appendix.html](http://www.postgresql.org/docs/9.3/static/errcodes-appendix.html)

Exemples:
```
INSERT INTO mytab(firstname, lastname) VALUES('Tom', 'Jones');
BEGIN
    UPDATE mytab SET firstname = 'Joe' WHERE lastname = 'Jones';
    x := x + 1;
    y := x / 0;
EXCEPTION
    WHEN division_by_zero THEN
        RAISE NOTICE 'caught division_by_zero';
        RETURN x;
END;

CREATE TABLE db (a INT PRIMARY KEY, b TEXT);

CREATE FUNCTION merge_db(key INT, data TEXT) RETURNS VOID AS
$$
BEGIN
    LOOP
        -- first try to update the key
        UPDATE db SET b = data WHERE a = key;
        IF found THEN
            RETURN;
        END IF;
        -- not there, so try to insert the key
        -- if someone else inserts the same key concurrently,
        -- we could get a unique-key failure
        BEGIN
            INSERT INTO db(a,b) VALUES (key, data);
            RETURN;
        EXCEPTION WHEN unique_violation THEN
            -- Do nothing, and loop to try the UPDATE again.
        END;
    END LOOP;
END;
$$
LANGUAGE plpgsql;

SELECT merge_db(1, 'david');
SELECT merge_db(1, 'dennis');
```

Poner los valores del campo num_empl de repventas a null. Saldrá un error porque este campo no puede ser null por ser clave primaria

```
CREATE OR REPLACE FUNCTION set_num_empl_null() RETURNS void AS $$
	BEGIN
		UPDATE repventas
		SET num_empl = null;
		RAISE NOTICE 'Setting to null successful';
		EXCEPTION
			-- not null violation error will show this message
			WHEN not_null_violation THEN
				RAISE NOTICE 'Setting num_empl to null not allowed!';
	END;
$$ LANGUAGE plpgsql;
```

Return del campo importe como int. Saldrá un error porque sus valores son de tipo numeric (No funciona, no encuentro el error code correcto para este caso)

```
CREATE OR REPLACE FUNCTION get_importe(in_pedido pedidos.num_pedido%TYPE) RETURNS int AS $$
	DECLARE
		text_var1 text;
		text_var2 text;
		text_var3 text;
		i numeric(8,2);
	BEGIN
		SELECT importe INTO i
		FROM pedidos
		WHERE num_pedido = in_pedido;
		RETURN i;
		EXCEPTION
			WHEN invalid_text_representation THEN
				RAISE NOTICE 'Invalid return data type. Importe is numeric';
			WHEN OTHERS THEN
				  GET STACKED DIAGNOSTICS text_var1 = MESSAGE_TEXT,
										  text_var2 = PG_EXCEPTION_DETAIL,
										  text_var3 = PG_EXCEPTION_HINT;
			RAISE NOTICE '%, %, %', text_var1, text_var2, text_var3;
		
	END;
$$ LANGUAGE plpgsql;
```

Llamar una función que no existe dentro de otra función

```
CREATE OR REPLACE FUNCTION function1() RETURNS void AS $$
	BEGIN
		SELECT function2();
		EXCEPTION
			WHEN undefined_function THEN
				RAISE NOTICE 'FUNCTION DOES NOT EXIST';
	END;
$$ LANGUAGE plpgsql;
```

Mostrar las ventas de la oficina de un empleado. Saldrá error porque hay una columna que tambien se llama ventas en repventas
```
CREATE OR REPLACE FUNCTION get_oficina_ventas(in_empl repventas.num_empl%TYPE) RETURNS numeric(8,2) AS $$
	BEGIN
		RETURN (SELECT ventas
				FROM oficinas, repventas
				WHERE oficina_rep = oficina
				AND num_empl = in_empl);
		EXCEPTION
			WHEN ambiguous_column THEN
				RAISE NOTICE 'BOTH TABLES REPVENTAS AND OFICINAS HAVE COLUMNS NAMED ventas';
	END;
$$ LANGUAGE plpgsql;
```

Mirar los pedidos del empleado 104. No tiene ningún pedido y lo trataremos como error

```
CREATE OR REPLACE FUNCTION get_pedidos(in_empl repventas.num_empl%TYPE) RETURNS repventas AS $$
	DECLARE
		pedidos_row pedidos%ROWTYPE;
	BEGIN
		SELECT *
		INTO pedidos_row
		FROM pedidos
		WHERE rep = in_empl;
		
		IF NOT FOUND THEN
			RAISE EXCEPTION 'EMPLOYEE % DID NOT SERVICE ANY ORDER', in_empl;
		END IF;
		RETURN pedidos_row;
	END;
$$ LANGUAGE plpgsql;
```

Obtenir informació de l'error:

```
GET STACKED DIAGNOSTICS variable = item [ , ... ];
```

item pot ser els valors de la taula [http://www.postgresql.org/docs/9.3/static/plpgsql-control-structures.html#PLPGSQL-EXCEPTION-DIAGNOSTICS-VALUES](http://www.postgresql.org/docs/9.3/static/plpgsql-control-structures.html#PLPGSQL-EXCEPTION-DIAGNOSTICS-VALUES)

Exemple:
```
DECLARE
  text_var1 text;
  text_var2 text;
  text_var3 text;
BEGIN
  -- some processing which might cause an exception
  -- ...
EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS text_var1 = MESSAGE_TEXT,
                          text_var2 = PG_EXCEPTION_DETAIL,
                          text_var3 = PG_EXCEPTION_HINT;
END;
```

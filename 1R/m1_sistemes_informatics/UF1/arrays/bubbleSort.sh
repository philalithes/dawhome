#!/bin/bash
# Filename:			bubbleSort.sh
# Author:			iaw47951368
# Date:				28/01/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./bubbleSort.sh [array]
# Description:		Script que ordena un array amb el mètode de la bombolla.

#posible control d'errors. Inacabat.
#array=$1
#controlArgs=${array[*]}
#if [ -z $controlArgs ]
#then
#	echo "l'argument ha de ser un array"
#	exit 1;
#fi

declare -a numbers
echo "declarant array..."
sleep 1
for i in {1..10}
do
	numbers[$i]=$(echo $RANDOM)
done
echo ${numbers[*]}
echo "ordenant array..."
sleep 1

for ((i = 0; i < ${#numbers[*]}; i++))
{
	for ((j = 0; j < ${#numbers[*]}-i; j++))
	{
		if ((numbers[j] < numbers[j+1]))
		then
			aux=${numbers[j]}
			numbers[j]=${numbers[j+1]}
			numbers[j+1]=$aux
		fi
	}	
}

echo ${numbers[*]}

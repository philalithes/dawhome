package daw.m08.uf2.APG;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

public class Person {
	/**** atributs ****/
	@NotBlank
	@NotNull
	@Length(min = 2)
	private String nom;
	
	private String cognoms;
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private Date naixement;
	
	//@NotBlank
	private char sexe;
	
	private Boolean isCarnet;
	
	private Integer edat;

	@NotEmpty
	private List<String> idiomes;

	/**** fi atributs ****/
	
	public String getCognoms() {
		return cognoms;
	}

	
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}

	public Date getNaixement() {
		return naixement;
	}

	public void setNaixement(Date naixement) {
		this.naixement = naixement;
	}

	public char getSexe() {
		return sexe;
	}

	public void setSexe(char sexe) {
		this.sexe = sexe;
	}

	public Boolean isCarnet() {
		return isCarnet;
	}

	public void setCarnet(Boolean isCarnet) {
		this.isCarnet = isCarnet;
	}

	


	public List<String> getIdiomes() {
		return idiomes;
	}


	public void setIdiomes(List<String> idiomes) {
		this.idiomes = idiomes;
	}


	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getEdat() {
		return edat;
	}

	public void setEdat(Integer edat) {
		this.edat = edat;
	}

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Boolean getIsCarnet() {
		return isCarnet;
	}


	public void setIsCarnet(Boolean isCarnet) {
		this.isCarnet = isCarnet;
	}


	@Override
	public String toString() {
		return "Person [nom=" + nom + ", cognoms=" + cognoms + ", naixement=" + naixement + ", sexe=" + sexe
				+ ", isCarnet=" + isCarnet + ", edat=" + edat + ", idiomes=" + idiomes + "]";
	}


	public Person(String nom, String cognoms, Date naixement, char sexe, Boolean isCarnet, Integer edat,
			List<String> idiomes) {
		super();
		this.nom = nom;
		this.cognoms = cognoms;
		this.naixement = naixement;
		this.sexe = sexe;
		this.isCarnet = isCarnet;
		this.edat = edat;
		this.idiomes = idiomes;
	}
	
	
	
	
}

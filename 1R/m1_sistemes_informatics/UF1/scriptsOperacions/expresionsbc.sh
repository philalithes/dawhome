#!/bin/bash
# Filename:			expresionsbc.sh
# Author:			iaw47951368
# Date:				24/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./expresionsbc.sh [args...]
# Description:		Analitza expressions booleanes i retorna TRUE o FALSE.

if [ $# -ne 3 ]
	then
	echo "escriu una expressió booleana"
	exit 1
fi

exp=$(echo "$1 $2 $3" | bc -l)

if [ $exp -eq 1 ]
	then
	echo "TRUE"
else
	echo "FALSE"
fi

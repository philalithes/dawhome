/*
 * Program.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
    
    /**
     * print helloWorld
     * 
     *
     * 
     * 
     * 
     * 
     */
    public static void main(String args[]) {
        //print
        System.out.println("Hola món!");
        //nom de l'usuari
        String name;
        int edat;
        //scanner per llegir el nom
        Scanner s = new Scanner(System.in);
        //pregunta
        System.out.println("Com et dius?");
        //introducció del nom de l'usuari
        name = s.next();
        //print
        System.out.printf("Hola " + name + "!!!");
        //pregunta
        System.out.println("Quina edat tens?");
        edat = s.nextInt();
        System.out.println("Tens " + edat + " anys");
    }
    
}

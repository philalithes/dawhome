package org.escoladeltreball.provam05uf2;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class MainTest {

	public static User user1;
	public static User user2;
	public static User user3;
	public static Leaderboard lb;

	@BeforeClass
	public static void testBefore() {
		user1 = new User(1l, "alex", 1500);
		user2 = new User(2l, "carlos", 1599);
		user3 = new User(3l, "pepe", 1100);
		lb = new Leaderboard();
		lb.addUser(user1);
		lb.addUser(user2);
		lb.addUser(user3);
	}

	/**
	 * Hi ha menys de 100 punts de diferencia entre l'alex i l'usuari amb més puntuació
	 */
	@Test
	public void test() {
		long dif = lb.difUserFirst(user1);
		assertTrue(dif < 100);
	}

}

#!/bin/bash
# Filename:			chronfor.sh
# Author:			iaw47951368
# Date:				15/12/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./chronfor.sh [args...]
# Description:		chrono

for num in $(seq 10 -1 1)
do
	echo -en "\r$num"
	sleep 1
done
echo ""

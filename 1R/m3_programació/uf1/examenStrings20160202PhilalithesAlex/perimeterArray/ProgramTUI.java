/*
 * ProgramTUI.java        1.0 Oct 30, 2016
 * Models the program.
 *
 * Copyright 2016 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.*;
public class ProgramTUI
{
    
    /**
     * Search the numbers on the perimeter of the array
     * 
     * @param array of numbers written in catalan
     * @return The array of strings with numbers written in catalan located on the perimeter of the array
     * 
     */
    public String[] stringsOnPerimeterArray(String[][] array) {
        //variables
        int k = 0;
        String[] perimetreArray = new String[array.length * 2 + array[0].length * 2 - 4];
        //omplir array amb strings.
        for (int i = 0; i < array.length; i++) { // files
            for (int j = 0; j < array[0].length; j++) {// columnes
                if ((i == 0 || i == array.length -1) && (j == 0 || j == array[0].length - 1)) {
                    perimetreArray[k] = array[i][j];
                    k++;
                } else {
                    k++;
                }
            }
        }
        
        System.out.println(Arrays.toString(perimetreArray));
        return perimetreArray;
    }
    
   /**
   * build a string with the numbers written in Catalan of the perimeter of the matrix and the sum
   * 
   * @param The array of strings with numbers written in catalan located on the perimeter of the array
   * @return The array of strings with numbers written in catalan located on the perimeter of the array
   * 
   */
    public String buildOutputString(String[] s) {
        //variables
        String outputString = "";
        //omplir array
        for (int i = 0; i < s.length; i++) {
            outputString += s[i];
            outputString += "+";
        }
        return outputString;
    }
    
   /**
   * converts a number written in Catalan to an integer
   * 
   * @param a string with a number written in catalan
   * @return corresponding to a integer number written in Catalan
   * 
   */
    public int convertNumber( String number){
      int integerNumber  = 0;        
      switch (number) {
        case "zero" :
            integerNumber = 0;
            break;
        case "u" :
            integerNumber = 1;
            break;
        case "dos" :
            integerNumber = 2;
            break;
        case "tres" :
            integerNumber = 3;
            break;
        case "quatre" :
            integerNumber = 4;
            break;
        case "cinc" :
            integerNumber = 5;
            break;
        case "sis" :
            integerNumber = 6;
            break;
        case "set" :
            integerNumber = 7;
            break;
        case "vuit" :
            integerNumber = 8;
            break;
        case "nou" :
            integerNumber = 9;
            break;
        case "deu" :
            integerNumber = 10;
            break;
        case "onze" :
            integerNumber = 11;
            break;
        case "dotze" :
            integerNumber = 12;
            break;
        case "tretze" :
            integerNumber = 13;
            break;
        case "catorze" :
            integerNumber = 14;
            break;
        case "quinze" :
            integerNumber = 15;
            break;
        case "setze" :
            integerNumber = 16;
            break; 
        case "disset" :
            integerNumber = 17;
            break;
        case "divuit" :
            integerNumber = 18;
            break;
        case "dinou" :
            integerNumber = 19;
            break;
        case "vint" :
            integerNumber = 20;
            break;
        }
        return integerNumber;
    }
    
    public static void main(String[] args) {
        //variables
        ProgramTUI p = new ProgramTUI();
        String[][] array1 = {{"u","dos","tres","quatre","cinc"},
                             {"sis","set","vuit","nou","deu"},
                             {"onze","dotze","tretze","catorze","quinze"},
                             {"setze","disset","divuit","dinou","vint"}}; 
        String[][] array2 = {{"u","dos","tres","quatre","cinc"},
                             {"sis","set","vuit","nou","deu"},
                             {"onze","dotze","tretze","catorze","quinze"}};
        String[] s;
        int resultat = 0;
        s = p.stringsOnPerimeterArray(array1);
        for (int i = 0; i < s.length; i++) {
            resultat += p.convertNumber(s[i]);
        }
        System.out.println(p.buildOutputString(s) + "= " + resultat);
        s = p.stringsOnPerimeterArray(array2);
        System.out.println(p.buildOutputString(s));
    }
    
}

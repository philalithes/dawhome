/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix un nombre enter positiu x, un nombre enter n i
     * una seqüència de n nombres enters i escriu si algun d'ells és múltiple de x 
     * (interrompent la lectura de la seqüència si el trobes).
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int x = 0;
        int i = 0;
        int intents = 0;
        int n = 0;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu un nombre x del que vulguis trobar un múltiple");
        x = sc.nextInt();
        System.out.println("Escriu un nombre d'intents");
        intents = sc.nextInt();
        //càlculs
        do {
            System.out.println("Intenta esbrinar un múltiple de x");
            n = sc.nextInt();
            i++;
            
        } while (i < intents && n % x != 0);
        if (n % x == 0) {
            System.out.println("Has trobat un múltiple de x!");
        }
        else {
            System.out.println("Has esgotat el nombre d'intents");
            
            
        }
    }
}


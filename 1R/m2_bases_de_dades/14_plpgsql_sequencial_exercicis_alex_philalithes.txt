-- --------------------------------------------
--    plpgsql - estructura seqüencial
-- --------------------------------------------

-- 1.- Escriu una funció plpgsql que rebi com a paràmetre el codi d'un fabricant i retorni el nombre de comandes de productes d'aquell fabricant.

training=> 
CREATE or replace FUNCTION getNumComandesFab(fa char(3)) RETURNS int AS $$
DECLARE counter int; BEGIN SELECT count(*) into counter FROM pedidos WHERE fab = fa;
return counter;
end;
$$ language plpgsql;
CREATE FUNCTION

-- 2.- Escriu una funció plpgsql que rebi com a paràmetre el codi d'un fabricant i retorni el codi de producte del producte amb més unitats venudes d'aquell fabricant.

training=> create or replace function getbestseller(fa char(3)) returns char(5) as $$
declare ret char(5); begin select producto into ret from pedidos where fab = 'aci' group by producto order by sum(cant) desc limit 1; return ret;
end;
$$ language plpgsql;
CREATE FUNCTION

-- 3.- (FU-4) Donat l'identificador d'un client que retorni la importància del client, és a dir, el percentatge dels imports de les comandes del client respecte al total dels imports de les comandes.

create or replace function client_important(cl smallint) returns numeric as $$
declare percent numeric; begin select (select sum(importe) from pedidos where clie = cl) * 100 / sum(importe) into percent from pedidos;
return percent;
end;
$$ language plpgsql;

-- 4.- (FU-11) Feu una funció que retorni tots els codis dels clients que no hagin comprat res durant el mes introduït.

create or replace function no_pedidos_mes(mes int) returns setof smallint as $$
begin
 return query select distinct num_clie
              from clientes
		where num_clie not in (select clie
					from pedidos
					where extract('month' from fecha_pedido) = mes);
return;
end;             


-- 5.- (FU-18) Crear una funció i les funcions auxiliars convenients per obtenir la següent informació:
-- Donat l'identificador d'un producte obtenir una taula amb les següents dades:
--  * Identificador de producte i fabricant.
--  * Nombre de representants de vendes que han venut aquest producte.
--  * Nombre de clients que han comprat el producte.
--  * Mitjana de l'import de les comandes d'aquest producte.
--  * Quantitat mínima i quantitat màxima que s'ha demanat del producte en una sola comanda.

-- 6.- (FU-19) Crear una funció que donada una oficina ens retorni una taula identificant els productes i mostrant la quantitat d'aquest producte que s'ha venut a l'oficina.

-- 7.- (average) Calcula la mitja aritmètica de tres nombres reals.

create or replace function average(a int, b int, c int) returns numeric as $$
declare ret numeric;
begin select (a + b + c) / 3. into ret;
return ret;
end;
$$ language plpgsql;

-- 8.- (hms2s) A partir d'un nombre enter d’hores, un nombre enter de minuts i un nombre enter de segons, escriu el nombre de segons equivalents.

create or replace function hms2s(h int, m int, s int) returns int as $$
declare seconds int;
begin select (h * 3600 + m * 60 + s) into seconds;
return seconds;
end;
$$ language plpgsql;

-- 9.- Donat un número de comanda i l'IVA a aplicar, calcula el preu de la comanda amb l'IVA aplicat.

create or replace function price_plus_iva(price numeric, iva numeric) returns numeric as $$
declare ret numeric;
begin select price + price * iva / 100 into ret;
return ret;
end;
$$ language plpgsql;

-- 10.- Donat un codi de fabricant, retorna els codis dels productes d'aquest fabricant.

#!/bin/bash
# Filename:			exercici3.sh
# Author:			iaw47951368
# Date:				04/03/16
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici2.sh arg1
# Description:			Script que mostra el nom i el numero d'escons dels partits polítics que podrien formar gobern amb el partit passat com a argument tenint en compte les incompatibilitats entre ells. L'script treballa amb un fitxer que	conté aquesta informació guardat a la variable $fitxer. Els partits han d'estar ordenats per escons de major a menor.

#Variable del fitxer, si en un futur es canvia el nom, s'ha de canviar aquesta variable.
fitxer=investidura_episodi1.txt

#funcions
function get_nom_escons {
	cat $fitxer | grep "^$1:" | cut -d":" -f2-3 --output-delimiter=" " #mostra els camps 2 i 3 de les línies que coincideixen amb la id passada com a argument. (agafa nom i escons)
}

function get_escons {
	cat $fitxer | grep "^$1:" | cut -d":" -f3 #agafa escons
}

function suma_escons {
	local escons=$(get_escons $1)
	get_nom_escons $1
	local i=1
	while [ $escons -lt 176 ] #fins que se sumi majoria
	do
		if  [ "$i" -ne "$1" ] && [ "$(cat $fitxer | grep "^$i": | cut -d":" -f4)" -ne "$1" ] #no sumar un partit amb sí mateix ni amb els partits incompatibles amb aquest
		then
			local new_escons=$(get_escons $i) #agafar escons del partit $i
			local escons=$(($escons+$new_escons)) #sumar escons
			get_nom_escons $i #imprimir per pantalla el partit sumat
		fi
		i=$(($i+1))
	done		
	echo "TOTAL: $escons"
}
#control d'arguments
if [ "$#" -ne 1 ] #si no hi ha només un argument...
then
	echo "error: número d'arguments incorrecte."
	echo "ús: $0 id."
	exit 1
fi

if [ ! $(echo "$1" | egrep "^[0-9]+$") ] #si hi ha un argument però no és un enter positiu...
then
	echo "error: l'argument $1 no és un número enter positiu"
	echo "ús: $0 id"
	exit 1
fi

if [ ! "$(cat $fitxer | grep "^$1:" | cut -d":" -f1)" ] #si l'argument no coincideix amb cap id del fitxer....
then
	echo "error: la id $1 no pertany a cap partit"
	exit 1
else	 # si hem arribat aquí és que tot ha anat bé.
	suma_escons "$1"
fi

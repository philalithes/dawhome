/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats dos nombres enters positius a i b, calcula el 
     * resultat de realitzar la seva multiplicació a partir de sumes.
     * És a dir: a⋅b=a+ a+ a+ ...+ a (a sumat b vegades)
     * 
     */
    
    public int multiply(int a, int b) {
        //variable declaration
        int n = 0;
        int acumulador = 0;
        //calcul
        while (n < b) {
            acumulador += a; 
            n++;
            
        }
        return acumulador;
    }
    
}

#!/bin/bash
# Fitxer: ex32_kp_comments.sh
# Autor: jamoros
# Data: 31/03/2011
# Versio: 0.1
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		No fem control d'errors


 

# Sumarem els nombres d ela següent manera
# Al principi la nostra suma valdrà zero
suma=0
# Per a cada un dels arguments afegiré, l'argument, a la suma anterior

while [ $# -gt 0 ]
do
	suma=$((suma+$1))
	shift # Això em substitueix el argumentN a la posició argumentN-1 i així succesivament, perdo el $1
done


# Mostraré el resultat

echo $suma




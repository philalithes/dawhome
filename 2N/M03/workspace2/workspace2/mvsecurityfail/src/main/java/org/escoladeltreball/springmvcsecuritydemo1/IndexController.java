package org.escoladeltreball.springmvcsecuritydemo1;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexController {
			
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String indexPage() {
		return "redirect:index";
	}
	
	@RequestMapping(value = "/index")
	public String indexPage(Model model) {
		
		
		return "index";
	}
}

/*
 * holaMon.c
 * 
 * Programa "Hola món!" en C
 * 
 * Copyright 2010 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 * 
 */

#include <stdio.h>

int main(int argc, char** argv){
	printf("Hola món!\n");	
	return 0;
}

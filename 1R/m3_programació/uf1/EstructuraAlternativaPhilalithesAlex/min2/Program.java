/*
 * Program.java        2015/23/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Obté el nombre mínim d'entre dos nombres reals
     * 
     * @param n1 = nombre real
     * @param n2 = nombre real
     * 
     * @retorn valor minim
     */
    public double min2(double n1, double n2){
        double min;
        min = n1;
        if(n2 < n1) {
            min = n2;
        }
        return min;
    }   
}

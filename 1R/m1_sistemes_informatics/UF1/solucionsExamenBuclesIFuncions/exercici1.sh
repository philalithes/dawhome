#!/bin/bash
# Filename:		exercici1.sh
# Author:		pingui
# Date:			03/03/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici1.sh [arg1...]
# Description:	Script que rep un número que representa la id d'un partit politic i
#				mostra el nom i els escons del partit polític 


# funció que rep la id d'un partit polític i retorna el seu número d'escons
function get_escons {
    cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f3
}

# funció que rep la id d'un partit polític i retorna el seu nom
function get_nom {
    cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f2
}

# funció que rep la id d'un partit polític i retorna el seu nom i número d'escons al mateix temps
function get_nom_escons {
	cat $resultats_eleccions_fitxer | grep "^$1:" | cut -d: -f2,3 | tr ":" "\t"
}

resultats_eleccions_fitxer=investidura_episodi1.txt

# Si no hi ha un únic argument informem de l'error i sortim informant al sistema
if [ $# -ne 1 ]
then
	echo "error: número de arguments incorrecte"
	echo "$0 id"
	echo "on id és el número del partit polític"
	exit 1
fi

# Si l'argument no és un enter informem de l'error i sortim informant al sistema
if ! echo $1 | grep '^[0-9]\+$' >& /dev/null
then
	echo "error: l'argument $1 no és un número enter positiu"
	exit 2
fi

# Cridem a la funció que ens retorna el nom i escons d'un partit polític
partit=$(get_nom_escons $1)


# Si la funció no retorna res és que no existeix aquest partit politic
# altrament mostrem la informació
if [ -z "$partit" ]
then
	echo "la id $1 no pertany a cap partit"
	exit 3
else
	echo $partit
fi


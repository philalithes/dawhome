package daw.m08.uf2.APG;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;


@Configuration
public class MyDelegatingWebMvcConfiguration extends DelegatingWebMvcConfiguration {

	@Bean
	@Override
	public RequestMappingHandlerMapping requestMappingHandlerMapping() {
		RequestMappingHandlerMapping a = super.requestMappingHandlerMapping();
		a.setRemoveSemicolonContent(false);
		a.setUseTrailingSlashMatch(false);
		return a;
	}
	@Bean
	@Override
	public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
		RequestMappingHandlerAdapter a = super.requestMappingHandlerAdapter();
		a.setIgnoreDefaultModelOnRedirect(true);
		return a;
	}
}

package org.escoladeltreball.m03_uf05_nf01_apg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/*
 * @Autor Alex Philalithes Gallego
 * @mail alexph_95@hotmail.com
 */
public class Main {

	private static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		
		
		//llista de candidats
		int nCandidats = sc.nextInt();
		List<Candidat> candidats = new ArrayList<>();
		for(int i = 0; i < nCandidats; i++) {
			candidats.add(new Candidat(sc.nextInt(), sc.next(), sc.next().charAt(0), sc.nextInt(), sc.next().charAt(0), sc.next().charAt(0), sc.nextInt()));
		}
		
		//ordenem per identificador codi
		Collections.sort(candidats, new Comparator<Candidat>(){
			public int compare(Candidat c1, Candidat c2) {
				return c1.getCodi() - c2.getCodi();
			}
		});
		
		//comparem candidats entre sí
		for (Candidat c1 : candidats) {
			for (Candidat c2 : candidats) {
				if (!c1.equals(c2)) { // no compara amb sí mateix
					if (c1.isCompatible(c2)) {
						c1.addCompatible(c2); //afegim c2 a la llista de compatibles de c1
					}
				}
			}
		}
		String strout = "";
		//agafem les llistes de compatibilitats, les ordenem per pes i la limitem a 3 candidats compatibles i alhora anem construint la sortida
		for (Candidat c1 : candidats) {
			int nCompatibles = 0;
			List<Candidat> compatibles = c1.getCompatibles();
			strout += c1.getCodi() + " " + c1.getNom() + "\t";
			for (Candidat c2 : compatibles) {
				c2.setPes(c1.pes(c2));
			}
			//ordenació dels compatibles
			Collections.sort(compatibles, new Comparator<Candidat>(){
				public int compare(Candidat c1, Candidat c2) {
					return c1.getPes() - c2.getPes();
				}
			});
			Collections.reverse(compatibles);
			for (Candidat c2 : compatibles) {
				if (nCompatibles < 3) {
					strout += c2.getNom() + "\t" + c2.getPes() + " ";
				} else {
					compatibles.remove(nCompatibles);
				}
				nCompatibles++;
			}
			strout += "\n";
		}
		System.out.println(strout);
		
		
	}

}

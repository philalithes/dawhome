package org.escoladeltreball.simplespringmavenprojectdemo1;

public class Classic extends Vehicle {

	private int anyProduccio;
	public Classic() {
		// TODO Auto-generated constructor stub
	}

	public Classic(String marca, Motor motor, int anyProduccio) {
		super(marca, motor);
		this.anyProduccio = anyProduccio;
	}

	public int getAnyProduccio() {
		return anyProduccio;
	}

	public void setAnyProduccio(int anyProduccio) {
		this.anyProduccio = anyProduccio;
	}

	@Override
	public String toString() {
		return "Classic [anyProduccio=" + anyProduccio + "]";
	}
	

}

/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Soci.
 * 
 * @author iaw47951368
 */
public class Soci {
	/**
	 * Description of the property dni.
	 */
	public String dni = "";
	
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	// Start of user code (user defined attributes for Soci)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Soci() {
		// Start of user code constructor for Soci)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Soci)
	
	// End of user code
	/**
	 * Returns dni.
	 * @return dni 
	 */
	public String getDni() {
		return this.dni;
	}
	
	/**
	 * Sets a value to attribute dni. 
	 * @param newDni 
	 */
	public void setDni(String newDni) {
	    this.dni = newDni;
	}

	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}



}

/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of AbastimentAigua.
 * 
 * @author iaw47951368
 */
public class AbastimentAigua {
	/**
	 * Description of the property volum.
	 */
	public double volum = 0D;
	
	// Start of user code (user defined attributes for AbastimentAigua)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public AbastimentAigua() {
		// Start of user code constructor for AbastimentAigua)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for AbastimentAigua)
	
	// End of user code
	/**
	 * Returns volum.
	 * @return volum 
	 */
	public double getVolum() {
		return this.volum;
	}
	
	/**
	 * Sets a value to attribute volum. 
	 * @param newVolum 
	 */
	public void setVolum(double newVolum) {
	    this.volum = newVolum;
	}



}

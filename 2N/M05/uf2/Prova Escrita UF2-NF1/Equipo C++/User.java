package org.escoladeltreball.transaccion;

public class User implements Comparable<User> {

	private long id;
	private String name;
	private long score;

	public User(long id, String name, long score) {
		this.id = id;
		this.name = name;
		this.score = score;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}
	
	@Override
	public String toString() {
		return String.format("Name : %s\t Score : %s\n", name, score);
	}

	@Override
	public int compareTo(User o) {
		return -new Long(score).compareTo(new Long(o.getScore()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof User)) {
			return false;
		}
		User other = (User) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}

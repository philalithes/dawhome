*Problema matemàtic plantejat a l’ensenyament l’any 1960:
Un camperol ven un sac de patates per 1000 Pts. Les seves despeses de producció son els 4/5 del preu de venda. Quin és el benefici que n’obté?

*el mateix problema, plantejat el 1965:
Un camperol ven un sac de patates per 1000 Pts. Les seves despeses de producció son els 4/5 del preu de venda, això suposa 800 Ptes. Quin és el seu benefici?

*ensenyament del mateix problema l’any 1970:
Un camperol canvia un conjunt P de patates per un conjunt M de monedes. el cardinal del conjunt M és igual a 1000 Ptes., i cada element val 1 pesseta. Dibuixa 1000 punts grossos qie representin els elements del conjunt M. el conjunt F de les despeses de producció te 200 punts grossos menys que el conjunt M. Representa el conjunt F com a subconjunt del conjunt M, estudia quina serà la seva unió i la seva intersecció i dona resposta a la següent qüestió: Quin és el cardinal del conjunt B dels beneficis?(dibuixa B en color vermell).

*La LOGSe planteja el mateix problema de la següent manera:
Un agricultor, ven un sac de patates per 1000 ptes. Les despeses de producció pugen a 800 ptes i el benefici és de 200 ptes.
activitat: Subratlla la paraula “patata” i discuteix sobre aquesta paraula amb el teu company.

*el mateix problema, a la propera reforma que es plantejarà a ensenyament podria ser:
el tiu ernes, pages, burges, latifundista, espanyol facista espekulador i intermediari, es 1 kapitalista insulidari i cntralista q sa enrikit am 200 pelas alvendre espekulan un futime de ptates. Biu a luest de madrit esplutan magrevis. Porta fills a 1 skola de pagu. analisa el tes, vusca les faltes d sintasi, dortugrafia, de puntiasio i si no les beus no t traumatisis, q no psa rs.
eskriu tono, politono o sonitono am la frse “KeLLeSLeRNeS”, i envia un sms a tots ls teus kuleges kumentan els avusos antidmukratiks d l ernes i kunvukan una mani spuntania en senyal de prutesta.
Si vas a la mani surtejaran un buga guapu.


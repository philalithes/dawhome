/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    /*
     * Llegeix un nombre enter n i n nombres enters i escriu el 
     * nombre més gran i el més petit d'ells.
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        int num = 0;
        int i;
        //interaccio amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("escriu un nombre n major que 0");
        int cant = sc.nextInt();
        System.out.println("escriu n nombres");
        num = sc.nextInt();
        int max = num;
        int min = num;
        for (i = 1; i < cant; i++) {
            num = sc.nextInt();
            if (num < min) {
                min = num;
            }
            if (num > max) {
                max = num;
            }
            
        }
        System.out.printf("nombres introduïts: %d, max: %d, min: %d",cant,max,min);
        
        
        
    }
}
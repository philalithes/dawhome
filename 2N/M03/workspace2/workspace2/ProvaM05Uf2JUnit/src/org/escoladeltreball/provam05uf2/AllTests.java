package org.escoladeltreball.provam05uf2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ LeaderboardGetTest.class, LeaderBoardTest.class, MainTest.class, UserGetTest.class, UserTest.class })
public class AllTests {

}

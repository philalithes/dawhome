package org.escoladeltreball.springdemo3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Profile(value = "debug")
@PropertySource("classpath:/systemproperties/appResource.properties")
public class BeansDefinition1 {

	@Autowired
	private Environment env;
	
	public BeansDefinition1() {

	}

	@Bean(name = "vermell")
	public Vehicle vehicle() {
		System.out.println(env.getProperty("beansdefinition1.title"));
		return new Vehicle("Vermell");
	}
}

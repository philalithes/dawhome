/*
 * Program.java        1.0 Oct 30, 2014
 *
 * Programa que nos dice la sanción correspondiente segun la velocidad en un tramo
 *
 * Copyright 2014 Chema García-Arias García-Morato <mrdelayman@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Traffic ticket
     * 
     * @param speed  car speed
     * @return the penalty and the stretch
     */
    public String penalty(int speed) {
        //Variable declaration
        double penalty;
        String s;
        //Diseño del algoritmo
        if (speed > 80 && speed <=99){
            penalty = speed; 
            s = "la sancion es de " + penalty + " euros";        
        }else if (speed > 99 && speed <=129){
            penalty = speed * 2; 
            s = "la sancion es de " + penalty + " euros";        
        }else if (speed > 129){
            penalty = speed * 3; 
            s = "la sancion es de " + penalty + " euros";        
        }else{
            s = "Vas muy despacio o muy lento :)";
        }
        return s;   
    }
}

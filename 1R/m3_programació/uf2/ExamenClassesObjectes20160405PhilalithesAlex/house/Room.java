/*
 * Room.java        1.0 March 14, 2016
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
 
package house;
 
public class Room 
{ 
    /** Create a room described "description". Is something like "kitchen" or "bedroom". **/
    private String description;
    /** The corresponding house */
    private House house = null;
    /** Exit's Room. Direction North **/
    private Room northExit = null;
    /** Exit's Room. Direction South **/
    private Room southExit = null;
    /** Exit's Room. Direction East **/
    private Room eastExit = null;
    /** Exit's Room. Direction West **/
    private Room westExit = null;
    /** Length of the room in north-south direction **/
    private double lengthDirectionNorthSouth;
    /** length of the room in east-west direction **/
    private double lengthDirectionEastWest;
    /** Room's height */
    private final static double HEIGHT = 3.00;

   
    // Constructor
    public Room(String description, double lengthDirectionNorthSouth, double lengthDirectionEastWest) 
    {
        this.description = description;
        this.lengthDirectionNorthSouth = lengthDirectionNorthSouth;
        this.lengthDirectionEastWest = lengthDirectionEastWest;
    }

    // Getters and setters
    
    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public House getHouse() {
        return this.house;
    }

    public void setHouse(House house) {
        this.house = house;
    }
    
    public Room getRoomNorthExit() {
        return this.northExit;
    }

    public void setRoomNorthExit(Room room) {
        this.northExit = room;
    }
    
    public Room getRoomSouthExit() {
        return this.southExit;
    }

    public void setRoomSouthExit(Room room) {
        this.southExit = room;
    }
    
    public Room getRoomEastExit() {
        return this.eastExit;
    }

    public void setRoomEastExit(Room room) {
        this.eastExit = room;
    }
    
    public Room getRoomWestExit() {
        return this.westExit;
    }

    public void setRoomWestExit(Room room) {
        this.westExit = room;
    }
     
    public double getLengthDirectionNorthSouth() {
        return this.lengthDirectionNorthSouth;
    }

    public void setLengthDirectionNorthSouth(double lengthDirectionNorthSouth) {
        this.lengthDirectionNorthSouth = lengthDirectionNorthSouth;
    }
    
    public double getLengthDirectionEastWest() {
        return this.lengthDirectionEastWest;
    }

    public void setLengthDirectionEastWest(double lengthDirectionEastWest) {
        this.lengthDirectionEastWest = lengthDirectionEastWest;
    }

    // Methods
    /**
     * Define the exits of this room.  Every direction either leads
     * to another room or is null (no exit there).
     * @param north The north exit.
     * @param east The east east.
     * @param south The south exit.
     * @param west The west exit.
     * @param length direction North-South
     * @param length direction East-West
     */
    public void setExits(Room north, Room east, Room south, Room west, double lengthDirectionNorthSouth, double lengthDirectionEastWest) 
    {
        //reutilització de codi
        setRoomNorthExit(north);
        setRoomEastExit(east);
        setRoomSouthExit(south);
        setRoomWestExit(west);
        setLengthDirectionNorthSouth(lengthDirectionNorthSouth);
        setLengthDirectionEastWest(lengthDirectionEastWest);           
    }
    
    /**
     * Determines if a room is bigger than another room of the same house
     * 
     * @param anotherRoom another room
     * @return the biggest room
     */
    public Room isBigger(Room anotherRoom) {
        //si no formen part de la mateixa casa retorna null
        if (!this.getHouse().getAddress().equals(anotherRoom.getHouse().getAddress())) {
            return null;
        }
        //crear variables amb el volum calculat (V = a * b * c)
        double thisVolume = this.getLengthDirectionNorthSouth() * this.getLengthDirectionEastWest() * this.HEIGHT;
        double anotherVolume = anotherRoom.getLengthDirectionNorthSouth() * anotherRoom.getLengthDirectionEastWest() * anotherRoom.HEIGHT;
        //comparar volum
        if (thisVolume > anotherVolume) {
            System.out.println("El " + this.getDescription() + " és més gran que el " + anotherRoom.getDescription());
            return this;
        } else {
                        System.out.println("El " + anotherRoom.getDescription() + " és més gran que el " + this.getDescription());
            return anotherRoom;
        }
    }
    
    /**
     * Makes a report of the room with the following format:
     * 
     * Descripció : 
     * Adreça casa : 
     * Descripció sortida a l'habitació que dona al Nord :
     * Descripció sortida a l'habitació que dona al Sud :
     * Descripció sortida a l'habitació que dona al Est :
     * Descripció sortida a l'habitació que dona al Oest :
     * Longitut de l'habitació drecció Nord-Sud :
     * Longitut de l'habitació drecció Est-Oest :
     */
    public void reportRoom() {
        System.out.println("Descripció : " + this.getDescription());
        System.out.println("Adreça casa : " + this.house.getAddress());
        if (getRoomNorthExit() != null)
        System.out.println("Descripció sortida a l'habitació que dona al Nord : " + getRoomNorthExit().getDescription());
        if (getRoomSouthExit() != null)
            System.out.println("Descripció sortida a l'habitació que dona al Sud : " + getRoomSouthExit().getDescription());
        if (getRoomEastExit() != null)
            System.out.println("Descripció sortida a l'habitació que dona al Est : " + getRoomEastExit().getDescription());
        if (getRoomWestExit() != null)
            System.out.println("Descripció sortida a l'habitació que dona al Oest : " + getRoomWestExit().getDescription());
        System.out.println("Longitut de l'habitació drecció Nord-Sud : " + getLengthDirectionNorthSouth());
        System.out.println("Longitut de l'habitació drecció Est-Oest : " + getLengthDirectionEastWest());
    }
    
    /**
     * Determine the distance to go two rooms. To be able to calculate these two rooms must form part of the same house
     * and both rooms have to be adjacent.
     * If these requirements are not met, the method returns -1. 
     * @param adjacent room
     * @return distance between rooms ( in metres )
     */
    double fewMetersBetweenTwoJointRooms(Room room) {
        //si no formen part de la mateixa casa retorna null
        if (!this.getHouse().getAddress().equals(room.getHouse().getAddress())) {
            return -1;
        }
        //variables
        double distancia;
        String missatge = "De l'habitació "; 
        missatge += this.getDescription();
        missatge += " fins l'habitació ";
        missatge += room.getDescription();
        //mirar que no hi hagi habitacions nules
        if (this.getRoomNorthExit() != null) {
            //comparar habitacions adjacents (nord)
            if (this.getRoomNorthExit().getDescription().equals(room.getDescription())) {
                distancia = this.getLengthDirectionNorthSouth() + room.getLengthDirectionNorthSouth();
                missatge += " sumen " + distancia + " metres";
                System.out.println(missatge);
                return distancia;
            }
        }
        //mirar que no hi hagi habitacions nules
        if (this.getRoomSouthExit() != null) {
            //comparar habitacions adjacents (nord)
            if (this.getRoomSouthExit().getDescription().equals(room.getDescription())) {
                distancia = this.getLengthDirectionNorthSouth() + room.getLengthDirectionNorthSouth();
                missatge += " sumen " + distancia + " metres";
                System.out.println(missatge);
                return distancia;
            }
        }
        //mirar que no hi hagi habitacions nules
        if (this.getRoomEastExit() != null) {
            //comparar habitacions adjacents (est)
            if (this.getRoomEastExit().getDescription().equals(room.getDescription())) {
                distancia = this.getLengthDirectionEastWest() + room.getLengthDirectionEastWest();
                missatge += " sumen " + distancia + " metres";
                System.out.println(missatge);
                return distancia;
            }
        }
        //mirar que no hi hagi habitacions nules
        if (this.getRoomWestExit() != null) {
            //comparar habitacions adjacents (oest)
            if (this.getRoomWestExit().getDescription().equals(room.getDescription())) {
                distancia = this.getLengthDirectionEastWest() + room.getLengthDirectionEastWest();
                missatge += " sumen " + distancia + " metres";
                System.out.println(missatge);
                return distancia;
            }
        }
        //si arriba aquí és que no són adjacents
        System.out.println("L'habitació " + this.getDescription() + " NO es troba junt l'habitació " + room.getDescription());
        return -1;
    }

}

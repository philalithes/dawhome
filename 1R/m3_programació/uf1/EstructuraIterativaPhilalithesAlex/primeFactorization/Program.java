/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Donat un nombre enter positiu, 
     * escriu la seva descomposició en producte de nombres primers.
     * 
     * 
     * 
     * 
     */
    
    public void primeFactorization(int n) {
        //variable declaration
        int i = n;
        //calcul
        while (i > 0) {
            if (n % i == 0) {
                System.out.println(i);
            }
            i--;
        }
    }
}

package org.escoladeltreball.springdemo4;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
public class BeansDef {

	@Autowired
	private Environment env;
	
	public BeansDef() {
		// TODO Auto-generated constructor stub
	}

	@Bean
	public AspectConfig aspectConfig() {
		return new AspectConfig();
	}
	
	@Bean(name = "login")
	public Login login() {
		return new LoginImpl();
	}
	
	@Bean
	public DataSource dataSource() {
		String url = env.getProperty("jdbc.url");
		String username = env.getProperty("jdbc.username");
		String password = env.getProperty("jdbc.password");
		String driver = env.getProperty("jdbc.driver");
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource(url, username, password);
		dataSource.setDriverClassName(driver);
		return dataSource;
	}
	//JDBC
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		return new JdbcTemplate(dataSource);
	}
	
	//ORM
	//EntityManagerFactory
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
		properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", env.getRequiredProperty("hibernate.format_sql"));
		properties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
		return properties;
	}
	
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource data) {
		LocalContainerEntityManagerFactoryBean l =
				new LocalContainerEntityManagerFactoryBean();
		l.setDataSource(data);
		l.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		l.setJpaProperties(hibernateProperties());
		return l;
	}
	
	//Transaction Manager
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager jpatm = new JpaTransactionManager();
		jpatm.setEntityManagerFactory(emf);
		return jpatm;
	}
	
//	@Bean
//	public EventRepository eventRepository() {
//		return new EventRepository();
//	}
}

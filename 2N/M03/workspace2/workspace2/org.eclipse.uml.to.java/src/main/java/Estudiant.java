/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Estudiant.
 * 
 * @author iaw47951368
 */
public class Estudiant {
	/**
	 * Description of the property dni.
	 */
	public String dni = "";
	
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	/**
	 * Description of the property telefon.
	 */
	public String telefon = "";
	
	// Start of user code (user defined attributes for Estudiant)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Estudiant() {
		// Start of user code constructor for Estudiant)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Estudiant)
	
	// End of user code
	/**
	 * Returns dni.
	 * @return dni 
	 */
	public String getDni() {
		return this.dni;
	}
	
	/**
	 * Sets a value to attribute dni. 
	 * @param newDni 
	 */
	public void setDni(String newDni) {
	    this.dni = newDni;
	}

	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}

	/**
	 * Returns telefon.
	 * @return telefon 
	 */
	public String getTelefon() {
		return this.telefon;
	}
	
	/**
	 * Sets a value to attribute telefon. 
	 * @param newTelefon 
	 */
	public void setTelefon(String newTelefon) {
	    this.telefon = newTelefon;
	}



}

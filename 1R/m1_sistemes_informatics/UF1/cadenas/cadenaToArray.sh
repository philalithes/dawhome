#!/bin/bash
# Filename:			cadenaToArray.sh
# Author:			iaw47951368
# Date:				11/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./cadenaToArray.sh [args...]
# Description:		afdsd

cadena="hola"
declare -a cadena_array

for i in seq 0 ${#cadena}
do
	cadena_array[$i]=${cadena:$i:1}
done

echo ${cadena_array[*]}

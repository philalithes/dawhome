package org.escoladeltreball.daw2_m03_uf06_nf01_APG;

import java.util.Comparator;

public class CompareByPreu implements Comparator<Dispositiu>{
	public CompareByPreu() {
		
	}
	
	public int compare(Dispositiu d1, Dispositiu d2) {
		return new Double(d1.getPreu()).compareTo(new Double(d2.getPreu()));
	}
}
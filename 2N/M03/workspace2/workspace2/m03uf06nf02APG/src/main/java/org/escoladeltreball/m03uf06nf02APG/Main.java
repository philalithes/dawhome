package org.escoladeltreball.m03uf06nf02APG;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {
	
	// ****** ******* ****** SELECTS ****** ******* ****** 
	/**
	 * @param id id del Jugador a seleccionar
	 * @param em EntityManager
	 * @return Jugador
	 */
	public static Jugador selectJugador(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectJugador");
		select.setParameter(1, id);
		return (Jugador) select.getSingleResult();
	}
	
	/**
	 * @param nom nom del Jugador a seleccionar
	 * @param em
	 * @return Jugador
	 * @throws NonUniqueNomException si hi ha més d'un
	 */
	public static Jugador selectJugador(String nom, EntityManager em) throws NonUniqueNomException {
		Query select = em.createNamedQuery("selectJugadorByNom");
		select.setParameter(1, nom);
		List<Object> jugadors = select.getResultList();
		if (jugadors.size() == 1) {
			return (Jugador) jugadors.get(0);
		} else if (jugadors.size() > 1) {
			throw new NonUniqueNomException("Hi ha més d'un jugador amb el nom " + nom);
		} else {
			throw new NonUniqueNomException("No s'ha trobat cap jugador amb el nom " + nom);
		}
	}
	/**
	 * @param id id del Equip a seleccionar
	 * @param em EntityManager
	 * @return Equip
	 */
	public static Equip selectEquip(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectEquip");
		select.setParameter(1, id);
		return (Equip) select.getSingleResult();
	}
	
	/**
	 * @param nom nom del Equip a seleccionar
	 * @param em
	 * @return Equip
	 * @throws NonUniqueNomException si hi ha més d'un
	 */
	public static Equip selectEquip(String nom, EntityManager em) throws NonUniqueNomException {
		Query select = em.createNamedQuery("selectEquipByNom");
		select.setParameter(1, nom);
		List<Object> equips = select.getResultList();
		if (equips.size() == 1) {
			return (Equip) equips.get(0);
		} else if (equips.size() > 1) {
			throw new NonUniqueNomException("Hi ha més d'un equip amb el nom " + nom);
		} else {
			throw new NonUniqueNomException("No s'ha trobat cap equip amb el nom " + nom);
		}
	}
	/**
	 * @param id id del Estadi a seleccionar
	 * @param em EntityManager
	 * @return Estadi
	 */
	public static Estadi selectEstadi(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectEstadi");
		select.setParameter(1, id);
		return (Estadi) select.getSingleResult();
	}
	
	/**
	 * @param nom nom de l'estadi
	 * @param em
	 * @return Estadi
	 * @throws NonUniqueNomException si hi ha més d'un
	 */
	public static Estadi selectEstadi(String nom, EntityManager em) throws NonUniqueNomException {
		Query select = em.createNamedQuery("selectEstadiByNom");
		select.setParameter(1, nom);
		List<Object> estadis = select.getResultList();
		if (estadis.size() == 1) {
			return (Estadi) estadis.get(0);
		} else if (estadis.size() > 1){
			throw new NonUniqueNomException("Hi ha més d'un estadi amb el nom " + nom);
		} else {
			throw new NonUniqueNomException("No s'ha trobat cap estadi amb el nom " + nom);			
		}
	}
	/**
	 * @param id id de la Competicio a seleccionar
	 * @param em EntityManager
	 * @return Competicio
	 */
	public static Competicio selectCompeticio(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectCompeticio");
		select.setParameter(1, id);
		return (Competicio) select.getSingleResult();
	}
	
	/**
	 * @param nom nom de la Competicio a seleccionar
	 * @param em
	 * @return Competicio
	 * @throws NonUniqueNomException si hi ha més d'una
	 */
	public static Competicio selectCompeticio(String nom, EntityManager em) throws NonUniqueNomException {
		Query select = em.createNamedQuery("selectCompeticioByNom");
		select.setParameter(1, nom);
		List<Object> competicions = select.getResultList();
		if (competicions.size() == 1) {
			return (Competicio) competicions.get(0);
		} else if (competicions.size() > 1) {
			throw new NonUniqueNomException("Hi ha més d'una competició amb el nom " + nom);
		} else {
			throw new NonUniqueNomException("No s'ha trobat cap competició amb el nom " + nom);
		}
	}
	
	/**
	 * Mètode per fer una query i retornar els objectes ràpidament
	 * @param query custom query
	 * @param em
	 * @return List<Object>
	 */
	public static List<Object> selectList(String query, EntityManager em) {
		Query select = em.createQuery(query);
		return select.getResultList();
	}
	
	// ****** ******* ****** UPDATES ****** ******* ******
	/**
	 * @param id id del jugador a actualitzar
	 * @param nom
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void updateJugador(long id, String nom, int dorsal, String posicio, EntityManager em) {
		Jugador j = selectJugador(id, em);
		j.setNom(nom);
		j.setDorsal(dorsal);
		j.setPosicio(posicio);
		em.merge(j);
	}
	
	/**
	 * @param id id del Equip a actualitzar
	 * @param nom
	 * @param nCompeticionsGuanyades
	 * @param capital
	 * @param em EntityManager
	 */
	public static void updateEquip(long id, String nom, int nCompeticionsGuanyades, double capital, EntityManager em) {
		Equip e = selectEquip(id, em);
		e.setNom(nom);
		e.setnCompeticionsGuanyades(nCompeticionsGuanyades);
		e.setCapital(capital);
		em.merge(e);
	}
	
	/**
	 * @param id id del Estadi a actualitzar
	 * @param nom
	 * @param dorsal
	 * @param qualitatTerreny
	 * @param em EntityManager
	 */
	public static void updateEstadi(long id, String nom, int capacitat, double qualitatTerreny, EntityManager em) {
		Estadi e = selectEstadi(id, em);
		e.setNom(nom);
		e.setCapacitat(capacitat);
		e.setQualitatTerreny(qualitatTerreny);
		em.merge(e);
	}
	
	/**
	 * @param id id de la Competicio a actualitzar
	 * @param nom
	 * @param premi
	 * @param maxEquips
	 * @param em EntityManager
	 */
	public static void updateCompeticio(long id, String nom, double premi, int maxEquips, EntityManager em) {
		Competicio c = selectCompeticio(id, em);
		c.setNom(nom);
		c.setPremi(premi);
		c.setMaxEquips(maxEquips);
		em.merge(c);
	}
	
	// ****** ******* ****** INSERTS ****** ******* ******
	/**
	 * Paràmetres del nou Jugador
	 * @param name
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void insertNewJugador(String name, int dorsal, String posicio, EntityManager em) {
		Jugador j = new Jugador(name, dorsal, posicio);
		em.persist(j);
	}
	
	/**
	 * Paràmetres del nou Equip
	 * @param name
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void insertNewEquip(String nom, int nCompeticionsGuanyades, double capital, EntityManager em) {
		Equip e = new Equip(nom, nCompeticionsGuanyades, capital);
		em.persist(e);
	}
	
	/**
	 * Paràmetres del nou Estadi
	 * @param nom
	 * @param capacitat
	 * @param qualitatTerreny
	 * @param em EntityManager
	 */
	public static void insertNewEstadi(String nom, int capacitat, double qualitatTerreny, EntityManager em) {
		Estadi e = new Estadi(nom, capacitat, qualitatTerreny);
		em.persist(e);
	}
	
	/**
	 * Paràmetres de la nova Competicio
	 * @param nom
	 * @param premi
	 * @param maxEquips
	 * @param em
	 */
	public static void insertNewCompeticio(String nom, double premi, int maxEquips, EntityManager em) {
		Competicio c = new Competicio(nom, premi, maxEquips);
		em.persist(c);
	}
	
	// ****** ******* ****** DELETES ****** ******* ******
	/**
	 * @param id id del Jugador a eliminar
	 * @param em EntityManager
	 */
	public static void deleteJugador(long id, EntityManager em) {
		em.remove(selectJugador(id, em));
	}
	
	/**
	 * @param id id del Equip a eliminar
	 * @param em EntityManager
	 */
	public static void deleteEquip(long id, EntityManager em) {
		em.remove(selectEquip(id, em));
	}
	
	/**
	 * @param id id del Estadi a eliminar
	 * @param em EntityManager
	 */
	public static void deleteEstadi(long id, EntityManager em) {
		em.remove(selectEstadi(id, em));
	}
	
	/**
	 * @param id id de la Competicio a eliminar
	 * @param em EntityManager
	 */
	public static void deleteCompeticio(long id, EntityManager em) {
		em.remove(selectCompeticio(id, em));
	}
	
	

	// ****** ******* ****** MAIN ****** ******* ******
	public static void main(String[] args){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("m03uf06nf02APG");
		EntityManager em = emf.createEntityManager();
		//INSERTS
		em.getTransaction().begin();
		
		insertNewJugador("Carlos", 1, "Porter", em);
		insertNewJugador("Juan", 34, "lel", em);
		insertNewJugador("Pepe", 2, "Centrecampista", em);
		
		insertNewEquip("Barcelona", 10, 10000, em);
		insertNewEquip("Madrid", 21, 23123.32, em);
		
		insertNewEstadi("Camp Nou", 200000, 9.2, em);
		insertNewEstadi("Bernabeu", 250000, 9, em);

		insertNewCompeticio("Lliga", 1000000, 50, em);
		insertNewCompeticio("Champions", 5000000, 10, em);
		
		em.getTransaction().commit();
		em.getTransaction().begin();
		

		Jugador juan = new Jugador();
		try {
			juan = selectJugador("Juan", em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Jugador pepe = new Jugador();
		try {
			pepe = selectJugador("Pepe", em);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Equip barca = new Equip();
		try {
			barca = selectEquip("Barcelona", em);
		} catch (NonUniqueNomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Equip madrid = new Equip();
		try {
			madrid = selectEquip("Madrid", em);
		} catch (NonUniqueNomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Estadi campnou = new Estadi();
		try {
			campnou = selectEstadi("Camp Nou", em);
		} catch (NonUniqueNomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Estadi bernabeu = new Estadi();
		try {
			bernabeu = selectEstadi("Bernabeu", em);
		} catch (NonUniqueNomException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Object> competicions = selectList("SELECT c FROM Competicio c", em);
		System.out.println(competicions);
		Competicio champions = (Competicio) competicions.get(0);
		Competicio lliga = (Competicio) competicions.get(1);
		
		barca.setEstadi(campnou);
		madrid.setEstadi(bernabeu);
		barca.addCompeticio(lliga);
		barca.addCompeticio(champions);
		madrid.addCompeticio(lliga);
		madrid.addCompeticio(champions);
		lliga.addEquip(barca);
		lliga.addEquip(madrid);
		champions.addEquip(barca);
		champions.addEquip(madrid);
		barca.addJugador(juan);
		madrid.addJugador(pepe);
		
		juan.setEquip(barca);
		pepe.setEquip(madrid);
		em.merge(pepe);
		em.merge(juan);
		em.getTransaction().commit();
		System.out.println("ok");
		em.close();
		emf.close();
	}

}

#!/bin/bash
# Filename:			s_o_n.sh
# Author:			iaw47951368
# Date:				15/12/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./s_o_n.sh [args...]
# Description:		Si escriu s o n surt del script.

while [ "$resposta" != "s" ] && [ "$resposta" != "n" ]
do
	echo "Escriu s o n"
	read resposta
done

echo "molbe!"

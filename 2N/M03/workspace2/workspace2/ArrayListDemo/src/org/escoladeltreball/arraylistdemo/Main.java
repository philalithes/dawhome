package org.escoladeltreball.arraylistdemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main {

	public static void main(String[] args) {
		List<String> l = new ArrayList<>();
		l.add("Bravo");
		l.add("Alfa");
		l.add("Foxtrot");
		l.add("Charlie");
		l.add("Echo");
		l.add("Delta");
		// // for simple
		// for (int i = 0; i < l.size(); i++) {
		// System.out.println(l.get(i));
		// }
		// // l.remove("Alfa");
		// System.out.println("*******************************");
		// // for each/for mejorado
		// for (String s : l) {
		// System.out.println(s);
		// }
		// System.out.println("*******************************");
		//
		// // iterator
		// for (Iterator<String> it = l.iterator(); it.hasNext();) {
		// System.out.println(it.next());
		//
		// }
		// System.out.println("*******************************");
		//
		// // list iterator
		// for (ListIterator<String> it = l.listIterator(); it.hasNext();) {
		// System.out.println(it.next());
		// }
		// System.out.println("*******************************");
		//
		// // acces en mode d'array
		//
		// String[] s = l.toArray(new String[] {});
		//
		// for (String str : s) {
		// System.out.println(str);
		// }
		//
		// System.out.println("*******************************");
		// // sublist
		// List<String> sl = l.subList(0, l.size() / 2);
		// for (String s2 : sl) {
		// System.out.println(s2);
		// }
		// System.out.println("*******************************");
		//
		// // Collections
		// sorted
		Collections.sort(l);
		// for (String s3 : l) {
		// System.out.println(s3);
		//
		// }
		// // binarysearch
		// int index = Collections.binarySearch(l, "Delta");
		// System.out.println(l.get(index));
		//
		// // binarysearch
		//
		// System.out.println("*******************************");
		//
		// Collections.reverse(l);
		// for (String s4 : l) {
		// System.out.println(s4);
		//
		// }
		// // binarysearch
		// index = Collections.binarySearch(l, "Bravo");
		// System.out.println(index);
		// Rotacio
		Collections.rotate(l, 2);
		for (String s4 : l) {
			System.out.println(s4);

		}
		System.out.println("*******************************");
		Collections.shuffle(l);
		for (String s : l) {
			System.out.println(s);

		}
	}

}

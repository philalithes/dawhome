/*
 * Program.java 2016/24/02
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
package person;

public class Person {
    /** The person's name */
    private String name;
    /** The Person's age */
    private int age;
    // The person's height
    private double height;
    // the person's sex
    private char sex;
    // if the person is married
    private boolean married;
    //bike's person
    private Bicycle bike;
    
    //constructors
    
    /**
     * Constructor by default
     */
    public Person() {
        this.married = true;
    }
    /**
     * constructor-
     * 
     * @param name the person's name
     */
    public Person(String name) {
        this.name = name;
    }
    /**
     * Constructor.
     * 
     * @param name the person's name
     * @param age the person's age
     * @param height the person's height
     * @param sex the person's sex. it can be 'M' (man) or 'W' (woman).
     * @param married true if the person is married, false otherwise.
     */
    public Person(String name, int age, double height, char sex, boolean married) {
        this.name = name;
        this.setAge(age);
        this.height = height;
        this.sex = sex;
        this.married = married;
    }
    /**
     * Gets the person's name
     * 
     * @return the person's name
     */
    public String getName() {
        return this.name;
    }
    /**
     * Sets the person's name
     * @param name the person's name
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * Gets the person's age.
     * 
     * @return the person's age
     */
    public int getAge(){
        return this.age;
    }
    /**
     * Sets the person's age
     * 
     * @param age the person's age
     */
    public void setAge(int age) {
        this.age = age;
    }
    /**
     * Gets the person's height
     *
     * @return the person's height
     */
    public double getHeight() {
        return this.height;
    }
    /**
     * Sets the person's height
     * 
     * @param height the person's height
     */
    public void setHeight(double height) {
        this.height = height;
    }
    /**
     * Gets the person's sex
     * 
     * @return sex the person's sex
     */
    public char getSex() {
        return this.sex;
    }
    /**
     * Sets the person's sex
     * 
     * @param sex the person's sex
     */
    public void setSex(char sex) {
        this.sex = sex;
    }
    /**
     * Gets the person's status
     * 
     * @return the person's status
     */
    public boolean getMarried() {
        return this.married;
    }
    /**
     * Sets the person's status
     * 
     * @param married the person's status
     */
    public void setMarried(boolean married) {
        this.married = married;
    }
    /*
     * Increases age by 1
     * 
     * @param no args used.
     */
    public void birthday() {
        this.age++;
    }
    /*
     * Converts the person to a string
     */
    public String toString() {
        String personString = "";
        personString += this.name;
        personString += this.age;
        personString += this.height;
        personString += this.sex;
        personString += this.married;
        return personString;
    }
}
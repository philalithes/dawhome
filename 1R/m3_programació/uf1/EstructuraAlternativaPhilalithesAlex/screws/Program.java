/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * A partir d'un nombre decimal positiu i el nombre de
     * xifres decimals a arrodonir, calcula el nombre arrodonit correctament.
     * 
     * @param cm = centímetres
     * 
     *
     * 
     * @retorn mida = mida textual
     */
    public String screwSize(double cm){
        //declaració de variables
        String mida;
        //càlcul mida
        if (cm >= 1 && cm < 3) {
            mida = "Petit";
        }
        else if (cm >= 3 && cm < 5) {
            mida = "Mitjà";
        }
        else if (cm >= 5 && cm < 6.5) {
            mida = "Gran";
        }
        else if (cm >= 6.5 && cm < 8.5) {
            mida = "Molt gran";
        }
        else {
            mida = "no catalogat";
        }
        return mida;
    }
    
    
}

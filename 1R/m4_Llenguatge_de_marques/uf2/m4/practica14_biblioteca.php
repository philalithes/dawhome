<!doctype html>
<html>
<body>
<?php
$doc = new DOMDocument('1.0', 'iso-8859-1');
$doc->formatOutput = true;
$doc->preserveWhiteSpace = false;

$arrel = $doc->createElement('Biblioteca');
$arrel = $doc->appendChild($arrel);

$unLlibre = $doc->createElement('llibre');
$unLlibre = $arrel->appendChild($unLlibre);

$dosLlibre = $doc->createElement('llibre');
$dosLlibre = $arrel->appendChild($dosLlibre);

$unTitol = $doc->createElement('titol');
$unTitol = $unLlibre->appendChild($unTitol);

$dosTitol = $doc->createElement('titol');
$dosTitol = $dosLlibre->appendChild($dosTitol);

$unAutor = $doc->createElement('autor');
$unAutor = $unLlibre->appendChild($unAutor);

$dosAutor = $doc->createElement('autor');
$dosAutor = $dosLlibre->appendChild($dosAutor);

$txt = utf8_encode('titol del primer llibre');
$text = $doc->createTextNode($txt);
$text = $unTitol->appendChild($text);

$txt = utf8_encode('titol del segon llibre');
$text = $doc->createTextNode($txt);
$text = $dosTitol->appendChild($text);

$txt = utf8_encode('pepito');
$text = $doc->createTextNode($txt);
$text = $unAutor->appendChild($text);

$txt = utf8_encode('manolito');
$text = $doc->createTextNode($txt);
$text = $dosAutor->appendChild($text);

$doc->save('Biblioteca.xml');

$stringXML = $doc->saveXML();
echo $stringXML;
?>
</body>
</html>
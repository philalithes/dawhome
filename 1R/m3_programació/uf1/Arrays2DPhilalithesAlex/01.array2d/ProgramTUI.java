/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class ProgramTUI {
    /*
     * Llegeix el nombre de files i el nombre de columnes d'una
     * taula bidimensional numèrica entera, llegeix la taula i escriu-la.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variables
        Scanner sc = new Scanner(System.in);
        System.out.println("Escriu el nombre de files:");
        int files = sc.nextInt();
        System.out.println("Escriu el nombre de columnes:");
        int columnes = sc.nextInt();
        int[][] array = new int[files][columnes];
        int i, j = 0;
        System.out.println("Introdueix els nombres de l'array");
        //introducció de valors
        for (i = 0; i < files; i++) {
            for (j = 0; j < array[0].length; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        //llegeix la taula i escriu-la
        for (i = 0; i < files; i++) {
        System.out.println(Arrays.toString(array[i]));
        
        }
    }
   
}

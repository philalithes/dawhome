/*
 * Program.java 2015/20/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {
 /*
  * Determina el punt mig de 2 coordenades
  * 
  * @param a = coordenada1 
  * 
  * @param b = coordenada2
  * 
  * 
  * @return swap
  */


    
    public double average2(double a, double b) {
        double average;
        average = (a + b) / 2;
        return average;
        
    }
    public static void main(String args[]){
        //declaracio de variables
        double m1;
        double m2;
        //interacció amb l'usuari
        Scanner input = new Scanner(System.in);
        System.out.println("escriu la coordenada ax");
        double ax = input.nextDouble();
        System.out.println("escriu la coordenada ay");
        double ay = input.nextDouble();
        System.out.println("escriu la coordenada bx");
        double bx = input.nextDouble();
        System.out.println("escriu la coordenada by");
        double by = input.nextDouble();
        System.out.println("calculant...");
        //càlcul de punt mig
        ProgramTUI p = new ProgramTUI();
        m1 = p.average2(ax,bx);
        m2 = p.average2(ay,by);
        //retorn
        System.out.printf("el punt mig és %.2f x, %.2f y",m1,m2);
    }
}

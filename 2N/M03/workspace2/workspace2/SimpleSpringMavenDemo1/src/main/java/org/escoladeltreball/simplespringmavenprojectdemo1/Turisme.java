package org.escoladeltreball.simplespringmavenprojectdemo1;

/**
 * @author iaw47951368
 * Relació d'herència
 */
public class Turisme extends Vehicle {
	
	private int places;



	@Override
	public String toString() {
		return "Turisme [places=" + places + ", getMotor()=" + getMotor() + ", getMarca()=" + getMarca() + ", getClass()=" + getClass() + "]";
	}

	public Turisme() {
		super();
	}

	public Turisme(String marca, Motor motor, int places) {
		super(marca, motor);
		this.places = places;
	}
	
	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}
	
	
}

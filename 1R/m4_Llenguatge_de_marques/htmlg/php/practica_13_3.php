<html>
    <body>
        <script></script>
        <?php
            $doc = new DOMDocument('1.0', 'UTF-8');
            // print
            $doc -> formatOutput = true;
            $doc -> prserveWhiteSpace = false;
            
            $doc->load('../data/biblio.xml');
            $root = $doc->firstChild;

            $bookOne = $doc->createElement('llibre');
            $bookOne = $root->appendChild($bookOne);

            $titleOne = $doc->createElement('title');
            $titleOne = $bookOne->appendChild($titleOne);

            $authorOne = $doc->createElement('Autor');
            $authorOne = $bookOne->appendChild($authorOne);

            $txt = utf8_encode('Third book');
            $text = $doc->createTextNode($txt);
            $text = $titleOne->appendChild($text);

            $txt = utf8_encode('Wade Wilson');
            $text = $doc->createTextNode($txt);
            $text = $authorOne->appendChild($text);

            $doc->save('../data/biblio.xml');
            echo $doc->saveXML();
        ?>
    </body>
</html>
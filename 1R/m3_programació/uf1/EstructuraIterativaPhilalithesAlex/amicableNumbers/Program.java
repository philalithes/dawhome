/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats dos nombres enters positius,
     * determina si són amics o no.
     * Dos nombres enters positius són amics si la suma 
     * dels divisors positius d'un és igual a l'altre número i a l'inrevés.
     * 
     * int n1 = nombre 1
     * int n2 = nombre 2
     * 
     * return true = són amics
     * return false = no són amics
     */
    
    public boolean areAmicableNumbers(int n1, int n2) {
        //variable declaration
        int i = 0;
        int acumulador1 = 0;
        int acumulador2 = 0;
        boolean amics = false;
        //calcul
        if (n1 > 0 && n2 > 0) {
            for (i = n1-1; i > 0; i--) {
                if (n1 % i == 0) {
                    acumulador1 += i;
                }
            }
            if (acumulador1 == n2) {
                for (i = n2-1; i > 0; i--) {
                    if (n2 % i == 0) {
                        acumulador2 += i;
                    }
                }
                if (acumulador2 == n1) {
                    amics = true;
                }
            }
        }
        return amics;
    }
}

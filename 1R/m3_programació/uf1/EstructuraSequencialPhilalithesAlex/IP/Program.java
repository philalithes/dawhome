/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if 4 numbers can be an IP address
     * 
     * @param a = number1
     * @param b = number2
     * @param c = number3
     * @param d = number4
     * 
     * @return true = can be an IP, false = can't be an IP
     */
    public boolean isIP(int a, int b, int c, int d){
        //variable declaration    
        boolean is;
        //calculate return
        is = a <= 255 && a >= 0 && b <= 255 && b >= 0 && c <= 255 && c >= 0 && d <= 255 && d >= 0;
        //return
        return is;
    }
    
}

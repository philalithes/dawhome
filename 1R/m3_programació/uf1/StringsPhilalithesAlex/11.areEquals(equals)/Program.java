/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Program {
    /*
     * Donades dues cadenes, determina si són iguals o no.
     * Fes dues versions del programa: una usant el mètode areEquals(s1,s2) i una altra versió
     * sense usar aquest mètode.
     * 
     * 
     */
    
    public boolean areEquals(String cadena1, String cadena2) {
        boolean areEquals = false;
        if (cadena1.equals(cadena2)) {
            areEquals = true;
        }
        return areEquals;
    }
}

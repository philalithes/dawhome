/*
 * Program.java        2015/9/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Donats dos nombres enters positius n1 i n2 amb n1 < n2,
     * escriu tots els nombres enters que són múltiples de n 1 més petits o iguals a n 2 en ordre creixent.
     * 
     */
    
    public void writeMultipleNumbers(int n1, int n2) {
        int num = n1;
        while(num <= n2) {
            num++;
            if(num % n1 == 0){
                System.out.printf("%d es multiplo de %d\n",num,n1);
            }
            
        }
    }
}
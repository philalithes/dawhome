package org.escoladeltreball.springrestdemo1;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
/*
 * Data Acces Object 
 */

@Component
public class ClientDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	private List<Client> clients = new ArrayList<Client>();
	
	public void persist(Client client) {
		em.persist(client);
	}
	
	public List<Client> findAll() {
		return em.createQuery("SELECT c FROM Client c").getResultList();
	}
	
	public Client findById(long id) {
		return (Client) em.createQuery("SELECT c FROM Client c WHERE id = " + id).getSingleResult();
	}
	
	public Client findByNom(String nom) {
		return (Client) em.createQuery("SELECT c FROM Client c WHERE name = '" + nom + "'").getSingleResult();
	}
	
	@Transactional
	public boolean insertClient(String nom) {
		em.getTransaction().begin();
		em.persist(new Client(nom));
		em.getTransaction().commit();
		return true;
	}
	
	@Transactional
	public boolean deleteClient(int id) {
		Client c = findById(id);
		em.getEntityManagerFactory().createEntityManager().persist(c);
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

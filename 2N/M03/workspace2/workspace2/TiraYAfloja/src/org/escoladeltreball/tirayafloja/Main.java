package org.escoladeltreball.tirayafloja;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		while (true) {
			int nens = sc.nextInt();

			List<Integer> pesosNens = new ArrayList<>();
			if (nens == 0) {
				sc.close();
				System.exit(0);
			}
			int pesTotal = 0;
			for (int i = 0; i < nens; i++) {
				pesosNens.add(sc.nextInt());
			}
			for (int i = 0; i < pesosNens.size(); i++) {
				pesTotal += pesosNens.get(i);
			}
			int pesMig = pesTotal / 2;
			//aproximació per l'esquerra
			int pesEsquerra = 0;
			int j = 0;
			int pesAnterior = 0;
			int nensEsquerra = 0;
			while (pesEsquerra < pesMig) { 
				pesAnterior = pesEsquerra;
				pesEsquerra += pesosNens.get(j);
				j++;
				nensEsquerra++;
			}
			j--;
			pesEsquerra = pesAnterior;
			int pesDreta = 0;
			for (int i = j; i < pesosNens.size(); i++) {
				pesDreta += pesosNens.get(i);
			}
			//aproximació per la dreta
			int pesDreta2 = 0;
			j = pesosNens.size() - 1;
			int pesEsquerra2 = 0;
			while (pesDreta2 < pesMig) {
				pesAnterior = pesDreta2;
				pesDreta2 += pesosNens.get(j);
				j--;
				System.out.println(j);
			}
			j++;
			pesDreta2 = pesAnterior;
			for (int i = j; i > 0; i--) {
				pesEsquerra2 += pesosNens.get(i);
				System.out.println(i + "b");
			}
			//mirar diferència més petita
			int diff1 = Math.abs(pesDreta - pesEsquerra);
			int diff2 = Math.abs(pesDreta2 - pesEsquerra2);
			System.out.println(diff1 + " " + diff2);
			if (diff1 < diff2) {
				System.out.println(nensEsquerra - 1 + " " + pesEsquerra + " " + pesDreta);
			} else {
				System.out.println(nensEsquerra + " " + pesEsquerra2 + " " + pesDreta2);
			}
		}
	}

}

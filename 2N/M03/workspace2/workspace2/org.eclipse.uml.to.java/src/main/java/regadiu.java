/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of regadiu.
 * 
 * @author iaw47951368
 */
public class regadiu extends Cultiu {
	/**
	 * Description of the property tipusRegadiu.
	 */
	public String tipusRegadiu = "";
	
	// Start of user code (user defined attributes for regadiu)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public regadiu() {
		// Start of user code constructor for regadiu)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for regadiu)
	
	// End of user code
	/**
	 * Returns tipusRegadiu.
	 * @return tipusRegadiu 
	 */
	public String getTipusRegadiu() {
		return this.tipusRegadiu;
	}
	
	/**
	 * Sets a value to attribute tipusRegadiu. 
	 * @param newTipusRegadiu 
	 */
	public void setTipusRegadiu(String newTipusRegadiu) {
	    this.tipusRegadiu = newTipusRegadiu;
	}



}

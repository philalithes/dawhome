<?php
//    echo 'Andrea Matillas Maians#EGIBA#10500#12000#10000#11300';
    $doc = new DOMDocument();
    $doc->formatOutput = true;
    $doc->preserveWhiteSpace = false;
    $doc->load('../xml/gym.xml');
    $nom = utf8_encode($_REQUEST['fnom']);
    $noms = $doc->getElementsByTagName('nom');
    $gimnasta = '';

    for ($i = 0; $i < $noms->length; $i++) {
        if ($noms->item($i)->nodeValue == $nom) {
            // Save data
            $npare = $noms->item($i)->parentNode;
            $c = $npare->getElementsByTagName("club");
            $club = $c->item(0)->nodeValue;
            $r = $npare->getElementsByTagName('fase1');
            $gimnasta = $nom . '#' . $club . '#';
            
            foreach($r->item(0)->childNodes as $p) {
                $gimnasta = $gimnasta . $p->nodeValue . '#';
            }
        }
    }
    echo $gimnasta;
?>
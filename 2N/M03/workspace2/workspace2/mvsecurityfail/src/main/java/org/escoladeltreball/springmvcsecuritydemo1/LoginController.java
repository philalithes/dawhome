package org.escoladeltreball.springmvcsecuritydemo1;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

	//login redirect
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String loginPage() {
		return "login";
	}
	
	//Form dispatcher
//	@RequestMapping(value="/login", method = RequestMethod.POST)
//	public String loginForm(Model model, Principal principal) {
//		model.addAttribute("name", principal.getName());
//		return "home";
//	}
}

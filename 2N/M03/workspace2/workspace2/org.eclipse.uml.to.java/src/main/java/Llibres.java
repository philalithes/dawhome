/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Llibres.
 * 
 * @author iaw47951368
 */
public class Llibres extends Material audiovisual {
	/**
	 * Description of the property autor.
	 */
	public String autor = "";
	
	/**
	 * Description of the property isbn.
	 */
	public String isbn = "";
	
	// Start of user code (user defined attributes for Llibres)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Llibres() {
		// Start of user code constructor for Llibres)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Llibres)
	
	// End of user code
	/**
	 * Returns autor.
	 * @return autor 
	 */
	public String getAutor() {
		return this.autor;
	}
	
	/**
	 * Sets a value to attribute autor. 
	 * @param newAutor 
	 */
	public void setAutor(String newAutor) {
	    this.autor = newAutor;
	}

	/**
	 * Returns isbn.
	 * @return isbn 
	 */
	public String getIsbn() {
		return this.isbn;
	}
	
	/**
	 * Sets a value to attribute isbn. 
	 * @param newIsbn 
	 */
	public void setIsbn(String newIsbn) {
	    this.isbn = newIsbn;
	}



}

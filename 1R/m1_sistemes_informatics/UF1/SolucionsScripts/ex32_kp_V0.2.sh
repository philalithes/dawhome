#!/bin/bash
# Fitxer: ex32_kp_comments.sh
# Autor: jamoros
# Data: 31/03/2011
# Versio: 0.1
# Descripcio: script que suma tots els números que se li passen per paràmetre
#		No fem control d'errors


 

# Sumarem els nombres d ela següent manera
# Al principi la nostra suma valdrà zero

suma=0
# Per a cada un dels arguments afegiré, l'argument, a la suma anterior

for i in `seq 1 $#`
do
	eval argument=\$$i     # Mireu la nota(*)
	suma=$((suma + $argument))
done

# Mostraré el resultat

echo $suma

# (*) Example 28-1. Indirect Variable References (http://tldp.org/LDP/abs/html/ivr.html)

<<*

Recordeu que amb vim podem canviar el nom de totes les variables de cop amb l'ordre:
:1,$ s/nom_antic/nom_nou/g

on 1,$ vol dir des de la 1a línia fins a l'última, la 's' substuitució,
i la g de 'global' que ho faci tantes vegades per línia com calgui,
si no la escrivim només ho faria una vegada per línia com a màxim

*


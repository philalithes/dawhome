/*
 * Program.java        2015/9/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determine if a number is even
     * 
     * @param a = number
     * 
     * 
     * @return true = even, false = odd
     */
    public boolean isEven(int a){
        //variable declaration    
        boolean even;
        //calculate return
        even = a % 2 == 0;
        //return
        return even;
    }
    
}

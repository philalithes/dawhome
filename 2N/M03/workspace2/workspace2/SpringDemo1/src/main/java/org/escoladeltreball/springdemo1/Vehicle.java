package org.escoladeltreball.springdemo1;

import java.io.Serializable;

public class Vehicle implements Serializable{

	private Motor motor;
	private String marca;
	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	private Vehicle() {
		// TODO Auto-generated constructor stub
	}

	private Vehicle(String marca) {
		this.marca = marca;
	}

	private Vehicle(Motor motor, String marca, String color) {
		this.motor = motor;
		this.marca = marca;
		this.color = color;
	}

	public String getMarca() {
		return marca;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "Vehicle [motor=" + motor + ", marca=" + marca + ", color=" + color + "]";
	}
}

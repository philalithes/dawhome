function readXML(fileXML) { //SYNC
    var xmlDoc;
    if (window.XMLHttpRequest) {
        xmlDoc = new window.XMLHttpRequest();
        xmlDoc.open("GET", fileXML, false);
        xmlDoc.send("");
        return xmlDoc.responseXML;
    } else if (ActiveXObject("Microsoft.XMLDOM")) { // IE 5-6
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.load(fileXML);
        xmlDoc.open("GET", fileXML, false);
        xmlDoc.send("");
        return xmlDoc;
    }
    alert("Error loading document");
    return null;
}
function llegir_fitxer_xml(url) //ASYNC
{
	fitxer_llegit=url;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() //server ready
	{
		if (xhttp.readyState == 4 && xhttp.status == 200)
		{			
			dades_llegides(xhttp);	
		}
	};
	xhttp.open("GET", url,true);
	xhttp.send();
}

function php_request(url) //ASYNC
{
	fitxer_llegit=url;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() //server ready
	{
		if (xhttp.readyState == 4 && xhttp.status == 200)
		{			
			php_response(xhttp);	
		}
	};
	xhttp.open("GET", url,true);
	xhttp.send();
}

function get_firstChild(n){
    //alert('a'+n);
    var y = n.firstChild;
    while(y.nodeType != 1){
           y = y.nextSibling;
    }
    return y;
}
function get_nextSibling(n){
    //alert('b'+n);
    var y = n.nextSibling;
    while(y.nodeType != 1){
        y = y.nextSibling;
    }
    return y;
}

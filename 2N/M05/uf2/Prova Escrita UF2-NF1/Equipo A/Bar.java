import java.util.ArrayList;
import java.util.List;

public class Bar {
	
	public String name = "";
	public String cif = "";
	public List<Cerveza> cervezas = new ArrayList<Cerveza>();
	public String address = "";
	public String coordenadas = "";
	
	public Bar() {
		super();
	}
	
	public boolean avisarPromocion(Object promocion) {
		boolean avisarPromocion = false;
		return avisarPromocion;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String newName) {
		this.name = newName;
	}
	
	public String getCif() {
		return this.cif;
	}

	public void setCif(String newCif) {
		this.cif = newCif;
	}

	public List<Cerveza> getCervezas() {
		return this.cervezas;
	}

	public void setCervezas(List<Cerveza> newCervezas) {
		this.cervezas = newCervezas;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String newAddress) {
		this.address = newAddress;
	}
	
	public String getCoordenadas() {
		return this.coordenadas;
	}
	
	public void setCoordenadas(String newCoordenadas) {
		this.coordenadas = newCoordenadas;
	}
}
/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donat un nombre enter positiu, determina si és parell.
     * No emprar els operadors divisió entera ni mòdul.
     * El nombre zero es considera parell.
     * 
     * 
     * 
     * 
     * return true = és parell
     * return false = no és parell
     */
    
    public boolean isEven(int n) {
        //variable declaration
        int naux = n;
        //calcul
        if (naux > 0) {
            while (naux >= 1) {
                naux = naux -2;
            }
        }
        //cas negatiu
        if (naux < 0) {
            while (naux <= -1) {
                naux = naux +2;
            }
        } 
        return naux == 0;
    }
}
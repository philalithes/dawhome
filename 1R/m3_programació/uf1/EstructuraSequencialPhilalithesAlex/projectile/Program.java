/*
 * ProgramTUI.java        2015/14/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    final double G = 9.81;
    /**
     * Calculate flightime and impact point of a projectile
     * 
     * @param v velocity (m/s)
     * @param a angle (º)
     * 
     *
     * @return d = distance (meters without decimals)
     * @return t = flight time (seconds without decimals)
     */
    public double flightTime(double v, double a) {
        //variable declaration    
        int t;
        
        double radians = a * Math.PI / 180;
        //calculate return
        t = (2 * v * Math.sin(radians) / G);
        //retorn
        return (int)t;
    }
    public double distance(double v, double a) {
        //variable declaration    
        int d;
        double radians = a * Math.PI / 180;
        //calculate return
        d = (Math.pow(v,2) * Math.sin(2 * radians) / G);
        //retorn
        return (int)d;
    }
}

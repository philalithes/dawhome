/*
 # Filename:	controller.js
 # Author:		Gio Mikee Plata Justiniano iaw1128309
 # Date:		02/03/2017
 # License:		This is free software, licensed under the GNU General Public License v3.
 # 				See http://www.gnu.org/licenses/gpl.html for more information.
 */
var Pelicula = require('../model/peli.js');

module.exports = function (app) {

    // List all movies
    llistarPelis = function (req, res) {
        return Pelicula.find(function (err, pelis) {
            if (!err) {
                console.log('mostrant pelis...');
                res.render('mostrarPelis', {title: 'Pel.licules', pelis: pelis}); // JADE
                return;
            }
            return console.log('ERROR' + err);
        });
    }

    // List a movie via id
    llistarPeli = function (req, res) {
        return Pelicula.find({Codpeli: req.params.codPeli}, function (err, pelis) {
            if (!err) {
                if (!pelis.length) {
                    return res.sendStatus(404);
                }
                console.log('mostrant peli...');
                var result = '<b>Titol: </b>' + pelis[0].Titol + '<br>';
                result += '<b>Sinopsi: </b>' + pelis[0].Sinopsi;
                result += '<br><a href="/index">Volver al indice</a>';
                return res.send(result);
            }
            return console.log('ERROR' + err);
        });
    }

    // Function for showing the  form
    peliculaForm = function (req, res) {
        res.render('add', {});
    }

    // Function for inserting a movie into the database
    addPelicula = function (req, res) {
        if (!req.body.codpeli || !req.body.titol || !req.body.sinopsi) {
            res.render('add', {error: 'ERROR: Todos los campos son obligatorios'});
        }

        // Check if a movie with the same codpeli already exists in the database
        Pelicula.find({Codpeli: req.body.codpeli}, function (err, pelis) {
            if (err) {
                return console.log('ERROR' + err);
            }

            // Show error if movie exists
            if (pelis.length) {
                return res.render('add', {error: 'ERROR: Ya existe una pelicula con el mismo codpeli'});
            }
        });

        // Create new movie
        var newPeli = Pelicula({
            Codpeli: req.body.codpeli,
            Titol: req.body.titol,
            Sinopsi: req.body.sinopsi
        });

        newPeli.save(function (err, peli) {
            // Return error if error occurred when inserting the movie
            if (err) {
                return console.log('ERROR' + err);
            }
            return res.render('add', {saved: 'Pelicula guardada correctamente'});
        });

    }

    // Function for listing the movies that can be deleted in the form
    peliculaDeleteForm = function (req, res) {
        // Get all the movies from the database
        return Pelicula.find(function (err, pelis) {
            // Send the movies to delete.html if no errors occurred
            if (!err) {
                console.log('mostrant pelis...');
                res.render('delete', {pelis: pelis}); // JADE
                return;
            }
            // Show error
            return console.log('ERROR' + err);
        });
    }

    // Function for deleting a movie from the database
    peliculaDelete = function (req, res) {

        // Delete a movie that has the codpeli specified from the form
        Pelicula.find({Codpeli: req.body.codpeli}).remove(function (err, peli) {
            // Show error if it occurs when deleting a movie
            if (err) {
                return console.log("ERROR " + err);
            }
            // Notify user that delete was done correctly
            res.render('delete', {deleted: 'Pelicula borrada correctamente'});
        });
    }

    // Function for listing movies that can be edited in the form
    peliculaEditForm = function (req, res) {
        // Get all the movies from the databnase
        return Pelicula.find(function (err, pelis) {
            // Send the movies to update.html if no erors occurred
            if (!err) {
                console.log('mostrant pelis...');
                res.render('update', {pelis: pelis}); // JADE
                return;
            }
            // Show error if it exists
            return console.log('ERROR' + err);
        });
    }

    // Function for showing the info of a selected movie from update.html
    getPeliculaEdit = function (req, res) {
        // Find the movie with the codpeli specified in update.html
        return Pelicula.find({Codpeli: req.body.codpeli}, function (err, peliEdit) {
            // Send the info of the movie to edit.html if no error occurs
            if (!err) {
                console.log('mostrant peli a editar...');
                res.render('update', {peliEdit: peliEdit}); // JADE
                return;
            }
            // Show error if it exists
            return console.log('ERROR' + err);
        });
    }

    // Function for saving the changes made to a movie into the database
    peliculaEdit = function (req, res) {

        // Find the movie with the _id sent from update.html
        Pelicula.find({_id: req.body.id}, function (err, peli) {
            // Show error if it exists
            if (err) {
                return console.log("ERROR " + err);
            }

            // Edit the fields of the selected movie
            peli[0].Titol = req.body.titol;
            peli[0].Sinopsi = req.body.sinopsi;

            // Save the changes into the database
            peli[0].save(function (err) {
                // Show error if it exists
                if (err) {
                    return console.log("ERROR " + err);
                }
                // Notify the user that the changes were saved
                return res.render('update', {edited: "Se ha editado la pelicula correctamente"});
            });
        })
    }


    // ---------------------- UF 4
    // Function for showing rent form
    peliculaRentForm = function (req, res) {
        // Get all the movies from the database
        return Pelicula.find(function (err, pelis) {
            // Send the movies to rent.html if no erors occurred
            if (!err) {
                console.log('mostrant pelis...');
                res.render('rent', {pelis: pelis}); // JADE
                return;
            }
            // Show error if it exists
            return console.log('ERROR' + err);
        });
    }

    // Function for renting a movie
    peliculaRent = function (req, res) {
        // Find the movie selected in form
        Pelicula.find({_id: req.body.id}, function (err, peli) {
            if (err) {
                return console.log("ERROR " + err);
            }

            var dvdsInRent = [],
                coddvd;

            // Go through prestecs array and get the dvds that are currently in rent
            peli[0].Prestecs.forEach(function (prestec) {
                if (!prestec.DataDev) { // Get the current dvd if datadev field doesn't exist
                    dvdsInRent.push(prestec.Coddvd);
                }
            });

            // Go through the dvds of a movie and find one that is available for rent
            peli[0].Dvds.forEach(function (dvd) {
                // Get index of current dvd
                var isRented = dvdsInRent.indexOf(dvd);

                if (isRented == -1) {   // Get this dvd if it's not in the dvdsInRent array
                    coddvd = dvd;
                    return;
                }
            })

            // Return error if there are no available dvds of the movie
            if (!coddvd) {
                return res.render('rent', {error: "No hay DVDs disponibles de la pelicula seleccionada."})
            }

            // Return error if no name is specified
            if (!req.body.soci_firstname.trim().length && !req.body.soci_lastname.trim().length) {
                return res.render('rent', {error: "No se ha especificado ningun nombre."})
            }

            // The user that will rent the movie
            var soci = req.body.soci_firstname.trim();
            // Add last name if it is specified
            if (req.body.soci_lastname.trim().length) {
                soci += ' ' + req.body.soci_lastname.trim();
            }

            // Rent the movie
            Pelicula.update({_id: req.body.id}, {
                    $inc: {DvdsPrestats: 1},
                    $push: {
                        Prestecs: {
                            DataPres: new Date(),
                            Soci: soci,
                            Coddvd: coddvd
                        }
                    }
                }, {},
                function (err, peli) {
                    if (err) return console.log(err);
                    return res.render('rent', {'rented': "Se ha alquilado la pelicula correctamente"});
                });
        });
    }

    // Function for showing the return form
    peliculaReturnForm = function (req, res) {
        // Get all the movies from the database
        return Pelicula.find(function (err, pelis) {
            // Send the movies to return.html if no erors occurred
            if (!err) {
                console.log('mostrant pelis...');
                res.render('return', {pelis: pelis}); // JADE
                return;
            }
            // Show error if it exists
            return console.log('ERROR' + err);
        });
    }

    // Function for returning the users that are renting a movie
    peliculaReturnGetSocis = function (req, res) {
        // Get the movie selected in the form
        Pelicula.find({_id: req.body.id}, function (err, peli) {
            if (err) {
                return console.log("ERROR " + err);
            }

            var socis = [];
            // Get only the users without DataDev field in Prestecs
            peli[0].Prestecs.forEach(function (prestec) {
                if (!prestec.DataDev) {
                    socis.push(prestec);
                }
            });

            // Show error if the selected movie has no rented dvds
            if (!peli[0].DvdsPrestats || !peli[0].Prestecs.length || !socis.length) {
                return res.render('return', {error: "No hay DVDs en alquiler de la pelicula seleccionada."})
            }

            // Send the movie prestecs info and its id to the form
            return res.render('return', {socis: socis, peliId: peli[0]._id});
        });
    }

    // Function for returning a movie rented by a user
    peliculaReturn = function (req, res) {
        // Find the selected movie and the user that rented it
        Pelicula.find(
            {_id: req.body.peliId},
            function (err, peli) {

                // Return error if it exists
                if (err) {
                    return console.log("ERROR " + err);
                }

                // The Dvd to be returned
                var coddvd;
                // Go through prestecs array and get the rented dvd of the specified user
                peli[0].Prestecs.forEach(function (prestec) {
                    if (prestec.Soci == req.body.soci && !prestec.DataDev) {
                        coddvd = prestec.Coddvd; // Save the coddvd
                        return; // Break the loop
                    }
                });

                // Update the selected movie and the user that rented it
                Pelicula.update({
                        _id: req.body.peliId,
                        Prestecs: {
                            $elemMatch: {
                                Coddvd: coddvd,
                                DataDev: {$exists: false},
                                Soci: req.body.soci
                            }
                        }
                    }, {
                        // Add DataDev field to the user who is returning the movie, and decrease DvdsPrestats by 1
                        $set: {"Prestecs.$.DataDev": new Date()},
                        $inc: {DvdsPrestats: -1}
                    }, {},
                    function (err, peli) {
                        if (err) return console.log(err);
                        // Notify that the movie has been returned
                        return res.render('return', {'returned': "Se ha devuelto la pelicula correctamente"});
                    });


            });
    }

    // index
    app.get(["/", "/index"], function (req, res) {
        return res.render('index', {});
    });

    // List all movies
    app.get("/list", llistarPelis);

    // List a specific movie
    app.get('/list/:codPeli', llistarPeli);

    // Show the form for inserting movies
    app.get('/add', peliculaForm);

    // Insert the movie
    app.post('/add', addPelicula);

    // Show the form for deleting movies
    app.get('/delete', peliculaDeleteForm);

    // Delete the movie
    app.post('/delete', peliculaDelete);

    // Show the form for selecting a movie to be edited
    app.get('/update', peliculaEditForm);

    // Show the selected movie to be edited
    app.post('/get_peli_update', getPeliculaEdit);

    // Save the changes made to a specific movie
    app.post('/update', peliculaEdit);


    // ----------- UF 4
    app.get('/rent', peliculaRentForm);
    app.post('/rent', peliculaRent);
    app.get('/return', peliculaReturnForm);
    app.post('/get_socis_peli', peliculaReturnGetSocis);
    app.post('/return', peliculaReturn);
}
package org.escoladeltreball.springdemo3;

public class Motor implements Engine {

	private int cavalls;
	private double oil;
	
	public Motor() {
		// TODO Auto-generated constructor stub
	}

	public int getCavalls() {
		return cavalls;
	}

	public Motor(int cavalls, double oil) {
		super();
		this.cavalls = cavalls;
		this.oil = oil;
	}

	public double getOil() {
		return oil;
	}

	public void setOil(double oil) {
		this.oil = oil;
	}

	public void setCavalls(int cavalls) {
		this.cavalls = cavalls;
	}

	@Override
	public String toString() {
		return "Motor [cavalls=" + cavalls + ", oil=" + oil + "]";
	}

	public Motor(int cavalls) {
		super();
		this.cavalls = cavalls;
	}

	public void start() {
		System.out.println("Arrancant...");
	}

	public void stop() {
		System.out.println("Parant...");		
	}

}

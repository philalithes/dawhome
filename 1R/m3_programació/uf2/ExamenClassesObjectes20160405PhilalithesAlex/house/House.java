/*
 * House.java        1.0 March 14, 2016
 *
 * Models the program.
 *
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

package house;
import org.joda.time.DateTime;
public class House 
{
    //** House's address **/
    private String address; 
    /** The date when bulding a house, in format dd/mm/yyyy */
    private final String buildingDate;
    /** Years for the first review **/
    private final static int ITV_YEARS = 15; /** Years for the first review **/
    
    // Constructor
    
    public House(String buildingDate) 
    {
        this.buildingDate = buildingDate;
    }
    
    // Getters and setters
     
    public String getAddress() {
        return this.address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBuildingDate() {
        return this.buildingDate;
    }
    
    // Methods
    
    /**
     * Determines if a house is out of date (ITV). 
     * 
     * @return true if the house is out of date (ITV), false otherwise
     */  
    
    public boolean isOutOfDateITV() {
        //crear objecte DateTime de l'atribut buildingDate
        DateTime buildingDateTime = JodaDT.parseDDMMYYYY(this.getBuildingDate()); 
        //crear objecte DateTime del dia d'avui
        DateTime now = new DateTime();
        //comparar si la data de construcció + 
        //els anys que han de passar per fer la primera revisió són abans del dia d'avui.
        if (buildingDateTime.plusYears(ITV_YEARS).isBefore(now)) {
            return true;
        }
        return false;
    }
    
    /**
     * Makes a report of the house with the following format:
     * 
     * Adreça : 
     * Any de construcció : 
     * Té de passar la revisió ITV ?
     */
    public void reportHouse() {
        String itv = "";
        if (this.isOutOfDateITV()) {
            itv = "SI";
        } else {
            itv = "NO";
        }
        System.out.println("Adreça : " + this.getAddress());
        System.out.println("Any de construcció : " + this.getBuildingDate());
        System.out.println("Té de passar la revisió ITV ? " + itv);
    }
    
}

package org.escoladeltreball.m03uf06nf02APG;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Entity implementation class for Entity: Competicio
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "selectCompeticio", query = "SELECT c FROM Competicio c WHERE c.id = ?1"),
	@NamedQuery(name = "selectCompeticioByNom", query = "SELECT c FROM Competicio c WHERE c.nom like ?1"),
	@NamedQuery(name = "updateCompeticio", query = "UPDATE Competicio c SET c.nom = ?1, c.premi = ?2, c.maxEquips = ?3 WHERE c.id = ?4")
})
public class Competicio implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nom;
	private Double premi;
	private Integer maxEquips;
	private static final long serialVersionUID = 1L;
	//owner
	@ManyToMany(cascade=CascadeType.PERSIST)
	private List<Equip> equips = new ArrayList<>();

	public void addEquip(Equip e) {
		equips.add(e);
	}
	
	public void removeEquip(Equip e) {
		equips.remove(e);
	}
	
	
	@Override
	public String toString() {
		return "Competicio [id=" + id + ", nom=" + nom + ", premi=" + premi + ", maxEquips=" + maxEquips + "]";
	}

	public Competicio(String nom, Double premi, Integer maxEquips) {
		super();
		this.nom = nom;
		this.premi = premi;
		this.maxEquips = maxEquips;
	}

	public Competicio() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public Double getPremi() {
		return this.premi;
	}

	public void setPremi(Double premi) {
		this.premi = premi;
	}   
	public Integer getMaxEquips() {
		return this.maxEquips;
	}

	public void setMaxEquips(Integer maxEquips) {
		this.maxEquips = maxEquips;
	}
   
}

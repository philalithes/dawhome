#!/bin/bash
# Filename:			30.sh
# Author:			iaw47951368
# Date:				26/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./30.sh noargs
# Description:		 un script que demani un caràcter i ens digui si és un número, una lletra o una altra cosa.

#demana caracter
echo "escriu 1 caràcter per analitzar"
read character
num_chars=$(echo -n $character | wc -m)
if [ "$num_chars" -eq 1 ]
then
	case $character in
		# és una lletra? 
		[[:lower:]] | [[:upper:]] ) echo "$character és una lletra"
	    ;;
	    # és un núm?
	    [0-9] )                     echo "$character és un número"
	    ;;
	    # altres coses
	    * )                         echo "$character no és ni un número ni una lletra"
	esac
else
	echo "ERROR: no has escrit 1 caràcter"
	exit 1
fi

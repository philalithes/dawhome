package org.escoladeltreball.simplespringmavenprojectdemo1;

public class Motor {

	private Integer cavalls;
	private Integer cilindrada;
	
	public Motor() {
		
	}

	@Override
	public String toString() {
		return "Motor [cavalls=" + cavalls + ", cilindrada=" + cilindrada + "]";
	}

	public Motor(Integer cavalls, Integer cilindrada) {
		super();
		this.cavalls = cavalls;
		this.cilindrada = cilindrada;
	}

	public Integer getCavalls() {
		return cavalls;
	}

	public void setCavalls(Integer cavalls) {
		this.cavalls = cavalls;
	}

	public Integer getCilindrada() {
		return cilindrada;
	}

	public void setCilindrada(Integer cilindrada) {
		this.cilindrada = cilindrada;
	}
	
}

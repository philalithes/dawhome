package org.escoladeltreball.m03uf06nf02APG;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * Entity implementation class for Entity: Estadi
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = "selectEstadi", query = "SELECT e FROM Estadi e WHERE e.id = ?1"),
	@NamedQuery(name = "selectEstadiByNom", query = "SELECT e FROM Estadi e WHERE e.nom like ?1"),
	@NamedQuery(name = "updateEstadi", query = "UPDATE Estadi e SET e.nom = ?1, e.capacitat = ?2, e.qualitatTerreny = ?3 WHERE e.id = ?4")
})
public class Estadi implements Serializable {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nom;
	private Integer capacitat;
	private Double qualitatTerreny;
	private static final long serialVersionUID = 1L;
	@OneToOne(cascade = CascadeType.PERSIST, mappedBy = "estadi")
	private Equip equip;

	
	
	public Equip getEquip() {
		return equip;
	}
	public void setEquip(Equip equip) {
		this.equip = equip;
	}
	@Override
	public String toString() {
		return "Estadi [id=" + id + ", nom=" + nom + ", capacitat=" + capacitat + ", qualitatTerreny=" + qualitatTerreny
				+ ", equip=" + equip + "]";
	}
	public Estadi(String nom, Integer capacitat, Double qualitatTerreny) {
		super();
		this.nom = nom;
		this.capacitat = capacitat;
		this.qualitatTerreny = qualitatTerreny;
	}
	public Estadi() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}   
	public Integer getCapacitat() {
		return this.capacitat;
	}

	public void setCapacitat(Integer capacitat) {
		this.capacitat = capacitat;
	}   
	public Double getQualitatTerreny() {
		return this.qualitatTerreny;
	}

	public void setQualitatTerreny(Double qualitatTerreny) {
		this.qualitatTerreny = qualitatTerreny;
	}
   
}

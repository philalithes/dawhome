/*
 * ProgramTUI.java        1.0 Oct 30, 2016
 *
 * Models the program.
 *
 * Copyright 2016 Alex Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Arrays;

public class ProgramTUI {

    /**
    * creates white character
    * 
    * @param array containing white character 
    */
    public void whiteLetter(char letter[][]) {
        //Omplir l'array amb caràcters en blanc
        //variables
        int i = 0;
        int j = 0;
        //recorregut
        for (i = 0; i < letter.length; i++) { //Recorregut per files
            for (j = 0; j < letter[0].length; j++) { //columnes
                letter[i][j] = ' ';
            }
        }
     }
    /**
    * shows a letter
    * 
    * @param array containing a letter 
    */ 
    public void viewLetter(char letter[][]) {
        //variables
        int i = 0;
        int j = 0;
        //Mostrar l'array per pantalla
        for (i = 0; i < letter.length; i++) {
            for (j = 0; j < letter[0].length; j++) {
                
                    
                System.out.print(letter[i][j]);
                if (j == letter[0].length - 1) {
                    System.out.println();
                }
            }
        }
     }
    
    /**
    * builds the letter L
    * 
    * @param array containing a letter L 
    */  
    public void letterL(char letter[][]) {
        //Construir la lletra L amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[4][j] = '*';
             }
             
         }
     }
    
    /**
    * builds the letter O
    * 
    * @param array containing a letter O 
    */ 
    public void letterO(char letter[][]) {
        //Construir la lletra O amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[0][j] = '*';
                 letter[4][j] = '*';
             }
             
         }
     }
     
    /**
    * builds the letter H
    * 
    * @param array containing a letter H 
    */
     public void letterH(char letter[][]) {
         //Construir la lletra H amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[2][j] = '*';
             }
             
         }
     }
     
    /**
    * builds the letter A
    * 
    * @param array containing a letter A 
    */
     public void letterA(char letter[][]) {
       //Construir la lletra H amb asteriscs.
         //variables
         int i = 0;
         int j = 0;
         //Recorregut i construcció de la lletra.
         for (i = 0; i < letter.length; i++) { //Recorregut per files
             for (j = 0; j < letter[0].length; j++) { //columnes
                 letter[i][0] = '*';
                 letter[i][4] = '*';
                 letter[0][j] = '*';
                 letter[3][j] = '*';
             }
             
         }
     }
     
     public static void main(String[] args) {
          ProgramTUI p = new ProgramTUI();
          int SIZE = 5;
          char letter [][] = new char [SIZE][SIZE];   // Matriu de longitut senar Minim 5X5
          
          //Primer omplim l'array amb caràcters en blanc
          p.whiteLetter(letter);
          //posem la lletra H dins l'array
          p.letterH(letter);
          //mostrem la lletra H
          p.viewLetter(letter);
          //Tornem a omplir l'array amb caràcters en blanc
          p.whiteLetter(letter);
          //posem la lletra O dins l'array
          p.letterO(letter);
          //mostrem la lletra O
          p.viewLetter(letter);
          //Tornem a omplir l'array amb caràcters en blanc
          p.whiteLetter(letter);
          //posem la lletra L dins l'array
          p.letterL(letter);
          //mostrem la lletra L
          p.viewLetter(letter);
          //Tornem a omplir l'array amb caràcters en blanc
          p.whiteLetter(letter);
          //posem la lletra A dins l'array
          p.letterA(letter);
          //mostrem la lletra A
          p.viewLetter(letter);
          
    }
}

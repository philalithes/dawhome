package org.escoladeltreball.m03uf05final.APG;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class Main {
	
	/**
	 * @Autor Alex Philalithes Gallego
	 */
	public static void main(String[] args) {
		//****** lectura de fitxers ******
		// paths als fitxers
		Path pathRegistres = Paths.get("registres.txt");
		Path pathHoraris = Paths.get("horaris.txt");
		// lectura del fitxer de horaris i guardar en ArrayList els professors
		List<Professor> professors = new ArrayList<>();
		try {
			professors = Files.lines(pathHoraris).map(Professor::new).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// lectura del fitxer de projectes i guardar en ArrayList
		List<Registre> registres = new ArrayList<>();
		try {
			registres = Files.lines(pathRegistres).map(Registre::new).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//****** fi de lectura de fitxers ******
		
		HorarisManager hm = new HorarisManager(professors, registres);
		hm.solve();
	}

}

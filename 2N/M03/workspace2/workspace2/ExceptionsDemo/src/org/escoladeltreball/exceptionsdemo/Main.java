package org.escoladeltreball.exceptionsdemo;

import java.io.EOFException;
import java.io.FileNotFoundException;

class MiClase {
	public void f() throws UniqueKeyException {
		System.out.println("En f()");
		throw new UniqueKeyException("Missatge de Exception");
	}

	public void g() {
		System.out.println("En g()");
	}

	public void h() throws RuntimeException {
		System.out.println("En h()");
	}
	
	public void k() throws EOFException, FileNotFoundException {
		System.out.println("En k()");
	}
}

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MiClase miClase = new MiClase();
		
		try {
			miClase.k();
		} catch (EOFException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		} catch (FileNotFoundException e) {
			
		} catch (Exception e){ //exceptions més concretes a dalt i més generals a baix
			
		} 
//
//		try {
//			miClase.f();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			System.out.println(e.getMessage());
//			e.printStackTrace();
//		}
//		miClase.g();
//		miClase.h();
	}

}

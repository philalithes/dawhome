/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula numèrica real, crea la seva taula
     * transposada. La taula transposada s'obté canviant les files per les columnes.
     * 
     * 
     */
    
    public double[][] transpose(double[][] array) {
        //variables
        int i, j = 0;
        double[][] transposed = new double[array.length][array[0].length];
        //calcul
        for (i = 0; i < array.length; i++) {
            for (j = 0; j < array[0].length; j++) {
                transposed[j][i] = array[i][j];
            }
        }
        return transposed;
    }
   
}

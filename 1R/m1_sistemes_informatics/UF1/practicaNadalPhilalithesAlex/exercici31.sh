#!/bin/bash
# Filename:			31
# Author:			iaw47951368
# Date:				26/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./31 noargs
# Description:		Demana un directori i llista el seu contingut.

#preguntem i capturem el directori.
echo "Escriu el nom d'un directori"
read directori

#es un directori?
if [ -d "$directori" ] #si ho és.
then
	ls -la "$directori"
else #no ho és
	echo ""$directori" no és un directori"
	exit 1
fi


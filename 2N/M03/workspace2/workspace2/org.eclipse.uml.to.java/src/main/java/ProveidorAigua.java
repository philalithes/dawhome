/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of ProveidorAigua.
 * 
 * @author iaw47951368
 */
public class ProveidorAigua {
	/**
	 * Description of the property nom.
	 */
	public String nom = "";
	
	/**
	 * Description of the property preuAigua.
	 */
	public double preuAigua = 0D;
	
	// Start of user code (user defined attributes for ProveidorAigua)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public ProveidorAigua() {
		// Start of user code constructor for ProveidorAigua)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for ProveidorAigua)
	
	// End of user code
	/**
	 * Returns nom.
	 * @return nom 
	 */
	public String getNom() {
		return this.nom;
	}
	
	/**
	 * Sets a value to attribute nom. 
	 * @param newNom 
	 */
	public void setNom(String newNom) {
	    this.nom = newNom;
	}

	/**
	 * Returns preuAigua.
	 * @return preuAigua 
	 */
	public double getPreuAigua() {
		return this.preuAigua;
	}
	
	/**
	 * Sets a value to attribute preuAigua. 
	 * @param newPreuAigua 
	 */
	public void setPreuAigua(double newPreuAigua) {
	    this.preuAigua = newPreuAigua;
	}



}

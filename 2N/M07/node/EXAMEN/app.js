var express = require("express"),
	bodyParser = require("body-parser"),
	mongoose = require("mongoose"),
	app = express();


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set("view engine", "jade");


mongoose.connect("mongodb://localhost/videoclub", function(err, res) {
	if (err) console.log("error al conectarse a la bd");
	else console.log("conectat a la bd");
});

require("./routes/controller")(app);

app.listen(3000, function() {
	console.log("node server running at localhost:3000")
});

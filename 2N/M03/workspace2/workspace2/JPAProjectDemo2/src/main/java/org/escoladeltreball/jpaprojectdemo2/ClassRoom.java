package org.escoladeltreball.jpaprojectdemo2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: ClassRoom
 *
 */
@Entity
@Table(name="classRoom")

public class ClassRoom implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	//inverse
	@OneToMany(mappedBy = "classRoom")
	private List<Student> students = new ArrayList<Student>();;


	private String name;
	private static final long serialVersionUID = 1L;
	
	

	@Override
	public String toString() {
		return "ClassRoom [id=" + id + ", name=" + name + "]";
	}

	public ClassRoom(String name) {
		super();
		this.name = name;
	}
	
	public void add(Student s) {
		students.add(s);
	}
	
//	public Student pop(Student s) {
//		for (Student s1 : students) {
//			if (s1 == s) {
//				return s1;
//			}
//		}
//		return null;
//	}
	
	public List<Student> getStudents() {
		return students;
	}
	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public ClassRoom() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}

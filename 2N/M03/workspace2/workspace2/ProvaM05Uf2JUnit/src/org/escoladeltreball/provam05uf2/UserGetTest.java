package org.escoladeltreball.provam05uf2;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

public class UserGetTest {

	public static User user1;
	@BeforeClass
	public static void testBefore() {
		user1 = new User(1l, "alex", 1500);
	}
	@Test
	public void test() {
		assertEquals("error en getId()",1l, user1.getId());
		assertEquals("error en getName()","alex", user1.getName());
		assertEquals("error en getScore()",1500, user1.getScore());
	}

}

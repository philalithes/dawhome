package org.escoladeltreball.iodemo7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.escoladeltreball.iodemo8.Moviment;

public class Main {

	public static void main(String[] args) {
		// try with resources
		/*
		 * millorable
		 */
		List<Moviment> moviments = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader("file1"));
				BufferedWriter writer1 = new BufferedWriter(new FileWriter("negatius"));
				BufferedWriter writer2 = new BufferedWriter(new FileWriter("positius"));) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				// System.out.println(line);
				String descripcio = line.split(" ")[0];
				String date = line.split(" ")[1];
				int cant = Integer.parseInt(line.split(" ")[2]);
				moviments.add(new Moviment(descripcio, date, cant));
			}
			for (Moviment m : moviments) {
				if (m.getCant() >= 0) {
					writer2.write(m.getDescripcio() + " " + m.getDate() + " " + m.getCant());
					writer2.newLine();
				} else {
					writer1.write(m.getDescripcio() + " " + m.getDate() + " " + m.getCant());
					writer1.newLine();
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}

/*
 * Program.java        2016/1/20
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     * Donada una cadena, crea una altra cadena amb els
     * caràcters de la cadena donada invertits.
     * 
     * 
     */
    
    public String reverse (String cadena) {
        //vars
        int occurrencesOfChar = 0;
        int i = 0;
        String novaCadena = "";
        //calc
        for (i = cadena.length()-1; i >= 0; i--) {
            novaCadena += cadena.charAt(i);
            
        }
        
        
        //ret
        return novaCadena;
        
        
    }
}

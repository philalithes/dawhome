package org.escoladeltreball.m03uf05nf02.APG;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	/**
	 * @Autor Alex Philalithes Gallego
	 */
	public static void main(String[] args) {
		// paths als fitxers
		Path pathRRHH = Paths.get("RRHH.txt");
		Path pathProjects = Paths.get("Projects.txt");
		// lectura del fitxer de RRHH i guardar en ArrayList
		List<Empleat> empleats = new ArrayList<>();
		try {
			empleats = Files.lines(pathRRHH).map(Empleat::new).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// lectura del fitxer de projectes i guardar en ArrayList
		List<Projecte> projectes = new ArrayList<>();
		try {
			projectes = Files.lines(pathProjects).map(Projecte::new).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// ordenem els projectes per pressupost
		projectes = projectes.stream()
				.sorted((p1, p2) -> new Long(p2.getPressupost()).compareTo(new Long(p1.getPressupost())))
				.collect(Collectors.toList());
		// asignar empleats a cada projecte
		for (Projecte p : projectes) {
			Manager.setProject(p, empleats);
		}
		String out = "";
		// escriu output
		for (Projecte p : projectes) {
			out += p.getNom();
			if (p.getBackEnd() == null || p.getFrontEnd() == null || p.getProjectManager() == null) {
				out += ",stand by\n";
			} else {
				out += "," + p.getProjectManager().getAlias() + "," + p.getBackEnd().getAlias() + ","
						+ p.getFrontEnd().getAlias() + "\n";
			}
		}
		System.out.println(out);

	}

}

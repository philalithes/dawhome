/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix un nombre enter x i una seqüència de
     * nombres enters acabada en zero i escriu si hi ha algun element més gran que x
     * (interrompent la lectura de nombres si trobes l'element).
     */
    
    public static void main(String[] args) {
        //variable declaration
        int x = 0;
        int n = Integer.MIN_VALUE;
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        sc.useLocale(Locale.ENGLISH);
        System.out.println("Escriu un nombre enter x (diferent de zero) 4i una seqüència de nombres enters acabada en zero. \nSi escrius un nombre més gran que x t'interrompiré.");
        x = sc.nextInt();
        //càlcul
        while (n <= x && n != 0) {
            System.out.println("Escriu un nombre");
            n = sc.nextInt();
        }
        if (n == 0) {
            System.out.println("Has escrit zero... Seqüència acabada.");
        }
        else {
            System.out.println("Has escrit un nombre més gran que x!");
            
        }
    }
}

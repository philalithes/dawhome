package org.escoladeltreball.parrilladadesalida;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {

	private static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {

		
			int size = 0;
		while (true) {
			
			size = sc.nextInt();
			if (size == 0) {
				System.exit(0);
			} else {
				List<Posicio> parrilla = new ArrayList<>();
				
				//omplir list
				for (int i = 0; i < size; i++) {
					parrilla.add(new Posicio(sc.nextInt(),sc.nextLine().trim()));
					
				}
				
				//setting positions
				for (int j = 1; j <= parrilla.size(); j++) {
					int pos = parrilla.get(j - 1).getPos();
					parrilla.get(j - 1).setPos(j + pos);
				}
				
				
				//ordenar parrilla
				Collections.sort(parrilla, new Comparator<Posicio>(){
					public int compare(Posicio p1, Posicio p2) {
						return p1.getPos() - p2.getPos();
					}
				});
				
				for (Posicio p : parrilla) {
					//validacion de pos
					if (p.getPos() < 1 || p.getPos() > 26) {
						System.out.println("IMPOSIBLE");
					} else {
						System.out.println(p);
					}
				}
				System.out.println("-----");
			}
		}
		
	}
	
	

}



<html>
    <body>
        <script></script>
        <?php
            $doc = new DOMDocument('1.0', 'UTF-8');
            // print
            $doc -> formatOutput = true;
            $doc -> prserveWhiteSpace = false;

            $root = $doc->createElement('Biblioteca');
            $root = $doc->appendChild($root);

            $bookOne = $doc->createElement('llibre');
            $bookOne = $root->appendChild($bookOne);

            $bookTwo = $doc->createElement('llibre');
            $bookTwo = $root->appendChild($bookTwo);

            $titleOne = $doc->createElement('title');
            $titleOne = $bookOne->appendChild($titleOne);

            $titleTwo = $doc->createElement('title');
            $titleTwo = $bookTwo->appendChild($titleTwo);

            $authorOne = $doc->createElement('Autor');
            $authorOne = $bookOne->appendChild($authorOne);

            $authorTwo = $doc->createElement('Autor');
            $authorTwo = $bookTwo->appendChild($authorTwo);

            $txt = utf8_encode('First book');
            $text = $doc->createTextNode($txt);
            $text = $titleOne->appendChild($text);

            $txt = utf8_encode('Second book');
            $text = $doc->createTextNode($txt);
            $text = $titleTwo->appendChild($text);

            $txt = utf8_encode('John Doe');
            $text = $doc->createTextNode($txt);
            $text = $authorOne->appendChild($text);

            $txt = utf8_encode('Jason Bourne');
            $text = $doc->createTextNode($txt);
            $text = $authorTwo->appendChild($text);

            $doc->save('../data/biblio.xml');
            echo $doc->saveXML();
        ?>
    </body>
</html>
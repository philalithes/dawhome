package daw.m08.uf2.APG;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.ViewResolver;
import org.thymeleaf.extras.springsecurity3.dialect.SpringSecurityDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;
import org.thymeleaf.templateresolver.UrlTemplateResolver;

import net.sourceforge.pagesdialect.PagesDialect;
import nz.net.ultraq.thymeleaf.LayoutDialect;

@Configuration
@PropertySource("classpath:thymeleaf.properties")
public class ThymeleafConfig {

	@Autowired private Environment env;
	
	@Bean TemplateResolver templateResolver() {
		ServletContextTemplateResolver tr = new ServletContextTemplateResolver();
		tr.setPrefix(env.getProperty("thymeleaf.prefix"));
		tr.setSuffix(env.getProperty("thymeleaf.suffix"));
		tr.setTemplateMode(env.getProperty("thymeleaf.mode"));
		tr.setCacheable(false);
		return tr;
	}
	
	@Bean
	public UrlTemplateResolver urlTemplateResolver() {
		return new UrlTemplateResolver();
	}
	
	@Bean
	public SpringTemplateEngine templateEngine() {
		SpringTemplateEngine te = new SpringTemplateEngine();
		te.setTemplateResolver(templateResolver());
		te.addDialect(new SpringSecurityDialect());
		te.addDialect(new PagesDialect());
		te.addDialect(new LayoutDialect());
		
		return te;
	}
	
	@Bean
	public ViewResolver thymeleafViewResolver() {
		ThymeleafViewResolver viewResolver = new ThymeleafViewResolver();
		viewResolver.setTemplateEngine(templateEngine());
		viewResolver.setCharacterEncoding("UTF-8");
		viewResolver.setOrder(Ordered.HIGHEST_PRECEDENCE);
		
		
		return viewResolver;
	}
}

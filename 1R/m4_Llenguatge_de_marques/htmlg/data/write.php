<?php
    header ('Content-type: text/html; charset=UTF-8');
    // Create file
    $file = fopen("file.txt", "w");
    fwrite($file, "first text line" . PHP_EOL);
    
    fwrite($file, "second line" . PHP_EOL);
    
    fclose($file);

    // Read file
    $file = fopen("file.txt", "r");
    while (!feof($file)) {
        echo fgets($file) . "<br>";
    }
    fclose($file);

    // Append lines
    $file = fopen("file.txt", "a");
    
    fwrite($file, "third line" . PHP_EOL);
    
    fwrite($file, "fourth line", PHP_EOL);

    fclose($file);
?>
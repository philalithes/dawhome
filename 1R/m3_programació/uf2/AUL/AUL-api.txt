AUL (Arrays Utils Library)

METHOD SUMMARY
==============
1-static double[] append(double[] a, double value)
	Returns an array with the same elements of a but with value added to the end of the array.
2-static int binarySearch(double[] a, double key)
	Searches the specified array of doubles for the specified value using the binary search algorithm.
3-static int binarySearch(double[] a, int fromIndex, int toIndex, double key)
	Searches a range of the specified array of doubles for the specified value using the binary 4-search algorithm.
4-static double[]	copyOf(double[] original, int newLength)
	Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.
5-static double[]	copyOfRange(double[] original, int from, int to)
	Copies the specified range of the specified array into a new array.
6-static boolean equals(double[] a, double[] a2)
	Returns true if the two specified arrays of doubles are equal to one another.
7-static void fill(double[] a, double val)
	Assigns the specified double value to each element of the specified array of doubles.
8-static void fill(double[] a, int fromIndex, int toIndex, double val)
	Assigns the specified double value to each element of the specified range of the specified array of doubles.
9-static double[] insert(double[] a, int index, double value)
	Returns an array with the same element of a but with value inserted in index.
10-static double[] union(double[] a, double[] b)
	Returns an array with all elements of a and, after them, all elements of b.
11-static double[] prepend(double[] a, double value)
	Returns an array with the same elements of a but with value added to the beginning of the array.
12-static double[] remove(double[] a, int index)
	Returns an array with the same elements of a but without value in index.
13-static void sort(double[] a)
	Sorts the specified array into ascending numerical order.
14-static void sort(double[] a, int fromIndex, int toIndex)
	Sorts the specified range of the array into ascending order.
15-static String toString(double[] a)
	Returns a string representation of the contents of the specified array.

Imagine 5 methods more that you like to have in an arrays library!

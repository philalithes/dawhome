package org.escoladeltreball.transaccion;

import java.util.Random;

public class Main {
	
	public static Random rnd = new Random(); 
	
	public static void main(String[] args) {
		Leaderboard scores = new Leaderboard();
		for (int i = 0; i < 30; i++) {
			User u = new User(i, "user"+i, (long)(rnd.nextInt(2000)));
			scores.addUser(u);
		}
		
		User u = new User(100L, "user", (long)(rnd.nextInt(2000)));
		System.out.println("The Winner is "+ scores.userWin().getName() + " scores " + scores.userWin().getScore() + "!!!");
		System.out.println("Diferencia entre el segundo usuario y el primero : "+ scores.difSecondFirst());
		System.out.println("******* Ranking de los 10 mejores usuarios!! ********");
		scores.bestTen();
		System.out.println("Usuario "+ u.getName() + " con score " + u.getScore() + " le faltan "+scores.difUserFirst(u) + " para llegar al mejor");
	}

}

/*
 * Program.java        3/11/2015
 *
 * Models the program.
 *
 * Copyright 2015 Alejandro Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
  
    /**
    * Mobile's bill
    * 
    * @param seconds spoken seconds 
    * @return euro cents to pay
    */
    public int mobileBill(long seconds) {
        //declaració de variables
        int minuts10c;
        int minuts3c;
        int bill;
        //càlcul
        if (seconds > 0 && seconds < 300) {
            minuts10c = ((int)seconds + 59) / 60;
            bill = 5 + minuts10c * 10;
        }
        else {
            minuts3c = (((int)seconds + 59) - 300) / 60;
            minuts10c = 5;
            bill = 5 + minuts10c * 10 + minuts3c * 3;
        }
       
        return bill;
            
            
    }
}

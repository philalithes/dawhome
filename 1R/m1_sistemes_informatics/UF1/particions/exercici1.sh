#!/bin/bash
# Filename:			exercici1.sh
# Author:			iaw47951368
# Date:				01/03/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./exercici1.sh arg 1
# Description:		mostrar el limit de credit d'un client.

#funcions
function get_credit_limit { #rep el num_clie i retorna el limit de credit-
cat $fitxer | grep "^$1:" | cut -d":" -f4
}

#variables
fitxer=clientes.txt

#control d'errors
if [ $# -ne 1 ]
then
	echo "error: sintaxi incorrecta"
	echo "us: ./exercici1.sh num_clie"
	echo "on num_clie és un enter que representa el client"
	exit 1
fi

#body
if [ -z "$(get_credit_limit $1)" ]
then
	echo "el client $1 no existeix a la nostra base de dades"
else
	get_credit_limit $1
fi


package org.iedt.datetimedemo1;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;

public class Main {

	public static void main(String[] args) {
//		LocalDate avui = LocalDate.now();
//		System.out.println(avui);
//		LocalDate ld1 = LocalDate.of(2000, Month.APRIL, 11);
//		System.out.println(ld1);
//		LocalDateTime ldt1 = avui.atTime(13, 34, 1);
//		System.out.println(ldt1);
//		
//		DayOfWeek day = avui.getDayOfWeek();
//		System.out.println(day);
//		System.out.println(ld1.isBefore(avui));
//		
//		LocalDate futur = avui.plusDays(60);
//		System.out.println(futur);
//		String s1 = "20161019";
//		String s2 = "2016-10-19";
//		LocalDate ld2 = LocalDate.parse(s2);
//		LocalDate ld3 = LocalDate.parse(s1, DateTimeFormatter.BASIC_ISO_DATE);
//		System.out.println(ld3);
//		
//		LocalDate ld4 = ld3.plus(123l, ChronoUnit.MILLENNIA);
//		System.out.println(ld4);
//		
//		LocalDateTime ldt = LocalDateTime.now();
//		System.out.println(ldt);
		
		LocalDateTime ld1 = LocalDateTime.of(2016, 9, 14, 12, 0);
		LocalDateTime ld2 = LocalDateTime.now();
		
		Duration d1 = Duration.between(ld1, ld2);
		System.out.println(d1.toDays());
		
		Instant i1 = Instant.now();
		System.out.println(i1);
	}

}

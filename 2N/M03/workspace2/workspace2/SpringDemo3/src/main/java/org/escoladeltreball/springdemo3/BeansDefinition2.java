package org.escoladeltreball.springdemo3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Profile("production")
@PropertySource("classpath:/systemproperties/appResource.properties")
public class BeansDefinition2 {

	@Autowired
	private Environment env;

	public BeansDefinition2() {
		// TODO Auto-generated constructor stub
	}

	@Bean
	public MobilAspect mobilAspect() {
		return new EngineAspect();
	}
	
	@Bean(name = "vermell")
	public Vehicle vehicle() {
		return new Vehicle("Vermell");
	}

	@Bean(name = "motor90")
	@Qualifier("classe A")
	public Motor motor90() {
		return new Motor(90, 40);
	}
	@Bean(name = "motor70")
	@Qualifier("classe B")
	public Motor motor70() {
		return new Motor(70);
	}
	@Bean(name = "motor40")
	@Qualifier("classe C")
	public Motor motor40() {
		return new Motor(70);
	}
}

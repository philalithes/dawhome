/*
 * ProgramTUI.java        1.0 Oct 30, 2014
 *
 * Dado un número del 1 al 10 nos devuelve una cadena de caracteres con su escritura en catalán
 *
 * Copyright 2014 Chema García-Arias García-Morato <mrdelayman@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Write a number on Catalan
     * 
     * @param number The number to write
     * @return The number written in catalan
     */
    public String numberNames0To10(int number) {
        // Declaración de variables
        String stringNumber;
        //Codificación del algoritmo
        if (number == 0){
            stringNumber = "zero";
        }else if (number == 1){
            stringNumber = "u";
        }else if (number == 2){
            stringNumber = "dos";
        }else if (number == 3){
            stringNumber = "tres";
        }else if (number == 4){
            stringNumber = "quatre";
        }else if (number == 5){
            stringNumber = "cinc";
        }else if (number == 6){
            stringNumber = "sis";
        }else if (number == 7){
            stringNumber = "set";
        }else if (number == 8){
            stringNumber = "vuit";
        }else if (number == 9){
            stringNumber = "nou";
        }else {
            stringNumber = "deu";
        }
        return stringNumber;
    }
    
    /**
     * Main program interactues with user
     * 
     * @param args Not used
     */
    public static void main(String[] args) {
        // Declaración de variables
        int number;
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        //Construimos un objeto de la clase program para llamar al método numberNames0To10
        ProgramTUI p = new ProgramTUI();
        System.out.printf("\nESTE PROGRAMA ESCRIBE EN CATALÁN UN NÚMERO DEL 0 AL 10 \n\n");  
        System.out.printf("\nIntroduzca el número que desea escribir: ");
        //Guardamos en la variable "number" el número que el usuario introduzca por teclado mediante scanner
        number = s.nextInt();
        //Llamamos al método y mostramos por pantalla el resultado que nos devuelve
        System.out.printf("\nEl número escrito en catalán es es: %s\n", p.numberNames0To10(number));
    }
}

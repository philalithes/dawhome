/*
 * ProgramTUI.java        2015/7/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Transport miles to meters
     * 
     * @param m = miles
     * 
     *
     * 
     * @return meters
     */
    public double miles2meters(double m){
        //variable declaration    
        double meters;
        //calculate return
        meters = m * 1832;
        //retorn
        return meters;
        
    }
}

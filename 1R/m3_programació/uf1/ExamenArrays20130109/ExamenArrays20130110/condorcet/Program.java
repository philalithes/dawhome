/*
 * Program.java        1.0 10/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Converts a 1D array with one vote to the corresponding 2D array with 0s or 1s.
     * 
     * @param vote a 1D array with the vote of one person
     * @return the 2D array corresponding to the vote array
     */
    public int[][] condorcetStep1(int[] vote) {
        //variables
        int[][] arrayVote = new int[2][vote.length];
        int[][] arrayVoteReturn = new int[vote.length][vote.length];
        int i = 0;
        int aux = 0;
        int j = 0;
        //calcul
        //construcció d'un array 2D amb el vot i els candidats.
        for (i = 0; i < vote.length; i++) {
            arrayVote[0][i] = i;
            arrayVote[1][i] = vote[i];
        }
        //ordenar per preferència de vot (fila 1). Mètode de la bombolla
        for (j = 1; j < arrayVote[0].length; j++) {
            for (i = 0; i < arrayVote[0].length-j; i++) {
                
                if (arrayVote[1][i] > arrayVote[1][i+1]) {
                    aux = arrayVote[1][i];
                    arrayVote[1][i] = arrayVote[1][i+1];
                    arrayVote[1][i+1] = aux;
                    aux = arrayVote[0][i];
                    arrayVote[0][i] = arrayVote[0][i+1];
                    arrayVote[0][i+1] = aux;
                }
            }
        }
        //construcció de l'array a retornar
        for (i = 0; i < arrayVote.length; i++) {
            for (j = 0; j < arrayVote.length; j++) {
                for (int k = 0; k < arrayVote.length; k++) {
                    if (arrayVote[0][j] < vote[k]) {
                        arrayVoteReturn[i][j] = 1;
                    } else {
                        arrayVoteReturn[i][j] = 0;
                    }
                }
            }
        }
        return arrayVoteReturn;
    }
        
    /**
     * Determines the winner.
     * 
     * @param result matrix with the sum of votes
     * @return the number of row of the winner
     */
    public int condorcetStep3(int[][] result) {
        // TODO
        return -1;
    }
}

#!/bin/bash
# Filename:			mostra_array.sh
# Author:			iaw47951368
# Date:				16/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./mostra_array.sh [args...]
# Description:		Funció que mostra els elements d'un array.

function show_elements_array {
	local arraytemp=(\${$1[@]})
	eval echo ${arraytemp[@]}
}

declare -a array
array=(1 2 34 4)
show_elements_array array

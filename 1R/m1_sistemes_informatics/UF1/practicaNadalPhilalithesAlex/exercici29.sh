#!/bin/bash
# Filename:			29.sh
# Author:			iaw47951368
# Date:				26/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./29.sh arg1 arg2
# Description:		Script que suma que donats dos arguments els sumi si el primer és menor que el segon i els resti en cas contrari.

if [ $# -ne 2 ]
then
	echo "escriu 2 arguments"
	exit 1
fi

if [ $1 -lt $2 ]
then
	echo "$(($1+$2))"
else
	echo "$(($1-$2))"
fi

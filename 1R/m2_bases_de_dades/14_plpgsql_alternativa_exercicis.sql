-- --------------------------------------------
--    plpgsql - estructura alternativa
-- --------------------------------------------

-- 1.- (sign) Determina si un nombre real és positiu, negatiu o zero. La funció retornarà les cadenes “positiu”, “negatiu” o “zero”, respectivament.

create or replace function sign(num int) returns varchar(7) as $$
  declare ret varchar(7);
  begin
    if num > 0 then 
      ret := 'positiu';
    elsif num < 0 then 
      ret := 'negatiu';
    else 
      ret := 'zero';
    end if;
    return ret;
  end;
$$ language plpgsql;

-- 2.- (triangle) A partir de les llargades de tres segments, esbrina si formen o no un triangle i si formen triangle de quin tipus. Per tal de que tres segments formin un triangle un segment qualsevol ha de ser més petit que la suma de la llargada dels altres dos. Si els tres segments formen un triangle, aquest pot ser equilàter (tres costats iguals), isòsceles (dos costats iguals) o escalè (tots els costats diferents). La funció retornarà "No és triangle", "Triangle equilàter", "Triangle isòsceles" o "Triangle escalè".

create or replace function triangle(c1 int, c2 int, c3 int) returns text as $$
	begin
		if c1 >= c2 + c3 or c2 >= c1 + c3 or c3 >= c1 + c2 then
			return 'no formen triangle';
		elsif c1 = c2 and c1 = c3 then
			return 'triangle equilàter';
		elsif c1 = c2 or c1 = c3 or c2 = c3 then
			return 'triangle isòsceles';
		else
			return 'triangle escalè';
		end if;
	end;
$$ language plpgsql;

-- 3.- Donada una cadena amb un NIF, determina si és un NIF correcte. S'ha de validar que la llargada de la cadena d'entrada sigui correcta (9 caràcters) i que la lletra sigui correcta. El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el residu de la divisió entera del DNI entre 23 . Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE i la transformació de codi és: 0 -> T, 1-> R, 2 -> W, etc. Exemple: Al DNI 37721039 li correspon el NIF 37721039G. Si la llargada és incorrecta retornarem un 1, si el problema és la lletra retornarem un 2 i si tot és correcte retornarem un 0.

create or replace function validNIF(nif text) returns int as $$
	declare 
	    residu int;
		cadena char(23) := 'TRWAGMYFPDXBNJZSQVHLCKE';
		lletra char(1);
	begin
		if char_length(nif) <> 9 then
			return 1;
		end if;
		residu := substring(nif from 1 for 8)::int % 23 + 1;
		lletra := substring(cadena from residu for 1);
		if substring(upper(nif) from 9 for 1) <> lletra then
			return 2;
		else 
			return 0;
		end if;
	end;
$$ language plpgsql;
-- 4.- Donada una comanda, esbrina si es va fer en un any de traspàs. Si era any de traspa retorna TRUE, sinó FALSE. Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de 4 però no de 100.

create or replace function anyTraspas(fecha date) returns boolean as $$
	declare
		anio int := extract('year' from fecha);
	begin
		if anio % 100 != 0 and anio % 4 = 0 or anio % 400 = 0 then
			return true;
		else
			return false;
		end if;
	end;
$$ language plpgsql;

create or replace function leapyearComanda(pedido pedidos.num_pedido%TYPE) returns boolean as $$
	declare
		fechaPedido date := (SELECT  fecha_pedido
			FROM pedidos
			WHERE num_pedido = pedido);
	begin
		return (select anyTraspas(fechaPedido));
	end;
$$ language plpgsql;
			
-- 5.- Crear una funció que insereixi una oficina a la taula oficinas. Per paràmetre arribaran totes les dades d'una oficina (oficina, ciudad, region, dir, objetivo, ventas). S'hauran de fer les següents comprovacions:
--  * El codi d'oficina no existeix a la taula oficinas
--  * La region es 'Este' o 'Oeste'
--  * El codi de director és valid
--  * L'objectiu és superior a 0.
--Si totes les dades són vàlides, l'oficina s'inserirà i retornarem TRUE. Altrament no s'inserirà i retornarem FALSE.
--
--IMPORTANT: feu totes les comprovacions en funcions separades per poder-les reutilitzar.

--comprova si existeix la oficina
create or replace function existeixOficina(oficina1 smallint) returns boolean as $$
	declare ofi smallint;
	begin
		select oficina into ofi from oficinas where oficina = oficina1;
		if ofi = oficina1 then
			return true;
		else 
			return false;
		end if;
	end;
$$ language plpgsql;

--comprova si existeix l'empleat
create or replace function existeixEmp(emp1 smallint) returns boolean as $$
	declare emp smallint;
	begin
		select num_empl into emp from repventas where num_empl = emp1;
		if emp = emp1 then
			return true;
		else
			return false;
		end if;
	end;
$$ language plpgsql;

-- funció principal
create or replace function afegirOficina(oficina1 smallint, ciudad1 varchar(15), region1 varchar(10), dir1 smallint, objetivo1 numeric(9,2), ventas1 numeric(9,2)) returns boolean as $$
	declare
	begin
		if existeixOficina(oficina1) then
			return false;
		elsif region1 <> 'Este' and region1 <> 'Oeste' then
			return false;
		elsif not existeixEmp(dir1) then
			return false;
		elsif objetivo1 < 0 then
			return false;
		else
			insert into oficinas 
			values(oficina1, ciudad1, region1, dir1, objetivo1, ventas1);
			return true;
		end if;
	end;
$$ language plpgsql;
		
		
-- 6.- Crear una funció que insereixi o modifiqui una oficina a la taula oficinas. Per paràmetre arribaran totes les dades d'una oficina (oficina, ciudad, region, dir, objetivo, ventas). S'hauran de fer les següents comprovacions:
-- * La region es 'Este' o 'Oeste'
--  * El codi de director és valid
--  * L'objectiu és superior a 0.
--Si totes les dades són vàlides i el codi d'oficina no existeix, l'oficina s'inserirà i retornarem un 1.
--Si totes les dades són vàlides i el codi d'oficina existeix, l'oficina amb aquest codi es modificarà i retornarem un 2. 
--Altrament no es farà res i retornarem un -1.

create or replace function insertar_editar_registro(ofi numeric, ciu text, reg text, dire numeric, obj numeric, vent numeric) returns numeric as $$
        declare
                retorno numeric;
                --Variables auxiliares de oficina
                s_ofi int; --Cantidad de registros de consulta
                --Variables auxiliares de region
                c_reg boolean;
                --Variables auxiliares de director
                s_dire int; --Cantidad de registros de consulta
                c_dire boolean; --Bool
                --Variables auxiliares de objetivo
                c_obj boolean;
                --Variable auxiliar de validez
                c_val boolean;
        begin
                --Condicion oficina
                s_ofi := (select count(*) from oficinas where oficina = ofi);
                --Condicion de region
                if reg = 'Este' or reg = 'Oeste' then
                        c_reg := true;
                else
                        c_reg := false;
                end if;
                --Condicion de director
                s_dire := (select count(*) from repventas where director = dire);
                if s_dire = 0 then --No existe director
                        c_dire := false;
                else --Si existe director
                        c_dire := true;
                end if;
                --Condicion objetivo
                if obj > 0 then
                        c_obj := true;
                else
                        c_obj := false;
                end if;                
                --Validez de las anteriores
                c_val := c_reg and c_dire and c_obj;
                if c_val = false then --Parametros no validos
                        retorno := -1;
                else --Parametros validos
                        if s_ofi != 0 then --Si que existe oficina; editamos
                                update oficinas set ciudad = ciu, region = reg, dir = dire, objetivo = obj, ventas = vent  where oficina = ofi;
                                retorno := 2;
                        else --La oficina no existe; insertamos
                                insert into oficinas values(ofi, ciu, reg, dire, obj, vent);
                                retorno := 1;
                        end if;
                end if;
                
                
                return retorno;
        end;
        $$ language 'plpgsql';

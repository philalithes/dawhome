#!/bin/bash
# Fitxer: ex31_kp_comments.sh
# Autor: jamoros
# Data: 29/03/2011
# Versio: 0.1
# Descripcio: script que rep un argument, si és un directori llista el seu contingut


# Si no rebo un únic argument mostrar un misstage d'error, ajuda i surto indicant l'error al sistema.

if [ $# -ne 1 ]
then
	echo "error: Has d'entrar un únic argument"
	echo "ús: $0 file1 "
	echo "    on file1 és un fitxer"
	exit 1
fi

# Altrament, si no existeix el fitxer mostro un missatge que indica que no existeix. Surto indicant l'error al sistema.
if [ ! -e $1 ]
then
	echo "$1 no existeix"
	exit 2
fi

# Altrament, si no és un directori mostro un missatge que indica que no ho és.
if [ ! -d $1 ]
then
	echo "$1 no és un directori"
# Altrament, fem un llistat de format llarg del directori
else
	ls -l $1
fi



# Nota1: (podria fins i tot indicar quin tipus de fitxer és (enllaç, dispositi per blocs...) amb una estructura 'case' (en llenguatges d'alt nivell com C++ o Java seria estructura 'switch')

#Nota2: podria fer més controls un altre control per si no hi ha permís de lectura al directori.


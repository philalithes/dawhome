/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of Material audiovisual.
 * 
 * @author iaw47951368
 */
public class Material audiovisual {
	/**
	 * Description of the property dataAdquisicio.
	 */
	public String dataAdquisicio = "";
	
	// Start of user code (user defined attributes for Material audiovisual)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public Material audiovisual() {
		// Start of user code constructor for Material audiovisual)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for Material audiovisual)
	
	// End of user code
	/**
	 * Returns dataAdquisicio.
	 * @return dataAdquisicio 
	 */
	public String getDataAdquisicio() {
		return this.dataAdquisicio;
	}
	
	/**
	 * Sets a value to attribute dataAdquisicio. 
	 * @param newDataAdquisicio 
	 */
	public void setDataAdquisicio(String newDataAdquisicio) {
	    this.dataAdquisicio = newDataAdquisicio;
	}



}

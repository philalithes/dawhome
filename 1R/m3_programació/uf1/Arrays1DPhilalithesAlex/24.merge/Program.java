
/*
 * Program.java        2015/12/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donades dues taules de tipus enter ordenades
     * ascendentment, crea i retorna una tercera taula que les fusioni de manera que aquesta
     * taula també estigui ordenada ascendentment. No usis cap mètode d'ordenació.
     */
    
    public int[] merge(int[] array1, int[] array2) {
        //variable declaration
        Program p = new Program();
        int[] arrayJoined;
        int aux;
        int i = 0;
        int k = 0;
        //calcul
        //unim els arrays
        arrayJoined = p.join(array1,array2);
        //ordenem l'array nou
        //calcul
        for (k = 1; k < arrayJoined.length; k++) {
            for (i = 0; i < arrayJoined.length-k; i++) {
                
                if (arrayJoined[i] > arrayJoined[i+1]) {
                    aux = arrayJoined[i];
                    arrayJoined[i] = arrayJoined[i+1];
                    arrayJoined[i+1] = aux;
                }
            }
        }
        return arrayJoined;
    }
    /*
     * Donades dues taules numèriques reals, construeixi una tercera
     * taula amb els elements de la primera taula seguits dels elements de la segona i retorna
     * questa tercera taula
     */
    
    public int[] join(int[] array1, int[] array2) {
        //variable declaration
        int n = 0;
        int i = 0;
        //esbrinar la longitud del tercer array:
        for (i = 0; i < array1.length; i++) {
            n++;
        }
        for (i = 0; i < array2.length; i++) {
            n++;
        }
        //declarar tercer array
        int[] array3 = new int[n];
        for (i = 0; i < array3.length; i++) {
            if (i < array1.length) {
                array3[i] = array1[i];
            } else {
                array3[i] = array2[i-array1.length];
            }
        }
        return array3;
            
    }
}
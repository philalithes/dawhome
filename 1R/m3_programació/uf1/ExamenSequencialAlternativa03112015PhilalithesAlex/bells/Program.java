/*
 * Program.java        3/11/2015
 *
 * Models the program.
 *
 * Copyright 2015 Alejandro Philalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Number of strokes of a clock
     * 
     * @param hour Hour to start counting 
     * @return Number of strokes
     */
    public int bells(int hour) {
          // Declaració de variables
        int quarts = 3 * hour;
        int campanadesHora;
        int bells;
        //càlcul
        if (hour == 0) {
            campanadesHora = 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 + 12 + 13 + 14 + 15 + 16 + 17 + 18 + 19
                + 20 + 21 + 22 + 23 + 24;
            bells = campanadesHora + quarts;
        }
        else {
            campanadesHora = 300 - ((int)Math.pow(hour,hour % 24));
            bells = campanadesHora + quarts;
        }
        return bells;
    }
}

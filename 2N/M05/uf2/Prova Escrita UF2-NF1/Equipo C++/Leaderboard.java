package org.escoladeltreball.transaccion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Leaderboard {
	
	private List<User> users = new ArrayList<>();

	public Leaderboard(List<User> users) {
		this.users = users;
	}
	
	public Leaderboard() {
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public boolean addUser(User user) {
		if (users.contains(user)) {
			int index = users.indexOf(user);
			if (user.getScore() > users.get(index).getScore()) {
				users.get(index).setScore(user.getScore());
				return true;
			} 
			return false;
		} 
		return users.add(user);		
	}
	
	public User userWin(){
		Collections.sort(users);
		return users.get(0);
		
	}
	
	public void bestTen(){
		Collections.sort(users);
		String s = "";
		for (int i = 0; i < 10; i++){
			s += "Name : "+ users.get(i).getName() + "\t Score : "+ users.get(i).getScore()+"\n";
		}
		System.out.println(s);
	}
	
	public long difSecondFirst(){
		Collections.sort(users);
		return users.get(0).getScore() - users.get(1).getScore();
	}
	 
	public long difUserFirst(User u){
		Collections.sort(users);
		return users.get(0).getScore() - u.getScore();
	}
	
	@Override
	public String toString() {
		Collections.sort(users);
		String s = "";
		for (User u : users) {
			s += "Name : "+ u.getName() + "\t Score : "+ u.getScore()+"\n";
		}
		return s;
	}
	
}

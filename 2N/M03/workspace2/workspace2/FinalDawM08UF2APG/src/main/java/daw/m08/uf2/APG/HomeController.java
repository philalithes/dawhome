package daw.m08.uf2.APG;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, HttpServletRequest request) {
		logger.info("***********************************************************");
		logger.info("**** Welcome home! The client locale is {}.", locale);
		logger.info("*** " + request.getRemoteAddr());
		logger.info("***********************************************************");
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		model.addAttribute(new Person());
		
		List<String> idiomes = new ArrayList<String>();
		idiomes.add("Català");
		idiomes.add("Castellà");
		idiomes.add("Anglès");
		idiomes.add("Francès");
		
		model.addAttribute("idiomes", idiomes);
		return "home";
	}
	
	@SuppressWarnings("deprecation")
	@PostMapping(value = "/resultat")
	public String saveStudent(@Valid Person person, BindingResult result, Locale locale, Model model) {
		logger.info(person.toString());
		
		if (result.hasErrors()) {
			Date date = new Date();
			DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
			
			String formattedDate = dateFormat.format(date);
			
			model.addAttribute("serverTime", formattedDate );
			model.addAttribute(person);
			
			
			List<String> idiomes = new ArrayList<String>();
			idiomes.add("Català");
			idiomes.add("Castellà");
			idiomes.add("Anglès");
			idiomes.add("Francès");
			
			model.addAttribute("idiomes", idiomes);
			
			return "home";
		}
		
		LocalDate dataNaixement = LocalDate.of(person.getNaixement().getYear(), person.getNaixement().getMonth(), person.getNaixement().getDay());
		LocalDate now = LocalDate.now();

		logger.info("***********************************************************");
		logger.info("****************************............................................................................................"
				+ "*******************************");
		logger.info("" + person.getNaixement().getYear());
		logger.info("" + person.getNaixement().getMonth());
		logger.info("" + person.getNaixement().getDay());
		logger.info("***********************************************************");
		logger.info("***********************************************************");
		
		int age = Period.between(dataNaixement, now).getYears();
		
		
		person.setEdat(age - 1900);
		
		model.addAttribute(person);
		return "resultat";
	}
	
}

var express 	= require('express'),
	bodyParser 	= require('body-parser'),
	mongoose 	= require('mongoose'),
	passport 	= require('passport'),
	cookieParser = require('cookie-parser'),
	session 	= require('express-session'),
	LocalStrategy = require('passport-local').Strategy,
	app 		= express();

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// opcional, el motor de renderitzat per defecte és jade
// on jade si es .ejs possariem ejs encomptes de jade
// app.engine('jade', require('jade').__express);

//app.use(app.router);
app.use(cookieParser());
//app.use(session);
app.use(session({
  name: 'session_videoclub',
  secret: 'wsrtfgttg5rtg5r4yyjujmt',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false, httpOnly: false }
}));

app.use(passport.initialize());
app.use(passport.session());

app.set('view engine', 'jade');

mongoose.connect('mongodb://localhost/videoclub', function(err, res) {
	if (err)
		console.log("error al conectarse a la bbdd");
	else 
		console.log("connectat");
});

require('./routes/controller')(app, passport, LocalStrategy);

app.listen(3001, function() {
	console.log("node server :3001");
});


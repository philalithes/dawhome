/*
 * Program.java        1.0 Oct 30, 2015
 *
 * Models the program.
 *
 * Copyright 2014 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
import java.util.Locale;
public class ProgramTUI
{
    
    /**
     * the taxpayer calculates rates
     * @param the taxpayer's income
     * @param marital status of the taxpayer 1 = married, 0 = single 
     * @return Returns to pay rates
     */
    public double TaxReturn(double income, int status) {
        //declaració de variables
        double income1 = 0;
        double income2 = 0;
        double income3 = 0;
        double taxes;
        //càlcul
        if (status == 0) {
            if (income < 20000) {
                income1 = income;
            }
            else if (income > 20000 && income < 50000) {
                income1 = 20000;
                income2 = income % 20000;
            }
            else {
                income1 = 20000;
                income2 = 50000;
                income3 = income % 50000;
            }
        }
        else {
            if (income < 30000) {
                income1 = income;
            }
            else if (income > 20000 && income < 50000) {
                income1 = 30000;
                income2 = income % 80000;
            }
            else {
                income1 = 30000;
                income2 = 80000;
                income3 = income % 80000;
            }
            
        }
        taxes = (income1 * 15 / 100) + (income2 * 28 / 100) + (income3 * 31 / 100); 
            return taxes;
        }
        
        public static void main(String[] args) {
            //variable declaration

            //interacció amb l'usuari
            System.out.println("posa el teu estat civil (escull un número)\n0-solter\n1-casat");
            Scanner sc = new Scanner(System.in);
            sc.useLocale(Locale.ENGLISH);
            int status = sc.nextInt();
            System.out.println("posa els teus ingressos");
            double income = sc.nextDouble();
            
            //imprenta de resultats
            
            ProgramTUI p = new ProgramTUI();
            System.out.printf("******************************************\n");
            System.out.printf("Ingressos\t\tTaxes\tEstat Civil\n%f\t",income);
            System.out.println(p.TaxReturn(income,status));
            System.out.printf("******************************************");
            
                               

    }
    }

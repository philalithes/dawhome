package org.escoladeltreball.simplespringmavenprojectdemo1;

import static java.lang.System.out;
public class Main {

	public static void main(String[] args) {

		FactoriaDeVehicles fdv1 = FactoriaDeVehicles.getInstance();
		FactoriaDeVehicles fdv2 = FactoriaDeVehicles();
		out.println(fdv1 == fdv2);
		
	}

}

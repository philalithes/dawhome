/*
 * ArraysUtilsDemo.java        1.0 01/02/2016
 *
 * Demo of Arrays Utils Library.
 */

package aul.src.aul;
import java.util.Scanner;
/**
 * Demo of European Date Utils Library.
 */
public class ArraysUtilsDemo {
    
    /**
     * Demo.
     * @param args Not used.
     */
    public static void main(String[] args) {
        //variables
        Scanner sc = new Scanner(System.in);
        int opcio = 0;
        
        //menú
        System.out.println("WELLCOME TO ARRAYS UTILS LIBRARY DEMO\nCopyright Joan Garcias <jgarciasc@hotmail.com> && Alex Philalithes <alexph_95@hotmail.com>");
        System.out.println("Escull un mètode per provar");
        System.out.println("1) append(double[] a, double value)");
        System.out.println("2) binarySearch(double[] a, double key)");
        System.out.println("3) binarySearch(double[] a, int fromIndex, int toIndex, double key)");
        System.out.println("4) copyOf(double[] original, int newLength)");
        System.out.println("5) copyOfRange(double[] original, int from, int to)");
        System.out.println("6) equals(double[] a, double[] a2)");
        System.out.println("7) fill(double[] a, double val)");
        System.out.println("8) fill(double[] a, int fromIndex, int toIndex, double val)");
        System.out.println("9) insert(double[] a, int index, double value)");
        System.out.println("10) union(double[] a, double[] b)");
        System.out.println("11) prepend(double[] a, double value)");
        System.out.println("12) remove(double[] a, int index)");
        System.out.println("13) sort(double[] a)");
        System.out.println("14) sort(double[] a, int fromIndex, int toIndex)");
        System.out.println("15) toString(double[] arr)");
        opcio = sc.nextInt();
        //array demo
        System.out.println("Construeix el teu array: (mètode create(longitud))");
        System.out.println("longitud:");
        int length = sc.nextInt();
        double[] demo = ArraysUtils.create(length);
        for (int i = 0; i < demo.length; i++) {
            System.out.println("valor demo[" + i + "]:");
            demo[i] = sc.nextDouble();
        }
        System.out.println("El teu array és:");
        System.out.println(ArraysUtils.toString(demo));
        
        //switch
        switch (opcio) {
            case 1: 
                System.out.println("append(double[] a, double value)");
                System.out.println("Descripció: afegeix un element al final de l'array");
                System.out.println("introdueix un valor 'value'");
                double value = sc.nextDouble();
                demo = ArraysUtils.append(demo,value);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 2: 
                System.out.println("binarySearch(double[] a, double key)");
                System.out.println("Descripció: retorna l'index del valor 'key', si no el troba retorna -1. L'array ha d'estar ordenat de manera ascendent.");
                System.out.println("introdueix un valor 'key'");
                double key = sc.nextDouble();
                int retorn = ArraysUtils.binarySearch(demo,key);
                System.out.println("retorn:");
                System.out.println(retorn);
                break;
                
            case 3:
                System.out.println("binarySearch(double[] a, int fromIndex, int toIndex, double key)");
                System.out.println("Descripció: retorna l'index del valor 'key' entre els index especificats, si no el troba retorna -1. L'array ha d'estar ordenat de manera ascendent.");
                System.out.println("introdueix un valor 'key'");
                key = sc.nextDouble();
                System.out.println("introdueix un valor 'fromIndex'");
                int fromIndex = sc.nextInt();
                System.out.println("introdueix un valor 'toIndex'");
                int toIndex = sc.nextInt();
                retorn = ArraysUtils.binarySearch(demo,fromIndex,toIndex,key);
                System.out.println("retorn:");
                System.out.println(retorn);
                break;
                
            case 4:
                System.out.println("copyOf(double[] original, int newLength)");
                System.out.println("Descripció: copia l'array amb un nou tamany, truncant o omplint amb 0s si cal");
                System.out.println("introdueix un valor 'newLength'");
                int newLength = sc.nextInt();
                demo = ArraysUtils.copyOf(demo,newLength);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 5:
                System.out.println("copyOfRange(double[] original, int from, int to)");
                System.out.println("Descripció: copia l'array amb un nou tamany, en el rang especificat");
                System.out.println("introdueix un valor 'from'");
                int from = sc.nextInt();
                System.out.println("introdueix un valor 'to'");
                int to = sc.nextInt();
                demo = ArraysUtils.copyOfRange(demo,from,to);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 6:
                System.out.println("equals(double[] a, double[] a2)");
                System.out.println("Descripció: retorna true si són iguals");
                System.out.println("introdueix un array per comparar amb l'anterior");
                //array demo2
                System.out.println("longitud:");
                length = sc.nextInt();
                double[] demo2 = ArraysUtils.create(length);
                for (int i = 0; i < demo2.length; i++) {
                    System.out.println("valor demo2[" + i + "]:");
                    demo2[i] = sc.nextDouble();
                }
                System.out.println("El teu array és:");
                System.out.println(ArraysUtils.toString(demo2));
                boolean equals = ArraysUtils.equals(demo,demo2);
                System.out.println("retorn:" + equals);
                break;
                
            case 7:
                System.out.println("fill(double[] a, double val)");
                System.out.println("Descripció: omple l'array amb el valor 'val'");
                System.out.println("introdueix un valor 'val'");
                int val = sc.nextInt();
                ArraysUtils.fill(demo,val);
                System.out.println("el teu array demo[] ara és:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 8:
                System.out.println("fill(double[] a, int fromIndex, int toIndex, double val)");
                System.out.println("Descripció: omple l'array amb el valor 'val' en el rang especificat");
                System.out.println("introdueix un valor 'fromIndex'");
                fromIndex = sc.nextInt();
                System.out.println("introdueix un valor 'toIndex'");
                toIndex = sc.nextInt();
                System.out.println("introdueix un valor 'val'");
                val = sc.nextInt();
                ArraysUtils.fill(demo,fromIndex,toIndex,val);
                System.out.println("el teu array demo[] ara és:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 9:
                System.out.println("insert(double[] a, int index, double value)");
                System.out.println("Descripció: retorna un array amb els mateixos elements que 'a' i amb el valor 'value' inserit a 'a[index]'");
                System.out.println("introdueix un valor 'index'");
                int index = sc.nextInt();
                System.out.println("introdueix un valor 'value'");
                value = sc.nextDouble();
                demo = ArraysUtils.insert(demo,index,value);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 10:
                System.out.println("union(double[] a, double[] b)");
                System.out.println("Descripció: retorna un array amb els mateixos elements que 'a' i després els elements de 'b'");
                System.out.println("introdueix un array 'b'");
                //array demo2
                System.out.println("longitud:");
                length = sc.nextInt();
                demo2 = ArraysUtils.create(length);
                for (int i = 0; i < demo2.length; i++) {
                    System.out.println("valor b[" + i + "]:");
                    demo2[i] = sc.nextDouble();
                }
                System.out.println("El teu array 'b' és:");
                System.out.println(ArraysUtils.toString(demo2));
                demo = ArraysUtils.union(demo,demo2);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 11:
                System.out.println("prepend(double[] a, double value)");
                System.out.println("Descripció: afegeix un valor al principi de l'array'");
                System.out.println("introdueix un valor 'value'");
                value = sc.nextDouble();
                demo = ArraysUtils.prepend(demo,value);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 12:
                System.out.println("remove(double[] a, int index)");
                System.out.println("Descripció: retorna un array amb els mateixos elements menys el valor de 'index'");
                System.out.println("introdueix un valor 'index'");
                index = sc.nextInt();
                demo = ArraysUtils.remove(demo,index);
                System.out.println("retorn:");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 13:
                System.out.println("sort(double[] a)");
                System.out.println("Descripció: ordena l'array pel mètode de la bombolla en ordre ascendent");
                ArraysUtils.sort(demo);
                System.out.println("El teu array demo[] ara és");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 14:
                System.out.println("sort(double[] a, int fromIndex, int toIndex)");
                System.out.println("Descripció: ordena l'array pel mètode de la bombolla en ordre ascendent en el rang especificat");
                System.out.println("introdueix un valor 'fromIndex'");
                fromIndex = sc.nextInt();
                System.out.println("introdueix un valor 'toIndex'");
                toIndex = sc.nextInt();
                ArraysUtils.sort(demo,fromIndex,toIndex);
                System.out.println("El teu array demo[] ara és");
                System.out.println(ArraysUtils.toString(demo));
                break;
                
            case 15:
                System.out.println("toString(double[] arr)");
                System.out.println("Descripció: converteix un array en un String");
                System.out.println(ArraysUtils.toString(demo));
                break;
        }
    }
}

/*
 * Program.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * determine a valid date
     * 
     * @param y = year 
     * @param m = month
     * @param d = day
     * 
     * @return true = is a valid date, false = isn't a valid date
     */
    public boolean isDate(int y, int m, int d){
        //variable declaration
        boolean is;
        //calculate return
        is = ((d > 0 && d < 32) && (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12)) || 
            ((d > 0 && d < 31) && (m == 4 || m == 6 || m == 9 || m == 11)) ||
            ((d > 0 && d < 30) && (m == 2 && y % 4 == 0 && y % 100 != 0 || y % 400 == 0)) ||
            ((d > 0 && d < 29) && m == 2);
        //return
        return is;
    }
    
}

/*
 * Program.java        2016/1/12
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    /*
     *  Donada una cadena, troba la primera vocal. Si no té
     * vocals, retornarem un espai en blanc.
     * 
     * 
     */
    
    public char firstVowel (String cadena) {
        //variable declaration
        char primeraVocal = ' ';
        int i = 0;
        boolean found = false;
        //calcul
        while (!found && i < cadena.length())  {
            if (cadena.charAt(i) == 'a' || cadena.charAt(i) == 'A' || cadena.charAt(i) == 'e' || cadena.charAt(i) == 'E' || cadena.charAt(i) == 'i' || cadena.charAt(i) == 'I' || cadena.charAt(i) == 'o' || cadena.charAt(i) == 'O' || cadena.charAt(i) == 'u' || cadena.charAt(i) == 'U') {
                primeraVocal = cadena.charAt(i);
                found = true;
            }
            i++;
        }
        //retorn
        return primeraVocal;
    
        
    }
}

#!/bin/sh

#DESCRIPCIÓ 		Script que et va demanant nombres fins que encertes el nombre que l'script ha determinat segons la PID del procés actual.
#declaració de variables
MAXIM=100               #nombre màxim que has d'endevinar (dificultat)    
INTENT=0                #variable de l'intent actual
NUM_INTENTS=0           #comptador d'intents
NOMBRE=$(( $$ % $MAXIM ))          #algorisme per determinar el nombre a endevinar

#bucle dels intents
while [ $INTENT -ne $NOMBRE ] #fins que l'usuari no endevini el nombre...
do
	echo -n "Intenta esbrinar el nombre" #pregunta
	read INTENT
	if [ "$INTENT" -lt $NOMBRE ] #l'usuari ha escrit un nombre més petit
	then
		echo "... més gran!"
	elif [ "$INTENT" -gt $NOMBRE ] #l'usuari ha escrit un nombre més gran
	then
		echo "... més petit!"
	fi
	NUM_INTENTS=$(($NUM_INTENTS+1)) #intents++
done
echo "Correcte!! $NOMBRE acertat en $NUM_INTENTS intents." #fi.
exit 0

package org.iedt.streamsdemo1;

public interface Productor {
	public abstract int produeix(String s);
}

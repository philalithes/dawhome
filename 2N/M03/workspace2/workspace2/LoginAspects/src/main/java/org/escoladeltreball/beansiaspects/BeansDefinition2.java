package org.escoladeltreball.beansiaspects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Profile("production")
@PropertySource("classpath:/systemproperties/appResource.properties")
public class BeansDefinition2 {

	@Autowired
	private Environment env;

	public BeansDefinition2() {
		// TODO Auto-generated constructor stub
	}

	@Bean
	public MobilAspect mobilAspect() {
		return new MobilAspect();
	}
	
	@Bean(name = "Samsung")
	public Mobil mobil() {
		return new Mobil("Samsung");
	}
	
	@Bean(name = "Battery100")
	@Qualifier("AAA")
	public Battery battery() {
		return new Battery(100);
	}
}

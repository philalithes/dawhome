<?php
	// Llefirm el fitxer gym2.json, visualitzem les dades d'objectes json, modifiquem una dada i guardem el fitxer
	
	$file='gym2.json';
	$dades = json_decode(file_get_contents($file),TRUE);
	var_dump($dades);
	
	// Visualitzem un valor
	echo "<br>";
	echo $dades['gimnastes'][0]['Nom'];
	echo "<br>";
	
	// Visualitzem totes les puntuacions de la Fase1 de la gimnasta 0
	foreach ($dades['gimnastes'][0]['Fase1'] as $index => $valor) 
	{
		echo $index . " : " . $valor."<br>";
	}
	echo "<br>";
	
	// Visualitzem totes les puntuacions de la Fase1 de totes les gimnastes
	for ($i=0; $i<count($dades['gimnastes']);$i++)
	{
		echo $dades['gimnastes'][$i]['Nom']."<br>";
		foreach ($dades['gimnastes'][$i]['Fase1'] as $index => $valor) 
		{
			echo $index . " : " . $valor."<br>";
		}
	}
	echo "<br>";
	
	// Visualitzem totes les puntuacions de totes les fases de totes les gimnastes
	for ($i=0; $i<count($dades['gimnastes']);$i++)
	{
		echo $dades['gimnastes'][$i]['Nom']."<br>";
		for ($x=1; $x<4;$x++)
		{
			foreach ($dades['gimnastes'][$i]['Fase'.$x] as $index => $valor) 
			{
				echo $index . " : " . $valor."<br>";
			}
		}
	}
	echo "<br>";
	
	// Modifiquem un valor
	$dades['gimnastes'][0]['Fase1']['Potro']=7000;

	file_put_contents($file, json_encode($dades,TRUE));

?>


#!/bin/bash
# Filename:			multiplicar.sh
# Author:			iaw47951368
# Date:				12/11/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./multiplicar.sh arg1 arg2
# Description:		multiplica els 2 arguments

expr $1 \* $2
if [ $? -en 0 ]
then echo "escriu 2 arguments per multiplicarlos"
fi

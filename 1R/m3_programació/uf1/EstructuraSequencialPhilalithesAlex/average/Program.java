/*
 * Program.java 2015/5/10
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
 /*
  * Fer la mitja de 3 variables
  * 
  * @param a
  * @param b
  * @param c
  * @return mitja
  */
    public double average(double a, double b, double c){
        //variable declaration
        double retorno;
        //calculate average
        retorno = (a + b + c) / 3;
        //return average
        return retorno;
    }
}

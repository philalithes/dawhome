var express = require("express"),
	bodyParser = require("body-parser"),
	mongoose = require("mongoose"),
	session = require("express-session"),
	cookieParser = require("cookie-parser"),
	passport = require("passport"),
	LocalStrategy = require("passport-local").Strategy,
	app = express();


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({ 
	secret: "pepe",
	resave: false,
	saveUninitialized: true,
	cookie: { secure: true }
}));

app.use(passport.initialize());
app.use(passport.session());

app.set("view engine", "jade");


mongoose.connect("mongodb://localhost/videoclub", function(err, res) {
	if (err) console.log("error al conectarse a la bd");
	else console.log("conectat a la bd");
});

require("./routes/controller")(app, passport, LocalStrategy);

app.listen(3000, function() {
	console.log("node server running at localhost:3000")
});

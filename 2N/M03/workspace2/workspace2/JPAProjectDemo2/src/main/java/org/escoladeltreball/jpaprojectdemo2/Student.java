package org.escoladeltreball.jpaprojectdemo2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Student
 *
 */
@NamedQueries(value = {
//	@NamedQuery(name = "AllStudents", query = "SELECT s FROM Student s"),
	@NamedQuery(name = "StudentGrade", query = "SELECT s.name, r.grade FROM Student s JOIN Record r"),
//	@NamedQuery(name = "StudentClass", query = "SELECT s.name, c.name FROM Student s join ClassRoom c on s.classroom_id = c.id"),
})
@Entity
@Table(name="student")

public class Student implements Serializable {

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private Integer age;
	//owner
	@JoinTable(name = "jointableStudentTeacher",
			joinColumns = { @JoinColumn(name = "student")},
			inverseJoinColumns = { @JoinColumn(name = "teacher")}
			)
	@ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST })
	private List<Teacher> teachers = new ArrayList<Teacher>();
	
	//owner
	@OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
	private Record record;
	private static final long serialVersionUID = 1L;
	//owner
	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.PERSIST })
//	@JoinColumn(name = "clase")
	private ClassRoom classRoom;
	
	
	
	
	public List<Teacher> getTeachers() {
		return teachers;
	}


	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}


	public Student(String name, Integer age, Record record, ClassRoom classRoom) {
		super();
		this.name = name;
		this.age = age;
		this.record = record;
		this.classRoom = classRoom;
	}
	public void addTeacher(Teacher t) {
		teachers.add(t);
	}
	
	public ClassRoom getClassRoom() {
		return classRoom;
	}


	public void setClassRoom(ClassRoom classRoom) {
		this.classRoom = classRoom;
	}


	public Record getRecord() {
		return record;
	}
	public void setRecord(Record record) {
		this.record = record;
	}
	
	



	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", age=" + age + ", teachers=" + teachers + ", record=" + record
				+ ", classRoom=" + classRoom + "]";
	}


	public Student() {
		super();
	}   
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}   
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}   
	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
   
}

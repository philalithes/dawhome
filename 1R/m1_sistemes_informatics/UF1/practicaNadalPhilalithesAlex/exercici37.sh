#!/bin/bash
# Filename:		exercici37.sh
# Author:		phila
# Date:			06/01/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici37.sh [arg1...]
# Description:		Script que suma tots els pesos dels fitxers regulars que es passen com a argument.

#control d'arguments
if [ $# -eq 0 ]
then
	echo "has de passar almenys un arxiu com a argument"
	echo "ajuda: ./exercici37.sh [arg1...]"
	exit 1
fi
#declarar acumulador
acumulador=0
#primer arxiu fora del bucle
if [ -f $1 ] #comprovar que $1 és un fitxer regular
then
	pes=$(ls -l $1 | tr -s ' ' | cut -d" " -f5)
	#sumar acumulador
	acumulador=$(($acumulador+$pes))
else
	echo "$1 no és un fitxer regular"
fi
#bucle per la resta d'arguments
while shift && [ $# -gt 0 ]
do
	if [ -f $1 ]
	then
		pes=$(ls -l $1 | tr -s ' ' | cut -d" " -f5)
		#sumar acumulador
		acumulador=$(($acumulador+$pes))
	else
	echo "$1 no és un fitxer regular"
	fi
done
#Resultat de la suma.
echo "$acumulador bytes"


/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada taula de nombres enters, determina si els seus
     * elements estan ordenats en ordre creixent o no.
     */
    
    public boolean areSorted(int[] array) {
        //variable declaration
        boolean areSorted = false;
        boolean notSorted = false;
        int i = 1;
        //calcul
        while (!notSorted && i  < array.length) {
            if (array[i-1] > array[i]) {
                notSorted = true;
            }
            i++;
        }
        if (i == array.length && notSorted == false) {
            areSorted = true;
        }
        return areSorted;
        
    }
}

var Pelicules = require("../model/peli.js");
/*var popups = require("popups");*/




module.exports = function(app) {
	principal = function(req, res) {
		res.render("principal");
	}

	llistarPelis = function(req, res) {

		return Pelicules.find(function(err, pelis) {
			if(!err) {
				//return res.send(pelis);
				res.render("mostrarPelis", {title:"Pelicules", pelis:pelis});
			} else 
				return console.log("error" + err);
		});
	}

	llistarPeli = function(req, res) {
		var id = parseInt(req.params.CodPeli);
		return Pelicules.findOne({"Codpeli" : id}, function(err, pelis) {
			if (!err) {
				res.render("mostrarPelis1", {title:"Resum peli", pelis:pelis});
			} else
				return console.log("err " + err);
		});
	}

	formInsert = function(req, res) {
		res.render("form", {accio:"Insereix", action:"/altaPeli"});
	}

	formUpdate = function(req, res) {
		res.render("form", {accio:"Modifica", action:"/modPeli"});
	}

	formDelete = function(req, res) {
		res.render("form", {accio:"Esborra", action:"/delPeli"});
	}

	formRent = function(req, res) {
		Pelicules.find({}, function(err, data) {
			if (err) throw err;
			res.render("formRent", {accio: "Llogar", action: "/mirarDispo", pelis: data});
		});
	}

	formReturn = function(req, res) {
		Pelicules.find({}, function(err, data) {
			if (err) throw err;
			res.render("formReturn", {accio: "Retornar", action: "/retornar", pelis: data});
		});
	}

	altaPeli = function(req, res) {
		var peliJSON = JSON.stringify(req.body);
		var peli = new Pelicules(JSON.parse(peliJSON));
		//peli nova
		Pelicules.find({"Codpeli" : peli.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				peli.save();
				res.render("mostrarPelis1", {title: "peli insertada", pelis: peli});
			} else {
				res.render("Error", {error: "la peli amb codi " + peli.Codpeli + " ja existeix"});

			}
		});
			
	}

	modPeli = function(req, res) {
		var peliJSON = JSON.stringify(req.body);
		var peli = new Pelicules(JSON.parse(peliJSON));

		Pelicules.find({"Codpeli" : peli.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + peli.Codpeli + " no existeix"})
			} else {
				peli.save();
			}
		});
	}

	delPeli = function(req, res) {
		Pelicules.find({"Codpeli" : req.body.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
			} else {
				Pelicules.remove({"Codpeli" : req.body.Codpeli}, function(err) {
					if (err) throw err;
					res.render("mostrarPelis1", {title: "peli borrada", isDelete: true});
				});
			}
		});
	}

	mirarDispo = function(req, res) {
		Pelicules.findOne({"Codpeli" : req.body.Codpeli}, function(err, data) {
			if (err) throw err;
			if (data.length == 0) {
				res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
			} else {
				res.render("mostrarDispo", {title: "Disponibilitat", pelis: data});
			}
		});
	}

	llogarPeli = function(req, res) {
		Pelicules.find({ "Codpeli": req.body.Codpeli }, function(err, peli) {
			if (err) {
                return console.log("ERROR " + err);
            }

            var arrDvdsPrestats = [];
            var dvdDisponible;

            // mirar quins dvds estan alquilats
            peli[0].prestecs.forEach(function (prestec) {
                if (!prestec.DataDev) { 
                    arrDvdsPrestats.push(prestec.Coddvd);
                }
            });

            // agafar un dvd no alquilat
            peli[0].dvds.forEach(function (dvd) {

                var indexOfDvd = arrDvdsPrestats.indexOf(dvd);

                if (indexOfDvd == -1) {
                    dvdDisponible = dvd;
                    return;
                }
            })

            if (!dvdDisponible) {
                return res.render('error', {error: "No hi ha dvds disponibles"})
            }
                    console.log(dvdDisponible)
                    console.log("asdf")

            // Rent the movie
            Pelicules.update({_id: req.body.id}, {
                    $inc: {DvdsPrestats: 1},
                    $push: {
                        prestecs: {
                            dataPres: new Date(),
                            soci: req.body.soci,
                            coddvd: dvdDisponible
                        }
                    }
                }, {},
                function (err, peli) {
                    if (err) return console.log(err);
                    return res.render('mostrarPelis1', {'title': "S'ha llogat una peli", pelis: peli});
                });
		});
	}

	retornarPeli = function(req, res) {
		Pelicules.findOneAndUpdate(
			{ 
				"Codpeli": req.body.Codpeli,
				"prestecs": {
			  		"$elemMatch" : {
			  			"dataDev": { $exists : false },
			  			"soci": req.body.soci 
			  		} 
		  		} 
		  	},
			{
				"$inc": {"dvdsPrestats": -1},
				"$set": {
					"prestecs.$.dataDev": new Date()
				}
			},
			function(err, data) {
				if (err) throw err;
				if (data.length == 0) {
					res.render("Error", {error: "la peli amb codi " + req.body.Codpeli + " no existeix"})
				} else {
					res.render("mostrarPelis1", {title: "S'ha retornat una peli", pelis: data});
				}
			}
		);
	}

	//********** EXERCICI 2 EXAMEN *************///

	top3 = function(req, res) {
		return Pelicules.aggregate(
			[
				{$unwind: "$prestecs"},
				{$group: 
					{
						"_id": {
							"_id":"$_id",
							"Titol": "$Titol",
							"Sinopsi": "$Sinopsi",
							"CodPeli": "$CodPeli",
						},
						prestecs: {$push: "$prestecs"},
						size: {$sum:1}
					}
				},
				{$sort: {size:1}},
				{$limit: 3},
				{$project: {
					Titol: 1,
					Sinopsi: 1,
					CodPeli: 1,
					size: 1, }
				}
			], 
			function(err, pelis) {
				console.log(pelis)
				if (err) throw err;

				res.render("mostrarTop", {title:"Top 3 Pelicules més llogades", pelis: pelis });
			});

	}

	//******************************************///

	app.get("/", principal);
	app.get("/list", llistarPelis);
	app.get("/list/:CodPeli", llistarPeli);
	app.get("/add", formInsert);
	app.get("/update", formUpdate);
	app.get("/delete", formDelete);
	app.get("/rent", formRent);
	app.get("/return", formReturn);
	app.get("/top3", top3);

	app.post("/altaPeli", altaPeli);
	app.post("/modPeli", modPeli);
	app.post("/delPeli", delPeli);
	app.post("/mirarDispo", mirarDispo);
	app.post("/llogarPeli", llogarPeli);
	app.post("/retornar", retornarPeli)

	//********** EXERCICI 1 EXAMEN *************///

	app.use(function(req, res) {
		console.log(req);
		res.render("pageNotFound", { url: req.headers.host + req.url });
	});

	//******************************************///


}


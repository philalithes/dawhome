package org.escoladeltreball.expressionsregulars;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int casos = Integer.parseInt(sc.nextLine());
		
		while (casos != 0) {
	
			
			
			for (int i = 0; i < casos; i++){
				String str = sc.nextLine();
				if (str.matches("[a-zA-Z]{3}.\\d{3}")) {
					System.out.println("VALIDA");
				} else {
					System.out.println("NO VALIDA");
				}
			}
			casos = Integer.parseInt(sc.nextLine());
			if (casos == 0) {
				System.exit(0);
			}
			System.out.println("----");
		}
	}

}

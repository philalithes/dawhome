package org.escoladeltreball.parrilladadesalida;

public class Posicio {

	private int pos;
	private String nom;

	public Posicio(int pos, String nom) {
		super();
		this.pos = pos;
		this.nom = nom;
	}

	@Override
	public String toString() {
		return pos + " " + nom;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

}

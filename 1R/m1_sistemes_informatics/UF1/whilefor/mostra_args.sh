#!/bin/bash
# Filename:			mostra_args.sh
# Author:			iaw47951368
# Date:				15/12/15
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./mostra_args.sh [args...]
# Description:		mostra els arguments que passi.

while [ $# -gt 0 ]
do
	echo $1
	shift
done

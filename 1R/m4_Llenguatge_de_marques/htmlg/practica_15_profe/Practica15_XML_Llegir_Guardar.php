<!--
Per navegar a l'arbre XML:
    $doc->parentNode
    $doc->childNodes
    $doc->firstChild
    $doc->lastChild
    $doc->nextSibling
    $doc->previousSibling
/* Properties */
public readonly string $nodeName ;
public string $nodeValue ;
public readonly int $nodeType ;
public readonly DOMNode $parentNode ;
public readonly DOMNodeList $childNodes ;
public readonly DOMNode $firstChild ;
public readonly DOMNode $lastChild ;
public readonly DOMNode $previousSibling ;
public readonly DOMNode $nextSibling ;
/* Methotds */
public string getAttribute ( string $name )
public DOMAttr getAttributeNode ( string $name )
public DOMAttr getAttributeNodeNS ( string $namespaceURI , string $localName )
public string getAttributeNS ( string $namespaceURI , string $localName )
public DOMNodeList getElementsByTagName ( string $name )
public DOMNodeList getElementsByTagNameNS ( string $namespaceURI , string $localName )
public bool hasAttribute ( string $name )
public bool hasAttributeNS ( string $namespaceURI , string $localName )
public bool removeAttribute ( string $name )
public bool removeAttributeNode ( DOMAttr $oldnode )
public bool removeAttributeNS ( string $namespaceURI , string $localName )
public DOMAttr setAttribute ( string $name , string $value )
public DOMAttr setAttributeNode ( DOMAttr $attr )
public DOMAttr setAttributeNodeNS ( DOMAttr $attr )
public void setAttributeNS ( string $namespaceURI , string $qualifiedName , string $value )
public void setIdAttribute ( string $name , bool $isId )

_?_?construct
createAttribute
createAttributeNS
createCDATASection
createComment
createDocumentFragment
createElement
createElementNS
createEntityReference
createProcessingInstruction
createTextNode
getElementById
getElementsByTagName
getElementsByTagNameNS
importNode
load
loadHTML
loadHTMLFile
loadXML
normalizeDocument
registerNodeClass
relaxNGValidate
relaxNGValidateSource
save
saveHTML
saveHTMLFile
saveXML
schemaValidate
schemaValidateSource
validate
xinclude
-->


<?php

$doc = new DOMDocument();
$doc->formatOutput = true;
$doc->preserveWhiteSpace = false;
$doc->load("gym.xml");

$funcio=$_POST['accio'];
$nom=utf8_encode($_POST['gnom']);
$nomm=utf8_encode($_POST['gnom']);
$club="PENDENT";
$barra=$_POST['fbarra'];
$potro=$_POST['fpotro'];
$asim=$_POST['fasim'];
$terra=$_POST['fterra'];
if ($barra=="")
{
	$barra=0;
}
if ($potro=="")
{
	$potro=0;
}
if ($terra=="")
{
	$terra=0;
}
if ($asim=="")
{
	$asim=0;
}


$noms=$doc->getElementsByTagName("nom");

if ($funcio=="MOD")
{
	$i=0;

	for( $i=0; $i<$noms->length; $i++)
	{

	   if ( $noms->item($i)->nodeValue == $nom )
	   {
			/* Guardar dades */
			$npare=$noms->item($i)->parentNode;
			$n=$npare->getElementsByTagName("nom");
			$n->item(0)->nodeValue=$nomm;
			$n=$npare->getElementsByTagName("barra");
			$n->item(0)->nodeValue=$barra;
			$n=$npare->getElementsByTagName("terra");
			$n->item(0)->nodeValue=$terra;
			$n=$npare->getElementsByTagName("asimetriques");
			$n->item(0)->nodeValue=$asim;
			$n=$npare->getElementsByTagName("potro");
			$n->item(0)->nodeValue=$potro;
			
	   }
	}  
}
else
{
	/* Buscar la gimnasta que arriba a nom
	$x=$doc->getElementsByTagName("nom");
	foreach($x->childNodes as $child) {
			echo $child->nodeValue;
	}		
	/*for ($i=0;$i<($x->length);$i++)
	{
		if ($x[$i]->childNodes(0)->nodeValue==$nom)
		{
			ECHO "NUM:".$i;
		}
	}

	 <nena>
		<nom>Laura Fornells Vives</nom>
		<club>SALT G.C.</club>
		<fase1>
		  <barra>2</barra>
		  <asimetriques>222</asimetriques>
		  <terra>2222</terra>
		  <potro>22</potro>
		</fase1>
	  </nena>
	*/


	$arrel=$doc->firstChild;

	$Un = $doc->createElement('nena');
	$Un = $arrel->appendChild($Un);


	$Dos = $doc->createElement('nom');
	$text = $doc->createTextNode($nomm);
	$Dos = $Un->appendChild($Dos);
	$Dos= $Dos->appendChild($text);


	$Dos = $doc->createElement('club');
	$text = $doc->createTextNode($club);
	$Dos = $Un->appendChild($Dos);
	$Dos= $Dos->appendChild($text);


	$Dos = $doc->createElement('fase1');
	$Dos = $Un->appendChild($Dos);
	
	$Tres = $doc->createElement('barra');
	$text = $doc->createTextNode($barra);
	$Tres = $Dos->appendChild($Tres);
	$Tres= $Tres->appendChild($text);
	
	$Tres = $doc->createElement('asimetriques');
	$text = $doc->createTextNode($asim);
	$Tres = $Dos->appendChild($Tres);
	$Tres= $Tres->appendChild($text);
	
	$Tres = $doc->createElement('terra');
	$text = $doc->createTextNode($terra);
	$Tres = $Dos->appendChild($Tres);
	$Tres= $Tres->appendChild($text);
	
	$Tres = $doc->createElement('potro');
	$text = $doc->createTextNode($potro);
	$Tres = $Dos->appendChild($Tres);
	$Tres= $Tres->appendChild($text);
}




$doc->save("gym.xml");
//header("Location: Practica15_XML_Llegir_Guardar_js.html"); 
?>


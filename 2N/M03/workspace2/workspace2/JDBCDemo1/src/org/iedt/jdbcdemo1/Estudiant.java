package org.iedt.jdbcdemo1;

public class Estudiant {
	private long id;
	private String name;
	private double nota;
	public Estudiant(long id, String name, double nota) {
		super();
		this.id = id;
		this.name = name;
		this.nota = nota;
	}
	@Override
	public String toString() {
		return "Estudiant [id=" + id + ", name=" + name + ", nota=" + nota + "]";
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getNota() {
		return nota;
	}
	public void setNota(double nota) {
		this.nota = nota;
	}
	
	
}

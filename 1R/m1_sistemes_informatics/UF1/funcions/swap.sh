#!/bin/bash
# Filename:			swap.sh
# Author:			iaw47951368
# Date:				16/02/16
# Version:			0.2
#					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./swap.sh [args...]
# Description:		funció que intercanvia variabes.
function swap1 {
	eval local aux=\$$1
	
	eval $1=\$$2
	eval $2=$aux
}
function swap2 {
	local aux=${!1}
	
	eval $1=${!2}
	eval $2=$aux
}
foo=2

a=1
b=2
echo "els valors de a i b són $a i $b"
swap$foo a b
echo "ara els valors de a i b són $a i $b"

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Donat un nombre enter positiu, escriu els nombres de 
     * Fibonacci (Lleonard de Pisa) inferiors o iguals a ell.
     * Els nombres de Fibonacci es defineixen de la manera següent:
     * El primer és 0, el segon és 1, el següent és la suma dels dos 
     * anteriors i així successivament.
     */
    
    public String fibonacci(int num){
        //variable declaration
        String fibonacci = "";
        int n1 = 0, n2 = 1;
        int n3 = 1;
        //calcul
        fibonacci = fibonacci + n1;
        while (n3 <= num){
            fibonacci = fibonacci + " " + n2;
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;       
        }   
        return fibonacci;
    }
}


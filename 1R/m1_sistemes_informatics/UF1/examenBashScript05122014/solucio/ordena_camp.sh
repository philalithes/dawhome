#!/bin/bash 
# Fitxer: ordena_camp.sh
# Autor: ordinari
# Data: 02/01/2015
# Versio: 0.1
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio:	Script que mostrarà les n primeres línies d'un fitxer (tabulat) ordenat (ascendent o descendent) per un camp (1, 2 o 3)
#		L'script demanarà a l'usuari de manera interactiva per aquest número n i pel camp 1, 2 o 3
#		Anàlogament a l'anterior exercici l'script es podrà executar amb l'argument opcional per la línia d'ordres '--desc'
#		També fem el mateix control d'errors				

unsorted_file=countries.txt # emmagatzemem el nom del fitxer a ordenar


# Fem un control dels arguments d'entrada
if [ $# -gt 1 ]     # Si el nombre d'arguments és superior a 1 ---> missatge d'error, ajuda i informem al sistema sortint
then
        echo "error: invalid arguments number" 
        echo "Ús: ./ordena.sh  [--desc]"
        exit 1
elif [ $# -eq 0 ]
then
        order_ascend="" # Hem escollit ordre ascendent (és l'opció per defecte)
elif [ "$1" = "--desc" ]  # si arribem a aquesta línia per força hi ha un únic argument
then
        order_ascend="-r" # Hem escollit ordre descendent
else
        echo "error: invalid option $1"    # Si l'únic argument que li passem és diferent de la cadena '--desc' --->  1 missatge d'error, ajuda i informem al sistema sortint
        echo "Ús: ./ordena.sh  [--desc]"
        exit 2
fi

# Mostrem les opcions a l'usuari i capturem la seva entrada

echo "Escull un dels següents números per ordenar el fitxer per la columna respectiva"
echo "1 --> Ordenarem pel nom del pais"
echo "2 --> Ordenarem pel percentatge de dones que tenen treballs VIP"
echo "3 --> Ordenarem pel percentatge de gent a la qual l'importa la religió"
read column_number

# Si l'usuari no ha entrat res o si ha entrat un número diferent de 1, 2, o 3 mostrem missatge d'error i informem al sistema sortint
if [ -z $column_number ] || [ $column_number -lt 1 ] || [ $column_number -gt 3 ]
then
	echo "error: you have to write 1, 2 or 3"
	exit 3
fi

max_countries=$(cat $unsorted_file | wc -l) # calculem el nombre de paisos (o línies del fitxer)

# Demanem a l'usuari aquesta vegada per les línies que vol que mostrem
echo "Escull els n primers paisos ordenats que vols que es mostrin: numero de 1 a $max_countries"

read countries_number  # capturem l'entrada de l'usuari

# Si l'usuari no ha entrat res o si ha entrat un número diferent fora de rang, mostrem missatge d'error i informem al sistema sortint
if [ -z $countries_number ] || [ $countries_number -lt 1 ] || [ $countries_number -gt $max_countries ]
then
	echo "error: you have to write a number in the range [1-$max_countries]"
	exit 4
fi



# Si l'usuari ha escollit una columna numèrica haurem d'afegir un '-n' a la comanda sort
if [ $column_number -eq 1 ]
then
	numeric_sort="" # si no posem aquesta opció també funcionarà
else
	numeric_sort="-n"
fi

sort $numeric_sort $order_ascend -t $'\t' -k $column_number,$column_number  $unsorted_file | head -$countries_number






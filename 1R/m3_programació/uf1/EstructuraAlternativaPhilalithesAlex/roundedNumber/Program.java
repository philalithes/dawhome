/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * A partir d'un nombre decimal positiu i el nombre de
     * xifres decimals a arrodonir, calcula el nombre arrodonit correctament.
     * 
     * @param n = nombre real
     * @param d = nombre de decimals
     * 
     *
     * 
     * @retorn n = nombre arrodonit
     */
    public double round(double n, int d){

        n = n * Math.pow(10,d+1);
        if (n % 10 > 5) {
            n = (int)n / 10;
            n = n + 1;   
        }
        else {
            n = (int)n / 10;
        }
        n = n / Math.pow(10,d);
        return n;
    }
    
    
}

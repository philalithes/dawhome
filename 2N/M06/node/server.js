var http = require("http");
var fs = require("fs");



var server=http.createServer(function(req,res){
    switch (req.url){
        case     '/' :
            res.writeHead(200,{'Content-Type':'text/plain'});
            res.end("hola");
            break;
        case    '/crearFich':
            res.writeHead(200,{'Content-Type':'text/html'});
            var string = "";
            while (string.length < 100) {
                switch (getRandomInt(1, 3)) {
                    case 1:
                        string += "A";
                        break;
                    case 2:
                        string += "B";
                        break;
                    case 3:
                        string += "C";
                        break;
                }
            }

            fs.writeFile(__dirname +'/fichero.txt', string, function(err){
                if (err) {
                    console.log(err);
                    res.end(err);
                }
            });
            console.log(string)
            res.write("Fichero creado");
            res.end();
            break;
        case    '/enviarClienteFich':
            res.writeHead(200,{'Content-Type':'text/html'});
            fs.readFile(__dirname + '/fichero.txt', "utf-8", function (err, data) {
                if (err) {
                    console.log(err);
                    res.end(err);
                }
                res.end(data);
            });
            break;
        default:
            res.writeHead(200,{'Content-Type':'text/html'});

            res.end("page not found");

            break;

    }
});

server.listen(8080);

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
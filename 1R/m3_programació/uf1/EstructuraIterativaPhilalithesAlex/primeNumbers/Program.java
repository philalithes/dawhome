/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */



public class Program {
    
    /*
     * Donat un nombre enter, determina si és un nombre
     * primer o no. Un nombre enter és primer si es més gran que 1 i només és divisible per 1 i
     * per ell mateix.
     */
    
    public boolean isPrime(int n) {
        //variable declaration
        boolean isPrime = false;
        int contador = 0;
        //càlcul
        for (int i = n; i > 1; i--) {
            if (n % i == 0) {
                contador++;
            }
        }
        if (contador == 1) {
            isPrime = true;
        }
        return isPrime;
    }
    
    /*
     * Donats dos nombres enters positius n1 i
     * n2 amb n1 < n2 i 1 < n1, 
     * escriu els nombres primers que hi ha dins l’interval [n1 , n2] en ordre
     * creixent.
     */
    
    public void writePrimeNumbers(int n1, int n2) {
        //variable declaration
        int i = n1;
        Program p = new Program();
        //càlcul
        for (i = n1; i <= n2; i++) {
            if (p.isPrime(i) == true) {
                System.out.println(i);
            }
        }
             
             
    }
}

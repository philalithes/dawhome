/*******************************************************************************
 * 2016, All rights reserved.
 *******************************************************************************/


// Start of user code (user defined imports)

// End of user code

/**
 * Description of DVD.
 * 
 * @author iaw47951368
 */
public class DVD extends Material audiovisual {
	/**
	 * Description of the property autor.
	 */
	public String autor = "";
	
	/**
	 * Description of the property estil.
	 */
	public String estil = "";
	
	// Start of user code (user defined attributes for DVD)
	
	// End of user code
	
	/**
	 * The constructor.
	 */
	public DVD() {
		// Start of user code constructor for DVD)
		super();
		// End of user code
	}
	
	// Start of user code (user defined methods for DVD)
	
	// End of user code
	/**
	 * Returns autor.
	 * @return autor 
	 */
	public String getAutor() {
		return this.autor;
	}
	
	/**
	 * Sets a value to attribute autor. 
	 * @param newAutor 
	 */
	public void setAutor(String newAutor) {
	    this.autor = newAutor;
	}

	/**
	 * Returns estil.
	 * @return estil 
	 */
	public String getEstil() {
		return this.estil;
	}
	
	/**
	 * Sets a value to attribute estil. 
	 * @param newEstil 
	 */
	public void setEstil(String newEstil) {
	    this.estil = newEstil;
	}



}

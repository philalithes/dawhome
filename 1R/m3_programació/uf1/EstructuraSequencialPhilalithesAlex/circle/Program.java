/*
 * ProgramTUI.java        2015/13/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculate area of a circle
     * 
     * @param r radius
     * 
     *
     * @return a = area of the circle
     * @return p = perimeter
     */
    public double area(double r){
        //variable declaration    
        double a;
        //calculate return
        a = Math.PI * Math.pow(r,2);
        //retorn
        return a;
    }
    public double perimeter(double r){
        //variable declaration    
        double p;
        //calculate return
        p = 2 * Math.PI * r;
        //retorn
        return p;
    }
}

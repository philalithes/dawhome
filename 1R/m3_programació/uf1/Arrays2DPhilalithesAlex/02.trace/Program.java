/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula bidimensional quadrada numèrica real, calcula
     * la seva traça. La traça d'una matriu és la suma dels elements de la diagonal principal.
     * 
     * 
     */
    
    public double trace(double[][] array) {
        //variables
        int i, j = 0;
        double acumulador = 0;
        for (i = 0; i < array.length; i++) {
                acumulador += array[i][i];
        }
        return acumulador;
    }
   
}

/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * A partir del mes i l'any d'una data, esbrina el
     * nombre de dies que té el mes d'aquest any.
     * 
     * @param m = mes
     * @param a = any
     *
     * 
     * @retorn d = dies del mes, retorna 0 si no és un mes vàlid.
     */
    public int getNumberOfDays(int m, int a){
        //declaració de variables
        int d;
        //càlcul
        if (m == 2 && a % 4 == 0 && a % 100 != 0 || a % 400 == 0) {
            d = 29;
        }
        else if (m == 2) {
            d = 28;
        }
        else if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
            d = 31;
        }
        else if (m == 4 || m == 6 || m == 9 || m == 11){
            d = 30;
        }
        else {
            d = 0;
        }
        return d;
    }
    
    
}

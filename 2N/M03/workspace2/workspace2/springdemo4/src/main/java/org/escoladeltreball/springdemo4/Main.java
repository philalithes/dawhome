package org.escoladeltreball.springdemo4;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/*
 * Client
 */
public class Main {

	public static void main(String[] args) {
		//Carreguem el Application Context amb annotacions
		AnnotationConfigApplicationContext ctx = new
				AnnotationConfigApplicationContext();
		ctx.register(SpringConfig.class);
		ctx.refresh();
		
		Login login = ctx.getBean("login", Login.class);
		boolean b = login.connect("user", "password");
		System.out.println(b);
		System.out.println("ok");
		ctx.close();
	}

}

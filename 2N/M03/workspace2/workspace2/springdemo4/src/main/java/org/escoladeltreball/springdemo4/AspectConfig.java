package org.escoladeltreball.springdemo4;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;

@Aspect
public class AspectConfig {

//	@Autowired
//	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private EventService eventService;
	
	public AspectConfig() {
		// TODO Auto-generated constructor stub
	}
	
	@Pointcut("execution(* connect(String, String))")
	public void connectPointcut() {}
	
	@Before("connectPointcut()")
	public void beforeConnectAdvice(JoinPoint joinPoint) {
//		jdbcTemplate.update("insert into events (comment) values (?)", "fdsfds");
		eventService.save(new Event("comentari"));
	};
}

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de nombres acabada en zero i
     * escriu el més gran i el més petit de la seqüència.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        //interacció amb l'usuari
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una sequència de nombres acabada en 0 i et diré el nombre més gran i el més petit.");
        //càlculs
        
        
        System.out.println("Escriu un nombre (si el primer nombre és 0, no acabarà la sequència):");
        int n = s.nextInt();
        int min = n;
        int max = n;
        do {
            System.out.println("Escriu un nombre: ");
            n = s.nextInt();
            if (n != 0) {
                if (n < min) {
                    min = n;
                }
                if (n > max) {
                    max = n;
                }
            }
        } while (n != 0);
        System.out.printf("El nombre més petit és: %d i el més gran és: %d",min, max);
        
        
    }
}


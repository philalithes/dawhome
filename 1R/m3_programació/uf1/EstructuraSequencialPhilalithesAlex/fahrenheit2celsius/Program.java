/*
 * Program.java        2015/7/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Convert  fahrenheit to celsius
     * 
     * @param f fahrenheit
     * 
     * 
     * @param c celsius
     */
    public double fahrenheit2celsius(int f){
        //variable declaration    
        double c;
        //calculate return
        c =  (f - 32) * 5 / 9.0;
        //return
        return c;
    }
    
}

/*
 * Program.java        2016/1/27
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.*;

public class Program {
    /*
     *  Donada una cadena amb el número de un DNI, genera una
     * cadena amb el NIF. El NIF s'obté a partir del DNI afegint-li la lletra que s'obté calculant el
     * residu de la divisió entera del DNI entre 23 . Les lletres són: TRWAGMYFPDXBNJZSQVHLCKE
     * i la transformació de codi és: 0 ->T, 1->R, 2->W, etc. Exemple: Al DNI 37721039 li
     * correspon el NIF 37721039G
     * 
     * 
     */
    public String dni2nif(String cadena) {
        //vars
        String nif = "";
        char[] lletra= {'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        int modul = (int) Long.parseLong(cadena) % 23;
        //calc
        nif = cadena + lletra[modul];
        //ret
        return nif;
    }
    
}
package org.escoladeltreball.simplespringmavenprojectdemo1;

import java.io.Serializable;

/**
 * @author iaw47951368
 * POJO Plain Old Java Object
 * Bean
 *
 */
public class Vehicle implements Serializable {

	private String marca;
	/* Composició */
	private Motor motor;
	
	public Vehicle() {

	}

	/**
	 * @param marca
	 * @param cilindrada
	 */
	public Vehicle(String marca, Motor motor) {
		super();
		this.marca = marca;
		this.motor = motor;
	}


	@Override
	public String toString() {
		return "Vehicle [marca=" + marca + ", motor=" + motor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((marca == null) ? 0 : marca.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (marca == null) {
			if (other.marca != null)
				return false;
		} else if (!marca.equals(other.marca))
			return false;
		return true;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}


}

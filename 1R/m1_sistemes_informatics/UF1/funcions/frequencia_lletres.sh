#!/bin/bash
# Filename:		frequencia_lletres.sh
# Author:		jamoros
# Date:			04/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
# 			See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./frequencia_lletres.sh [arg1...]
# Description:		Script que rep per l'entrada estàndard un text
# 			i retorna la frequència de cada caràcter.


# Declarem una cadena que contingui l'alfabet anglès (sense espais en blanc)
cadena_alfabet=$(echo {a..z}|tr -d " ")

#Creem un arrai de la mateixa longitud que l'alfabet utilitzat
declare -a freq_lletres

#index de l'últim element de l'array de frequencies
ultim_index_freq=$((${#cadena_alfabet}-1))

# Posem tots els comptadors de frequències a zero
for i in `seq 0 $ultim_index_freq`
do
	freq_lletres[$i]=0
done

# El text el rebem per l'entrada estàndard, l'emmagatzemem a una variable
cadena_text=`cat -`


#Calculem la longitud del text
long_tex=${#cadena_text}

for i in `seq 0 $(($long_tex-1))`
do
	#Agafem el caracter de la posicio "i". Utilitzem sintaxi de substring (cadena:posicio_inicial:nombre_de_caràcters_a_retallar)
	car_text=${cadena_text:$i:1}
	# Recorrem l'arrai de l'alfabet (cadena_alfabet) i sumem 1 a la posicio del arrai de frequències ()
	# de la lletra per asignar un caràcter més a la seva posició
	j=0
	while [ "$car_text" !=  "${cadena_alfabet:$j:1}" ] && [ $j -le $ultim_index_freq ]	
	# while [ "$car_text" !=  "${cadena_alfabet:$j:1}" -a $j -le $ultim_index_freq ] # és millor l'anterior
	do
		j=$((j+1))
	done

	#Demanem per quina de les dues condicions ha sortit
	if [ $j -le $ultim_index_freq ]
	then
		freq_lletres[$j]=$((freq_lletres[$j]+1))
	fi
done

cadena_alfabet_array=($(echo $cadena_alfabet| grep -o '.'))
echo -e "${cadena_alfabet_array[@]}\n${freq_lletres[@]}" | column -t 


# EXERCICI
# Llegiu http://tldp.org/LDP/abs/html/dblparens.html (llegiu també els exemples que hi ha al final)
# i canvieu la sintaxi que es pugui en aquest script utilitzant "((...))"
# Més ajudes: man bash (seccio Arrays)                                            
# Sobre cadenes http://www.the-evangelist.info/articulos/tag/bash/             


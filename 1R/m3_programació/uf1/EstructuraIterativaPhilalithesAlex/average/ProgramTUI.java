/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class ProgramTUI {
    
    /*
     * Llegeix una seqüència de nombres no negatius acabada 
     * en zero i escrigui la mitjana aritmètica dels nombres entrats.
     * La mitjana aritmètica d'una seqüència numèrica es calcula
     * sumant els elements de la seqüència i dividint pel nombre d'elements.
     * 
     * exclou el 0 final.
     */
    
    public static void main(String[] args) {
        //variable declaration
        int n = 0;
        int nombres = 0;
        int suma = 0;
        //interacció amb l'usuari
        Scanner s = new Scanner(System.in);
        s.useLocale(Locale.ENGLISH);
        System.out.println("Escriu una sequència de nombres acabada en 0 i faré la mitjana aritmètica.");
        //càlculs
       
        do {
            System.out.println("Escriu un nombre: ");
            n = s.nextInt();
            if (n != 0) {
            nombres++;
            suma += n;
            }
        } while (n != 0);
        double mitjana = (double)suma / nombres;
        System.out.printf("La mitjana dels nombres introduïts excepte el 0 és: %f.",mitjana);
        
        
    }
}


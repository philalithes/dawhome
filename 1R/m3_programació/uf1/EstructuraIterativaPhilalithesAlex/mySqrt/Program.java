/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    
    /*
     * Donat un nombre enter positiu, calcula la part entera de la 
     * seva arrel quadrada. No usar la funció Math.sqrt().
     * 
     * return int arrel.
     * 
     * 
     */
    
    public int mySqrt(int n) {
        //variable declaration
        int i = n;
        int test, sqrt = 0;
        boolean found = false;
            //calcul
            while(i >= 1 && !found) {
            test = i * i;
            if(test <= n){
                sqrt = i;
                found = true;
            }
            i--;
        }
        return sqrt;
    }
}

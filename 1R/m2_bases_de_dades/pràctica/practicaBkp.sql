--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: addcamisetas(smallint, smallint, character varying); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION addcamisetas(idcami smallint, addunidades smallint, talla character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
update camiseta_talla
set unidades = unidades + addUnidades
where id_camiseta = idCami;
end;
$$;


ALTER FUNCTION public.addcamisetas(idcami smallint, addunidades smallint, talla character varying) OWNER TO alex;

--
-- Name: addcds(smallint, smallint); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION addcds(idcd smallint, addunidades smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
update cds
set unidades = unidades + addUnidades
where id_cd = idCD;
end;
$$;


ALTER FUNCTION public.addcds(idcd smallint, addunidades smallint) OWNER TO alex;

--
-- Name: addpuntos(); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION addpuntos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare
percent smallint := 10;
begin
update clientes
set puntos = puntos + new.importe * percent / 100
where id_cliente = new.cliente;
return new;
end;
$$;


ALTER FUNCTION public.addpuntos() OWNER TO alex;

--
-- Name: block(); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION block() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
raise exception 'Operation not permitted';
end;
$$;


ALTER FUNCTION public.block() OWNER TO alex;

--
-- Name: importecompra(smallint, smallint); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION importecompra(id smallint, cant smallint) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
declare
import double precision := 0;
begin
import := (select precio
from proveedores
where id = producto) * cant;
return import;
end;
$$;


ALTER FUNCTION public.importecompra(id smallint, cant smallint) OWNER TO alex;

--
-- Name: importepedido(smallint, smallint); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION importepedido(idproducto smallint, cant smallint) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
declare
import double precision := 0;
begin
import := (select precio
from productos
where id = idProducto) * cant;
return import;
end;
$$;


ALTER FUNCTION public.importepedido(idproducto smallint, cant smallint) OWNER TO alex;

--
-- Name: newcamiseta(smallint, character varying, character varying, double precision); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newcamiseta(idcami smallint, artist character varying, model character varying, price double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
insert into productos
values (idCami, price);
insert into producto_stock
values (idCami);
insert into camisetas
values (artist, model, idCami);
end;
$$;


ALTER FUNCTION public.newcamiseta(idcami smallint, artist character varying, model character varying, price double precision) OWNER TO alex;

--
-- Name: newcd(smallint, character varying, character varying, interval, smallint, double precision); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newcd(idcd smallint, artist character varying, title character varying, duracion interval, unidades smallint, price double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
insert into productos
values (idCD, price);
insert into producto_stock
values (idCD);
insert into cds
values (idCD, artist, title, duracion, unidades);
end;
$$;


ALTER FUNCTION public.newcd(idcd smallint, artist character varying, title character varying, duracion interval, unidades smallint, price double precision) OWNER TO alex;

--
-- Name: newclient(smallint, text, integer); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newclient(id smallint, dire text, telefono integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
puntsInici smallint:= 100;
begin
insert into clientes
values (id, dire, telefono, puntsInici);
end;
$$;


ALTER FUNCTION public.newclient(id smallint, dire text, telefono integer) OWNER TO alex;

--
-- Name: newcompra(smallint, smallint, smallint); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newcompra(empl smallint, prod smallint, cant smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
insert into compras
values (empl, prod, cant, localtimestamp, importecompra(prod, cant));
end;
$$;


ALTER FUNCTION public.newcompra(empl smallint, prod smallint, cant smallint) OWNER TO alex;

--
-- Name: newentrada(smallint, character varying, date, text, smallint, double precision); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newentrada(identrada smallint, artist character varying, fecha date, lugar text, unidades smallint, price double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
insert into productos
values (idEntrada, price);
insert into entradas
values (fecha, lugar, artist, unidades, idEntrada);
end;
$$;


ALTER FUNCTION public.newentrada(identrada smallint, artist character varying, fecha date, lugar text, unidades smallint, price double precision) OWNER TO alex;

--
-- Name: newpedido(smallint, smallint, smallint, smallint); Type: FUNCTION; Schema: public; Owner: alex
--

CREATE FUNCTION newpedido(clie smallint, empl smallint, product smallint, cant smallint) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
insert into pedidos (cantidad, fecha_pedido, cliente, empleado, importe, id_producto)
values (cant, localtimestamp, clie, empl, importePedido(product, cant), product);
end;
$$;


ALTER FUNCTION public.newpedido(clie smallint, empl smallint, product smallint, cant smallint) OWNER TO alex;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: camiseta_talla; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE camiseta_talla (
    id_camiseta smallint,
    talla character varying(6),
    unidades smallint
);


ALTER TABLE public.camiseta_talla OWNER TO postgres;

--
-- Name: camisetas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE camisetas (
    artista character varying(30),
    modelo character varying(20),
    id_camiseta smallint NOT NULL
);


ALTER TABLE public.camisetas OWNER TO postgres;

--
-- Name: cds; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cds (
    id_cd smallint NOT NULL,
    artista character varying(30),
    titulo character varying(30),
    duracion interval minute,
    unidades smallint
);


ALTER TABLE public.cds OWNER TO postgres;

--
-- Name: clientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE clientes (
    id_cliente smallint NOT NULL,
    direccion text,
    telefono integer,
    puntos smallint
);


ALTER TABLE public.clientes OWNER TO postgres;

--
-- Name: compras; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE compras (
    num_compra integer NOT NULL,
    empleado smallint NOT NULL,
    producto smallint,
    cantidad smallint,
    fecha date,
    importe double precision
);


ALTER TABLE public.compras OWNER TO postgres;

--
-- Name: compras_num_compra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE compras_num_compra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compras_num_compra_seq OWNER TO postgres;

--
-- Name: compras_num_compra_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE compras_num_compra_seq OWNED BY compras.num_compra;


--
-- Name: empleados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empleados (
    num_empl smallint NOT NULL,
    turno character varying(20),
    nombre character varying(20)
);


ALTER TABLE public.empleados OWNER TO postgres;

--
-- Name: entradas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE entradas (
    fecha date,
    lugar text,
    artista character varying(30),
    disponibles smallint,
    id_entrada smallint NOT NULL
);


ALTER TABLE public.entradas OWNER TO postgres;

--
-- Name: pedidos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pedidos (
    cantidad smallint NOT NULL,
    num_pedido integer NOT NULL,
    fecha_pedido date,
    cliente smallint NOT NULL,
    empleado smallint NOT NULL,
    importe double precision NOT NULL,
    id_producto smallint NOT NULL
);


ALTER TABLE public.pedidos OWNER TO postgres;

--
-- Name: pedidos_num_pedido_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pedidos_num_pedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pedidos_num_pedido_seq OWNER TO postgres;

--
-- Name: pedidos_num_pedido_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pedidos_num_pedido_seq OWNED BY pedidos.num_pedido;


--
-- Name: producto_stock; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE producto_stock (
    id smallint NOT NULL
);


ALTER TABLE public.producto_stock OWNER TO postgres;

--
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos (
    id smallint NOT NULL,
    precio double precision NOT NULL
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- Name: proveedores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proveedores (
    id_proveedor smallint NOT NULL,
    telefono integer,
    direccion text,
    producto smallint,
    precio double precision
);


ALTER TABLE public.proveedores OWNER TO postgres;

--
-- Name: proveedorescamisetas; Type: VIEW; Schema: public; Owner: alex
--

CREATE VIEW proveedorescamisetas AS
 SELECT proveedores.id_proveedor,
    proveedores.telefono,
    proveedores.direccion,
    proveedores.producto,
    proveedores.precio
   FROM proveedores
  WHERE (proveedores.producto IN ( SELECT camisetas.id_camiseta
           FROM camisetas));


ALTER TABLE public.proveedorescamisetas OWNER TO alex;

--
-- Name: proveedorescds; Type: VIEW; Schema: public; Owner: alex
--

CREATE VIEW proveedorescds AS
 SELECT proveedores.id_proveedor,
    proveedores.telefono,
    proveedores.direccion,
    proveedores.producto,
    proveedores.precio
   FROM proveedores
  WHERE (proveedores.producto IN ( SELECT cds.id_cd
           FROM cds));


ALTER TABLE public.proveedorescds OWNER TO alex;

--
-- Name: tallas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tallas (
    talla character varying(6) NOT NULL
);


ALTER TABLE public.tallas OWNER TO postgres;

--
-- Name: num_compra; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compras ALTER COLUMN num_compra SET DEFAULT nextval('compras_num_compra_seq'::regclass);


--
-- Name: num_pedido; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedidos ALTER COLUMN num_pedido SET DEFAULT nextval('pedidos_num_pedido_seq'::regclass);


--
-- Data for Name: camiseta_talla; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY camiseta_talla (id_camiseta, talla, unidades) FROM stdin;
\.


--
-- Data for Name: camisetas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY camisetas (artista, modelo, id_camiseta) FROM stdin;
metallica	golf	1
slayer	angel of death	2
\.


--
-- Data for Name: cds; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cds (id_cd, artista, titulo, duracion, unidades) FROM stdin;
3	iron maiden	the trooper	00:05:00	20
4	iron maiden	fear of the dark	01:00:00	20
\.


--
-- Data for Name: clientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY clientes (id_cliente, direccion, telefono, puntos) FROM stdin;
1	valencia 4	934543956	103
\.


--
-- Data for Name: compras; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY compras (num_compra, empleado, producto, cantidad, fecha, importe) FROM stdin;
\.


--
-- Name: compras_num_compra_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('compras_num_compra_seq', 1, false);


--
-- Data for Name: empleados; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY empleados (num_empl, turno, nombre) FROM stdin;
1	9-18	alex
\.


--
-- Data for Name: entradas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY entradas (fecha, lugar, artista, disponibles, id_entrada) FROM stdin;
\.


--
-- Data for Name: pedidos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pedidos (cantidad, num_pedido, fecha_pedido, cliente, empleado, importe, id_producto) FROM stdin;
3	6	2016-05-30	1	1	27	4
\.


--
-- Name: pedidos_num_pedido_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pedidos_num_pedido_seq', 6, true);


--
-- Data for Name: producto_stock; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY producto_stock (id) FROM stdin;
3
4
1
2
\.


--
-- Data for Name: productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos (id, precio) FROM stdin;
3	9
4	9
1	15
2	10
\.


--
-- Data for Name: proveedores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY proveedores (id_proveedor, telefono, direccion, producto, precio) FROM stdin;
\.


--
-- Data for Name: tallas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tallas (talla) FROM stdin;
\.


--
-- Name: camisetapk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY camisetas
    ADD CONSTRAINT camisetapk PRIMARY KEY (id_camiseta);


--
-- Name: cdpk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cds
    ADD CONSTRAINT cdpk PRIMARY KEY (id_cd);


--
-- Name: clientepk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY clientes
    ADD CONSTRAINT clientepk PRIMARY KEY (id_cliente);


--
-- Name: comprapk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY compras
    ADD CONSTRAINT comprapk PRIMARY KEY (num_compra);


--
-- Name: conciertopk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY entradas
    ADD CONSTRAINT conciertopk PRIMARY KEY (id_entrada);


--
-- Name: empleadospk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empleados
    ADD CONSTRAINT empleadospk PRIMARY KEY (num_empl);


--
-- Name: pedidospk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pedidos
    ADD CONSTRAINT pedidospk PRIMARY KEY (num_pedido);


--
-- Name: prodstockpk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY producto_stock
    ADD CONSTRAINT prodstockpk PRIMARY KEY (id);


--
-- Name: productospk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos
    ADD CONSTRAINT productospk PRIMARY KEY (id);


--
-- Name: proveedorpk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proveedores
    ADD CONSTRAINT proveedorpk PRIMARY KEY (id_proveedor);


--
-- Name: tallaspk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tallas
    ADD CONSTRAINT tallaspk PRIMARY KEY (talla);


--
-- Name: blockpunts; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER blockpunts BEFORE UPDATE OF puntos ON clientes FOR EACH STATEMENT EXECUTE PROCEDURE block();

ALTER TABLE clientes DISABLE TRIGGER blockpunts;


--
-- Name: puntoscliente; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER puntoscliente AFTER INSERT ON pedidos FOR EACH ROW EXECUTE PROCEDURE addpuntos();


--
-- Name: camifk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY camiseta_talla
    ADD CONSTRAINT camifk FOREIGN KEY (id_camiseta) REFERENCES camisetas(id_camiseta) MATCH FULL;


--
-- Name: clientesfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedidos
    ADD CONSTRAINT clientesfk FOREIGN KEY (cliente) REFERENCES clientes(id_cliente) MATCH FULL;


--
-- Name: emplfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedidos
    ADD CONSTRAINT emplfk FOREIGN KEY (empleado) REFERENCES empleados(num_empl) MATCH FULL;


--
-- Name: emplfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compras
    ADD CONSTRAINT emplfk FOREIGN KEY (empleado) REFERENCES empleados(num_empl) MATCH FULL;


--
-- Name: prodfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY producto_stock
    ADD CONSTRAINT prodfk FOREIGN KEY (id) REFERENCES productos(id) MATCH FULL;


--
-- Name: prodfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compras
    ADD CONSTRAINT prodfk FOREIGN KEY (producto) REFERENCES producto_stock(id) MATCH FULL;


--
-- Name: productofk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY entradas
    ADD CONSTRAINT productofk FOREIGN KEY (id_entrada) REFERENCES productos(id) MATCH FULL;


--
-- Name: productofk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedidos
    ADD CONSTRAINT productofk FOREIGN KEY (id_producto) REFERENCES productos(id) MATCH FULL;


--
-- Name: productofk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY proveedores
    ADD CONSTRAINT productofk FOREIGN KEY (producto) REFERENCES producto_stock(id) MATCH FULL;


--
-- Name: stockfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY camisetas
    ADD CONSTRAINT stockfk FOREIGN KEY (id_camiseta) REFERENCES producto_stock(id) MATCH FULL;


--
-- Name: stockfk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cds
    ADD CONSTRAINT stockfk FOREIGN KEY (id_cd) REFERENCES producto_stock(id) MATCH FULL;


--
-- Name: tallafk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY camiseta_talla
    ADD CONSTRAINT tallafk FOREIGN KEY (talla) REFERENCES tallas(talla) MATCH FULL;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--


/*
 * Program.java        2015/26/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * Determines whether a linear system has a solution.
     *
     * @param a the first parameter of the first equation.
     * @param b the second parameter of the first equation.
     * @param c the third parameter of the first equation.
     * @param d the first parameter of the second equation.
     * @param e the second parameter of the second equation.
     * @param f the third parameter of the second equation.
     * @return true if the system has a solution, false otherwise.
     */
    public boolean hasSolution(double a, double b, double c, double d, double e, double f) {
        //variable declaration
        double ds;
        //calculate
        ds = a * e - d * b;
        if(ds == 0) {
            return false;
        }       
        else {
            return true;
        }
    }
    
    /**
     * Calculates the solution of a linear system.
     *
     * @param a the first parameter of the first equation.
     * @param b the second parameter of the first equation.
     * @param c the third parameter of the first equation.
     * @param d the first parameter of the second equation.
     * @param e the second parameter of the second equation.
     * @param f the third parameter of the second equation.
     * @return "Sistema incompatible" if there is no solution,
     * "x = solx, y = soly", where solx is the value of x and soly the
     * value of y, if there is a solution.
     */
    public String calculateSolution(double a, double b, double c, double d, double e, double f) {
        //variable declaration
        double dx = c * e - b * f;
        double dy = a * f - c * d;
        double ds = a * e - d * b;
        double solx;
        double soly;
        String solution;
        //cramer
        solx = dx / ds;
        soly = dy / ds;
        if (ds == 0) {
            solution = "sistema incompatible";
        }
        else {
            solution = "x = " + solx + ", y = " + soly;
        }
        return solution;
    }
}

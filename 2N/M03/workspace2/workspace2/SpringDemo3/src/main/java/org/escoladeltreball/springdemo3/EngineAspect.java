package org.escoladeltreball.springdemo3;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class EngineAspect {

	@Pointcut("execution(* start())")
	public void pointcut1() {	}
	
//	@Before("pointcut1()")
//	public void beforePointcut1(JoinPoint joinPoint) {
//		System.out.println("checking oil levels");
//		System.out.println(joinPoint.getTarget());
//	}
	
//	@After
//	public void afterPointcut1() {}
//	
	@Around("pointcut1()")
	public void aroundPointcut1(ProceedingJoinPoint pjp) {
		System.out.println("around...");
		try {
			Motor m = (Motor) pjp.getTarget();
			if (m.getOil() >= 50) {
				pjp.proceed();
			} else {
				System.out.println("oil level low");
			}
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public EngineAspect() {
		// TODO Auto-generated constructor stub
	}

}

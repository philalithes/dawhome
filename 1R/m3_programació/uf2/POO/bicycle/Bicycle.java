/*
 * Program.java 2016/24/02
 * 
 * Modelizes the program.
 * 
 * Copyright 2015 Alex Philalithes <alexph_95@hotmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * 
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Bicycle {
    /* model */
    private String model;
    /* plats */
    private int frontSprocket;
    /* pinyons */
    private int rearSprocket;
    /* nombre de plats */
    private final int nFrontSprockets;
    /* nombre de pinyons */
    private final int nRearSprockets;
    /* velocitat */
    private double v;
    /* velocitat màxima */
    private final static int VMAX = 100;
    /* acceleració */
    private final static int DV = 5;
    
    //constructors
    
    /*
     * Sense arguments
     */
    public Bicycle() {
        this.model = "Mountain Bike";
        this.frontSprocket = 3;
        this.rearSprocket = 1;
        this.nFrontSprockets = 3;
        this.nRearSprockets = 5;
        this.v = 0;
    }
    
    public Bicycle(int nFrontSprockets, int nRearSprockets, double v) {
        this.model = "Mountain Bike";
        this.frontSprocket = 3;
        this.rearSprocket = 1;
        this.nFrontSprockets = nFrontSprockets;
        this.nRearSprockets = nRearSprockets;
        this.v = v;
    }
    
    public Bicycle(String model, int frontSprocket, int rearSprocket, int nFrontSprockets, int nRearSprockets, double v) {
        this.model = model;
        this.frontSprocket = frontSprocket;
        this.rearSprocket = rearSprocket;
        this.nFrontSprockets = nFrontSprockets;
        this.nRearSprockets = nRearSprockets;
        this.v = v;
    }
    
    //setter and getters
    public String getModel() {
        return model;
    }
    
    public void setModel(String model) {
        this.model = model.trim();
    }
    
    public int getRearSprocket() {
        return rearSprocket;
    }
    
    public void setRearSprocket(int rearSprocket) {
        this.rearSprocket = rearSprocket;
    }
    public int getFrontSprocket() {
        return frontSprocket;
    }
    
    public void setFrontSprocket(int frontSprocket) {
        this.frontSprocket = frontSprocket;
    }
    
    public double getV() {
        return v;
    }
    
    public void setV(double v) {
        this.v = v;
    }
    //accelerar
    public void accelerate() {
        double newV = this.v + Bicycle.DV;
        if (newV > Bicycle.VMAX) {
            newV = Bicycle.VMAX;
        }
        this.v = newV;
    }
    //decelerar
    public void deccelerate() {
        double newV = this.v - Bicycle.DV;
        if (newV > Bicycle.VMAX) {
            newV = Bicycle.VMAX;
        }
        this.v = newV;
    }
    //canviar de plat
    public boolean changeFrontSprocket(int n) {
        boolean isChanged = false;
        if (v != 0) {
            if (n > 0 && frontSprocket < this.nFrontSprockets) {
                this.frontSprocket++;
                isChanged = true;
            } else if (n < 0 && this.frontSprocket > 1) {
                this.frontSprocket--;
                isChanged = true;
            } else {
                isChanged = false;
            }
        } else {
            isChanged = false;
        }
        return isChanged;
    }
    //canviar pinyó
    public boolean changeRearSprocket(int n) {
        if (v != 0) {
            if (n > 0 && rearSprocket < nRearSprockets) {
                this.frontSprocket++;
                return true;
            } else if (n < 0 && rearSprocket > 1) {
                this.rearSprocket--;
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

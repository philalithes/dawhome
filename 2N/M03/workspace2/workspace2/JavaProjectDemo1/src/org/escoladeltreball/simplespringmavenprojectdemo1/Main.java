package org.escoladeltreball.simplespringmavenprojectdemo1;

import static java.lang.System.out;
public class Main {

	public static void main(String[] args) {

		FactoriaDeVehicles fdv1 = FactoriaDeVehicles.getInstance();
//		FactoriaDeVehicles fdv2 = FactoriaDeVehicles.getInstance();
//		out.println(fdv1 == fdv2);
//		System.out.println(fdv1);
//		System.out.println(fdv2);
		
		Vehicle v = fdv1.getVehicle(TipusConductor.PAREDEFAMILIA);
		System.out.println(v);
	}

}

/*
 * ArraysUtils.java        1.0 03/01/2016
 *
 * Library with utilities for working with arrays.
 *
 */

package aul.src.aul;

/**
 * Library with utilities for working with arrays.
 */
public class ArraysUtils {
    
    /**
     * Creates an array.
     * 
     * @param int the length of the new array
     * @return double[]
     */
    public static double[] create(int length) {
        double[] arr = new double[length];
        return arr;
    }
    /*
     * Anyade un elemento al array.
     * @param double[] array
     * @param double value to add
     * @return double[] the array with value added to the end
     */
    static double[] append(double[] a, double value) {
        //Augmentar tamanyo de arr
        double[] b = new double[a.length+1];
        //Grabar datos en arr
        for (int i = 0; i < a.length; i++) {
            b[i] = a[i];
        }
        //Grabar ultimo dato
        b[a.length] = value;
        return b;
    }
    /**
     * 
     * Searches the specified array of doubles for the specified value using the binary search algorithm.
     * 
     * @param a an array
     * @param fromIndex
     * @param toIndex
     * @param key
     * @return index 
     */
    static int binarySearch(double[] a, double key) {
          
    //variable declaration
        boolean found = false;
        int low = 0;
        int high = a.length;
        int test = 0;
        int index = -1;
        //calcul
        while (low + 1 < high && !found) {
            test = (low + high) / 2;
            if (a[test] > key) {
                high = test;
            }
            else if (a[test] == key) {
                index = test;
                found = true;
            }
            else {
                low = test;
            }
        }
        if (a[low] == key) {
            index = low;
        }
        return index;


          
    }
    /*
     *  Searches a range of the specified array of doubles for the specified value using the binary
     * @param a an array
     * @param fromIndex
     * @param toIndex
     * @param key
     * @return index
     */
    static int binarySearch(double[] a, int fromIndex, int toIndex, double key) {
        //variable declaration
        boolean found = false;
        int low = fromIndex;
        int high = toIndex;
        int test = 0;
        int index = -1;
        //calcul
        while (low + 1 < high && !found) {
            test = (low + high) / 2;
            if (a[test] > key) {
                high = test;
            }
            else if (a[test] == key) {
                index = test;
                found = true;
            }
            else {
                low = test;
            }
        }
        if (a[low] == key) {
            index = low;
        }
        return index;
    }
     /*
     *  Copies the specified array, truncating or padding with zeros (if necessary) so the copy has the specified length.
     * @param original the original array
     * @param newLength the new length
     * @return the new array
     */
    static double[] copyOf(double[] original, int newLength){
          double[] array = new double[newLength];
          for (int i = 0; i < original.length; i++){
                if (i < array.length) {
                array[i] = original[i];
                }
          }
          return array;
    } 
    /*
     *  Copies the specified range of the specified array into a new array.
     * @param original the original array
     * @param from start of range
     * @param to end of range
     */
    static double[] copyOfRange(double[] original, int from, int to) {
        //vars
        double[] array = new double[to - from];
        //calc
        for (int i = 0; i < array.length; i++) {
           array[i] = original[from + i];
        }
        //ret
        return array;
    }
    /**
     * Returns true if the two specified arrays of doubles are equal to one another.
     * @param double[] a
     * @param double[] a2
     * @return boolean
     */
    public static boolean equals(double[] a, double[] a2){
          boolean equals = false;
          if (a.length == a2.length) {
                int i = 0;
                boolean exit = false;
                while (!exit && i < a.length) {
                      if (a[i] != a2[i]) {
                            exit = true;
                      }
                      i++;
                }
                if (i == a.length) {
                    equals = true;
                }
          }
          
          return equals;
    }
    /**
     *  Assigns the specified double value to each element of the specified array of doubles.
     * @param an array
     * @param val a value
     * @return the array filled with val
     */
    static void fill(double[] a, double val) {
        //calc
        for (int i = 0; i < a.length; i++) {
            a[i] = val;
        }
    }
    /**
     * Assigns the specified double value to each element of the specified range of the specified array of doubles.
     * @param double[] a
     * @param int fromIndex
     * @param double val
     */
    static void fill(double[] a, int fromIndex, int toIndex, double val){
           for (int i = fromIndex; i <= toIndex; i++) {
            a[i] = val;
        }
    }
    /**
     *  Returns an array with the same element of a but with value inserted in index.
     * @param a an array
     * @param index the index
     * @param value a value
     * @return the array with value inserted
     */
    static double[] insert(double[] a, int index, double value) {
        a[index] = value;
        return a;
    }
    /**
     * Returns an array with all elements of a and, after them, all elements of b.
     * @param double[] a
     * @param double[] b
     * @return double[]
     */
    static double[] union(double[] a, double[] b){
          double[] ret = new double[a.length+b.length];
          for(int i = 0; i < a.length; i++){
                ret[i] = a[i];
          }
          for(int j = 0; j < b.length; j++){
                ret[a.length+j] = b[j];
          }
          return ret;
    }    
    /**
     *  Returns an array with the same elements of a but with value added to the beginning of the array.
     * @param
     * @param
     * @return
     */
    static double[] prepend(double[] a, double value) {
        //vars
        double[] b = new double[a.length+1];
        //calc
        b[0] = value;
        for (int i = 1; i < b.length; i++) {
            b[i] = a[i-1];
        }
        
        //ret
        return b;
    }
    /**
     * Returns an array with the same elements of a but without value in index.
     * @param double[] a
     * @param int index
     * @return double[]
     */
    static double[] remove(double[] a, int index){
          double[] ret = new double[a.length - 1];
          int i = 0;
          int j = 0;
          while (i != index) {
                  ret[j] = a[i];
                  i++;
                  j++;
              }
          i++;
          while (j < ret.length) {
              ret[j] = a[i];
              j++;
              i++;
          }
              
          return ret;
    }
    /**
     *  Sorts the specified array into ascending numerical order.
     * @param a an array
     */
    static void sort(double[] a) {
        //variable declaration
        double aux;
        int i = 0;
        int k = 0;
        //calcul
        for (k = 1; k < a.length; k++) {
            for (i = 0; i < a.length-k; i++) {
                if (a[i] > a[i+1]) {
                    aux = a[i];
                    a[i] = a[i+1];
                    a[i+1] = aux;
                }
            }
        }
    }
   /**
    * Sorts the specified range of the array into ascending order.  
    * @param double[]
    * @param int fromIndex
    * @param int toIndex
    * @return 
    */
    static void sort(double[] a, int fromIndex, int toIndex){
          double aux;
        int i = 0;
        int k = 0;
        int j = 0;
        //calcul
        for (k = fromIndex; k < toIndex; k++) {
            for (i = fromIndex; i < toIndex - j; i++) {
                if (a[i] > a[i+1]) {
                    aux = a[i];
                    a[i] = a[i+1];
                    a[i+1] = aux;
                }
            }
            j++;
        }
    }
    /**
     * Convierte array en String
     * @param double[] an array
     * @return String
     */
    public static String toString(double[] arr){
          String ret = "{";
          for (int i = 0; i < arr.length; i++){
              if (i == arr.length - 1) {
                ret += arr[i];
              } else {
                  ret += arr[i] + ", ";
              }
          }
          return ret + "}";
    }
}

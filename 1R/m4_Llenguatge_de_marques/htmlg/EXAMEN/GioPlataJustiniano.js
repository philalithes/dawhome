function readXML(fileXML) {
    var xmlDoc;
    if (window.XMLHttpRequest) {
        xmlDoc = new window.XMLHttpRequest();
        xmlDoc.open("GET", fileXML, false);
        xmlDoc.send("");
        return xmlDoc.responseXML;
    } else if (ActiveXObject("Microsoft.XMLDOM")) { // IE 5-6
        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
        xmlDoc.async = false;
        xmlDoc.load(fileXML);
        xmlDoc.open("GET", fileXML, false);
        xmlDoc.send("");
        return xmlDoc;
    }
    alert("Error loading document");
    return null;
}

fitxer_llegit='';
function   llegir_fitxer_xml(url)
{
	fitxer_llegit=url;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (xhttp.readyState == 4 && xhttp.status == 200)
		{			
			dades_llegides(xhttp);	
		}
	};
	xhttp.open("GET", url,true);
	xhttp.send();
}

function executar_ordre(url) {
    ordre=url;
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() 
	{
		if (xhttp.readyState == 4 && xhttp.status == 200)
		{			
			ordre_executada(xhttp);	
		}
	};
	xhttp.open("GET", url,true);
	xhttp.send();
}

function get_firstChild(n) {
    var y = n.firstChild;
    while (y.nodeType != 1) {
        y = y.nextSibling;
    }
    return y;
}

function get_nextSibling(n) {
    var y = n.nextSibling;
    while (y.nodeType != 1) {
        y = y.nextSibling;
    }
    return y;
}
-- loop
create or replace function count_letter(t text, c char) returns int as $$
	declare
		count int := 0;
		i int := 1;
	begin
		loop
			if substring(t from i for 1) = c then
				count := count + 1;
			end if;
			if i = char_length(t) then
				exit;
			end if;
		    i = i + 1;
		end loop;
		return count;
	end;
$$ language plpgsql;
			
-- while
create or replace function count_letter(t text, c char) returns int as $$
	declare
		count int := 0;
		i int := 1;
	begin
		while i <= length(t) loop
			if substring(t from i for 1) = c then
				count := count + 1;
			end if;
		    i = i + 1;
		end loop;
		return count;
	end;
$$ language plpgsql;

-- for
create or replace function nums_desc() returns setof int as $$
	declare 
		i int;
	begin
		for i in reverse 10..1 by 2 loop
			return next i;
		end loop;
		return;
	end;
$$ language plpgsql;

-- for query
create or replace function sube_ventas() returns void as $$
	declare
		query record;
		ventas numeric;
	begin
		for query in select rep, importe
			from repventas


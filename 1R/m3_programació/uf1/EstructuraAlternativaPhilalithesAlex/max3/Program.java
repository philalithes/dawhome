/*
 * Program.java        2015/23/10
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    
    /**
     * obté el nombre màxim entre 3 nombres reals
     * 
     * @param n1 = nombre real
     * @param n2 = nombre real
     * @param n3 = nombre real
     * 
     * 
     * @retorn el màxim nombre
     */
    public double max3(double n1, double n2, double n3){
        double max;
        if(n1 > n2 && n1 > n3) {
        max = n1;
        }
        else if(n2 > n1 && n2 > n3) {
        max = n2;
        }
        else {
        max = n3;
        }
        return max;
    }
    
    
}

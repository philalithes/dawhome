/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class ProgramTUI {
    /*
     * Llegeix i escriu els elements d'una taula numèrica real.
     * 
     * 
     */
    
    public static void main(String[] args) {
        //variable declaration
        //interacció amb l'usuari
        Scanner sc = new Scanner(System.in);
        System.out.println("Quants valors té la taula?");
        int cant = sc.nextInt();
        double[] taula = new double[cant];
        System.out.println("Escriu els valors de la taula");
        for (int n = 0; n < cant; n++) {
            double valor = sc.nextDouble();
            taula[n] = valor;
        }
        
        for (int i = 0; i < taula.length; i++) {
            System.out.println(taula[i]);
        }
        
    }
}

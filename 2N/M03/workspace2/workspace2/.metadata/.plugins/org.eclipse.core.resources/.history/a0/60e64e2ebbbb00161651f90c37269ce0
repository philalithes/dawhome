package org.escoladeltreball.m03uf06nf02APG;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Main {
	
	// ****** ******* ****** SELECTS ****** ******* ****** 
	/**
	 * @param id id del Jugador a seleccionar
	 * @param em EntityManager
	 * @return Jugador
	 */
	public static Jugador selectJugador(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectJugador");
		select.setParameter(1, id);
		return (Jugador) select.getSingleResult();
	}
	
	/**
	 * @param id id del Equip a seleccionar
	 * @param em EntityManager
	 * @return Equip
	 */
	public static Equip selectEquip(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectEquip");
		select.setParameter(1, id);
		return (Equip) select.getSingleResult();
	}
	
	/**
	 * @param id id del Estadi a seleccionar
	 * @param em EntityManager
	 * @return Estadi
	 */
	public static Estadi selectEstadi(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectEstadi");
		select.setParameter(1, id);
		return (Estadi) select.getSingleResult();
	}
	
	/**
	 * @param id id de la Competicio a seleccionar
	 * @param em EntityManager
	 * @return Competicio
	 */
	public static Competicio selectCompeticio(long id, EntityManager em) {
		Query select = em.createNamedQuery("selectCompeticio");
		select.setParameter(1, id);
		return (Competicio) select.getSingleResult();
	}
	
	// ****** ******* ****** UPDATES ****** ******* ******
	/**
	 * @param id id del jugador a actualitzar
	 * @param nom
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void updateJugador(long id, String nom, int dorsal, String posicio, EntityManager em) {
		Jugador j = selectJugador(id, em);
		j.setNom(nom);
		j.setDorsal(dorsal);
		j.setPosicio(posicio);
		em.merge(j);
	}
	
	/**
	 * @param id id del Equip a actualitzar
	 * @param nom
	 * @param nCompeticionsGuanyades
	 * @param capital
	 * @param em EntityManager
	 */
	public static void updateEquip(long id, String nom, int nCompeticionsGuanyades, double capital, EntityManager em) {
		Equip e = selectEquip(id, em);
		e.setNom(nom);
		e.setnCompeticionsGuanyades(nCompeticionsGuanyades);
		e.setCapital(capital);
		em.merge(e);
	}
	
	/**
	 * @param id id del Estadi a actualitzar
	 * @param nom
	 * @param dorsal
	 * @param qualitatTerreny
	 * @param em EntityManager
	 */
	public static void updateEstadi(long id, String nom, int capacitat, double qualitatTerreny, EntityManager em) {
		Estadi e = selectEstadi(id, em);
		e.setNom(nom);
		e.setCapacitat(capacitat);
		e.setQualitatTerreny(qualitatTerreny);
		em.merge(e);
	}
	
	/**
	 * @param id id de la Competicio a actualitzar
	 * @param nom
	 * @param premi
	 * @param maxEquips
	 * @param em EntityManager
	 */
	public static void updateCompeticio(long id, String nom, double premi, int maxEquips, EntityManager em) {
		Competicio c = selectCompeticio(id, em);
		c.setNom(nom);
		c.setPremi(premi);
		c.setMaxEquips(maxEquips);
		em.merge(c);
	}
	
	// ****** ******* ****** INSERTS ****** ******* ******
	/**
	 * Paràmetres del nou Jugador
	 * @param name
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void insertNewJugador(String name, int dorsal, String posicio, EntityManager em) {
		Jugador j = new Jugador(name, dorsal, posicio);
		em.persist(j);
	}
	
	/**
	 * Paràmetres del nou Equip
	 * @param name
	 * @param dorsal
	 * @param posicio
	 * @param em EntityManager
	 */
	public static void insertNewEquip(String nom, int nCompeticionsGuanyades, double capital, EntityManager em) {
		Equip e = new Equip(nom, nCompeticionsGuanyades, capital);
		em.persist(e);
	}
	
	/**
	 * Paràmetres del nou Estadi
	 * @param nom
	 * @param capacitat
	 * @param qualitatTerreny
	 * @param em EntityManager
	 */
	public static void insertNewEstadi(String nom, int capacitat, double qualitatTerreny, EntityManager em) {
		Estadi e = new Estadi(nom, capacitat, qualitatTerreny);
		em.persist(e);
	}
	
	/**
	 * Paràmetres de la nova Competicio
	 * @param nom
	 * @param premi
	 * @param maxEquips
	 * @param em
	 */
	public static void insertNewCompeticio(String nom, double premi, int maxEquips, EntityManager em) {
		Competicio c = new Competicio(nom, premi, maxEquips);
		em.persist(c);
	}
	
	// ****** ******* ****** DELETES ****** ******* ******
	/**
	 * @param id id del Jugador a eliminar
	 * @param em EntityManager
	 */
	public static void deleteJugador(long id, EntityManager em) {
		em.remove(selectJugador(id, em));
	}
	
	/**
	 * @param id id del Equip a eliminar
	 * @param em EntityManager
	 */
	public static void deleteEquip(long id, EntityManager em) {
		em.remove(selectEquip(id, em));
	}
	
	/**
	 * @param id id del Estadi a eliminar
	 * @param em EntityManager
	 */
	public static void deleteEstadi(long id, EntityManager em) {
		em.remove(selectEstadi(id, em));
	}
	
	/**
	 * @param id id de la Competicio a eliminar
	 * @param em EntityManager
	 */
	public static void deleteCompeticio(long id, EntityManager em) {
		em.remove(selectCompeticio(id, em));
	}

	// ****** ******* ****** MAIN ****** ******* ******
	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("m03uf06nf02APG");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		insertNewJugador("Pepe", 1, "Porter", em);
		insertNewJugador("Carlos", 2, "Centrecampista", em);
		
		insertNewEquip("Barcelona", 10, 10000, em);
		insertNewEquip("Madrid", 21, 23123.32, em);
		
		insertNewEstadi("Camp Nou", 200000, 9.2, em);
		insertNewEstadi("Bernabeu", 250000, 9, em);

		
		em.getTransaction().commit();
		em.getTransaction().begin();
		
		updateJugador(1, "Juan", 5, "Defensa", em);
		
		Jugador juan = selectJugador(1, em);
		System.out.println(juan);
		
		Equip barca = selectEquip(3, em);
		System.out.println(barca);
//		deleteJugador(1, em);
		
		em.getTransaction().commit();
		System.out.println("ok");
		em.close();
		emf.close();
	}

}

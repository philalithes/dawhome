package org.escoladeltreball.m03uf05nf02.APG;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Empleat {

	private long id;
	private String alias;
	private Enum rol;
	private String date;
	private boolean hasProject;
	private Enum categoria;

	public Empleat(long id, String alias, Enum rol, String date) {
		super();
		this.id = id;
		this.alias = alias;
		this.rol = rol;
		this.date = date;
	}

	@Override
	public String toString() {
		return "Empleat [id=" + id + ", alias=" + alias + ", rol=" + rol + ", date=" + date + ", hasProject="
				+ hasProject + ", categoria=" + categoria + "]";
	}

	public boolean isHasProject() {
		return hasProject;
	}

	public void setHasProject(boolean hasProject) {
		this.hasProject = hasProject;
	}

	public Enum getCategoria() {
		return categoria;
	}

	public void setCategoria(Enum categoria) {
		this.categoria = categoria;
	}

	public Empleat(String registre) {
		String[] splitRegist = registre.split(",");
		id = Long.parseLong(splitRegist[0]);
		alias = splitRegist[1];
		rol = Rols.valueOf(splitRegist[2]);
		date = splitRegist[3];
		LocalDate ld = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		LocalDateTime ldt = ld.atStartOfDay();
		LocalDateTime now = LocalDateTime.now();
		Duration antiguitat = Duration.between(ldt, now);
		long diesAntiguitat = antiguitat.toDays();
		if (diesAntiguitat < 365) {
			categoria = Categoria.A;
		} else if (diesAntiguitat > 365 && diesAntiguitat < 1095) {
			categoria = Categoria.B;
		} else {
			categoria = Categoria.C;
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Enum getRol() {
		return rol;
	}

	public void setRol(Enum rol) {
		this.rol = rol;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}

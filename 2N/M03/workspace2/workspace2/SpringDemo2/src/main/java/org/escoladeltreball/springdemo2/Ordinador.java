package org.escoladeltreball.springdemo2;

public class Ordinador {

	private Cpu cpu;
	private String marca;
	public Ordinador() {
		// TODO Auto-generated constructor stub
	}
	public Cpu getCpu() {
		return cpu;
	}
	public void setCpu(Cpu cpu) {
		this.cpu = cpu;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	@Override
	public String toString() {
		return "Ordinador [cpu=" + cpu + ", marca=" + marca + "]";
	}

}

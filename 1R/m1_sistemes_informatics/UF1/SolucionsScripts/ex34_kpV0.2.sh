#!/bin/bash
# Fitxer: ex34_kp_comments.sh
# Autor: jamoros
# Data: 04/04/2011
# Versio: 0.2
# Descripcio:    script que determina si els número que se li passa com a paràmetre és parell o senar. Utilitza una funció


# funció que retorna 0 si el numero é parell i 1 si és senar
es_parell() {
	if [ $(($1%2)) -eq 0 ]
	then
        	return 0
	else
        	return 1
	fi
}

# Si el nombre d'arguments és diferent de 1 mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
if [ $# -ne 1 ]
then
	echo "Has d'entrar un únic argument"
	echo "ús: $0 número"
	echo "    on número és un nombre enter"
	exit 1
fi

# Si l'argument no és nombre enter mostrem missatge d'error, ajuda i surtim indicant l'error al sistema
echo $1 | grep '^[-+]\{0,1\}[0-9]\+$' > /dev/null
if [ $? -ne 0 ]
then
	echo "error: has introduit un argument que no és un número enter: $1 "
	exit 2
fi

# Si el nombre enter és parell mostrem un missatge indicant-lo
if  es_parell $1 
then
	echo "$1 és parell"

# Altrament mostrem un missatge indicant que és senar
else
	echo "$1 és senar"
fi 




<<*
Una altra opció seria:
es_parell $1
if  [ $? -eq 0 ]
then
        echo "$1 és parell"

# Altrament mostrem un missatge indicant que és senar
else
        echo "$1 és senar"
fi
*

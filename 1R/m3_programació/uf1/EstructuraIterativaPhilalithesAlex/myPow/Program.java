/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;

public class Program {
    /*
     * Donats el valor de la base, un nombre enter i l’exponent,
     * un nombre enter no negatiu, calcula el valor de la potència a^b.
     * No emprar la funció Math.pow().
     * 
     */
    
    public int myPow(int a, int b) {
        //variable declaration
        int n = 0;
        int acumulador = 1;
        //calcul
        while (n < b) {
            acumulador *= a; 
            n++;
            
        }
        return acumulador;
    }
    
}

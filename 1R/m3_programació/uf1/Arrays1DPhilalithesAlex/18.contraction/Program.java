
/*
 * Program.java        2015/12/2
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donats dos vectors numèrics reals amb el mateix
     * nombre d'elements, calcula la seva contracció. La contracció es calcula com la suma dels
     * productes dels elements d'ambdós vectors del mateix índex
     */
    
    public double contraction(double[] array1, double[] array2) {
        //variable declaration
        int suma = 0;
        //calcul
        for (int i = 0; i < array1.length; i++) {
            suma += array1[i] * array2[i];
        }
        return suma;
        
    }
}

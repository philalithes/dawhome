var mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;

// instància de l'esquema
/*
var peliSchema = new Schema( {
	Codpeli: Number,
	Titol: String,
	Sinopsi : String,
	dvds : Number,
	dvdsPrestats : Number,
	prestecs : [{
		dataPres : Date,
		soci : String,
		dataDev: Date
	}]
});
*/
// instància de l'esquema
var peliSchema = new Schema( {
	Codpeli: Number,
	Titol: String,
	Sinopsi : String,
	dvds : [Number],
	dvdsPrestats : Number,
	prestecs : [{
	    dataPres : Date,
	    soci: String,   //representa el nom i cognoms del soci
	    coddvd: Number,
	    dataDev: Date
	}]
});



// registrem la col·lecció Pelicules amb un esquema concret i exportem
module.exports = mongoose.model('Pelicules', peliSchema);
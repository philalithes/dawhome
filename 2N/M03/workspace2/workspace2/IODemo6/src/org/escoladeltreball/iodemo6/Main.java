package org.escoladeltreball.iodemo6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

		public static void main(String[] args) {
			//try with resources
			try (BufferedReader reader = new BufferedReader(new FileReader("file1"));
					BufferedWriter writer = new BufferedWriter(new FileWriter("file_out"))) {
				String line = null;
				while ((line = reader.readLine()) != null) {
					//System.out.println(line);
					writer.write(line);
					writer.newLine();;
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}

/*
 * Program.java        2015/10/11
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {
    /*
     * Escriu els nombres enters positius narcisistes de tres xifres.
     * Els nombres narcisistes de tres xifres són de la forma:abc= a³ + b³ + c³
     * 
     */
    
    public void writeNarcissisticNumbers() {
        //variable declaration
        int n = 0;

        //Mentre no arribem a 999
        for (n = 100; n <= 999; n++) {
            //si el numero és narcicista
            if (n == Math.pow(digitPosition(n,3),3) + Math.pow(digitPosition(n,2),3) + Math.pow(digitPosition(n,1),3)) {
                //el mostrem
                
                System.out.println(n);
            }
        }
    }  
    public int digitPosition(int a, int b){
        //variable declaration    
        int digit;
        //calculate return
        digit = (int)(a / Math.pow(10,b -1) % 10);
        //return
        return digit;
    }
}
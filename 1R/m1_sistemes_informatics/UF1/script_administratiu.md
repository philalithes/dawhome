### SCRIPT ADMINISTRATIU

*L'objectiu d'aquesta pràctica és fer un script administratiu molt complet que faciliti
una de les tasques més habituals d'un administrador de sistemes:
vetllar per un consum responsable de l'espai de d'emmagatzematge en xarxa.
Abans però necessitem treballar certs conceptes.*

1 Com trobo els fitxers més grans d'una certa quantitat? I d'una certa antigüitat?

2 Anem a configurar l'enviament de correu electrònic a la nostra intranet 
(també podria configurar un compte de correu d'un proveedor extern: gmail, yahoo... però aquest no és l'objectiu d'aquest exercici)
	
Com a root instal·larem sendmail si no està instal·lat, 
utilitzarem sendmail.sendmail com a mta
i inicialitzarem el servei de sendmail:


	[root@localhost ~]# yum install sendmail -y

	[root@localhost ~]# alternatives --config mta

	[root@localhost ~]# systemctl start sendmail.service


Quina és la variable que conté el directori on s'emmagatzema el correu? $MAIL
Quin és aquest directori? /var/spool/mail/iaw47951368
Hi ha algun altre directori on també es desi el correu? Com ho expliques?
A quin fitxer de configuració es troba aquesta variable? 
A quin fitxer de configuració es troba la variable CREATE_MAIL_SPOOL (yes/no)
que em diu si es crearà la bústia de correu o no ?
Com puc per exemple enviar amb una sola ordre un email amb un contingut que ja tinc a un fitxer? 
(jugueu amb canonades, és a dir amb "pipes")


3 Comanda at. Volem executar una ordre d'aqui a 1 minut. Com ho faríem?
L'ordre at rep la comanda a executar des de l'entrada estàndard. 
Ajudant-vos del man, executeu l'ordre "touch f1" d'aqui un minut.
Feu el mateix amb l'ordre *echo "hola"*. Si el resultat us sorpren busqueu ajuda al man.
Un cop resolt el misteri anterior, sabrieu mostrar el missatge a la terminal a on us trobeu?
(Hint: tty command)

___

SCRIPT:

Fer un shell script que:

	a Llisti tots els usuaris que tinguin arxius més grans de X Mbytes i més antics de Y dies.
	b A continuació el script ha de generar un arxiu que contingui el nom de tots els fitxers amb el path complet i enviar un correu a cada usuari que compleixi aquesta condició amb un avís que han de salvar (backup) aquests arxius ja que seran eliminats del sistema.
	c Aixi cada usuari rebra en el correu mencionat els noms dels arxius en qüestió afectats.
	d Finalment utilitzar l'ordre at per executar la comanda que esborri tots els arxius del fitxer generat 48 hores després d'haver enviat el correu.
	Treballarem en local, i l'script se suposa que l'executa l'administrador del sistema, o sigui root.
	És recomanable que creeu 4 o 5 usuaris i jugueu amb diferents mides i antiguitats de fitxers.



**RECORDEU FER PRIMER UNA CAPÇALERA, UNA BONA DESCRIPCIÓ I COMENTARIS**

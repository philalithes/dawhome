package org.escoladeltreball.pesandocarretas;

public class Carro {
	
	private int pes;

	public Carro(int pes) {
		super();
		this.pes = pes;
	}

	public int getPes() {
		return pes;
	}

	public void setPes(int pes) {
		this.pes = pes;
	}

	@Override
	public String toString() {
		return "" + pes;
	}
	
	
	
}

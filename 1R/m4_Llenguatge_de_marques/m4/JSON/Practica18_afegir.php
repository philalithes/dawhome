<?php
	// Llefirm el fitxer gym3.json, visualitzem les dades d'objectes json, modifiquem una dada i guardem el fitxer
	
	$file='gym3.json';
	$dades = json_decode(file_get_contents($file),TRUE);
	

	$nom = utf8_encode($_POST['nom']);
	$club = utf8_encode($_POST['club']);


	$barra1 = utf8_encode($_POST['fase1Barra']);
	$barra2 = utf8_encode($_POST['fase2Barra']);
	$barra3 = utf8_encode($_POST['fase3Barra']);
	
	
	$asi1 = utf8_encode($_POST['fase1Asi']);
	$asi2 = utf8_encode($_POST['fase2Asi']);
	$asi3 = utf8_encode($_POST['fase3Asi']);
	
	$potro1 = utf8_encode($_POST['fase1Potro']);
	$potro2 = utf8_encode($_POST['fase2Potro']);
	$potro3 = utf8_encode($_POST['fase3Potro']);
	
	$terra1 = utf8_encode($_POST['fase1Terra']);
	$terra2 = utf8_encode($_POST['fase2Terra']);
	$terra3 = utf8_encode($_POST['fase3Terra']);

	
	$dades['gimnastes'][$nom]['Club'] = $club;
	$dades['gimnastes'][$nom]['Fase1']['Barra'] = $barra1;
	$dades['gimnastes'][$nom]['Fase1']['Asimetriques'] = $asi1;
	$dades['gimnastes'][$nom]['Fase1']['Terra'] = $terra1;
	$dades['gimnastes'][$nom]['Fase1']['Potro'] = $potro1;
	
	$dades['gimnastes'][$nom]['Fase2']['Barra'] = $barra2;
	$dades['gimnastes'][$nom]['Fase2']['Asimetriques']= $asi2;
	$dades['gimnastes'][$nom]['Fase2']['Terra'] = $terra2;
	$dades['gimnastes'][$nom]['Fase2']['Potro'] = $potro2;

	$dades['gimnastes'][$nom]['Fase3']['Barra'] = $barra3;
	$dades['gimnastes'][$nom]['Fase3']['Asimetriques'] = $asi3;
	$dades['gimnastes'][$nom]['Fase3']['Terra'] = $terra3;
	$dades['gimnastes'][$nom]['Fase3']['Potro'] = $potro3;


	file_put_contents($file, json_encode($dades,TRUE));

?>


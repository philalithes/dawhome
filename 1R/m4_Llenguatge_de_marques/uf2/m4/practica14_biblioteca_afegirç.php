<!doctype html>
<html>
<body>
<?php

$doc = new DOMDocument('1.0', 'utf-8');
$doc->formatOutput = true;
$doc->preserveWhiteSpace = false;
$doc->load('Biblioteca.xml');

$arrel = $doc->firstChild;

$nouLlibre = $doc->createElement('llibre');
$nouLlibre = $arrel->appendChild($nouLlibre);

$nouTitol = $doc->createElement('titol');
$nouTitol = $nouLlibre->appendChild($nouTitol);

$txt = utf8_encode('titol del tercer llibre');
$text = $doc->createTextNode($txt);
$text = $nouTitol->appendChild($text);

$nouAutor = $doc->createElement('autor');
$nouAutor = $nouLlibre->appendChild($nouAutor);

$txt = utf8_encode('benito');
$text = $doc->createTextNode($txt);
$text = $nouAutor->appendChild($text);

$doc->save('Biblioteca.xml');

$stringXML = $doc->saveXML();
echo $stringXML;
?>
</body>
</html>
package org.escoladeltreball.springdemo4;

public interface Login {

	/**
	 * Monitoritzat
	 */
	public abstract boolean connect(String name, String password) throws RuntimeException;
	
}

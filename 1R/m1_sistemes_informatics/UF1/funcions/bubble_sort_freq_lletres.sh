#!/bin/bash 
# Fitxer: bubble_sort_freq_lletres.sh
# Autor: jamoros
# Data: 05/02/2015
# Versio: 0.1
# This is free software, licensed under the GNU General Public License v3.
# See http://www.gnu.org/licenses/gpl.html for more information."
# Descripcio:	Script que ordena amb el mètode de la bombolla un array d'chars_frequency
#				i la seva parella array de caràcters.
#				Farem l'array hardcoded i amb valors aleatoris utilitzant la variable RANDOM

declare -a character=({a..z})  # declarem l'array de chars ( a b c ... x y z )
chars_num=${#character[@]}	# guardem la longutud de l'array de chars

 
# fem un array de la mateixa longitud que el de caràcters amb numeros calculats aleatòriament
chars_frequency=( $( \
			for i in $( seq 1 $chars_num )
			do
				echo -n "$RANDOM "
			done )
		)

# Mostrem els 2 arrays inicialment sense ordre (en format taula)
echo -e "\nInicialment tenim:"
echo -e "${character[@]}\n${chars_frequency[@]}" | column -t 

# Fem el mètode la bombolla per ordenar de manera descendent els enters
# Si fem un swap de 2 posicions de l'array d'enters
# també fem swap de les mateixes posicions de l'array de chars
for (( i=chars_num-1; i>0; i--))
{
    for (( j=0; j<i; j++ ))
    {
        # Si aquesta parella no està ordenada descendenment fem swap als dos arrays
        if (( chars_frequency[j] < chars_frequency[j+1] ))
        then
                temp=${chars_frequency[j]}
                chars_frequency[j]=${chars_frequency[j+1]}
                chars_frequency[j+1]=$temp

				temp=${character[j]}
				character[j]=${character[j+1]}
				character[j+1]=$temp

        fi
    }
}

# Mostrem els arrays ara ja ordenats (en format taula)
echo -e "\nDesprés d'ordenar:"
echo -e "${character[@]}\n${chars_frequency[@]}" | column -t 


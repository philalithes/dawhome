/*
 * Program.java        2015/11/24
 *
 * Models the program.
 *
 * Copyright 2015 AlexPhilalithes <alexph_95@hotmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;
import java.util.Locale;
import java.util.Arrays;

public class Program {
    /*
     * Donada una taula de nombres enters,
     * determina si algun element està repetit.
     */
    
    public boolean severalCopies(double[] array) {
        //variable declaration
        boolean copies = false;
        //calcul
        for (int i = 0; i < array.length; i++) {
            for (int n = 0; n < array.length; n++) {
                if (n != i && array[i] == array[n])
                    copies = true;
            }
        }
        return copies;
        
    }
}

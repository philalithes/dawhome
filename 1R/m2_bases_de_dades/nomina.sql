create or replace function nomina(emp repventas.num_empl%TYPE) returns numeric as $$
	declare
		plus_antiguitat_anual constant numeric  := 5;
		plus_menors_35 constant int  := 150;
		plus_majors_55 constant int  := 100;
		plus_zona_est constant numeric  := 2;
		retencio constant numeric := 18;
		emp_info repventas%ROWTYPE;
		base numeric := 0;
		base_final numeric := 0;
		plus numeric := 0;
		i int;
		zona varchar(6);
	begin
		--obtenim la informació de l'empleat
		select * 
		into emp_info 
		from repventas 
		where num_empl = emp;
		--obtenim salari base
		if emp_info.titulo = 'VP Ventas' then
			base := 2000;
		elsif emp_info.titulo = 'Dir Ventas' then
			base := 1500;
		else
			base := 1000;
		end if;
		--per cada any augmenta la base plus_antiguitat_anual% començant desde el 2on any.
		for i in 2..date_part('years', age(emp_info.contrato)) loop
			base := base + base * (plus_antiguitat_anual / 100);
		end loop;
		--plus edat
		if emp_info.edad < 35 then
			plus := plus_menors_35;
		elsif emp_info.edad > 55 then
			plus := plus_majors_55;
		end if;
		--plus zona aplicat a la base
		select region into zona from oficinas where oficina = emp_info.oficina_rep;
		if zona = 'este' then
			base_final := base + base * (plus_zona_est / 100);
		else
			base_final := base;
		end if;
		--apliquem plus
		base_final := base_final + plus;
		--retenció
		base_final := base_final - base_final * (retencio / 100);
		return base_final;
	end;
$$ language plpgsql;
